﻿global using Microsoft.AspNetCore.Components;
global using Microsoft.AspNetCore.Components.Authorization;
global using Microsoft.AspNetCore.Components.Web;
global using Microsoft.AspNetCore.HttpOverrides;
global using Microsoft.AspNetCore.Identity;
global using Microsoft.EntityFrameworkCore;

global using ProjectDataCore.Data.Account;
global using ProjectDataCore.Data.Database;
global using ProjectDataCore.Data.Services;
global using ProjectDataCore.Data.Services.Routing;
global using ProjectDataCore.Data.Services.Page;
global using ProjectDataCore.Data.Services.Roster;
global using ProjectDataCore.Data.Structures.Assignable;
global using ProjectDataCore.Data.Structures.Page;

global using System.Reflection;