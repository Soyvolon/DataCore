using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using System.Net.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Components.Routing;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.Web.Virtualization;
using Microsoft.JSInterop;
using ProjectDataCore;
using ProjectDataCore.Shared;
using ProjectDataCore.Components.Framework.Roster;
using ProjectDataCore.Components.Nav;
using ProjectDataCore.Components.Util;
using ProjectDataCore.Components.Framework.Editors;
using ProjectDataCore.Data.Structures.Assignable.Configuration;
using ProjectDataCore.Data.Structures;
using ProjectDataCore.Data.Structures.Nav;
using ProjectDataCore.Data.Services.User;
using ProjectDataCore.Data.Services.Bus.Scoped;
using ProjectDataCore.Data.Structures.Page.ContextMenu;
using ProjectDataCore.Components.Page;

namespace ProjectDataCore.Shared;
public partial class MainLayout
{
#pragma warning disable CS8618 // Injections are never null.

    [Inject]
    public IUserService UserService { get; set; }

    [Inject]
    public IAssignableDataService AssignableDataService { get; set; }

    [Inject]
    public ILocalUserService LocalUserService { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

    /// <summary>
    /// The currently signed in user.
    /// </summary>
    public DataCoreUser? ActiveUser { get; set; }

    protected override async Task OnParametersSetAsync()
    {
        await base.OnParametersSetAsync();

        ActiveUser = LocalUserService.LocalUser;

        if(ActiveUser is not null)
        {
            await LocalUserService.InitalizeIfDeinitalizedAsync(ActiveUser.Id);
        }
        else
        {
            LocalUserService.DeinitalizeIfInitalized();
        }
    }
}
