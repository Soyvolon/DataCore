﻿import { Sortable, Plugins } from '@shopify/draggable';

window.SortableInterop = (() => {
    const liveSorts = {};

    return {
        registerSort(guid, dotNetRef, sortChangedMethod = "") {
            const selector = `[data-sortable-container="${guid}"]`;
            const containers = document.querySelectorAll(selector);

            if (containers.length === 0) {
                return;
            }

            const sortable = new Sortable(containers, {
                draggable: '.sortable',
                mirror: {
                    appendTo: selector,
                    constrainDimensions: true,
                }
            });

            sortable.on("sortable:stop", (evt) => {
                let oldI = evt.oldIndex;
                let newI = evt.newIndex;

                dotNetRef.invokeMethodAsync(sortChangedMethod, oldI, newI);
            });

            this.saveSort(guid, sortable);
        },

        saveSort(guid, sortable) {
            liveSorts[guid] = {
                sort: sortable
            };
        },

        destroySort(guid) {
            let item = liveSorts[guid];
            if (item) {
                delete liveSorts[guid];
            }
        }
    }
})();