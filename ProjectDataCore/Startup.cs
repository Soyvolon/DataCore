﻿using AngleSharp.Text;

using AspNet.Security.OpenId;

using Cloudcrate.AspNetCore.Blazor.Browser.Storage;

using DSharpPlus;

using MailKit.Net.Smtp;

using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore.Diagnostics;

using ProjectDataCore.Components.Framework.Components;
using ProjectDataCore.Data.Services.Account;
using ProjectDataCore.Data.Services.Alert;
using ProjectDataCore.Data.Services.Assignable;
using ProjectDataCore.Data.Services.Auth;
using ProjectDataCore.Data.Services.Bus;
using ProjectDataCore.Data.Services.Bus.Global;
using ProjectDataCore.Data.Services.Bus.Scoped;
using ProjectDataCore.Data.Services.Form;
using ProjectDataCore.Data.Services.History;
using ProjectDataCore.Data.Services.HotReload;
using ProjectDataCore.Data.Services.HTML;
using ProjectDataCore.Data.Services.Import;
using ProjectDataCore.Data.Services.Keybindings;
using ProjectDataCore.Data.Services.Logging;
using ProjectDataCore.Data.Services.Mail;
using ProjectDataCore.Data.Services.Nav;
using ProjectDataCore.Data.Services.User;
using ProjectDataCore.Data.Structures.Mail;
using ProjectDataCore.Data.Structures.Nav;
using ProjectDataCore.Data.Structures.Page.Attributes;
using ProjectDataCore.Data.Structures.Routing;
using ProjectDataCore.Data.Structures.Util;

using System.Collections.Concurrent;
using System.Diagnostics;
using System.Linq;
using System.Reflection.Metadata;

using static System.Formats.Asn1.AsnWriter;

namespace ProjectDataCore;

public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

	public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
    public void ConfigureServices(IServiceCollection services)
    {
        #region Database Setup

        services.AddDbContextFactory<ApplicationDbContext>(options =>
            options.UseNpgsql(
#if DEBUG
                Configuration.GetConnectionString("development")
#else
                Configuration.GetConnectionString("release")
#endif
            )
#if DEBUG
            .EnableSensitiveDataLogging()
            .EnableDetailedErrors()
#endif
            // Warnings for loading multiple collections without splitting the query.
            // We want this behaviour for accurate data loading (and our lists are not
            // of any large size for the roster) so we are ignoring these.
            .ConfigureWarnings(w =>
                w.Ignore(RelationalEventId.MultipleCollectionIncludeWarning)
            ), ServiceLifetime.Singleton);

        services.AddScoped(p
            => p.GetRequiredService<IDbContextFactory<ApplicationDbContext>>().CreateDbContext());

#endregion

#region Accounts

        services.AddIdentity<DataCoreUser, DataCoreRole>()
            .AddDefaultTokenProviders()
            .AddRoleManager<DataCoreRoleManager>()
            .AddSignInManager<DataCoreSignInManager>()
            .AddEntityFrameworkStores<ApplicationDbContext>();

#endregion

#region Blazor

        services.AddRazorPages();
        services.AddServerSideBlazor();

#if DEBUG
        services.AddDatabaseDeveloperPageExceptionFilter();
#endif

#endregion

#region Password

        services.Configure<IdentityOptions>(options =>
        {
            options.Password.RequireDigit = false;
            options.Password.RequireLowercase = false;
            options.Password.RequireNonAlphanumeric = false;
            options.Password.RequireUppercase = false;
            options.Password.RequiredLength = 2;
            options.Password.RequiredUniqueChars = 0;
        });

#endregion

#region Services
        // Scoped Services
        services.AddScoped<IModularRosterService, ModularRosterService>()
            .AddScoped<IPageEditService, PageEditService>()
            .AddScoped<IScopedUserService, ScopedUserService>()
            .AddScoped<IUserService, UserService>()
            .AddScoped<INavModuleService, NavModuleService>()
            .AddScoped<IAssignableDataService, AssignableDataService>()
            .AddScoped<IAlertService, AlertService>()
            .AddScoped<IImportService, ImportService>()
            .AddScoped<IScopedDataBus, ScopedDataBus>()
            .AddScoped<IEditHistoryService, EditHistoryService>()
            .AddScoped<ILocalUserService, LocalUserService>()
            .AddScoped<IUserService, UserService>()
            .AddScoped<IKeybindingService, KeybindingService>()
            .AddScoped<IFormResponseService, FormResponseService>()
            .AddScoped<ILinkedAssignableService, LinkedAssignableService>();

        // Singleton Services
        services.AddSingleton<IRoutingService, RoutingService>()
            .AddSingleton<RoutingService.RoutingServiceSettings>(x =>
            {
                var asm = Assembly.GetAssembly(typeof(Components._Imports))
                    ?? throw new Exception("Missing components assembly.");

                //var cls = typeof(StaticComponentBase);
                //var attrs = asm.GetTypes().Where(x => x.IsSubclassOf(cls))
                //	.ToList(x => (x.GetCustomAttributes(typeof(StaticPageAttribute), true)
                //                    .FirstOrDefault() as StaticPageAttribute, x))
                //	.Where(x => x.Item1 is not null);

                //            Dictionary<string, Type> statics = new();
                //            foreach (var pair in attrs)
                //                _ = statics.TryAdd(pair.Item1!.Route, pair.Item2);

                return new(asm);
            })
            .AddSingleton<IAccountLinkService, AccountLinkService>()
            .AddSingleton<IGlobalDataBus, GlobalDataBus>()
            .AddSingleton<IModularRosterService, ModularRosterService>()
            .AddSingleton<IPageEditService, PageEditService>()
            .AddSingleton<IScopedUserService, ScopedUserService>()
            .AddSingleton<INavModuleService, NavModuleService>()
            .AddSingleton<IAssignableDataService, AssignableDataService>()
            .AddSingleton<ITextEditorAuthorizationService, TextEditorAuthorizationService>()
            .AddSingleton<IInstanceLogger, InstanceLogger>()
            .AddSingleton<IHTMLService, HTMLService>()
            .AddSingleton<IDataCoreAuthorizationService, DataCoreAuthorizationService>()
            .AddSingleton<IFormService, FormService>()
            .AddSingleton<HotReloadHandler>();

        // Service Extensions
        services.AddStorage();
#endregion

#region Email Setup
        var mailSection = Configuration.GetRequiredSection("Email");
        services.AddSingleton(new MailConfiguration()
        {
            Client = mailSection["Client"],
            Port = mailSection["Port"].ToInteger(0),
            Email = mailSection["Email"],
            RequireLogin = mailSection["RequireLogin"].ToBoolean(false),
            User = mailSection["User"],
            Password = mailSection["Password"]
        })
            .AddScoped<SmtpClient>()
            .AddScoped<ICustomMailSender, CustomMailSender>();
#endregion

#region Discord Setup

#endregion

#region Authentication

        services.AddAuthentication()
            .AddSteam("Steam", "Steam", options =>
            {
                options.CorrelationCookie = new()
                {
                    IsEssential = true,
                    SameSite = SameSiteMode.None,
                    SecurePolicy = CookieSecurePolicy.Always
                };

                options.Events = new OpenIdAuthenticationEvents
                {
                    OnTicketReceived = async context =>
                    {
                        if (context.Request.Cookies.TryGetValue("token", out var token))
                        {
                            if (context.Request.QueryString.HasValue)
                            {
                                var query = context.Request.Query["openid.identity"];

                                var link = context.HttpContext.RequestServices.GetRequiredService<IAccountLinkService>();

                                string id = query;
                                id = id[(id.LastIndexOf("/") + 1)..(id.Length)];

                                try
                                {
                                    var redir = await link.BindSteamUserAsync(token!, id);

                                    context.Response.Cookies.Append("redir", redir);
                                }
                                catch (Exception ex)
                                {
                                    context.Response.Cookies.Append("error", ex.Message);
                                }

                                context.Success();
                            }
                        }
                        else
                        {
                            context.Response.Cookies.Append("error", "No token was found.");
                        }
                    }
                };
            })
            .AddOAuth("Discord", "Discord", options =>
            {
                options.AuthorizationEndpoint = $"{Configuration["Config:Discord:Api"]}/oauth2/authorize";

                options.CallbackPath = new PathString("/authorization-code/discord-callback"); // local auth endpoint
                options.AccessDeniedPath = new PathString("/api/link/denied");

                options.ClientId = Configuration["Discord:ClientId"];
                options.ClientSecret = Configuration["Discord:ClientSecret"];
                options.TokenEndpoint = $"{Configuration["Config:Discord:TokenEndpoint"]}";

                options.Scope.Add("identify");
                options.Scope.Add("email");

                options.CorrelationCookie = new()
                {
                    IsEssential = true,
                    SameSite = SameSiteMode.None,
                    SecurePolicy = CookieSecurePolicy.Always
                };

                options.Events = new OAuthEvents
                {
                    OnCreatingTicket = async context =>
                    {
                        // get user data
                        var client = new DiscordRestClient(new()
                        {
                            Token = context.AccessToken,
                            TokenType = TokenType.Bearer
                        });

                        var user = await client.GetCurrentUserAsync();

                        if (context.Request.Cookies.TryGetValue("token", out var token))
                        {
                            var link = context.HttpContext.RequestServices.GetRequiredService<IAccountLinkService>();

                            try
                            {
                                // verify the user information grabbed matches the user info
                                // saved from the Initial command
                                var redir = await link.BindDiscordAsync(token, user.Id.ToString(), user.Email);

                                context.Response.Cookies.Append("redir", redir);
                            }
                            catch (Exception ex)
                            {
                                context.Response.Cookies.Append("error", ex.Message);
                            }

                            context.Success();
                        }
                        else
                        {
                            context.Response.Cookies.Append("error", "No token found.");
                        }
                    },
                    OnRemoteFailure = async context =>
                    {
                        // TODO remove token from cookies and delete server token cache.
                        if (context.Request.Cookies.TryGetValue("token", out var token))
                        {
                            var link = context.HttpContext.RequestServices.GetRequiredService<IAccountLinkService>();
                            await link.AbortLinkAsync(token!);
                        }
                    }
                };
            });

#endregion

#region Permissions

#endregion
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        // Configure the HTTP request pipeline.
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
            app.UseMigrationsEndPoint();
        }
        else
        {
            app.UseExceptionHandler("/Error");
            // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            app.UseHsts();
        }

        Task.Run(async () =>
        {
            using var scope = app.ApplicationServices.CreateScope();
            var _logger = scope.ServiceProvider.GetRequiredService<ILogger<Startup>>();

            _logger.LogInformation("Running Startup Tasks...");
            _logger.LogInformation("Validating Database...");

            #region Database Validation
            var dbFac = scope.ServiceProvider.GetRequiredService<IDbContextFactory<ApplicationDbContext>>();
            await using var db = await dbFac.CreateDbContextAsync();
            ApplyDatabaseMigrations(db);
            #endregion

            _logger.LogInformation("Validating Upload Locations...");

            #region File Validation
            var webHostEnvironment = scope.ServiceProvider.GetRequiredService<IWebHostEnvironment>();
            var unsafeUploadsPath = Path.Combine(webHostEnvironment.ContentRootPath,
                webHostEnvironment.EnvironmentName, "unsafe_uploads");

            Directory.CreateDirectory(unsafeUploadsPath);
            #endregion

            _logger.LogInformation("Validating Admin Access...");

            #region Admin Validation

            var usrMngr = scope.ServiceProvider.GetRequiredService<UserManager<DataCoreUser>>();
            var usr = await usrMngr.FindByNameAsync("Administrator");
            if (usr is null)
            {
                usr = new()
                {
                    UserName = "Administrator",
                    Administrator = true,
                    Registered = true
                };

                await usrMngr.CreateAsync(usr, Configuration["Startup:Password"]);
                var usrServ = scope.ServiceProvider.GetRequiredService<IAssignableDataService>();

                usr = await usrMngr.FindByNameAsync("Administrator");
                await usrServ.EnsureAssignableValuesAsync<DataCoreUser, Guid>(usr);
            }
            else
            {
                usr.Administrator = true;
                usr.UserName = "Administrator";

                await usrMngr.UpdateAsync(usr);
            }

            #endregion

            _logger.LogInformation("Validating Default User...");

            #region Default User Validation

            var dusr = await usrMngr.FindByNameAsync("Default");
            if (dusr is null)
            {
                dusr = new()
                {
                    UserName = "Default",
                    Default = true,
                    Registered = true
                };

                await usrMngr.CreateAsync(dusr);
                var usrServ = scope.ServiceProvider.GetRequiredService<IAssignableDataService>();

                dusr = await usrMngr.FindByNameAsync("Default");
                await usrServ.EnsureAssignableValuesAsync<DataCoreUser, Guid>(dusr);
            }
            else
            {
                dusr.Default = true;
                dusr.UserName = "Default";

                await usrMngr.UpdateAsync(dusr);
            }

            #endregion

            _logger.LogInformation("Initializing Hot Reload...");

            #region Hot Reload Setup
            // We just want to initalize it.
            _ = scope.ServiceProvider.GetRequiredService<HotReloadHandler>();
			#endregion

			_logger.LogInformation("Validating Home Page...");

			#region Home Page Setup/Validation
			await ValidateHomePageAsync(scope);
			#endregion

			_logger.LogInformation("Validating Static Pages...");

			#region Static Page Setup/Validation
			await ValidateStaticPagesAsync(scope);
            #endregion

            _logger.LogInformation("Reloading Custom CSS...");

            #region Reload Custom CSS
            if (Configuration["CustomCSS:RunOnStartup"].ToBoolean())
            {
                var html = scope.ServiceProvider.GetRequiredService<IHTMLService>();
                await html.UpdateCustomSiteCSSAsync();
            }
            #endregion

            _logger.LogInformation("Validating NavBar...");

            #region NavBar Validation
            await ValidateNavBarAsync(scope);

            #endregion

            _logger.LogInformation("Reloading Routing Service Cache...");

            #region Routing Service Cache
            var routing = scope.ServiceProvider.GetRequiredService<IRoutingService>();
            routing.ReloadCache();
            #endregion

			_logger.LogInformation("... Startup Tasks Completed.");
        }).GetAwaiter().GetResult();

        app.UseForwardedHeaders(new ForwardedHeadersOptions()
        {
            ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
        });

        //app.UseHttpsRedirection();
        app.UseStaticFiles();

        app.UseRouting();

        app.UseAuthentication();
        app.UseAuthorization();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
            endpoints.MapBlazorHub();
            endpoints.MapFallbackToPage("/_Host");
        });
    }

    private static async Task ValidateNavBarAsync(IServiceScope scope)
    {
        var navModuleService = scope.ServiceProvider.GetRequiredService<INavModuleService>();
        var navModules = await navModuleService.GetAllModules();
        var pageService = scope.ServiceProvider.GetRequiredService<IPageEditService>();
        var staticPages = await pageService.GetAllStaticPagesAsync();

        #region Admin Home Area
        var adminStaticPage2 = staticPages.Find(e => e.RouteRoot?.Route == "admin");

        if (!navModules.Exists(e => e.Page == adminStaticPage2))
        {
            NavModule adminPage2 = new(adminStaticPage2) {StaticPage = true};
            await navModuleService.CreateNavModuleAsync(adminPage2);
            staticPages.Remove(adminStaticPage2);
        }
        #endregion

        var adminPage = await navModuleService.GetNavModuleFromPage(adminStaticPage2);
        foreach (var page in staticPages.Where(page => !navModules.Exists(e => e.Page == page)))
        {
            if (page.RouteRoot.FullRoute.Contains("admin"))
                await navModuleService.CreateNavModuleAsync(new NavModule(page, adminPage) {StaticPage = true});
            else
                await navModuleService.CreateNavModuleAsync(new NavModule(page) {StaticPage = true});
        }
        
    }

    private static void ApplyDatabaseMigrations(DbContext database)
    {
        try
        {
            if (!(database.Database.GetPendingMigrations()).Any())
            {
                return;
            }

            database.Database.Migrate();
            database.SaveChanges();
        }
        catch
        {
            Console.WriteLine("[ERROR] Failed to get/update migrations.");
        }
    }

    private static async Task ValidateHomePageAsync(IServiceScope scope)
    {
		var routeService = scope.ServiceProvider.GetRequiredService<IRoutingService>();
        var root = await routeService.GetOrCreateDefaultRouteRoot(true);

        if (root.Page is null)
            root.CreatePage();

        await routeService.UpdateOrAddRouteRootAsync(root);
    }

    private static async Task ValidateStaticPagesAsync(IServiceScope scope)
    {
        var asm = Assembly.GetAssembly(typeof(Components._Imports))
                    ?? throw new Exception("Missing components assembly.");

        var cls = typeof(StaticComponentBase);
        var attrs = asm.GetTypes().Where(x => x.IsSubclassOf(cls))
            .ToList(pageType => pageType.GetCustomAttributes(typeof(StaticPageAttribute), true)
                .FirstOrDefault() as StaticPageAttribute);

        var routeService = scope.ServiceProvider.GetRequiredService<IRoutingService>();
        var pageService = scope.ServiceProvider.GetRequiredService<IPageEditService>();
        var savedPages = await pageService.GetAllStaticPagesAsync();

        List<CustomPageSettings> toAddUpdate = new();

        foreach (var attr in attrs)
        {
            if (attr is null)
                continue;

            var listingValue = attr.PageComponentListing;
            var existing = savedPages
                .Where(x => x.Layout?.Component?.ListingValue == listingValue)
                .FirstOrDefault();

            existing ??= new();
            existing.Layout ??= new();
            existing.Layout.Component ??= new();
            existing.IsStaticPage = true;

            existing.Name = attr.Name;

            existing.Layout.Component.ListingValue 
                = listingValue;

            toAddUpdate.Add(existing);

            if (existing.RouteRoot is null)
            {
                var fullRoute = attr.Route;

                int counter = 1;
                var baseRoute = fullRoute;
                while (toAddUpdate.Where(x => x.RouteRoot?.FullRoute == fullRoute)
                    .FirstOrDefault() is not null)
                {
                    fullRoute = baseRoute + counter++.ToString();
                }

                var rootRes = await routeService.GetRouteRootFromRootAsync(fullRoute);

                if (!rootRes.GetResult(out var root, out _))
                {
                    var newRootRes = await routeService.AddRouteRootAsync(fullRoute);
                    if (!newRootRes.GetResult(out root, out var err))
                        throw new Exception("Failed to add a new route root: " + err[0]);
                }                

                existing.RouteRoot = root;
            }
        }

        var toDelete = savedPages.Where(x => !toAddUpdate.Contains(x));

        await pageService.BulkUpdateOrAddPagesAsync(toAddUpdate);
        await pageService.BulkDeletePagesAsync(toDelete);
    }
}