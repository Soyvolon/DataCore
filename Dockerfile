#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.
ARG netversion=8.0

# do setup for the actual app.
FROM mcr.microsoft.com/dotnet/aspnet:$netversion AS base
WORKDIR /app

# Install/restore dependencies.
FROM mcr.microsoft.com/dotnet/sdk:$netversion AS prebuild
ARG netversion

WORKDIR /src
COPY ["ProjectDataCore/ProjectDataCore.csproj", "ProjectDataCore/"]
COPY ["ProjectDataCore.Components/ProjectDataCore.Components.csproj", "ProjectDataCore.Components/"]
COPY ["ProjectDataCore.Data/ProjectDataCore.Data.csproj", "ProjectDataCore.Data/"]
RUN dotnet restore "ProjectDataCore/ProjectDataCore.csproj"
COPY . .

# Install and run webpack.
FROM node:18 as nodebuild
ARG netversion
COPY --from=prebuild /src /src

WORKDIR /src
RUN npm install
RUN npx webpack --mode=production --config webpack.config.js

# Build the project.
FROM prebuild as build
COPY --from=nodebuild /src /src

WORKDIR "/src/ProjectDataCore"
RUN dotnet build "ProjectDataCore.csproj" -p:SKIP_PREPOST_COMMANDS=True -c Release -o /app/build

# Run postcss commands.
FROM nodebuild AS postbuild
COPY --from=build /src /src

WORKDIR "/src/ProjectDataCore"
RUN npx postcss obj/Release/net${netversion}/scopedcss/bundle/ProjectDataCore.styles.css -o wwwroot/css/scoped/project.min.css --verbose
RUN npx postcss obj/Release/net${netversion}/scopedcss/projectbundle/ProjectDataCore.bundle.scp.css -o wwwroot/css/scoped/bundle.min.css --verbose
RUN npx postcss wwwroot/css/app.css -o wwwroot/css/app.min.css --verbose

WORKDIR "/src/ProjectDataCore.Components"
RUN npx postcss obj/Release/net${netversion}/scopedcss/bundle/ProjectDataCore.Components.styles.css -o wwwroot/css/scoped/project.min.css --verbose
RUN npx postcss obj/Release/net${netversion}/scopedcss/projectbundle/ProjectDataCore.Components.bundle.scp.css -o wwwroot/css/scoped/bundle.min.css --verbose
RUN npx postcss wwwroot/css/components.css -o wwwroot/css/components.min.css --verbose

# publish the app.
FROM build AS publish
COPY --from=postbuild /src /src

WORKDIR "/src/ProjectDataCore"
RUN dotnet publish "ProjectDataCore.csproj" -p:SKIP_PREPOST_COMMANDS=True -c Release -o /app/publish -p:UseAppHost=false

# execute the app.
FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "ProjectDataCore.dll"]
