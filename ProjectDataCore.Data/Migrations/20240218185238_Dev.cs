﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace ProjectDataCore.Data.Migrations
{
    /// <inheritdoc />
    public partial class Dev : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Administrator = table.Column<bool>(type: "boolean", nullable: false),
                    Default = table.Column<bool>(type: "boolean", nullable: false),
                    Registered = table.Column<bool>(type: "boolean", nullable: false),
                    Impersonating = table.Column<bool>(type: "boolean", nullable: false),
                    CurrentlyImpersonatingKey = table.Column<Guid>(type: "uuid", nullable: true),
                    UserName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    Email = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "boolean", nullable: false),
                    PasswordHash = table.Column<string>(type: "text", nullable: true),
                    SecurityStamp = table.Column<string>(type: "text", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "text", nullable: true),
                    PhoneNumber = table.Column<string>(type: "text", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "boolean", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "boolean", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "boolean", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUsers_AspNetUsers_CurrentlyImpersonatingKey",
                        column: x => x.CurrentlyImpersonatingKey,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "AuthorizationAreas",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Category = table.Column<string>(type: "text", nullable: false),
                    AllowUnauthorized = table.Column<bool>(type: "boolean", nullable: false),
                    AllowOnlyAuthorized = table.Column<bool>(type: "boolean", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthorizationAreas", x => x.Key);
                });

            migrationBuilder.CreateTable(
                name: "AuthorizationTags",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthorizationTags", x => x.Key);
                });

            migrationBuilder.CreateTable(
                name: "DynamicForms",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DynamicForms", x => x.Key);
                });

            migrationBuilder.CreateTable(
                name: "UserSelectComponentSettings",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    Properties = table.Column<List<string>>(type: "text[]", nullable: false),
                    IsStaticList = table.Column<List<bool>>(type: "boolean[]", nullable: false),
                    Formats = table.Column<List<string>>(type: "text[]", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserSelectComponentSettings", x => x.Key);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    RoleId = table.Column<Guid>(type: "uuid", nullable: false),
                    ClaimType = table.Column<string>(type: "text", nullable: true),
                    ClaimValue = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserId = table.Column<Guid>(type: "uuid", nullable: false),
                    ClaimType = table.Column<string>(type: "text", nullable: true),
                    ClaimValue = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "text", nullable: false),
                    ProviderKey = table.Column<string>(type: "text", nullable: false),
                    ProviderDisplayName = table.Column<string>(type: "text", nullable: true),
                    UserId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<Guid>(type: "uuid", nullable: false),
                    RoleId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<Guid>(type: "uuid", nullable: false),
                    LoginProvider = table.Column<string>(type: "text", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Value = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RosterObject",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Discriminator = table.Column<string>(type: "character varying(13)", maxLength: 13, nullable: false),
                    OccupiedById = table.Column<Guid>(type: "uuid", nullable: true),
                    ParentRosterId = table.Column<Guid>(type: "uuid", nullable: true),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RosterObject", x => x.Key);
                    table.ForeignKey(
                        name: "FK_RosterObject_AspNetUsers_OccupiedById",
                        column: x => x.OccupiedById,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_RosterObject_RosterObject_ParentRosterId",
                        column: x => x.ParentRosterId,
                        principalTable: "RosterObject",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserKeybinding",
                columns: table => new
                {
                    KeyPressed = table.Column<string>(type: "text", nullable: false),
                    Keybinding = table.Column<int>(type: "integer", nullable: false),
                    ShiftKey = table.Column<bool>(type: "boolean", nullable: false),
                    CtrlKey = table.Column<bool>(type: "boolean", nullable: false),
                    AltKey = table.Column<bool>(type: "boolean", nullable: false),
                    MetaKey = table.Column<bool>(type: "boolean", nullable: false),
                    DataCoreUserId = table.Column<Guid>(type: "uuid", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Key = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserKeybinding", x => x.KeyPressed);
                    table.ForeignKey(
                        name: "FK_UserKeybinding_AspNetUsers_DataCoreUserId",
                        column: x => x.DataCoreUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LinkedAssignableInputSettings",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    DataAuthorizationAreaKey = table.Column<Guid>(type: "uuid", nullable: true),
                    OnlyLocalUsersRosterTree = table.Column<bool>(type: "boolean", nullable: false),
                    OnlyLocalUsersRosterTreeAndChildTrees = table.Column<bool>(type: "boolean", nullable: false),
                    OnlyLocalUsersRosterChildren = table.Column<bool>(type: "boolean", nullable: false),
                    IncludeIfObjectHasAnyTag = table.Column<bool>(type: "boolean", nullable: false),
                    ExcludeIfObjectHasAnyTag = table.Column<bool>(type: "boolean", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LinkedAssignableInputSettings", x => x.Key);
                    table.ForeignKey(
                        name: "FK_LinkedAssignableInputSettings_AuthorizationAreas_DataAuthor~",
                        column: x => x.DataAuthorizationAreaKey,
                        principalTable: "AuthorizationAreas",
                        principalColumn: "Key");
                });

            migrationBuilder.CreateTable(
                name: "RouteRoots",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    Route = table.Column<string>(type: "text", nullable: false),
                    FullRoute = table.Column<string>(type: "text", nullable: false),
                    DefaultRoute = table.Column<bool>(type: "boolean", nullable: false),
                    AuthorizationAreaKey = table.Column<Guid>(type: "uuid", nullable: true),
                    CanEditAuthorizationAreaKey = table.Column<Guid>(type: "uuid", nullable: true),
                    OnNavigationChecks = table.Column<List<Guid>>(type: "uuid[]", nullable: false),
                    Editable = table.Column<bool>(type: "boolean", nullable: false),
                    ParentRootKey = table.Column<Guid>(type: "uuid", nullable: true),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RouteRoots", x => x.Key);
                    table.ForeignKey(
                        name: "FK_RouteRoots_AuthorizationAreas_AuthorizationAreaKey",
                        column: x => x.AuthorizationAreaKey,
                        principalTable: "AuthorizationAreas",
                        principalColumn: "Key");
                    table.ForeignKey(
                        name: "FK_RouteRoots_AuthorizationAreas_CanEditAuthorizationAreaKey",
                        column: x => x.CanEditAuthorizationAreaKey,
                        principalTable: "AuthorizationAreas",
                        principalColumn: "Key");
                    table.ForeignKey(
                        name: "FK_RouteRoots_RouteRoots_ParentRootKey",
                        column: x => x.ParentRootKey,
                        principalTable: "RouteRoots",
                        principalColumn: "Key");
                });

            migrationBuilder.CreateTable(
                name: "AssignableConfigurations",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    AssignableType = table.Column<int>(type: "integer", nullable: false),
                    PropertyName = table.Column<string>(type: "text", nullable: false),
                    NormalizedPropertyName = table.Column<string>(type: "text", nullable: false),
                    TypeName = table.Column<string>(type: "text", nullable: false),
                    AllowMultiple = table.Column<bool>(type: "boolean", nullable: false),
                    AllowedInput = table.Column<int>(type: "integer", nullable: false),
                    FormKey = table.Column<Guid>(type: "uuid", nullable: true),
                    Discriminator = table.Column<string>(type: "character varying(55)", maxLength: 55, nullable: false),
                    BooleanValueAssignableConfiguration_AllowedValues = table.Column<List<bool>>(type: "boolean[]", nullable: true),
                    DataCoreUserValueAssignableConfiguration_LinkFlag = table.Column<bool>(type: "boolean", nullable: true),
                    DateOnlyValueAssignableConfiguration_AllowedValues = table.Column<List<DateOnly>>(type: "date[]", nullable: true),
                    DateTimeValueAssignableConfiguration_AllowedValues = table.Column<List<DateTime>>(type: "timestamp with time zone[]", nullable: true),
                    DoubleValueAssignableConfiguration_AllowedValues = table.Column<List<double>>(type: "double precision[]", nullable: true),
                    IntegerValueAssignableConfiguration_AllowedValues = table.Column<List<int>>(type: "integer[]", nullable: true),
                    RosterSlotValueAssignableConfiguration_LinkFlag = table.Column<bool>(type: "boolean", nullable: true),
                    StringValueAssignableConfiguration_AllowedValues = table.Column<List<string>>(type: "text[]", nullable: true),
                    TimeOnlyValueAssignableConfiguration_AllowedValues = table.Column<List<TimeOnly>>(type: "time without time zone[]", nullable: true),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssignableConfigurations", x => x.Key);
                    table.ForeignKey(
                        name: "FK_AssignableConfigurations_DynamicForms_FormKey",
                        column: x => x.FormKey,
                        principalTable: "DynamicForms",
                        principalColumn: "Key");
                });

            migrationBuilder.CreateTable(
                name: "DynamicFormBuckets",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    RequireApproval = table.Column<bool>(type: "boolean", nullable: false),
                    FormKey = table.Column<Guid>(type: "uuid", nullable: false),
                    IsDefaultForKey = table.Column<Guid>(type: "uuid", nullable: true),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DynamicFormBuckets", x => x.Key);
                    table.ForeignKey(
                        name: "FK_DynamicFormBuckets_DynamicForms_FormKey",
                        column: x => x.FormKey,
                        principalTable: "DynamicForms",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DynamicFormBuckets_DynamicForms_IsDefaultForKey",
                        column: x => x.IsDefaultForKey,
                        principalTable: "DynamicForms",
                        principalColumn: "Key");
                });

            migrationBuilder.CreateTable(
                name: "DynamicFormTag",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    FormKey = table.Column<Guid>(type: "uuid", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DynamicFormTag", x => x.Key);
                    table.ForeignKey(
                        name: "FK_DynamicFormTag_DynamicForms_FormKey",
                        column: x => x.FormKey,
                        principalTable: "DynamicForms",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AuthorizationBindings",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    AuthorizationAreaKey = table.Column<Guid>(type: "uuid", nullable: false),
                    Scope = table.Column<string>(type: "text", nullable: false),
                    FullRosterTreeKey = table.Column<Guid>(type: "uuid", nullable: true),
                    RosterTreeKey = table.Column<Guid>(type: "uuid", nullable: true),
                    RosterSlotKey = table.Column<Guid>(type: "uuid", nullable: true),
                    UserKey = table.Column<Guid>(type: "uuid", nullable: true),
                    TagKey = table.Column<Guid>(type: "uuid", nullable: true),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthorizationBindings", x => x.Key);
                    table.ForeignKey(
                        name: "FK_AuthorizationBindings_AspNetUsers_UserKey",
                        column: x => x.UserKey,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_AuthorizationBindings_AuthorizationAreas_AuthorizationAreaK~",
                        column: x => x.AuthorizationAreaKey,
                        principalTable: "AuthorizationAreas",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AuthorizationBindings_AuthorizationTags_TagKey",
                        column: x => x.TagKey,
                        principalTable: "AuthorizationTags",
                        principalColumn: "Key");
                    table.ForeignKey(
                        name: "FK_AuthorizationBindings_RosterObject_FullRosterTreeKey",
                        column: x => x.FullRosterTreeKey,
                        principalTable: "RosterObject",
                        principalColumn: "Key");
                    table.ForeignKey(
                        name: "FK_AuthorizationBindings_RosterObject_RosterSlotKey",
                        column: x => x.RosterSlotKey,
                        principalTable: "RosterObject",
                        principalColumn: "Key");
                    table.ForeignKey(
                        name: "FK_AuthorizationBindings_RosterObject_RosterTreeKey",
                        column: x => x.RosterTreeKey,
                        principalTable: "RosterObject",
                        principalColumn: "Key");
                });

            migrationBuilder.CreateTable(
                name: "AuthorizationTagBindings",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    TagKey = table.Column<Guid>(type: "uuid", nullable: false),
                    RosterTreeKey = table.Column<Guid>(type: "uuid", nullable: true),
                    RosterSlotKey = table.Column<Guid>(type: "uuid", nullable: true),
                    UserId = table.Column<Guid>(type: "uuid", nullable: true),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthorizationTagBindings", x => x.Key);
                    table.ForeignKey(
                        name: "FK_AuthorizationTagBindings_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_AuthorizationTagBindings_AuthorizationTags_TagKey",
                        column: x => x.TagKey,
                        principalTable: "AuthorizationTags",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AuthorizationTagBindings_RosterObject_RosterSlotKey",
                        column: x => x.RosterSlotKey,
                        principalTable: "RosterObject",
                        principalColumn: "Key");
                    table.ForeignKey(
                        name: "FK_AuthorizationTagBindings_RosterObject_RosterTreeKey",
                        column: x => x.RosterTreeKey,
                        principalTable: "RosterObject",
                        principalColumn: "Key");
                });

            migrationBuilder.CreateTable(
                name: "RosterDisplaySettings",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    HostRosterId = table.Column<Guid>(type: "uuid", nullable: true),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RosterDisplaySettings", x => x.Key);
                    table.ForeignKey(
                        name: "FK_RosterDisplaySettings_RosterObject_HostRosterId",
                        column: x => x.HostRosterId,
                        principalTable: "RosterObject",
                        principalColumn: "Key");
                });

            migrationBuilder.CreateTable(
                name: "RosterOrders",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    TreeToOrderId = table.Column<Guid>(type: "uuid", nullable: true),
                    SlotToOrderId = table.Column<Guid>(type: "uuid", nullable: true),
                    ParentObjectId = table.Column<Guid>(type: "uuid", nullable: false),
                    Order = table.Column<int>(type: "integer", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RosterOrders", x => x.Key);
                    table.ForeignKey(
                        name: "FK_RosterOrders_RosterObject_ParentObjectId",
                        column: x => x.ParentObjectId,
                        principalTable: "RosterObject",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RosterOrders_RosterObject_SlotToOrderId",
                        column: x => x.SlotToOrderId,
                        principalTable: "RosterObject",
                        principalColumn: "Key");
                    table.ForeignKey(
                        name: "FK_RosterOrders_RosterObject_TreeToOrderId",
                        column: x => x.TreeToOrderId,
                        principalTable: "RosterObject",
                        principalColumn: "Key");
                });

            migrationBuilder.CreateTable(
                name: "AuthorizationTagLinkedAssignableInputSettingsBinding",
                columns: table => new
                {
                    TagKey = table.Column<Guid>(type: "uuid", nullable: false),
                    IncludeFilterKey = table.Column<Guid>(type: "uuid", nullable: false),
                    ExcludeFilterKey = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthorizationTagLinkedAssignableInputSettingsBinding", x => new { x.IncludeFilterKey, x.TagKey });
                    table.ForeignKey(
                        name: "FK_AuthorizationTagLinkedAssignableInputSettingsBinding_Author~",
                        column: x => x.TagKey,
                        principalTable: "AuthorizationTags",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AuthorizationTagLinkedAssignableInputSettingsBinding_Linked~",
                        column: x => x.ExcludeFilterKey,
                        principalTable: "LinkedAssignableInputSettings",
                        principalColumn: "Key");
                    table.ForeignKey(
                        name: "FK_AuthorizationTagLinkedAssignableInputSettingsBinding_Linke~1",
                        column: x => x.IncludeFilterKey,
                        principalTable: "LinkedAssignableInputSettings",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CustomPageSettings",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    IsStaticPage = table.Column<bool>(type: "boolean", nullable: false),
                    RouteRootKey = table.Column<Guid>(type: "uuid", nullable: true),
                    RedirectTo = table.Column<string>(type: "text", nullable: true),
                    Name = table.Column<string>(type: "text", nullable: true),
                    LayoutId = table.Column<Guid>(type: "uuid", nullable: true),
                    AuthorizationAreaKey = table.Column<Guid>(type: "uuid", nullable: true),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomPageSettings", x => x.Key);
                    table.ForeignKey(
                        name: "FK_CustomPageSettings_AuthorizationAreas_AuthorizationAreaKey",
                        column: x => x.AuthorizationAreaKey,
                        principalTable: "AuthorizationAreas",
                        principalColumn: "Key");
                    table.ForeignKey(
                        name: "FK_CustomPageSettings_RouteRoots_RouteRootKey",
                        column: x => x.RouteRootKey,
                        principalTable: "RouteRoots",
                        principalColumn: "Key");
                });

            migrationBuilder.CreateTable(
                name: "BucketActions",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    BucketKey = table.Column<Guid>(type: "uuid", nullable: false),
                    ActionTypes = table.Column<int>(type: "integer", nullable: false),
                    Triggers = table.Column<int>(type: "integer", nullable: false),
                    MoveResponse_MoveToKey = table.Column<Guid>(type: "uuid", nullable: true),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BucketActions", x => x.Key);
                    table.ForeignKey(
                        name: "FK_BucketActions_DynamicFormBuckets_BucketKey",
                        column: x => x.BucketKey,
                        principalTable: "DynamicFormBuckets",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BucketActions_DynamicFormBuckets_MoveResponse_MoveToKey",
                        column: x => x.MoveResponse_MoveToKey,
                        principalTable: "DynamicFormBuckets",
                        principalColumn: "Key");
                });

            migrationBuilder.CreateTable(
                name: "DynamicFormResponses",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    SubmitterId = table.Column<Guid>(type: "uuid", nullable: true),
                    BucketKey = table.Column<Guid>(type: "uuid", nullable: false),
                    FormKey = table.Column<Guid>(type: "uuid", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DynamicFormResponses", x => x.Key);
                    table.ForeignKey(
                        name: "FK_DynamicFormResponses_AspNetUsers_SubmitterId",
                        column: x => x.SubmitterId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_DynamicFormResponses_DynamicFormBuckets_BucketKey",
                        column: x => x.BucketKey,
                        principalTable: "DynamicFormBuckets",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DynamicFormResponses_DynamicForms_FormKey",
                        column: x => x.FormKey,
                        principalTable: "DynamicForms",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ComponentPool",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    PageSettingsKey = table.Column<Guid>(type: "uuid", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ComponentPool", x => x.Key);
                    table.ForeignKey(
                        name: "FK_ComponentPool_CustomPageSettings_PageSettingsKey",
                        column: x => x.PageSettingsKey,
                        principalTable: "CustomPageSettings",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LayoutNodes",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    ParentNodeId = table.Column<Guid>(type: "uuid", nullable: true),
                    PageSettingsId = table.Column<Guid>(type: "uuid", nullable: true),
                    AuthorizationAreaKey = table.Column<Guid>(type: "uuid", nullable: true),
                    Order = table.Column<int>(type: "integer", nullable: false),
                    Rows = table.Column<bool>(type: "boolean", nullable: false),
                    RawNodeWidths = table.Column<string>(type: "text", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LayoutNodes", x => x.Key);
                    table.ForeignKey(
                        name: "FK_LayoutNodes_AuthorizationAreas_AuthorizationAreaKey",
                        column: x => x.AuthorizationAreaKey,
                        principalTable: "AuthorizationAreas",
                        principalColumn: "Key");
                    table.ForeignKey(
                        name: "FK_LayoutNodes_CustomPageSettings_PageSettingsId",
                        column: x => x.PageSettingsId,
                        principalTable: "CustomPageSettings",
                        principalColumn: "Key");
                    table.ForeignKey(
                        name: "FK_LayoutNodes_LayoutNodes_ParentNodeId",
                        column: x => x.ParentNodeId,
                        principalTable: "LayoutNodes",
                        principalColumn: "Key");
                });

            migrationBuilder.CreateTable(
                name: "NavModules",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    DisplayName = table.Column<string>(type: "text", nullable: false),
                    Href = table.Column<string>(type: "text", nullable: true),
                    HasMainPage = table.Column<bool>(type: "boolean", nullable: false),
                    ParentId = table.Column<Guid>(type: "uuid", nullable: true),
                    PageId = table.Column<Guid>(type: "uuid", nullable: true),
                    AuthKey = table.Column<Guid>(type: "uuid", nullable: true),
                    DifAuth = table.Column<bool>(type: "boolean", nullable: false),
                    StaticPage = table.Column<bool>(type: "boolean", nullable: false),
                    LinkToPage = table.Column<bool>(type: "boolean", nullable: false),
                    Order = table.Column<int>(type: "integer", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NavModules", x => x.Key);
                    table.ForeignKey(
                        name: "FK_NavModules_AuthorizationAreas_AuthKey",
                        column: x => x.AuthKey,
                        principalTable: "AuthorizationAreas",
                        principalColumn: "Key");
                    table.ForeignKey(
                        name: "FK_NavModules_CustomPageSettings_PageId",
                        column: x => x.PageId,
                        principalTable: "CustomPageSettings",
                        principalColumn: "Key");
                    table.ForeignKey(
                        name: "FK_NavModules_NavModules_ParentId",
                        column: x => x.ParentId,
                        principalTable: "NavModules",
                        principalColumn: "Key");
                });

            migrationBuilder.CreateTable(
                name: "UserScopes",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    PageId = table.Column<Guid>(type: "uuid", nullable: false),
                    FormScopeDefinitionKey = table.Column<Guid>(type: "uuid", nullable: true),
                    UseUserDefinition = table.Column<bool>(type: "boolean", nullable: false),
                    IncludeLocalUser = table.Column<bool>(type: "boolean", nullable: false),
                    DisplayName = table.Column<string>(type: "text", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserScopes", x => x.Key);
                    table.ForeignKey(
                        name: "FK_UserScopes_CustomPageSettings_PageId",
                        column: x => x.PageId,
                        principalTable: "CustomPageSettings",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserScopes_DynamicForms_FormScopeDefinitionKey",
                        column: x => x.FormScopeDefinitionKey,
                        principalTable: "DynamicForms",
                        principalColumn: "Key");
                });

            migrationBuilder.CreateTable(
                name: "ApplyValuesConfiguration",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    ActionKey = table.Column<Guid>(type: "uuid", nullable: false),
                    AssignableToApplyToKey = table.Column<Guid>(type: "uuid", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplyValuesConfiguration", x => x.Key);
                    table.ForeignKey(
                        name: "FK_ApplyValuesConfiguration_AssignableConfigurations_Assignabl~",
                        column: x => x.AssignableToApplyToKey,
                        principalTable: "AssignableConfigurations",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ApplyValuesConfiguration_BucketActions_ActionKey",
                        column: x => x.ActionKey,
                        principalTable: "BucketActions",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AssignToConfiguration",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    ActionKey = table.Column<Guid>(type: "uuid", nullable: false),
                    UserToAssignKey = table.Column<Guid>(type: "uuid", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssignToConfiguration", x => x.Key);
                    table.ForeignKey(
                        name: "FK_AssignToConfiguration_AssignableConfigurations_UserToAssign~",
                        column: x => x.UserToAssignKey,
                        principalTable: "AssignableConfigurations",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AssignToConfiguration_BucketActions_ActionKey",
                        column: x => x.ActionKey,
                        principalTable: "BucketActions",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DynamicFormTagBucketActionBindingAdd",
                columns: table => new
                {
                    TagKey = table.Column<Guid>(type: "uuid", nullable: false),
                    AddActionKey = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DynamicFormTagBucketActionBindingAdd", x => new { x.AddActionKey, x.TagKey });
                    table.ForeignKey(
                        name: "FK_DynamicFormTagBucketActionBindingAdd_BucketActions_AddActio~",
                        column: x => x.AddActionKey,
                        principalTable: "BucketActions",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DynamicFormTagBucketActionBindingAdd_DynamicFormTag_TagKey",
                        column: x => x.TagKey,
                        principalTable: "DynamicFormTag",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DynamicFormTagBucketActionBindingRemove",
                columns: table => new
                {
                    TagKey = table.Column<Guid>(type: "uuid", nullable: false),
                    RemoveActionKey = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DynamicFormTagBucketActionBindingRemove", x => new { x.RemoveActionKey, x.TagKey });
                    table.ForeignKey(
                        name: "FK_DynamicFormTagBucketActionBindingRemove_BucketActions_Remov~",
                        column: x => x.RemoveActionKey,
                        principalTable: "BucketActions",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DynamicFormTagBucketActionBindingRemove_DynamicFormTag_TagK~",
                        column: x => x.TagKey,
                        principalTable: "DynamicFormTag",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AssignableValues",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    ForUserId = table.Column<Guid>(type: "uuid", nullable: true),
                    FormResponseKey = table.Column<Guid>(type: "uuid", nullable: true),
                    AssignableConfigurationId = table.Column<Guid>(type: "uuid", nullable: false),
                    UserSteamLinkKey = table.Column<Guid>(type: "uuid", nullable: true),
                    UserDiscordLinkKey = table.Column<Guid>(type: "uuid", nullable: true),
                    UserAccessCodeKey = table.Column<Guid>(type: "uuid", nullable: true),
                    Discriminator = table.Column<string>(type: "character varying(34)", maxLength: 34, nullable: false),
                    BooleanAssignableValue_SetValue = table.Column<List<bool>>(type: "boolean[]", nullable: true),
                    LinkedKey = table.Column<Guid>(type: "uuid", nullable: true),
                    DateOnlyAssignableValue_SetValue = table.Column<List<DateOnly>>(type: "date[]", nullable: true),
                    DateTimeAssignableValue_SetValue = table.Column<List<DateTime>>(type: "timestamp with time zone[]", nullable: true),
                    DoubleAssignableValue_SetValue = table.Column<List<double>>(type: "double precision[]", nullable: true),
                    IntegerAssignableValue_SetValue = table.Column<List<int>>(type: "integer[]", nullable: true),
                    RosterSlotAssignableValue_LinkedKey = table.Column<Guid>(type: "uuid", nullable: true),
                    StringAssignableValue_SetValue = table.Column<List<string>>(type: "text[]", nullable: true),
                    TimeOnlyAssignableValue_SetValue = table.Column<List<TimeOnly>>(type: "time without time zone[]", nullable: true),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssignableValues", x => x.Key);
                    table.ForeignKey(
                        name: "FK_AssignableValues_AspNetUsers_ForUserId",
                        column: x => x.ForUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_AssignableValues_AspNetUsers_LinkedKey",
                        column: x => x.LinkedKey,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_AssignableValues_AspNetUsers_UserAccessCodeKey",
                        column: x => x.UserAccessCodeKey,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_AssignableValues_AspNetUsers_UserDiscordLinkKey",
                        column: x => x.UserDiscordLinkKey,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_AssignableValues_AspNetUsers_UserSteamLinkKey",
                        column: x => x.UserSteamLinkKey,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_AssignableValues_AssignableConfigurations_AssignableConfigu~",
                        column: x => x.AssignableConfigurationId,
                        principalTable: "AssignableConfigurations",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AssignableValues_DynamicFormResponses_FormResponseKey",
                        column: x => x.FormResponseKey,
                        principalTable: "DynamicFormResponses",
                        principalColumn: "Key");
                    table.ForeignKey(
                        name: "FK_AssignableValues_RosterObject_RosterSlotAssignableValue_Lin~",
                        column: x => x.RosterSlotAssignableValue_LinkedKey,
                        principalTable: "RosterObject",
                        principalColumn: "Key");
                });

            migrationBuilder.CreateTable(
                name: "BucketChangeData",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    ResponseKey = table.Column<Guid>(type: "uuid", nullable: false),
                    FromBucketKey = table.Column<Guid>(type: "uuid", nullable: true),
                    ToBucketKey = table.Column<Guid>(type: "uuid", nullable: false),
                    TriggeredById = table.Column<Guid>(type: "uuid", nullable: true),
                    TriggeredByReason = table.Column<string>(type: "text", nullable: true),
                    ChangedOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BucketChangeData", x => x.Key);
                    table.ForeignKey(
                        name: "FK_BucketChangeData_AspNetUsers_TriggeredById",
                        column: x => x.TriggeredById,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_BucketChangeData_DynamicFormBuckets_FromBucketKey",
                        column: x => x.FromBucketKey,
                        principalTable: "DynamicFormBuckets",
                        principalColumn: "Key");
                    table.ForeignKey(
                        name: "FK_BucketChangeData_DynamicFormBuckets_ToBucketKey",
                        column: x => x.ToBucketKey,
                        principalTable: "DynamicFormBuckets",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BucketChangeData_DynamicFormResponses_ResponseKey",
                        column: x => x.ResponseKey,
                        principalTable: "DynamicFormResponses",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DynamicFormTagResponseBinding",
                columns: table => new
                {
                    TagKey = table.Column<Guid>(type: "uuid", nullable: false),
                    ResponseKey = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DynamicFormTagResponseBinding", x => new { x.ResponseKey, x.TagKey });
                    table.ForeignKey(
                        name: "FK_DynamicFormTagResponseBinding_DynamicFormResponses_Response~",
                        column: x => x.ResponseKey,
                        principalTable: "DynamicFormResponses",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DynamicFormTagResponseBinding_DynamicFormTag_TagKey",
                        column: x => x.TagKey,
                        principalTable: "DynamicFormTag",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PageComponentSettingsBase",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    ListingValue = table.Column<int>(type: "integer", nullable: false),
                    DisplayName = table.Column<string>(type: "text", nullable: false),
                    ParentNodeId = table.Column<Guid>(type: "uuid", nullable: true),
                    AuthorizationAreaKey = table.Column<Guid>(type: "uuid", nullable: true),
                    Discriminator = table.Column<string>(type: "character varying(34)", maxLength: 34, nullable: false),
                    ProvidingScopeId = table.Column<Guid>(type: "uuid", nullable: true),
                    AssignableConfigurationKey = table.Column<Guid>(type: "uuid", nullable: true),
                    DataLabel = table.Column<string>(type: "text", nullable: true),
                    LinkedAssignableInputSettingsKey = table.Column<Guid>(type: "uuid", nullable: true),
                    AllowTagEditing = table.Column<bool>(type: "boolean", nullable: true),
                    AllowDelete = table.Column<bool>(type: "boolean", nullable: true),
                    AllowMove = table.Column<bool>(type: "boolean", nullable: true),
                    MaxItemsPerPage = table.Column<int>(type: "integer", nullable: true),
                    Bucket = table.Column<Guid>(type: "uuid", nullable: true),
                    Scoped = table.Column<bool>(type: "boolean", nullable: true),
                    AllowUserListing = table.Column<bool>(type: "boolean", nullable: true),
                    LevelFromTop = table.Column<int>(type: "integer", nullable: true),
                    Depth = table.Column<int>(type: "integer", nullable: true),
                    RosterToDisplay = table.Column<Guid>(type: "uuid", nullable: true),
                    RawContents = table.Column<string>(type: "text", nullable: true),
                    PrivateEdit = table.Column<bool>(type: "boolean", nullable: true),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PageComponentSettingsBase", x => x.Key);
                    table.ForeignKey(
                        name: "FK_PageComponentSettingsBase_AssignableConfigurations_Assignab~",
                        column: x => x.AssignableConfigurationKey,
                        principalTable: "AssignableConfigurations",
                        principalColumn: "Key");
                    table.ForeignKey(
                        name: "FK_PageComponentSettingsBase_AuthorizationAreas_AuthorizationA~",
                        column: x => x.AuthorizationAreaKey,
                        principalTable: "AuthorizationAreas",
                        principalColumn: "Key");
                    table.ForeignKey(
                        name: "FK_PageComponentSettingsBase_LayoutNodes_ParentNodeId",
                        column: x => x.ParentNodeId,
                        principalTable: "LayoutNodes",
                        principalColumn: "Key");
                    table.ForeignKey(
                        name: "FK_PageComponentSettingsBase_LinkedAssignableInputSettings_Lin~",
                        column: x => x.LinkedAssignableInputSettingsKey,
                        principalTable: "LinkedAssignableInputSettings",
                        principalColumn: "Key");
                });

            migrationBuilder.CreateTable(
                name: "ApplyToValueMap",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    ConfigurationKey = table.Column<Guid>(type: "uuid", nullable: false),
                    ValueFromKey = table.Column<Guid>(type: "uuid", nullable: false),
                    ValueToKey = table.Column<Guid>(type: "uuid", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplyToValueMap", x => x.Key);
                    table.ForeignKey(
                        name: "FK_ApplyToValueMap_ApplyValuesConfiguration_ConfigurationKey",
                        column: x => x.ConfigurationKey,
                        principalTable: "ApplyValuesConfiguration",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ApplyToValueMap_AssignableConfigurations_ValueFromKey",
                        column: x => x.ValueFromKey,
                        principalTable: "AssignableConfigurations",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ApplyToValueMap_AssignableConfigurations_ValueToKey",
                        column: x => x.ValueToKey,
                        principalTable: "AssignableConfigurations",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BaseAssignableConfigAssignToConfigurationBinding",
                columns: table => new
                {
                    ToAssignToKey = table.Column<Guid>(type: "uuid", nullable: false),
                    ConfigurationKey = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BaseAssignableConfigAssignToConfigurationBinding", x => new { x.ConfigurationKey, x.ToAssignToKey });
                    table.ForeignKey(
                        name: "FK_BaseAssignableConfigAssignToConfigurationBinding_AssignToCo~",
                        column: x => x.ConfigurationKey,
                        principalTable: "AssignToConfiguration",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BaseAssignableConfigAssignToConfigurationBinding_Assignable~",
                        column: x => x.ToAssignToKey,
                        principalTable: "AssignableConfigurations",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BucketActionFilters",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    ActionKey = table.Column<Guid>(type: "uuid", nullable: false),
                    Order = table.Column<int>(type: "integer", nullable: false),
                    StartParentheses = table.Column<bool>(type: "boolean", nullable: false),
                    EndParentheses = table.Column<bool>(type: "boolean", nullable: false),
                    JoinToPreviousWith = table.Column<int>(type: "integer", nullable: false),
                    Comparison = table.Column<int>(type: "integer", nullable: false),
                    ValueToCompareToKey = table.Column<Guid>(type: "uuid", nullable: true),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BucketActionFilters", x => x.Key);
                    table.ForeignKey(
                        name: "FK_BucketActionFilters_AssignableValues_ValueToCompareToKey",
                        column: x => x.ValueToCompareToKey,
                        principalTable: "AssignableValues",
                        principalColumn: "Key");
                    table.ForeignKey(
                        name: "FK_BucketActionFilters_BucketActions_ActionKey",
                        column: x => x.ActionKey,
                        principalTable: "BucketActions",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LinkSettings",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    RequireDiscordLink = table.Column<bool>(type: "boolean", nullable: false),
                    DiscordLinkConfigurationKey = table.Column<Guid>(type: "uuid", nullable: true),
                    RequireSteamLink = table.Column<bool>(type: "boolean", nullable: false),
                    SteamLinkConfigurationKey = table.Column<Guid>(type: "uuid", nullable: true),
                    RequireAccessCodeForRegister = table.Column<bool>(type: "boolean", nullable: false),
                    AccessCodeConfigurationKey = table.Column<Guid>(type: "uuid", nullable: true),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LinkSettings", x => x.Key);
                    table.ForeignKey(
                        name: "FK_LinkSettings_AssignableValues_AccessCodeConfigurationKey",
                        column: x => x.AccessCodeConfigurationKey,
                        principalTable: "AssignableValues",
                        principalColumn: "Key");
                    table.ForeignKey(
                        name: "FK_LinkSettings_AssignableValues_DiscordLinkConfigurationKey",
                        column: x => x.DiscordLinkConfigurationKey,
                        principalTable: "AssignableValues",
                        principalColumn: "Key");
                    table.ForeignKey(
                        name: "FK_LinkSettings_AssignableValues_SteamLinkConfigurationKey",
                        column: x => x.SteamLinkConfigurationKey,
                        principalTable: "AssignableValues",
                        principalColumn: "Key");
                });

            migrationBuilder.CreateTable(
                name: "AssignableScopeListenerContainer",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    ProvidingScopeId = table.Column<Guid>(type: "uuid", nullable: false),
                    ListeningComponentId = table.Column<Guid>(type: "uuid", nullable: false),
                    Order = table.Column<int>(type: "integer", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssignableScopeListenerContainer", x => x.Key);
                    table.ForeignKey(
                        name: "FK_AssignableScopeListenerContainer_PageComponentSettingsBase_~",
                        column: x => x.ListeningComponentId,
                        principalTable: "PageComponentSettingsBase",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AssignableScopeListenerContainer_UserScopes_ProvidingScopeId",
                        column: x => x.ProvidingScopeId,
                        principalTable: "UserScopes",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AssignableScopeProviderContainer",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    ListeningScopeId = table.Column<Guid>(type: "uuid", nullable: false),
                    ProvidingComponentId = table.Column<Guid>(type: "uuid", nullable: false),
                    Order = table.Column<int>(type: "integer", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssignableScopeProviderContainer", x => x.Key);
                    table.ForeignKey(
                        name: "FK_AssignableScopeProviderContainer_PageComponentSettingsBase_~",
                        column: x => x.ProvidingComponentId,
                        principalTable: "PageComponentSettingsBase",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AssignableScopeProviderContainer_UserScopes_ListeningScopeId",
                        column: x => x.ListeningScopeId,
                        principalTable: "UserScopes",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AssignableValueRenderers",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    ParameterComponentSettingsId = table.Column<Guid>(type: "uuid", nullable: true),
                    RosterComponentSettings_UserListKey = table.Column<Guid>(type: "uuid", nullable: true),
                    RosterComponentSettingsId_UserList = table.Column<Guid>(type: "uuid", nullable: true),
                    DisplayName = table.Column<string>(type: "text", nullable: false),
                    ScopeProviderKey = table.Column<Guid>(type: "uuid", nullable: false),
                    PropertyName = table.Column<string>(type: "text", nullable: false),
                    Static = table.Column<bool>(type: "boolean", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssignableValueRenderers", x => x.Key);
                    table.ForeignKey(
                        name: "FK_AssignableValueRenderers_PageComponentSettingsBase_Paramete~",
                        column: x => x.ParameterComponentSettingsId,
                        principalTable: "PageComponentSettingsBase",
                        principalColumn: "Key");
                    table.ForeignKey(
                        name: "FK_AssignableValueRenderers_PageComponentSettingsBase_RosterCo~",
                        column: x => x.RosterComponentSettings_UserListKey,
                        principalTable: "PageComponentSettingsBase",
                        principalColumn: "Key");
                });

            migrationBuilder.CreateTable(
                name: "BucketValueDisplayOrder",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    SettingsKey = table.Column<Guid>(type: "uuid", nullable: false),
                    ConfigurationKey = table.Column<Guid>(type: "uuid", nullable: false),
                    Order = table.Column<int>(type: "integer", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BucketValueDisplayOrder", x => x.Key);
                    table.ForeignKey(
                        name: "FK_BucketValueDisplayOrder_AssignableConfigurations_Configurat~",
                        column: x => x.ConfigurationKey,
                        principalTable: "AssignableConfigurations",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BucketValueDisplayOrder_PageComponentSettingsBase_SettingsK~",
                        column: x => x.SettingsKey,
                        principalTable: "PageComponentSettingsBase",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ButtonComponentData",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    ButtonStyle = table.Column<string>(type: "text", nullable: false),
                    BucketKey = table.Column<Guid>(type: "uuid", nullable: true),
                    TagsToApplyOnSubmit = table.Column<List<Guid>>(type: "uuid[]", nullable: false),
                    InvokeReset = table.Column<bool>(type: "boolean", nullable: false),
                    InvokeSubmit = table.Column<bool>(type: "boolean", nullable: false),
                    ButtonComponentSettingsKey = table.Column<Guid>(type: "uuid", nullable: true),
                    BucketComponentSettingsKey = table.Column<Guid>(type: "uuid", nullable: true),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ButtonComponentData", x => x.Key);
                    table.ForeignKey(
                        name: "FK_ButtonComponentData_PageComponentSettingsBase_BucketCompone~",
                        column: x => x.BucketComponentSettingsKey,
                        principalTable: "PageComponentSettingsBase",
                        principalColumn: "Key");
                    table.ForeignKey(
                        name: "FK_ButtonComponentData_PageComponentSettingsBase_ButtonCompone~",
                        column: x => x.ButtonComponentSettingsKey,
                        principalTable: "PageComponentSettingsBase",
                        principalColumn: "Key");
                });

            migrationBuilder.CreateTable(
                name: "ComponentPoolBinding",
                columns: table => new
                {
                    PoolKey = table.Column<Guid>(type: "uuid", nullable: false),
                    SettingsKey = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ComponentPoolBinding", x => new { x.PoolKey, x.SettingsKey });
                    table.ForeignKey(
                        name: "FK_ComponentPoolBinding_ComponentPool_PoolKey",
                        column: x => x.PoolKey,
                        principalTable: "ComponentPool",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ComponentPoolBinding_PageComponentSettingsBase_SettingsKey",
                        column: x => x.SettingsKey,
                        principalTable: "PageComponentSettingsBase",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DisplayComponentData",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    AuthorizedRaw = table.Column<string>(type: "text", nullable: false),
                    UnAuthorizedRaw = table.Column<string>(type: "text", nullable: false),
                    DisplayComponentSettingsKey = table.Column<Guid>(type: "uuid", nullable: true),
                    BucketComponentSettingsKey = table.Column<Guid>(type: "uuid", nullable: true),
                    RosterComponentSettingsKey = table.Column<Guid>(type: "uuid", nullable: true),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DisplayComponentData", x => x.Key);
                    table.ForeignKey(
                        name: "FK_DisplayComponentData_PageComponentSettingsBase_BucketCompon~",
                        column: x => x.BucketComponentSettingsKey,
                        principalTable: "PageComponentSettingsBase",
                        principalColumn: "Key");
                    table.ForeignKey(
                        name: "FK_DisplayComponentData_PageComponentSettingsBase_DisplayCompo~",
                        column: x => x.DisplayComponentSettingsKey,
                        principalTable: "PageComponentSettingsBase",
                        principalColumn: "Key");
                    table.ForeignKey(
                        name: "FK_DisplayComponentData_PageComponentSettingsBase_RosterCompon~",
                        column: x => x.RosterComponentSettingsKey,
                        principalTable: "PageComponentSettingsBase",
                        principalColumn: "Key");
                });

            migrationBuilder.CreateTable(
                name: "EditableComponentSettingsRosterDisplaySettings",
                columns: table => new
                {
                    EditableComponentsAllowedEditorsKey = table.Column<Guid>(type: "uuid", nullable: false),
                    EditableDisplaysKey = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EditableComponentSettingsRosterDisplaySettings", x => new { x.EditableComponentsAllowedEditorsKey, x.EditableDisplaysKey });
                    table.ForeignKey(
                        name: "FK_EditableComponentSettingsRosterDisplaySettings_PageComponen~",
                        column: x => x.EditableComponentsAllowedEditorsKey,
                        principalTable: "PageComponentSettingsBase",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EditableComponentSettingsRosterDisplaySettings_RosterDispla~",
                        column: x => x.EditableDisplaysKey,
                        principalTable: "RosterDisplaySettings",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RosterDisplayColumnOrder",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    SettingsKey = table.Column<Guid>(type: "uuid", nullable: false),
                    ConfigurationKey = table.Column<Guid>(type: "uuid", nullable: false),
                    Order = table.Column<int>(type: "integer", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RosterDisplayColumnOrder", x => x.Key);
                    table.ForeignKey(
                        name: "FK_RosterDisplayColumnOrder_AssignableConfigurations_Configura~",
                        column: x => x.ConfigurationKey,
                        principalTable: "AssignableConfigurations",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RosterDisplayColumnOrder_PageComponentSettingsBase_Settings~",
                        column: x => x.SettingsKey,
                        principalTable: "PageComponentSettingsBase",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ComparisonPair",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    FilterCompareFromKey = table.Column<Guid>(type: "uuid", nullable: true),
                    ConfigurationKey = table.Column<Guid>(type: "uuid", nullable: false),
                    Order = table.Column<int>(type: "integer", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ComparisonPair", x => x.Key);
                    table.ForeignKey(
                        name: "FK_ComparisonPair_AssignableConfigurations_ConfigurationKey",
                        column: x => x.ConfigurationKey,
                        principalTable: "AssignableConfigurations",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ComparisonPair_BucketActionFilters_FilterCompareFromKey",
                        column: x => x.FilterCompareFromKey,
                        principalTable: "BucketActionFilters",
                        principalColumn: "Key");
                });

            migrationBuilder.CreateTable(
                name: "AssignableValueConversions",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    RendererId = table.Column<Guid>(type: "uuid", nullable: false),
                    ValueName = table.Column<string>(type: "text", nullable: false),
                    String_FormatString = table.Column<string>(type: "text", nullable: true),
                    Numeric_FormatString = table.Column<string>(type: "text", nullable: true),
                    DateTime_ToStringPattern = table.Column<string>(type: "text", nullable: true),
                    DateTime_ConvertToTimeSpan = table.Column<bool>(type: "boolean", nullable: false),
                    DateTime_ManualTimeSpanComparision = table.Column<bool>(type: "boolean", nullable: false),
                    DateTime_TimeSpanConversionCompareTo = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Bool_FormatOnTrue = table.Column<string>(type: "text", nullable: false),
                    Bool_FormatOnFalse = table.Column<string>(type: "text", nullable: false),
                    Multi_MaxValues = table.Column<int>(type: "integer", nullable: false),
                    Multi_Separator = table.Column<string>(type: "text", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssignableValueConversions", x => x.Key);
                    table.ForeignKey(
                        name: "FK_AssignableValueConversions_AssignableValueRenderers_Rendere~",
                        column: x => x.RendererId,
                        principalTable: "AssignableValueRenderers",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ApplyToValueMap_ConfigurationKey",
                table: "ApplyToValueMap",
                column: "ConfigurationKey");

            migrationBuilder.CreateIndex(
                name: "IX_ApplyToValueMap_ValueFromKey",
                table: "ApplyToValueMap",
                column: "ValueFromKey");

            migrationBuilder.CreateIndex(
                name: "IX_ApplyToValueMap_ValueToKey",
                table: "ApplyToValueMap",
                column: "ValueToKey");

            migrationBuilder.CreateIndex(
                name: "IX_ApplyValuesConfiguration_ActionKey",
                table: "ApplyValuesConfiguration",
                column: "ActionKey");

            migrationBuilder.CreateIndex(
                name: "IX_ApplyValuesConfiguration_AssignableToApplyToKey",
                table: "ApplyValuesConfiguration",
                column: "AssignableToApplyToKey");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_CurrentlyImpersonatingKey",
                table: "AspNetUsers",
                column: "CurrentlyImpersonatingKey");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AssignableConfigurations_FormKey",
                table: "AssignableConfigurations",
                column: "FormKey");

            migrationBuilder.CreateIndex(
                name: "IX_AssignableScopeListenerContainer_ListeningComponentId",
                table: "AssignableScopeListenerContainer",
                column: "ListeningComponentId");

            migrationBuilder.CreateIndex(
                name: "IX_AssignableScopeListenerContainer_ProvidingScopeId",
                table: "AssignableScopeListenerContainer",
                column: "ProvidingScopeId");

            migrationBuilder.CreateIndex(
                name: "IX_AssignableScopeProviderContainer_ListeningScopeId",
                table: "AssignableScopeProviderContainer",
                column: "ListeningScopeId");

            migrationBuilder.CreateIndex(
                name: "IX_AssignableScopeProviderContainer_ProvidingComponentId",
                table: "AssignableScopeProviderContainer",
                column: "ProvidingComponentId");

            migrationBuilder.CreateIndex(
                name: "IX_AssignableValueConversions_RendererId",
                table: "AssignableValueConversions",
                column: "RendererId");

            migrationBuilder.CreateIndex(
                name: "IX_AssignableValueRenderers_ParameterComponentSettingsId",
                table: "AssignableValueRenderers",
                column: "ParameterComponentSettingsId");

            migrationBuilder.CreateIndex(
                name: "IX_AssignableValueRenderers_RosterComponentSettings_UserListKey",
                table: "AssignableValueRenderers",
                column: "RosterComponentSettings_UserListKey");

            migrationBuilder.CreateIndex(
                name: "IX_AssignableValues_AssignableConfigurationId",
                table: "AssignableValues",
                column: "AssignableConfigurationId");

            migrationBuilder.CreateIndex(
                name: "IX_AssignableValues_FormResponseKey",
                table: "AssignableValues",
                column: "FormResponseKey");

            migrationBuilder.CreateIndex(
                name: "IX_AssignableValues_ForUserId",
                table: "AssignableValues",
                column: "ForUserId");

            migrationBuilder.CreateIndex(
                name: "IX_AssignableValues_LinkedKey",
                table: "AssignableValues",
                column: "LinkedKey");

            migrationBuilder.CreateIndex(
                name: "IX_AssignableValues_RosterSlotAssignableValue_LinkedKey",
                table: "AssignableValues",
                column: "RosterSlotAssignableValue_LinkedKey");

            migrationBuilder.CreateIndex(
                name: "IX_AssignableValues_UserAccessCodeKey",
                table: "AssignableValues",
                column: "UserAccessCodeKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AssignableValues_UserDiscordLinkKey",
                table: "AssignableValues",
                column: "UserDiscordLinkKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AssignableValues_UserSteamLinkKey",
                table: "AssignableValues",
                column: "UserSteamLinkKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AssignToConfiguration_ActionKey",
                table: "AssignToConfiguration",
                column: "ActionKey");

            migrationBuilder.CreateIndex(
                name: "IX_AssignToConfiguration_UserToAssignKey",
                table: "AssignToConfiguration",
                column: "UserToAssignKey");

            migrationBuilder.CreateIndex(
                name: "IX_AuthorizationBindings_AuthorizationAreaKey",
                table: "AuthorizationBindings",
                column: "AuthorizationAreaKey");

            migrationBuilder.CreateIndex(
                name: "IX_AuthorizationBindings_FullRosterTreeKey",
                table: "AuthorizationBindings",
                column: "FullRosterTreeKey");

            migrationBuilder.CreateIndex(
                name: "IX_AuthorizationBindings_RosterSlotKey",
                table: "AuthorizationBindings",
                column: "RosterSlotKey");

            migrationBuilder.CreateIndex(
                name: "IX_AuthorizationBindings_RosterTreeKey",
                table: "AuthorizationBindings",
                column: "RosterTreeKey");

            migrationBuilder.CreateIndex(
                name: "IX_AuthorizationBindings_TagKey",
                table: "AuthorizationBindings",
                column: "TagKey");

            migrationBuilder.CreateIndex(
                name: "IX_AuthorizationBindings_UserKey",
                table: "AuthorizationBindings",
                column: "UserKey");

            migrationBuilder.CreateIndex(
                name: "IX_AuthorizationTagBindings_RosterSlotKey",
                table: "AuthorizationTagBindings",
                column: "RosterSlotKey");

            migrationBuilder.CreateIndex(
                name: "IX_AuthorizationTagBindings_RosterTreeKey",
                table: "AuthorizationTagBindings",
                column: "RosterTreeKey");

            migrationBuilder.CreateIndex(
                name: "IX_AuthorizationTagBindings_TagKey",
                table: "AuthorizationTagBindings",
                column: "TagKey");

            migrationBuilder.CreateIndex(
                name: "IX_AuthorizationTagBindings_UserId",
                table: "AuthorizationTagBindings",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AuthorizationTagLinkedAssignableInputSettingsBinding_Exclud~",
                table: "AuthorizationTagLinkedAssignableInputSettingsBinding",
                column: "ExcludeFilterKey");

            migrationBuilder.CreateIndex(
                name: "IX_AuthorizationTagLinkedAssignableInputSettingsBinding_TagKey",
                table: "AuthorizationTagLinkedAssignableInputSettingsBinding",
                column: "TagKey");

            migrationBuilder.CreateIndex(
                name: "IX_BaseAssignableConfigAssignToConfigurationBinding_ToAssignTo~",
                table: "BaseAssignableConfigAssignToConfigurationBinding",
                column: "ToAssignToKey");

            migrationBuilder.CreateIndex(
                name: "IX_BucketActionFilters_ActionKey",
                table: "BucketActionFilters",
                column: "ActionKey");

            migrationBuilder.CreateIndex(
                name: "IX_BucketActionFilters_ValueToCompareToKey",
                table: "BucketActionFilters",
                column: "ValueToCompareToKey");

            migrationBuilder.CreateIndex(
                name: "IX_BucketActions_BucketKey",
                table: "BucketActions",
                column: "BucketKey");

            migrationBuilder.CreateIndex(
                name: "IX_BucketActions_MoveResponse_MoveToKey",
                table: "BucketActions",
                column: "MoveResponse_MoveToKey");

            migrationBuilder.CreateIndex(
                name: "IX_BucketChangeData_FromBucketKey",
                table: "BucketChangeData",
                column: "FromBucketKey");

            migrationBuilder.CreateIndex(
                name: "IX_BucketChangeData_ResponseKey",
                table: "BucketChangeData",
                column: "ResponseKey");

            migrationBuilder.CreateIndex(
                name: "IX_BucketChangeData_ToBucketKey",
                table: "BucketChangeData",
                column: "ToBucketKey");

            migrationBuilder.CreateIndex(
                name: "IX_BucketChangeData_TriggeredById",
                table: "BucketChangeData",
                column: "TriggeredById");

            migrationBuilder.CreateIndex(
                name: "IX_BucketValueDisplayOrder_ConfigurationKey",
                table: "BucketValueDisplayOrder",
                column: "ConfigurationKey");

            migrationBuilder.CreateIndex(
                name: "IX_BucketValueDisplayOrder_SettingsKey",
                table: "BucketValueDisplayOrder",
                column: "SettingsKey");

            migrationBuilder.CreateIndex(
                name: "IX_ButtonComponentData_BucketComponentSettingsKey",
                table: "ButtonComponentData",
                column: "BucketComponentSettingsKey");

            migrationBuilder.CreateIndex(
                name: "IX_ButtonComponentData_ButtonComponentSettingsKey",
                table: "ButtonComponentData",
                column: "ButtonComponentSettingsKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ComparisonPair_ConfigurationKey",
                table: "ComparisonPair",
                column: "ConfigurationKey");

            migrationBuilder.CreateIndex(
                name: "IX_ComparisonPair_FilterCompareFromKey",
                table: "ComparisonPair",
                column: "FilterCompareFromKey");

            migrationBuilder.CreateIndex(
                name: "IX_ComponentPool_PageSettingsKey",
                table: "ComponentPool",
                column: "PageSettingsKey");

            migrationBuilder.CreateIndex(
                name: "IX_ComponentPoolBinding_SettingsKey",
                table: "ComponentPoolBinding",
                column: "SettingsKey");

            migrationBuilder.CreateIndex(
                name: "IX_CustomPageSettings_AuthorizationAreaKey",
                table: "CustomPageSettings",
                column: "AuthorizationAreaKey");

            migrationBuilder.CreateIndex(
                name: "IX_CustomPageSettings_RouteRootKey",
                table: "CustomPageSettings",
                column: "RouteRootKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DisplayComponentData_BucketComponentSettingsKey",
                table: "DisplayComponentData",
                column: "BucketComponentSettingsKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DisplayComponentData_DisplayComponentSettingsKey",
                table: "DisplayComponentData",
                column: "DisplayComponentSettingsKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DisplayComponentData_RosterComponentSettingsKey",
                table: "DisplayComponentData",
                column: "RosterComponentSettingsKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DynamicFormBuckets_FormKey",
                table: "DynamicFormBuckets",
                column: "FormKey");

            migrationBuilder.CreateIndex(
                name: "IX_DynamicFormBuckets_IsDefaultForKey",
                table: "DynamicFormBuckets",
                column: "IsDefaultForKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DynamicFormResponses_BucketKey",
                table: "DynamicFormResponses",
                column: "BucketKey");

            migrationBuilder.CreateIndex(
                name: "IX_DynamicFormResponses_FormKey",
                table: "DynamicFormResponses",
                column: "FormKey");

            migrationBuilder.CreateIndex(
                name: "IX_DynamicFormResponses_SubmitterId",
                table: "DynamicFormResponses",
                column: "SubmitterId");

            migrationBuilder.CreateIndex(
                name: "IX_DynamicForms_Name",
                table: "DynamicForms",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_DynamicFormTag_FormKey",
                table: "DynamicFormTag",
                column: "FormKey");

            migrationBuilder.CreateIndex(
                name: "IX_DynamicFormTagBucketActionBindingAdd_TagKey",
                table: "DynamicFormTagBucketActionBindingAdd",
                column: "TagKey");

            migrationBuilder.CreateIndex(
                name: "IX_DynamicFormTagBucketActionBindingRemove_TagKey",
                table: "DynamicFormTagBucketActionBindingRemove",
                column: "TagKey");

            migrationBuilder.CreateIndex(
                name: "IX_DynamicFormTagResponseBinding_TagKey",
                table: "DynamicFormTagResponseBinding",
                column: "TagKey");

            migrationBuilder.CreateIndex(
                name: "IX_EditableComponentSettingsRosterDisplaySettings_EditableDisp~",
                table: "EditableComponentSettingsRosterDisplaySettings",
                column: "EditableDisplaysKey");

            migrationBuilder.CreateIndex(
                name: "IX_LayoutNodes_AuthorizationAreaKey",
                table: "LayoutNodes",
                column: "AuthorizationAreaKey");

            migrationBuilder.CreateIndex(
                name: "IX_LayoutNodes_PageSettingsId",
                table: "LayoutNodes",
                column: "PageSettingsId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_LayoutNodes_ParentNodeId",
                table: "LayoutNodes",
                column: "ParentNodeId");

            migrationBuilder.CreateIndex(
                name: "IX_LinkedAssignableInputSettings_DataAuthorizationAreaKey",
                table: "LinkedAssignableInputSettings",
                column: "DataAuthorizationAreaKey");

            migrationBuilder.CreateIndex(
                name: "IX_LinkSettings_AccessCodeConfigurationKey",
                table: "LinkSettings",
                column: "AccessCodeConfigurationKey");

            migrationBuilder.CreateIndex(
                name: "IX_LinkSettings_DiscordLinkConfigurationKey",
                table: "LinkSettings",
                column: "DiscordLinkConfigurationKey");

            migrationBuilder.CreateIndex(
                name: "IX_LinkSettings_SteamLinkConfigurationKey",
                table: "LinkSettings",
                column: "SteamLinkConfigurationKey");

            migrationBuilder.CreateIndex(
                name: "IX_NavModules_AuthKey",
                table: "NavModules",
                column: "AuthKey");

            migrationBuilder.CreateIndex(
                name: "IX_NavModules_PageId",
                table: "NavModules",
                column: "PageId");

            migrationBuilder.CreateIndex(
                name: "IX_NavModules_ParentId",
                table: "NavModules",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_PageComponentSettingsBase_AssignableConfigurationKey",
                table: "PageComponentSettingsBase",
                column: "AssignableConfigurationKey");

            migrationBuilder.CreateIndex(
                name: "IX_PageComponentSettingsBase_AuthorizationAreaKey",
                table: "PageComponentSettingsBase",
                column: "AuthorizationAreaKey");

            migrationBuilder.CreateIndex(
                name: "IX_PageComponentSettingsBase_LinkedAssignableInputSettingsKey",
                table: "PageComponentSettingsBase",
                column: "LinkedAssignableInputSettingsKey");

            migrationBuilder.CreateIndex(
                name: "IX_PageComponentSettingsBase_ParentNodeId",
                table: "PageComponentSettingsBase",
                column: "ParentNodeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_RosterDisplayColumnOrder_ConfigurationKey",
                table: "RosterDisplayColumnOrder",
                column: "ConfigurationKey");

            migrationBuilder.CreateIndex(
                name: "IX_RosterDisplayColumnOrder_SettingsKey",
                table: "RosterDisplayColumnOrder",
                column: "SettingsKey");

            migrationBuilder.CreateIndex(
                name: "IX_RosterDisplaySettings_HostRosterId",
                table: "RosterDisplaySettings",
                column: "HostRosterId");

            migrationBuilder.CreateIndex(
                name: "IX_RosterObject_OccupiedById",
                table: "RosterObject",
                column: "OccupiedById");

            migrationBuilder.CreateIndex(
                name: "IX_RosterObject_ParentRosterId",
                table: "RosterObject",
                column: "ParentRosterId");

            migrationBuilder.CreateIndex(
                name: "IX_RosterOrders_ParentObjectId",
                table: "RosterOrders",
                column: "ParentObjectId");

            migrationBuilder.CreateIndex(
                name: "IX_RosterOrders_SlotToOrderId",
                table: "RosterOrders",
                column: "SlotToOrderId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_RosterOrders_TreeToOrderId",
                table: "RosterOrders",
                column: "TreeToOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_RouteRoots_AuthorizationAreaKey",
                table: "RouteRoots",
                column: "AuthorizationAreaKey");

            migrationBuilder.CreateIndex(
                name: "IX_RouteRoots_CanEditAuthorizationAreaKey",
                table: "RouteRoots",
                column: "CanEditAuthorizationAreaKey");

            migrationBuilder.CreateIndex(
                name: "IX_RouteRoots_FullRoute",
                table: "RouteRoots",
                column: "FullRoute",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_RouteRoots_ParentRootKey",
                table: "RouteRoots",
                column: "ParentRootKey");

            migrationBuilder.CreateIndex(
                name: "IX_UserKeybinding_DataCoreUserId",
                table: "UserKeybinding",
                column: "DataCoreUserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserScopes_FormScopeDefinitionKey",
                table: "UserScopes",
                column: "FormScopeDefinitionKey");

            migrationBuilder.CreateIndex(
                name: "IX_UserScopes_PageId",
                table: "UserScopes",
                column: "PageId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ApplyToValueMap");

            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "AssignableScopeListenerContainer");

            migrationBuilder.DropTable(
                name: "AssignableScopeProviderContainer");

            migrationBuilder.DropTable(
                name: "AssignableValueConversions");

            migrationBuilder.DropTable(
                name: "AuthorizationBindings");

            migrationBuilder.DropTable(
                name: "AuthorizationTagBindings");

            migrationBuilder.DropTable(
                name: "AuthorizationTagLinkedAssignableInputSettingsBinding");

            migrationBuilder.DropTable(
                name: "BaseAssignableConfigAssignToConfigurationBinding");

            migrationBuilder.DropTable(
                name: "BucketChangeData");

            migrationBuilder.DropTable(
                name: "BucketValueDisplayOrder");

            migrationBuilder.DropTable(
                name: "ButtonComponentData");

            migrationBuilder.DropTable(
                name: "ComparisonPair");

            migrationBuilder.DropTable(
                name: "ComponentPoolBinding");

            migrationBuilder.DropTable(
                name: "DisplayComponentData");

            migrationBuilder.DropTable(
                name: "DynamicFormTagBucketActionBindingAdd");

            migrationBuilder.DropTable(
                name: "DynamicFormTagBucketActionBindingRemove");

            migrationBuilder.DropTable(
                name: "DynamicFormTagResponseBinding");

            migrationBuilder.DropTable(
                name: "EditableComponentSettingsRosterDisplaySettings");

            migrationBuilder.DropTable(
                name: "LinkSettings");

            migrationBuilder.DropTable(
                name: "NavModules");

            migrationBuilder.DropTable(
                name: "RosterDisplayColumnOrder");

            migrationBuilder.DropTable(
                name: "RosterOrders");

            migrationBuilder.DropTable(
                name: "UserKeybinding");

            migrationBuilder.DropTable(
                name: "UserSelectComponentSettings");

            migrationBuilder.DropTable(
                name: "ApplyValuesConfiguration");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "UserScopes");

            migrationBuilder.DropTable(
                name: "AssignableValueRenderers");

            migrationBuilder.DropTable(
                name: "AuthorizationTags");

            migrationBuilder.DropTable(
                name: "AssignToConfiguration");

            migrationBuilder.DropTable(
                name: "BucketActionFilters");

            migrationBuilder.DropTable(
                name: "ComponentPool");

            migrationBuilder.DropTable(
                name: "DynamicFormTag");

            migrationBuilder.DropTable(
                name: "RosterDisplaySettings");

            migrationBuilder.DropTable(
                name: "PageComponentSettingsBase");

            migrationBuilder.DropTable(
                name: "AssignableValues");

            migrationBuilder.DropTable(
                name: "BucketActions");

            migrationBuilder.DropTable(
                name: "LayoutNodes");

            migrationBuilder.DropTable(
                name: "LinkedAssignableInputSettings");

            migrationBuilder.DropTable(
                name: "AssignableConfigurations");

            migrationBuilder.DropTable(
                name: "DynamicFormResponses");

            migrationBuilder.DropTable(
                name: "RosterObject");

            migrationBuilder.DropTable(
                name: "CustomPageSettings");

            migrationBuilder.DropTable(
                name: "DynamicFormBuckets");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "RouteRoots");

            migrationBuilder.DropTable(
                name: "DynamicForms");

            migrationBuilder.DropTable(
                name: "AuthorizationAreas");
        }
    }
}
