﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ProjectDataCore.Data.Migrations
{
    /// <inheritdoc />
    public partial class RosterEditing : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "DisableEditing",
                table: "PageComponentSettingsBase",
                type: "boolean",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "EditingAuthAreaKey",
                table: "PageComponentSettingsBase",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PageComponentSettingsBase_EditingAuthAreaKey",
                table: "PageComponentSettingsBase",
                column: "EditingAuthAreaKey");

            migrationBuilder.AddForeignKey(
                name: "FK_PageComponentSettingsBase_AuthorizationAreas_EditingAuthAre~",
                table: "PageComponentSettingsBase",
                column: "EditingAuthAreaKey",
                principalTable: "AuthorizationAreas",
                principalColumn: "Key");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PageComponentSettingsBase_AuthorizationAreas_EditingAuthAre~",
                table: "PageComponentSettingsBase");

            migrationBuilder.DropIndex(
                name: "IX_PageComponentSettingsBase_EditingAuthAreaKey",
                table: "PageComponentSettingsBase");

            migrationBuilder.DropColumn(
                name: "DisableEditing",
                table: "PageComponentSettingsBase");

            migrationBuilder.DropColumn(
                name: "EditingAuthAreaKey",
                table: "PageComponentSettingsBase");
        }
    }
}
