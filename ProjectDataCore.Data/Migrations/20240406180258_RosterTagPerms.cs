﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ProjectDataCore.Data.Migrations
{
    /// <inheritdoc />
    public partial class RosterTagPerms : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddUniqueConstraint(
                name: "AK_AuthorizationTags_Name",
                table: "AuthorizationTags",
                column: "Name");

            migrationBuilder.CreateTable(
                name: "RosterTagPermissions",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    AuthorizationTagKey = table.Column<Guid>(type: "uuid", nullable: false),
                    CanEditSelf = table.Column<bool>(type: "boolean", nullable: false),
                    CanEditTree = table.Column<bool>(type: "boolean", nullable: false),
                    CanEditChildren = table.Column<bool>(type: "boolean", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RosterTagPermissions", x => x.Key);
                    table.ForeignKey(
                        name: "FK_RosterTagPermissions_AuthorizationTags_AuthorizationTagKey",
                        column: x => x.AuthorizationTagKey,
                        principalTable: "AuthorizationTags",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RosterTagPermissionAuthorizationTagWhitelistBinding",
                columns: table => new
                {
                    RosterTagPermissionKey = table.Column<Guid>(type: "uuid", nullable: false),
                    AuthorizationTagKey = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RosterTagPermissionAuthorizationTagWhitelistBinding", x => new { x.AuthorizationTagKey, x.RosterTagPermissionKey });
                    table.ForeignKey(
                        name: "FK_RosterTagPermissionAuthorizationTagWhitelistBinding_Authori~",
                        column: x => x.AuthorizationTagKey,
                        principalTable: "AuthorizationTags",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RosterTagPermissionAuthorizationTagWhitelistBinding_RosterT~",
                        column: x => x.RosterTagPermissionKey,
                        principalTable: "RosterTagPermissions",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RosterTagPermissionAuthorizationTagWhitelistBinding_RosterT~",
                table: "RosterTagPermissionAuthorizationTagWhitelistBinding",
                column: "RosterTagPermissionKey");

            migrationBuilder.CreateIndex(
                name: "IX_RosterTagPermissions_AuthorizationTagKey",
                table: "RosterTagPermissions",
                column: "AuthorizationTagKey");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RosterTagPermissionAuthorizationTagWhitelistBinding");

            migrationBuilder.DropTable(
                name: "RosterTagPermissions");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_AuthorizationTags_Name",
                table: "AuthorizationTags");
        }
    }
}
