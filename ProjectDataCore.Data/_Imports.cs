﻿global using Microsoft.EntityFrameworkCore;
global using Microsoft.AspNetCore.Components;

global using ProjectDataCore.Data.Database;
global using ProjectDataCore.Data.Structures.Result;
global using ProjectDataCore.Data.Structures.Roster;
global using ProjectDataCore.Data.Structures.Model.Roster;
global using ProjectDataCore.Data.Structures.Util;
global using ProjectDataCore.Data.Account;
global using ProjectDataCore.Data.Services;
global using ProjectDataCore.Data.Services.Page;
global using ProjectDataCore.Data.Structures.Page.Components;
global using ProjectDataCore.Data.Structures.Page.Attributes;
global using ProjectDataCore.Data.Structures.Model.Page;

global using System;
global using System.Collections.Generic;
global using System.Linq;
global using System.Text;
global using System.Threading.Tasks;