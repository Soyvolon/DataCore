﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.History.PageEdit.General;
public class GeneralHistoryItem<T> : EditHistoryItemBase
{
    private T[] Parameters { get; init; }
    private Func<IServiceProvider, T[], Task<ActionResult>> OnUndo { get; init; }
    private Func<IServiceProvider, T[], Task<ActionResult>> OnRedo { get; init; }

    public GeneralHistoryItem(string name, 
        Func<IServiceProvider, T[], Task<ActionResult>> undo, 
        Func<IServiceProvider, T[], Task<ActionResult>> redo,
        params T[] parameters)
        : base(name)
    {
        Parameters = parameters;
        OnUndo = undo;
        OnRedo = redo;
    }

    internal override async Task<ActionResult> Redo(IServiceProvider serviceProvider)
        => await OnRedo.Invoke(serviceProvider, Parameters);

    internal override async Task<ActionResult> Undo(IServiceProvider serviceProvider)
        => await OnUndo.Invoke(serviceProvider, Parameters);
}
