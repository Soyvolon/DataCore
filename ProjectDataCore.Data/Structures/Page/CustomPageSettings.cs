﻿using ProjectDataCore.Data.Structures.Auth;
using ProjectDataCore.Data.Structures.Page.Components;
using ProjectDataCore.Data.Structures.Page.Components.Layout;
using ProjectDataCore.Data.Structures.Page.Components.Pool;
using ProjectDataCore.Data.Structures.Page.Components.Scope;
using ProjectDataCore.Data.Structures.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Page;

/// <summary>
/// Details the settings for a specific page based upon its
/// <see cref="CompositeRoute"/>
/// </summary>
public class CustomPageSettings : DataObject<Guid>
{
    /// <summary>
    /// If true, this page is a hard coded page.
    /// </summary>
    public bool IsStaticPage { get; set; }

    /// <summary>
    /// The root route for this component. All routes are grouped into a root route.
    /// </summary>
    [NoObjectTreeDiscovery]
    public RouteRoot? RouteRoot { get; set; }
    /// <summary>
    /// The Key for the <see cref="RouteRoot"/>.
    /// </summary>
    public Guid? RouteRootKey { get; set; }

    /// <summary>
    /// If true, redirect the user to the URL in this
    /// variable when they land on this page.
    /// </summary>
    public string? RedirectTo { get; set; }

    /// <summary>
    /// The Display name for this page
    /// </summary>
    public string? Name { get; set; }

    /// <summary>
    /// The component to display on this page. It can be a layout
    /// component, meaning the page can have multiple different
    /// items displayed.
    /// </summary>
    public LayoutNode? Layout { get; set; }
    /// <summary>
    /// The ID for the layout in <see cref="Layout"/>
    /// </summary>
    public Guid? LayoutId { get; set; }
    /// <summary>
    /// The user scopes for this page.
    /// </summary>
    public List<AssignableScope> AssignableScopes { get; set; } = new();
    /// <summary>
    /// The component pools for this page.
    /// </summary>
    public List<ComponentPool> ComponentPools { get; set; } = new();

    /// <summary>
    /// The authorization for this page.
    /// </summary>
    [NoObjectTreeDiscovery]
    public AuthorizationArea? AuthorizationArea { get; set; }
    public Guid? AuthorizationAreaKey { get; set; }

    /// <summary>
    /// Gets a list of component settings from all child layout nodes.
    /// </summary>
    /// <remarks>
    /// If this object has been pulled from the database without being
    /// properly loaded, this method will not return a full list of
    /// components.
    /// </remarks>
    /// <returns>A <see cref="List{T}"/> of <see cref="PageComponentSettingsBase"/> for all child <see cref="LayoutNode"/>s
    /// that have a registered component.</returns>
    public List<PageComponentSettingsBase> GetChildComponents()
    {
        List<PageComponentSettingsBase> components = new();

        if (Layout is not null)
        {
            Stack<LayoutNode> nodeStack = new();
            nodeStack.Push(Layout);
            while (nodeStack.TryPop(out var node))
            {
                if (node.Component is not null)
                    components.Add(node.Component);

                foreach (var child in node.Nodes)
                    nodeStack.Push(child);
            }
        }

        return components;
	}

    public bool RemoveScope(AssignableScope scope)
    {
        if (!AssignableScopes.Remove(scope))
            return false;

        return scope.DetachSelf();
    }
}