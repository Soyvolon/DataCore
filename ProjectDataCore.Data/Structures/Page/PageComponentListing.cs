﻿namespace ProjectDataCore.Data.Structures.Page;

/*
 * This listing of pages is used by the routing service to identity and load
 * the proper type of a page on startup. Then, during runtime, the page settings
 * objects take their stored listing value - compare it against the renders cache -
 * and load the proper type.
 * 
 * I chose to move to this system because the previous system would break as soon
 * as you moved a file around, and seeing as a developer should be able to reorder
 * folders/files when needed, that wasn't going to work long term. This requires
 * more setup when doing the programming, but moving a file will no longer
 * break all page rendering for existing components.
 * 
 * -- Aria Bounds (2023-07-25)
 */

/// <summary>
/// A listing of all available components the website
/// can render.
/// </summary>
public enum PageComponentListing
{
	// Admin (aprox. 00-99)
	Admin_AdminHome						= 00,
	Admin_RosterEditor					= 01,
	Admin_PageEditor					= 02,
	Admin_NavbarEditor					= 03,
	Admin_FormEditor					= 04,
	Admin_DataImportPage				= 05,
	Admin_ImpersonationArea				= 06,
	Admin_AuthorizationTagEditor		= 07,
	Admin_AuthorizationAreaEditor		= 08,
	Admin_AdminAssignableValueEditor	= 09,
	Admin_AccountHome					= 10,

	// Forms (aprox. 100-199)
	Forms_AssignableValueDisplay		= 100,
	Forms_AssignableValueEdit			= 101,
	Forms_FormButton					= 102,
	Forms_FormBucket					= 103,

	// Roster (aprox. 200-299)
	Roster_RosterDisplay				= 200,
}
