﻿using ProjectDataCore.Data.Structures.Page.Components.Parameters.Form;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Page.Data;
public class ButtonComponentData : DataObject<Guid>
{
	public string ButtonStyle { get; set; } = "button-op-success place-self-center";

	/// <summary>
	/// The bucket to submit to. If null,
	/// uses the default bucket.
	/// </summary>
	public Guid? BucketKey { get; set; }

	public List<Guid> TagsToApplyOnSubmit { get; set; } = new();

	/// <summary>
	/// Will this button reset the parent form?
	/// </summary>
	public bool InvokeReset { get; set; } = false;

	/// <summary>
	/// Will this button invoke a save?
	/// </summary>
	public bool InvokeSubmit { get; set; } = false;

	[NoObjectTreeDiscovery]
	public ButtonComponentSettings? ButtonComponentSettings { get; set; }
	public Guid? ButtonComponentSettingsKey { get; set; }
	[NoObjectTreeDiscovery]
	public BucketComponentSettings? BucketComponentSettings { get; set; }
	public Guid? BucketComponentSettingsKey { get; set; }
}
