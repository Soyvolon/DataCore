﻿using ProjectDataCore.Data.Structures.Assignable.Render;
using ProjectDataCore.Data.Structures.Page.Components.Parameters;
using ProjectDataCore.Data.Structures.Page.Components.Parameters.Form;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Page.Data;
public partial class DisplayComponentData : DataObject<Guid>
{
	[GeneratedRegex(@"{{.*?:.*?}}", RegexOptions.IgnoreCase)]
	private static partial Regex MarkupRegex();

	public string AuthorizedRaw { get; set; } = "";
	public string UnAuthorizedRaw { get; set; } = "";

	public MarkupString GetAuthorizedRawMarkup()
		=> GetDisplayMarkup(AuthorizedRaw);
		
	public MarkupString GetUnAuthorizedRawMarkup()
		=> GetDisplayMarkup(UnAuthorizedRaw);

	private MarkupString GetDisplayMarkup(string raw)
	{
		if (DisplayComponentSettings is null)
			return new(raw);

		var matches = MarkupRegex().Matches(raw);
		var keys = matches
			.Cast<Match>()
			.Select(e => (Raw: e.Value, Lower: e.Value.ToLower()))
			.ToList();

		if (keys.Count <= 0)
			return new(raw);

		StringBuilder main = new();
		foreach (var provider in DisplayComponentSettings.ScopeProviders)
		{
			StringBuilder sb = new();
			sb.Append(AuthorizedRaw);

			var scope = provider.ProvidingScope;
			var assignable = scope.GetSingleAssignable();

			if (assignable is null)
				continue;

			foreach (var renderer in DisplayComponentSettings.AssignableValueRenderers)
			{
				if (renderer.ScopeProviderKey != provider.ProvidingScopeId)
					continue;

				var values = renderer.GetValuePairs(assignable);

				foreach (var value in values)
				{
					var (Raw, Lower) = keys
						.Where(e => e.Lower == value.Key)
						.FirstOrDefault();

					if (Raw is null)
						continue;

					sb.Replace(Raw, value.Value.Value);
				}
			}

			main.Append(sb);
		}

		return new(main.ToString());
	}

	public DisplayComponentData GetDisplayCopy()
	{
		var dc = new DisplayComponentData()
		{
			AuthorizedRaw = AuthorizedRaw,
			UnAuthorizedRaw = UnAuthorizedRaw
		};

		dc.DisplayComponentSettings = new()
		{ 
			Data = dc,
			AssignableValueRenderers = DisplayComponentSettings?.AssignableValueRenderers ?? []
		};

		return dc;
	}


    public DisplayComponentSettings? DisplayComponentSettings { get; set; }
	public Guid? DisplayComponentSettingsKey { get; set; }
	[NoObjectTreeDiscovery]
	public BucketComponentSettings? BucketComponentSettings { get; set; }
	public Guid? BucketComponentSettingsKey { get; set; }
	[NoObjectTreeDiscovery]
	public RosterComponentSettings? RosterComponentSettings { get; set; }
	public Guid? RosterComponentSettingsKey { get; set; }
}
