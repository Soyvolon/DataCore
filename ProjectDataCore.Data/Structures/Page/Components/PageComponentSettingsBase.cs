﻿using ProjectDataCore.Data.Structures.Auth;
using ProjectDataCore.Data.Structures.Page.Components.Parameters;
using ProjectDataCore.Data.Structures.Page.Components.Pool;
using ProjectDataCore.Data.Structures.Page.Components.Scope;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Page.Components;

/// <summary>
/// Base settings for all component setting objects.
/// </summary>
public class PageComponentSettingsBase : DataObject<Guid>
{
    /// <summary>
    /// The listing value for the page to render.
    /// </summary>
    public PageComponentListing ListingValue { get; set; }

    /// <summary>
    /// The display name for this component.
    /// </summary>
    public string DisplayName { get; set; } = "Default Component";

    /// <summary>
    /// The parent settings object. May be null if the page reference is not null for a layout component.
    /// </summary>
    public Layout.LayoutNode? ParentNode { get; set; }
    /// <summary>
    /// The key for the <see cref="ParentNode"/>
    /// </summary>
    public Guid? ParentNodeId { get; set; }

    /// <summary>
    /// The area providing authorization for this component.
    /// </summary>
    [NoObjectTreeDiscovery]
    public AuthorizationArea? AuthorizationArea { get; set; }
    public Guid? AuthorizationAreaKey { get; set; }

    /// <summary>
    /// The the containers for the <see cref="AssignableScope"/>s that this component gets data from (with ordering).
    /// </summary>
    public List<AssignableScopeListenerContainer> ScopeProviders { get; set; } = new();
    /// <summary>
    /// The containers for the <see cref="AssignableScope"/>s that this component sends data to (with ordering).
    /// </summary>
    public List<AssignableScopeProviderContainer> ScopeListeners { get; set; } = new();

    /// <summary>
    /// The component pools this page is attached to.
    /// </summary>
    public List<ComponentPool> ComponentPools { get; set; } = new();
    public List<ComponentPoolBinding> ComponentPoolBindings { get; set; } = new();

    private List<AssignableScope>? orderedScopeProviders = null;
    public List<AssignableScope> GetOrderedScopeProviders()
	{
        bool recal = orderedScopeProviders is null;
        
        if (!recal)
		{
            recal = orderedScopeProviders!.Count != ScopeProviders.Count;
		}

        if (recal)
		{
            orderedScopeProviders = ScopeProviders
                .OrderBy(x => x.Order)
                .ToList(x => x.ProvidingScope);
        }

        // One final check.
        orderedScopeProviders ??= new();

        return orderedScopeProviders;
	}

    private List<AssignableScope>? orderedScopeListeners = null;
    public List<AssignableScope> GetOrderedScopeListeners()
    {
        bool recal = orderedScopeListeners is null;

        if (!recal)
        {
            recal = orderedScopeListeners!.Count != ScopeListeners.Count;
        }

        if (recal)
        {
            orderedScopeListeners = ScopeListeners
                .OrderBy(x => x.Order)
                .ToList(x => x.ListeningScope);
        }

        // One final check.
        orderedScopeListeners ??= new();

        return orderedScopeListeners;
    }
}
