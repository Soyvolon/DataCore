﻿using ProjectDataCore.Data.Services.User;
using ProjectDataCore.Data.Structures.Assignable;
using ProjectDataCore.Data.Structures.Assignable.Configuration;
using ProjectDataCore.Data.Structures.Assignable.Value;
using ProjectDataCore.Data.Structures.Form;
using ProjectDataCore.Data.Structures.Page.Components.Layout;
using ProjectDataCore.Data.Structures.Page.Components.Pool.Callback;
using ProjectDataCore.Data.Structures.Page.Components.Pool;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using static System.Formats.Asn1.AsnWriter;

namespace ProjectDataCore.Data.Structures.Page.Components.Scope;
public class AssignableScope : DataObject<Guid>
{
	public delegate void AssignablesChangedEvent(PageComponentSettingsBase sender);
	public event AssignablesChangedEvent? AssignablesChanged;

	public CustomPageSettings Page { get; set; }
    public Guid PageId { get; set; }

    /// <summary>
    /// The object that sets the definition for what values
    /// can be configured.
    /// </summary>
    public DynamicForm? FormScopeDefinition { get; set; }
    /// <summary>
    /// The key for <see cref="FormScopeDefinition"/>.
    /// </summary>
    public Guid? FormScopeDefinitionKey { get; set; }
    /// <summary>
    /// If true, ignore <see cref="FormScopeDefinition"/> and use the
    /// configurations that are applied to every user.
    /// </summary>
    public bool UseUserDefinition { get; set; } = true;

    public List<AssignableScopeProviderContainer> ScopeProviders { get; set; } = new();
    public List<AssignableScopeListenerContainer> ScopeListeners { get; set; } = new();

    public bool IncludeLocalUser { get; set; } = false;
    public string DisplayName { get; set; } = "Unnamed Scope";

    #region Non-Database Parameters
    private HashSet<IAssignable> UsersInScope { get; init; } = new();
    private bool Initialized { get; set; } = false;
    #endregion

    public void Initialize(PageComponentSettingsBase? sender, DataCoreUser? activeUser)
    {
        if (Initialized)
            return;

        Initialized = true;

        if (!UseUserDefinition && FormScopeDefinition is not null)
            RegisterAssignable(sender, FormScopeDefinition.CreateResponse());
        else if (IncludeLocalUser && activeUser is not null)
            RegisterAssignable(sender, activeUser);
    }

    public void RegisterAssignable(PageComponentSettingsBase? sender, IAssignable user)
    {
        UsersInScope.Add(user);

        if (sender is not null)
            AssignablesChanged?.Invoke(sender);
    }

    public bool RemoveAssignable(PageComponentSettingsBase? sender, IAssignable user)
    {
        var res = UsersInScope.Remove(user);

        if (res && sender is not null)
		    AssignablesChanged?.Invoke(sender);

        return res;
	}

    public IAssignable? GetSingleAssignable()
        => UsersInScope.FirstOrDefault();

    public IAssignable[] GetAllAssignables()
        => UsersInScope.ToArray();

    public async Task<IAssignable?> GetConfigurationAssignableAsync(IUserService userService)
    {
        if (!UseUserDefinition)
        {
            var response = FormScopeDefinition?.CreateResponse();
            return response;
        }

        var defaultUser = await userService.GetDefaultUserAsync();
        return defaultUser;
    }

    private List<PageComponentSettingsBase>? orderedProvidingComponents = null;
    public List<PageComponentSettingsBase> GetOrderedProvidingComponents()
    {
        bool recal = orderedProvidingComponents is null;

        if (!recal)
        {
            recal = orderedProvidingComponents!.Count != ScopeProviders.Count;
        }

        if (recal)
        {
            orderedProvidingComponents = ScopeProviders
                .OrderBy(x => x.Order)
                .ToList(x => x.ProvidingComponent);
        }

        // One final check.
        orderedProvidingComponents ??= new();

        return orderedProvidingComponents;
    }

    private List<PageComponentSettingsBase>? orderedListeningComponents = null;
    public List<PageComponentSettingsBase> GetOrderedListeningComponents()
    {
        bool recal = orderedListeningComponents is null;

        if (!recal)
        {
            recal = orderedListeningComponents!.Count != ScopeListeners.Count;
        }

        if (recal)
        {
            orderedListeningComponents = ScopeListeners
                .OrderBy(x => x.Order)
                .ToList(x => x.ListeningComponent);
        }

        // One final check.
        orderedListeningComponents ??= new();

        return orderedListeningComponents;
    }

    public void AttachListener(PageComponentSettingsBase component)
    {
        AssignableScopeListenerContainer container = new()
        {
            ListeningComponent = component,
            ProvidingScope = this,
            Order = ScopeListeners.Count
        };

        this.ScopeListeners.Add(container);
        component.ScopeProviders.Add(container);
    }

    public void AttachProvider(PageComponentSettingsBase component)
    {
        AssignableScopeProviderContainer container = new()
        {
            ProvidingComponent = component,
            ListeningScope = this,
            Order = ScopeListeners.Count
        };

        this.ScopeProviders.Add(container);
        component.ScopeListeners.Add(container);

        UseUserDefinition = true;
    }

    public void AttachForm(DynamicForm form)
    {
        FormScopeDefinition = form;
        FormScopeDefinitionKey = form.Key;

        UseUserDefinition = false;
    }

    public bool DetachSelf()
    {
        bool rm = true;
        for (int i = 0; i < ScopeProviders.Count; i++)
        {
            AssignableScopeProviderContainer? provide = ScopeProviders[i];

            rm = DetachProvider(provide);

            if (!rm)
                return false;
        }

        for (int i = 0; i < ScopeListeners.Count; i++)
        {
            AssignableScopeListenerContainer? listen = ScopeListeners[i];

            rm = DetachListener(listen);

            if (!rm)
                return false;
        }

        return rm;
    }

    public bool DetachProvider(AssignableScopeProviderContainer provider)
    {
        if (!ScopeProviders.Remove(provider))
            return false;

        var comp = provider.ProvidingComponent;
        return comp.ScopeListeners.Remove(provider);
    }

    public bool DetachListener(AssignableScopeListenerContainer listener)
    {
        if (!ScopeListeners.Remove(listener))
            return false;

        var comp = listener.ListeningComponent;
        return comp.ScopeProviders.Remove(listener);
    }
}
