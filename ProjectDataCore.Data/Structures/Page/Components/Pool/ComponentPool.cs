﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectDataCore.Data.Structures.Page.Components.Pool.Callback;

namespace ProjectDataCore.Data.Structures.Page.Components.Pool;
public class ComponentPool : DataObject<Guid>
{
    public delegate void ValuesRequestedEvent(PageComponentSettingsBase sender, Action<ValuesRequestedEventCallbackArgs> callback, 
        ValuesRequestedEventArgs args);
    public event ValuesRequestedEvent? ValuesRequested;

    public delegate void ClearValuesRequestedEvent(PageComponentSettingsBase sender);
    public event ClearValuesRequestedEvent? ClearValuesRequested;

    #region Database
    public string Name { get; set; } = "Unnamed Component Pool";

    public CustomPageSettings PageSettings { get; set; }
    public Guid PageSettingsKey { get; set; }

    public List<PageComponentSettingsBase> Components { get; set; } = new();
    public List<ComponentPoolBinding> ComponentPoolBindings { get; set; } = new();
    #endregion

    public List<ValuesRequestedEventCallbackArgs> RequestValues(PageComponentSettingsBase sender,
        ValuesRequestedEventArgs args)
    {
        List<ValuesRequestedEventCallbackArgs> callbacks = new();
        ValuesRequested?.Invoke(sender, callbacks.Add, args);

        return callbacks;
    }

    public void RequestClearValues(PageComponentSettingsBase sender)
    {
        ClearValuesRequested?.Invoke(sender);
    }
}
