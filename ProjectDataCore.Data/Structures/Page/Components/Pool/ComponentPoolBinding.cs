﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Page.Components.Pool;
public class ComponentPoolBinding
{
    public ComponentPool Pool { get; set; }
    public Guid PoolKey { get; set; }

    public PageComponentSettingsBase Settings { get; set; }
    public Guid SettingsKey { get; set; }
}
