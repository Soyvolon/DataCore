﻿using ProjectDataCore.Data.Structures.Assignable.Value;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Page.Components.Pool.Callback;
public class AssignableValueRequestedCallbackArgs : ValuesRequestedEventCallbackArgs
{
    public BaseAssignableValue? Value { get; set; }
}
