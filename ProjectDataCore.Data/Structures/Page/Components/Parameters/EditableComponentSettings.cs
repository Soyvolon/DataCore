﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Page.Components;

public class EditableComponentSettings : ParameterComponentSettingsBase
{
    public List<RosterDisplaySettings> EditableDisplays { get; set; } = new();
}
