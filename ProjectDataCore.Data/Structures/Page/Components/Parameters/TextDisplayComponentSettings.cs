﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Html;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Page.Components.Parameters;
public class TextDisplayComponentSettings : PageComponentSettingsBase
{
    public string RawContents { get; set; } = "";

    #region Editing Permissions
    public bool PrivateEdit { get; set; } = false;
    #endregion

    #region Non-database Values
    public MarkupString Display => new(RawContents);
    #endregion
}
