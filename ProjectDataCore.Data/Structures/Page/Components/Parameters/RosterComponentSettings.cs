﻿using ProjectDataCore.Data.Structures.Assignable.Configuration;
using ProjectDataCore.Data.Structures.Assignable.Render;
using ProjectDataCore.Data.Structures.Auth;
using ProjectDataCore.Data.Structures.Page.Data;
using ProjectDataCore.Data.Structures.Util.Interfaces;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Page.Components.Parameters;

public class RosterComponentSettings : PageComponentSettingsBase, ISortable
{
    public bool Scoped { get; set; }

    #region Unscoped Settings
    // These settings are always in effect for the roster display.
    /// <summary>
    /// IF true the roster will only show a list of members rather than
    /// an ordered collection of elements for this roster.
    /// </summary>
    /// <remarks>
    /// When this option is true further details can be displayed about a single
    /// user. Furthermore, this allows further sorting and search options to
    /// be conducted on the roster itself.
    /// </remarks>
    public bool AllowUserListing { get; set; }

    #region User Listing
    /// <summary>
    /// A list of properties to display when the roster is in the user
    /// listing mode.
    /// </summary>
    /// <remarks>
    /// Each item in this list becomes its own column in the user listing.
    /// </remarks>
    public List<RosterDisplayColumnOrder> UserListDisplayedProperties { get; set; } = [];
    #endregion

    #region Default Display
    /// <summary>
    /// The configuration for displaying values when they are in the default configuration.
    /// </summary>
	public DisplayComponentData RosterTreeDisplayComponentData { get; set; } = new();
    #endregion
    #endregion

    #region Scoped Settings
    // These settings are only in effect if the Scoped parameter
    // is set to true - otherwise they are ignored.

    /*
     * Scoped settings are used to render a roster based on the
     * logged in user. For example, if the user is in a position
     * that is at a depth of 4 - the roster would move up until
     * it reaches LevelFromTop. Then, it would render from
     * LevelFromTop to Depth. So, if you wanted a roster that
     * only showed the command staff, you could set LevelFromTop
     * to 1 and the Depth to 1. If you wanted to show the entire users
     * tree from a point, you can set Depth to -1.
     * 
     * -- Aria Bounds (2024-02-11)
     */

    /// <summary>
    /// The level from the top most roster part to start the display at,
    /// for the scoped user.
    /// </summary>
    /// <remarks>
    /// The top level portion of the Roster is 0. If the user does not have
    /// a display available because they are either not on the roster, or 
    /// their scope is too high for the level from top provided, no 
    /// roster information will be displayed.
    /// </remarks>
    public int LevelFromTop { get; set; }
    /// <summary>
    /// The max depth of the roster to display along the scoped path.
    /// </summary>
    /// <remarks>
    /// When set to -1, the roster will show everything that is 
    /// underneath <see cref="LevelFromTop"/>.
    /// </remarks>
    public int Depth { get; set; }
    #endregion

    #region Editing
    /// <summary>
    /// If true, disables the roster slot edit buttons.
    /// </summary>
    public bool DisableEditing { get; set; } = false;
    /// <summary>
    /// If set, this auth area gives anyone who passes it's check editing
    /// permissions on the roster regardless of if tag based edit checks succeed.
    /// </summary>
    [NoObjectTreeDiscovery]
    public AuthorizationArea? EditingAuthArea { get; set; }
    /// <summary>
    /// The key for <see cref="EditingAuthArea"/>.
    /// </summary>
    public Guid? EditingAuthAreaKey { get; set; }
    #endregion

    /// <summary>
    /// The roster to display.
    /// </summary>
    public Guid RosterToDisplay { get; set; }

    public void RecalculateOrder(int oldIndex, int newIndex)
    {
        if (newIndex < 0)
            return;

        var old = UserListDisplayedProperties.ElementAtOrDefault(oldIndex);
        if (old is null)
            return;

        UserListDisplayedProperties.RemoveAt(oldIndex);

        if (newIndex > UserListDisplayedProperties.Count)
            UserListDisplayedProperties.Add(old);
        else UserListDisplayedProperties.Insert(newIndex, old);

        RecalculateOrder();
    }

    public void RecalculateOrder()
    {
        for (int i = 0; i < UserListDisplayedProperties.Count; i++)
            UserListDisplayedProperties[i].Order = i;
    }
}
