﻿using ProjectDataCore.Data.Structures.Assignable;
using ProjectDataCore.Data.Structures.Assignable.Configuration;
using ProjectDataCore.Data.Structures.Assignable.Settings;
using ProjectDataCore.Data.Structures.Form;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Page.Components.Parameters.Form;
public class FormComponentSettings : PageComponentSettingsBase
{
    public Guid ProvidingScopeId { get; set; }
    
    [NoObjectTreeDiscovery]
    public BaseAssignableConfiguration? AssignableConfiguration { get; set; }
    public Guid? AssignableConfigurationKey { get; set; }

    /// <summary>
    /// The label to display when editing this assignable configuration, or
    /// the label on a button if the component is a button.
    /// </summary>
    public string DataLabel { get; set; } = "";

    /// <summary>
    /// The settings for linked assignable inputs.
    /// </summary>
    public LinkedAssignableInputSettings? LinkedAssignableInputSettings { get; set; }
    public Guid? LinkedAssignableInputSettingsKey { get; set; }
}
