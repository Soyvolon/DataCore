﻿using ProjectDataCore.Data.Structures.Form;
using ProjectDataCore.Data.Structures.Page.Data;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Page.Components.Parameters.Form;

/// <summary>
/// Holds information about buttons.
/// </summary>
public class ButtonComponentSettings : FormComponentSettings
{
	public required ButtonComponentData Data { get; set; } = new();
}
