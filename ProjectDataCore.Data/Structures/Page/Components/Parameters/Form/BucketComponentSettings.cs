﻿using Microsoft.AspNetCore.Components.Web;

using ProjectDataCore.Data.Structures.Auth;
using ProjectDataCore.Data.Structures.Page.Data;
using ProjectDataCore.Data.Structures.Util.Interfaces;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Page.Components.Parameters.Form;
public class BucketComponentSettings : FormComponentSettings, ISortable
{
	public List<ButtonComponentData> Actions { get; set; } = new();

	public bool AllowTagEditing { get; set; } = false;
	public bool AllowDelete { get; set; } = false;
	public bool AllowMove { get; set; } = false;

	public int MaxItemsPerPage { get; set; } = 10;

	public required DisplayComponentData ItemSummaryDisplayConfig { get; set; }

	/// <summary>
	/// The buckets that can be displayed with this component.
	/// </summary>
	public Guid Bucket { get; set; } = new();

    /// <summary>
    /// The order of items to display when an element is selected
    /// for an in-depth view.
    /// </summary>
	public List<BucketValueDisplayOrder> DisplayOrders { get; set; } = new();

    public void RecalculateOrder(int oldIndex, int newIndex)
    {
        if (newIndex < 0)
            return;

        var old = DisplayOrders.ElementAtOrDefault(oldIndex);
        if (old is null)
            return;

        DisplayOrders.RemoveAt(oldIndex);

        if (newIndex > DisplayOrders.Count)
            DisplayOrders.Add(old);
        else DisplayOrders.Insert(newIndex, old);

        RecalculateOrder();
    }

    public void RecalculateOrder()
    {
        for (int i = 0; i < DisplayOrders.Count; i++)
            DisplayOrders[i].Order = i;
    }
}
