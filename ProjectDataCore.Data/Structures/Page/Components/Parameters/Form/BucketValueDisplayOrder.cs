﻿using ProjectDataCore.Data.Structures.Assignable.Configuration;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Page.Components.Parameters.Form;

#nullable disable
public class BucketValueDisplayOrder : DataObject<Guid>
{
    [NoObjectTreeDiscovery]
    public BucketComponentSettings Settings { get; set; }
    public required Guid SettingsKey { get; set; }

    [NoObjectTreeDiscovery]
    public required BaseAssignableConfiguration AssignableConfiguration { get; set; }
    public required Guid ConfigurationKey { get; set; }

    public int Order { get; set; } = -1;
}
#nullable enable
