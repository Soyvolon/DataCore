﻿
using ProjectDataCore.Data.Structures.Assignable.Render;
using ProjectDataCore.Data.Structures.Page.Data;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Page.Components.Parameters;

/// <summary>
/// Displays a data values.
/// </summary>
public partial class DisplayComponentSettings : ParameterComponentSettingsBase
{
    public required DisplayComponentData Data { get; set; }
}
