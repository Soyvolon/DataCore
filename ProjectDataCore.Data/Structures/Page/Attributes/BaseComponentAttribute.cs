﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Page.Attributes;

[AttributeUsage(AttributeTargets.Class)]
public class BaseComponentAttribute : Attribute
{
	public PageComponentListing PageComponentListing { get; set; }

	public BaseComponentAttribute(PageComponentListing pageComponentListing)
		: base()
	{
		PageComponentListing = pageComponentListing;
	}
}
