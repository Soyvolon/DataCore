﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Page.Attributes;

[AttributeUsage(AttributeTargets.Class)]
public class StaticPageAttribute : BaseComponentAttribute
{
    public string Name { get; set; } = "";
    public string Route { get; set; } = "";

    public StaticPageAttribute(PageComponentListing listing)
        : base(listing)
    {

    }
}
