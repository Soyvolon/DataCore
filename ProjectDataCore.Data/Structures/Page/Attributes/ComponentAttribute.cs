﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Page.Attributes;

[AttributeUsage(AttributeTargets.Class)]
public class ComponentAttribute : BaseComponentAttribute
{
    public string Name { get; set; } = "";
    public string ShortName { get; set; } = "";
    public string Category { get; set; } = "";
    public string IconPath { get; set; } = "~/svg/mat-icons/add.svg";

    public Type ComponentSettingsType { get; set; } = typeof(PageComponentSettingsBase);

    public ComponentAttribute(Type componentSettingsType, PageComponentListing listing)
        : base(listing) 
    {
        ComponentSettingsType = componentSettingsType;
    }
}