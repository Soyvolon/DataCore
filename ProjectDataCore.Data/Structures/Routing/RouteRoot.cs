﻿using ProjectDataCore.Data.Structures.Auth;
using ProjectDataCore.Data.Structures.Page;
using ProjectDataCore.Data.Structures.Util;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Routing;
public class RouteRoot : DataObject<Guid>
{
    /// <summary>
    /// The starting route for this section of routes.
    /// </summary>
    public string Route { get; set; } = "";

    /// <summary>
    /// The full route for this section of routes, including the roots
    /// of all parent routes.
    /// </summary>
    public string FullRoute { get; private set; } = "";

    /// <summary>
    /// The default route. Aka '/'
    /// </summary>
    public bool DefaultRoute { get; internal set; } = false;

    /// <summary>
    /// The page attached to this route.
    /// </summary>
    public CustomPageSettings? Page { get; set; }

    /// <summary>
    /// The authorization provider for this route.
    /// </summary>
    public AuthorizationArea? AuthorizationArea { get; set; }
    public Guid? AuthorizationAreaKey { get; set; }

    /// <summary>
    /// The authorization provider for who can edit this route
    /// root.
    /// </summary>
    public AuthorizationArea? CanEditAuthorizationArea { get; set; }
    public Guid? CanEditAuthorizationAreaKey { get; set; }

    /// <summary>
    /// The list of Auth Area Keys that needs to be checked
    /// when a user navigates to this page.
    /// </summary>
    /// <remarks>
    /// A route provides these values <em>in no specific order</em>
    /// </remarks>
    public List<Guid> OnNavigationChecks { get; set; } = new();

    /// <summary>
    /// If set to false, this route is not editable (nor subpages of this route) 
    /// beyond chaning access permissions.
    /// </summary>
    /// <remarks>
    /// As this flag is mostly pointless for a user generated page, there is no option to
    /// set it by the user. You mainly see this flag in static generated pages.
    /// </remarks>
    public bool Editable { get; set; }

    public List<RouteRoot> ChildRoots { get; set; } = new();

    public RouteRoot? ParentRoot { get; set; }
    public Guid? ParentRootKey { get; set; }

    public void CreatePage()
    {
        Page = new()
        {
            Layout = new()
        };
    }

    internal void UpdateFullRoute(ApplicationDbContext context)
    {
        context.Entry(this)?.Reference(e => e.ParentRoot).Load();

        ParentRoot?.UpdateFullRoute(context);

        if (DefaultRoute)
            FullRoute = "/";
        else if (ParentRoot?.DefaultRoute ?? false)
            FullRoute = '/' + Route;
        else FullRoute = ParentRoot?.FullRoute + '/' + Route;
    }

    internal void UpdateOnNavigateChecks(ApplicationDbContext context)
    {
        OnNavigationChecks.Clear();

        if (AuthorizationAreaKey is not null)
            OnNavigationChecks.Add(AuthorizationAreaKey.Value);

		context.Entry(this)?.Reference(e => e.ParentRoot).Load();

        if (ParentRoot is not null)
        {
            ParentRoot.UpdateOnNavigateChecks(context);

            OnNavigationChecks.AddRange(ParentRoot.OnNavigationChecks);
        }
	}
}
