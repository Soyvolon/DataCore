﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Routing;

public class RouteRootTree
{
    public RouteRoot? Root { get; set; }
    public SortedList<string, RouteRootTree> ChildRoots { get; set; } = new();

    public void AddRoot(RouteRoot route)
        => AddRoot(route, null);

    private void AddRoot(RouteRoot root, string? route)
    {
        route ??= root.FullRoute;

        if (route == "/")
        {
            Root = root;
            return;
        }

        var part = route[(route.IndexOf("/") + 1)..];
        bool hasNextArea = part.Contains('/');
        if (hasNextArea)
            part = part[..part.IndexOf("/")];

        if (!ChildRoots.ContainsKey(part))
            ChildRoots.Add(part, new());

        string next;
        if (hasNextArea)
            next = route[(part.Length + 1)..];
        else next = "/";

        ChildRoots[part].AddRoot(root, next);
    }

    public Func<RouteRoot, Task>? OnEditRouteRoot { get; set; }
}
