﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Util;

public static class DictionaryExtensions
{
    public static Dictionary<TKey, TValue> ShallowCopy<TKey, TValue>(this Dictionary<TKey, TValue> source) where TKey : notnull
    {
        var copy = new Dictionary<TKey, TValue>();
        foreach(var pair in source)
        {
            copy.Add(pair.Key, pair.Value);
        }

        return copy;
    }

    public static bool TryAddSet<T>(
        this Dictionary<Type, Dictionary<T, object>> source, 
        Type t, T key, object value)
        where T : unmanaged, IComparable, IEquatable<T>
    {
        if (!source.TryGetValue(t, out var dict))
        {
            dict = new();
            source.Add(t, dict);
        }

        return dict.TryAdd(key, value);
    }
}
