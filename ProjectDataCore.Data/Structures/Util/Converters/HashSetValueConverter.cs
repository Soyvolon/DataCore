﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Util.Converters;
public class HashSetValueConverter<T> : ValueConverter<HashSet<T>, List<T>>
{
    public HashSetValueConverter()
        : base(
            v => v.ToList(),
            v => new(v)
        ) { }
}
