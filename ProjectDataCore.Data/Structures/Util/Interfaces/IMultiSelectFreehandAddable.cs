﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Util.Interfaces;
public interface IMultiSelectFreehandAddable
{
    public Task ConfigureAsync(string value);
}
