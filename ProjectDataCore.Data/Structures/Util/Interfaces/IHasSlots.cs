﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Util.Interfaces;
public interface IHasSlots
{
    public IEnumerable<RosterSlot> GetSlots();
}
