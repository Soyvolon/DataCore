﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Util.List;
public class MultiListSelectorSingleValueWrapper<T> : IList<T>
{
    private List<T> Values { get; init; } = new List<T>();
    private Func<T?> OnGet { get; init; }
    private Action<T?> OnSet { get; init; }

    public T this[int index] { get => Values[index]; set => Values[index] = value; }
    public int Count => Values.Count;
    public bool IsReadOnly => false;

    public MultiListSelectorSingleValueWrapper(Func<T?> get, Action<T?> set)
    {
        OnGet = get;
        OnSet = set;

        var cur = OnGet();
        if (cur is not null)
            Values.Add(cur);
    }

    public void Add(T item)
    {
        Clear();

        Values.Add(item);

        OnSet(item);
    }

    public void Clear()
    {
        Values.Clear();
        OnSet(default);
    }

    public bool Contains(T item)
        => Values.Contains(item);

    public void CopyTo(T[] array, int arrayIndex)
        => Values.CopyTo(array, arrayIndex);

    public IEnumerator<T> GetEnumerator()
        => Values.GetEnumerator();

    public int IndexOf(T item)
        => Values.IndexOf(item);

    public void Insert(int index, T item)
        => Add(item);

    public bool Remove(T item)
    {
        if (OnGet()?.Equals(item) ?? false)
        {
            Clear();
            return true;
        }

        return false;
    }

    public void RemoveAt(int index)
        => Remove(Values.ElementAtOrDefault(index)!);

    IEnumerator IEnumerable.GetEnumerator()
        => Values.GetEnumerator();
}
