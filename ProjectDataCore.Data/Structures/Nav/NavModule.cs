﻿using ProjectDataCore.Data.Structures.Page;
using System.ComponentModel.DataAnnotations.Schema;
using ProjectDataCore.Data.Structures.Auth;

namespace ProjectDataCore.Data.Structures.Nav;

/// <summary>
/// Used to represent modules for the top navigation bar
/// </summary>
public class NavModule : DataObject<Guid>
{
    public string DisplayName { get; set; }
    public string? Href { get; set; }
    public List<NavModule> SubModules { get; set; } = new();
    public bool HasMainPage { get; set; } = false;
    public NavModule? Parent { get; set; }
    public Guid? ParentId { get; set; }
    public CustomPageSettings? Page { get; set; }
    public Guid? PageId { get; set; }
    public AuthorizationArea? Auth { get; set; }
    public Guid? AuthKey { get; set; }
    public bool DifAuth { get; set; } = false;
    public bool StaticPage { get; set; } = false;
    public bool LinkToPage { get; set; } = false;
    public int Order { get; set; } = -1;
    

    // It gets angry when I dont have a default constructor, I dont know why -Thunder
    public NavModule(){}
    
    public NavModule(CustomPageSettings settings)
    {
        DisplayName = settings.Name!;
        HasMainPage = true;
        PageId = settings.Key;
        LinkToPage = true;
    }

    public NavModule(CustomPageSettings settings, Guid parentId)
    {
        ParentId = parentId;
        DisplayName = settings.Name!;
        HasMainPage = true;
        PageId = settings.Key;
        LinkToPage = true;
    }
    
    public NavModule(CustomPageSettings settings, NavModule parent)
    {
        ParentId = parent.Key;
        DisplayName = settings.Name!;
        HasMainPage = true;
        PageId = settings.Key;
        LinkToPage = true;
        Order = parent.GetSubModuleOrder();
    }

    public AuthorizationArea? GetAuth()
    {
        return DifAuth ? Auth : Page?.AuthorizationArea;
    }

    public string GetHref()
    {
        if (LinkToPage && Page?.RouteRoot != null)
        {
            return Page.RouteRoot.FullRoute;
        }

        return Href ?? "/";
    }

    private int GetSubModuleOrder()
    {
        return SubModules.Count == 0 ? 0 : SubModules.Select(e => e.Order).Max() + 1;
    }
}