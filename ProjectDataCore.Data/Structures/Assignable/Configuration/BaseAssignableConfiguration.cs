﻿using ProjectDataCore.Data.Structures.Account;
using ProjectDataCore.Data.Structures.Assignable.Value;
using ProjectDataCore.Data.Structures.Form;
using ProjectDataCore.Data.Structures.Form.Bucket;
using ProjectDataCore.Data.Structures.Form.Bucket.Action;
using ProjectDataCore.Data.Structures.Page.Components.Parameters.Form;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ProjectDataCore.Data.Structures.Assignable.Configuration;

public class BaseAssignableConfiguration : DataObject<Guid>
{
    public enum InputType
    {
        /// <summary>
        /// Only allow preset values to be selected.
        /// </summary>
        [Description("Static Only")]
        StaticOnly = -1,
        /// <summary>
        /// Allow both preset values and custom input.
        /// </summary>
        [Description("Both")]
        Both = 0,
        /// <summary>
        /// Only allow custom input.
        /// </summary>
        [Description("Freehand Only")]
        FreehandOnly = 1,
    }

    public enum InternalAssignableType
    {
        /// <summary>
        /// Designates a property for an <see cref="Account.DataCoreUser"/>
        /// </summary>
        UserProperty = 0,
        /// <summary>
        /// Designates a property for a <see cref="DynamicForm"/>
        /// </summary>
        FormProperty = 1,
    }

    /// <summary>
    /// The internal type for database operations based on pulling bulk assignable values of a specific type.
    /// </summary>
    public InternalAssignableType AssignableType { get; set; } = InternalAssignableType.UserProperty;
    /// <summary>
    /// The name of the property to be selected. Must be unique.
    /// </summary>
    public string PropertyName { get; set; }
    /// <summary>
    /// A normalized version of the property name.
    /// </summary>
    public string NormalizedPropertyName { get; set; }
    /// <summary>
    /// The name of the type this configuration support. This value is assigned at creation
    /// and should not be changed.
    /// </summary>
    public string TypeName { get; set; }
    /// <summary>
    /// If this property allows for multiple of the values to be selected.
    /// </summary>
    public bool AllowMultiple { get; set; } = false;
    /// <summary>
    /// Determines the type of inputs allowed for this property.
    /// </summary>
    public InputType AllowedInput { get; set; } = InputType.StaticOnly;
    /// <summary>
    /// The list of values that use this configuration.
    /// </summary>
    public List<BaseAssignableValue> AssignableValues { get; set; } = [];

    /// <summary>
    /// The form this configuration belongs to.
    /// </summary>
    /// <remarks>
    /// This must be populated if <see cref="AssignableType"/> is
    /// <see cref="InternalAssignableType.FormProperty"/>
    /// </remarks>
    public DynamicForm? Form { get; set; }
    /// <summary>
    /// The key for the <see cref="Form"/>.
    /// </summary>
    public Guid? FormKey { get; set; }

    /// <summary>
    /// A list of form components this config is bound to.
    /// </summary>
    public List<FormComponentSettings> FormComponents { get; set; } = [];

    public List<AssignToConfiguration> ValueToAssignToAction { get; set; } = [];
    public List<BaseAssignableConfigAssignToConfigurationBinding> ValueToAssignToActionActionBindings { get; set; } = [];
    public List<BucketValueDisplayOrder> BucketValueDisplayOrders { get; set; } = [];
    public List<RosterDisplayColumnOrder> RosterDisplayColumnOrders { get; set; } = [];

    public static List<(Type type, string name)> GetConfigurationTypes(bool userTypesOnly)
    {
		var asbly = Assembly.GetAssembly(typeof(BaseAssignableConfiguration));

        List<(Type type, string name)> configurationTypes = [];
		if (asbly is not null)
		{
			var types = asbly.GetTypes()
				.Where(x => {
                    var attr = x.GetCustomAttribute<AssignableConfigurationAttribute>();
                    if (attr is not null)
                    {
                        return !(userTypesOnly && attr.NotForUsers);
                    }

                    return false;
                });

			foreach (var t in types)
			{
				var dets = t.GetCustomAttribute<AssignableConfigurationAttribute>();

				configurationTypes.Add((t, dets!.Name));
			}
		}

        return configurationTypes;
	}

    public virtual Type GetValueType()
    {
        return GetType();
    }

    internal virtual void Configure(ModelBuilder builder)
    {

    }

    public Type GetAssignableValueType()
    {
        var type = GetType();
        var assignableType = type
            ?.GetCustomAttribute<AssignableConfigurationAttribute>()
            ?.Configures ?? typeof(BaseAssignableValue);

        return assignableType;
    }

    public BaseAssignableValue? CreateInstance()
    {
        var toActivate = GetAssignableValueType();
        var instance = (BaseAssignableValue?)Activator.CreateInstance(toActivate);

        if (instance is null)
            return null;

        instance.AssignableConfiguration = this;
        instance.AssignableConfigurationId = Key;

        return instance;
    }

    public string GetPropConfigDisplay()
        => $"{PropertyName} ({TypeName})";

    public BaseAssignableValue? GetAssignableValue(IAssignable assignable)
        => assignable.GetAssignablePropertyContainer(PropertyName);
}
