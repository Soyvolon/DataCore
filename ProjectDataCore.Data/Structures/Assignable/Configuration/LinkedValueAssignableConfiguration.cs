﻿using ProjectDataCore.Data.Structures.Assignable.Value;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Assignable.Configuration;

[AssignableConfiguration("User", typeof(DataCoreUserAssignableValue), NotForUsers = true)]
public class DataCoreUserValueAssignableConfiguration 
    : LinkedValueBaseAssignableConfiguration<DataCoreUser, Guid> 
        //DataCoreUserValueAssignableConfiguration, DataCoreUserAssignableValue>
{
    internal override void Configure(ModelBuilder builder)
    {
        base.Configure(builder);

        //var obj = builder.Entity<DataCoreUserAssignableValue>();
        //obj.HasOne(e => e.LinkedValue)
        //    .WithMany()
        //    .HasForeignKey(e => e.LinkedKey);
    }
}

[AssignableConfiguration("Slot", typeof(RosterSlotAssignableValue), NotForUsers = true)]
public class RosterSlotValueAssignableConfiguration 
    : LinkedValueBaseAssignableConfiguration<RosterSlot, Guid>
        //RosterSlotValueAssignableConfiguration, RosterSlotAssignableValue>
{
    internal override void Configure(ModelBuilder builder)
    {
        base.Configure(builder);

        //var obj = builder.Entity<RosterSlotAssignableValue>();
        //obj.HasOne(e => e.LinkedValue)
        //    .WithMany()
        //    .HasForeignKey(e => e.LinkedKey);
    }
}
