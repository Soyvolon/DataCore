﻿using ProjectDataCore.Data.Structures.Assignable.Value;
using ProjectDataCore.Data.Structures.Util.Interfaces;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Assignable.Configuration;

public class ValueBaseAssignableConfiguration<T> : BaseAssignableConfiguration, IAssignableConfiguration<T>
    where T : IComparable
{
    public List<T> AllowedValues { get; set; } = new();

    public void AddElement(object o)
    {
        AllowedValues.Add((T)o);
    }

    public void MoveElement(int currentIndex, int newIndex)
    {
        var cur = AllowedValues[currentIndex];
        AllowedValues.RemoveAt(currentIndex);
        AllowedValues.Insert(newIndex, cur);
    }

    public void RemoveElement(int index)
    {
        AllowedValues.RemoveAt(index);
    }

    public override Type GetValueType()
    {
        return typeof(T);
    }

    internal override void Configure(ModelBuilder builder)
    {
        base.Configure(builder);

        var valueType = GetAssignableValueType();
        var propName = nameof(IAssignableValue<T>.SetValue);

        var obj = builder.Entity(valueType);
        obj.Property(propName)
            .HasColumnName($"{valueType.Name}_{propName}");

        var cfgType = GetType();
        propName = nameof(AllowedValues);

        var cfg = builder.Entity(cfgType);
        cfg.Property(propName)
            .HasColumnName($"{cfgType.Name}_{propName}");
    }
}

public class LinkedValueBaseAssignableConfiguration<TValue, TKey>
    : BaseAssignableConfiguration, ILinkedAssignableConfiguration<TValue, TKey>
    where TKey : unmanaged, IComparable, IEquatable<TKey>
    where TValue : class, IKeyable<TKey>
{
    public bool? LinkFlag { get; set; } = true;

    public override Type GetValueType()
    {
        return typeof(TValue);
    }

    internal override void Configure(ModelBuilder builder)
    {
        base.Configure(builder);
        
        var valueType = GetAssignableValueType();
        var propName = nameof(ILinkedAssignableValue<TValue, TKey>.LinkedValue);
        var keyName = nameof(ILinkedAssignableValue<TValue, TKey>.LinkedKey);

        var obj = builder.Entity(valueType);

        obj.HasOne(propName)
            .WithMany()
            .HasForeignKey(keyName);

        var cfgType = GetType();
        var flagName = nameof(LinkFlag);

        var cfg = builder.Entity(cfgType);
        cfg.Property(flagName)
            .HasColumnName($"{cfgType.Name}_{flagName}");
     }
}
