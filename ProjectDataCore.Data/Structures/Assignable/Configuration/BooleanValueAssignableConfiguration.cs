﻿using ProjectDataCore.Data.Structures.Assignable.Value;

namespace ProjectDataCore.Data.Structures.Assignable.Configuration;

[AssignableConfiguration("Boolean", typeof(BooleanAssignableValue))]
public class BooleanValueAssignableConfiguration : ValueBaseAssignableConfiguration<bool>
{
    internal override void Configure(ModelBuilder builder)
    {
        base.Configure(builder);
        
        var obj = builder.Entity<BooleanAssignableValue>();
        obj.Property(e => e.SetValue)
            .HasColumnName($"{nameof(BooleanAssignableValue)}_{nameof(BooleanAssignableValue.SetValue)}");

        var cfg = builder.Entity<BooleanValueAssignableConfiguration>();
        cfg.Property(e => e.AllowedValues)
            .HasColumnName(
            $"{nameof(BooleanValueAssignableConfiguration)}_{nameof(AllowedValues)}");
    }
}