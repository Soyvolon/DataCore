﻿using ProjectDataCore.Data.Structures.Auth;
using ProjectDataCore.Data.Structures.Auth.Tags;
using ProjectDataCore.Data.Structures.Page.Components.Parameters.Form;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Assignable.Settings;
public class LinkedAssignableInputSettings : DataObject<Guid>
{
	[NoObjectTreeDiscovery]
	public AuthorizationArea? DataAuthorizationArea { get; set; }
	public Guid? DataAuthorizationAreaKey { get; set; }

	public List<FormComponentSettings> FormComponents { get; set; } = new();

	// These are radio options - only one should be true at any time.
	// In the event more than one is true, all filters will be applied.
    #region Radios
    /// <summary>
    /// If true, show only values that are within the roster tree of the
    /// user - aka, those who are in the same level as the current user.
    /// </summary>
    public bool OnlyLocalUsersRosterTree { get; set; } = false;
	/// <summary>
	/// If true, show only values that are in or below the roster tree of the
	/// user - aka, those who are in the same level or lower than the current user.
	/// </summary>
	public bool OnlyLocalUsersRosterTreeAndChildTrees { get; set; } = false;
	/// <summary>
	/// If true, show only values that are below the roster tree of the
	/// user - aka, those who are lower than the current user.
	/// </summary>
	public bool OnlyLocalUsersRosterChildren { get; set; } = false;
	#endregion

	/// <summary>
	/// Configures the Any/All behavior for <see cref="IncludeTags"/>.
	/// </summary>
	public bool IncludeIfObjectHasAnyTag { get; set; } = false;
    /// <summary>
    /// Only objects with any/all of these tags will be included in the results.
    /// </summary>
    /// <remarks>
    /// The Any/All tags behavior is configured by <see cref="IncludeIfObjectHasAnyTag"/>.
    /// </remarks>
    [NoObjectTreeDiscovery]
    public List<AuthorizationTag> IncludeTags { get; set; } = new();
	public List<AuthorizationTagLinkedAssignableInputSettingsBinding> IncludeTagBindings { get; set; } = new();

    /// <summary>
    /// Configures the Any/All behavior for <see cref="ExcludeTags"/>.
    /// </summary>
    public bool ExcludeIfObjectHasAnyTag { get; set; } = true;
    /// <summary>
    /// Objects with any/all of these tags will be excluded from the results.
    /// </summary>
	/// <remarks>
	/// The Any/All tags behavior is configured by <see cref="ExcludeIfObjectHasAnyTag"/>.
	/// </remarks>
    [NoObjectTreeDiscovery]
    public List<AuthorizationTag> ExcludeTags { get; set; } = new();
    public List<AuthorizationTagLinkedAssignableInputSettingsBinding> ExcludeTagBindings { get; set; } = new();
}
