﻿using ProjectDataCore.Data.Structures.Assignable.Value;
using ProjectDataCore.Data.Structures.Page;
using ProjectDataCore.Data.Structures.Page.Components.Parameters;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Assignable.Render;
public class AssignableValueRenderer : DataObject<Guid>
{
    #region FKs
    /// <summary>
    /// The base component that this renderer is attached to.
    /// </summary>
    public ParameterComponentSettingsBase? ParameterComponentSettings { get; set; }
    /// <summary>
    /// The ID of the <see cref="ParameterComponentSettings"/>.
    /// </summary>
    public Guid? ParameterComponentSettingsId { get; set; }

    /// <summary>
    /// The base roster component this renderer is attached to.
    /// </summary>
    public RosterComponentSettings? RosterComponentSettings_UserList { get; set; }
    /// <summary>
    /// The ID of the <see cref="RosterComponentSettingsId_UserList"/>
    /// </summary>
    public Guid? RosterComponentSettingsId_UserList { get; set; }
    #endregion

    /// <summary>
    /// The display name for this <see cref="AssignableValueRenderer"/> object.
    /// </summary>
    public string DisplayName { get; set; } = "New Renderer";

    public Guid ScopeProviderKey { get; set; } = default;
    /// <summary>
    /// The name of the assignable value or static property to access.
    /// </summary>
    public string PropertyName { get; set; }
    /// <summary>
    /// If <b>true</b>, the <see cref="PropertyName"/> references a static property.
    /// </summary>
    public bool Static { get; set; }

    /// <summary>
    /// The conversion objects for this assignable value.
    /// </summary>
    public List<AssignableValueConversion> Conversions { get; set; } = new();

    /// <summary>
    /// Gets the value pairs for this assignable value.
    /// </summary>
    /// <param name="assignable">The user object to retrieve values from.</param>
    /// <returns>A dictionary with string keys that can be matched to tokens in a string and
    /// values of <see cref="AssignableValueRenderData"/> that contain the value information
    /// for the configured conversion.</returns>
    public Dictionary<string, AssignableValueRenderData> GetValuePairs(IAssignable assignable)
    {
        BaseAssignableValue? value;
        if(Static)
        {
            value = assignable.GetStaticPropertyContainer(PropertyName);
        }
        else
        {
            value = assignable.GetAssignablePropertyContainer(PropertyName);
        }

        var res = new Dictionary<string, AssignableValueRenderData>();
        if (value is null)
            return res;

        foreach(var conv in Conversions)
        {
            var name = $"{DisplayName}:{conv.ValueName}".ToLower();

            var convRes = conv.FormatAssignable(value);

            var fullData = new AssignableValueRenderData(name, null, convRes.Item1);
            _ = res.TryAdd(fullData.Composite, fullData);

            int i = 1;
            foreach (string single in convRes.Item2)
            {
                var dataPart = new AssignableValueRenderData(name, i++, single);
                _ = res.TryAdd(dataPart.Composite, dataPart);
            }
        }

        return res;
    }
}
