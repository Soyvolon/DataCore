﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Assignable;
public interface IUserAssignable
{
	public void AssignUser(DataCoreUser user);
}
