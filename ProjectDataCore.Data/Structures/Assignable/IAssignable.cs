﻿using ProjectDataCore.Data.Structures.Assignable.Configuration;
using ProjectDataCore.Data.Structures.Assignable.Value;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Assignable;

/// <summary>
/// Implement this to provide assignable value support to a class.
/// </summary>
public interface IAssignable
{
	public List<BaseAssignableValue> AssignableValues { get; set; }
	public BaseAssignableConfiguration.InternalAssignableType GetInternalAssignableType();
}

public static class IAssignableExtensions
{
	public static BaseAssignableValue? GetStaticPropertyContainer(this IAssignable assignable, string property)
	{
		var typ = assignable.GetType();
		var p = typ.GetProperty(property);
		object? val = null;
		if (p is not null)
		{
			val = p.GetValue(assignable);

		}

		return val switch
		{
			int a => new IntegerAssignableValue()
			{
				SetValue = new()
					{
						a
					}
			},
			double d => new DoubleAssignableValue()
			{
				SetValue = new()
					{
						d
					}
			},

			string s => new StringAssignableValue()
			{
				SetValue = new()
					{
						s
					}
			},

			bool b => new BooleanAssignableValue()
			{
				SetValue = new()
					{
						b
					}
			},

			DateTime dt => new DateTimeAssignableValue()
			{
				SetValue = new()
					{
						dt
					}
			},
			TimeOnly tonly => new TimeOnlyAssignableValue()
			{
				SetValue = new()
					{
						tonly
					}
			},
			DateOnly donly => new DateOnlyAssignableValue()
			{
				SetValue = new()
					{
						donly
					}
			},

			_ => null,
		};
	}

	public static BaseAssignableValue? GetAssignablePropertyContainer(this IAssignable assignable, string property)
	{
		var nprop = property.Normalize();
		var data = assignable.AssignableValues
			.Where(x => x.AssignableConfiguration.NormalizedPropertyName == nprop)
			.FirstOrDefault();

		return data;
	}
}