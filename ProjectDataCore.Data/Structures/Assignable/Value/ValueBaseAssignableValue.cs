﻿using ProjectDataCore.Data.Structures.Util.Interfaces;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Assignable.Value;

public class ValueBaseAssignableValue<T> : BaseAssignableValue, IAssignableValue<T>
    where T : IComparable
{
    /// <summary>
    /// The currently set value.
    /// </summary>
    public List<T> SetValue { get; set; } = new();

    public override IComparable? GetValue()
    {
        return SetValue.FirstOrDefault();
    }

    public override List<IComparable?> GetValues()
    {
        return SetValue.ToList(x => (IComparable?)x);
    }

    public override void ReplaceValue(IComparable value, int? index = null)
    {
        if(index is null)
        {
            // equivalent of the set value return.
            SetValue = ((List<IComparable>)value).ToList(x => (T)x);
        }
        else if (index > SetValue.Count - 1)
        {
            AddValue(value);
        }
        else
        {
            SetValue[index.Value] = (T)value;
        }
    }

    public override void AddValue(IComparable value)
    {
        if (SetValue.Count == 0
            || AssignableConfiguration.AllowMultiple)
            SetValue.Add((T)value);
        else SetValue[0] = (T)value;
    }

    public override void ConvertAndReplaceValue(IComparable value, int? index = null)
    {
        var newVal = Convert.ChangeType(value, typeof(T)) as IComparable;
        ReplaceValue(newVal!, index);
    }

    public override void ConvertAndAddValue(IComparable value)
    {
        var newVal = Convert.ChangeType(value, typeof(T)) as IComparable;
        AddValue(newVal!);
    }

    public override void ClearValue()
        => SetValue.Clear();

    public override BaseAssignableValue Clone()
    {
        var clone = (ValueBaseAssignableValue<T>)this.MemberwiseClone();
        clone.SetValue = SetValue.ToList();
        return clone;
    }
}

public class LinkedValueBaseAssignableValue<TValue, TKey>
    : BaseAssignableValue, ILinkedAssignableValue<TValue, TKey>
    where TKey : unmanaged, IComparable, IEquatable<TKey>
    where TValue : IKeyable<TKey>, IComparable
{
    public TValue? LinkedValue { get; set; }
    public TKey? LinkedKey { get; set; }

    public override IComparable? GetValue()
    {
        return LinkedValue;
    }

    public override void AddValue(IComparable value)
    {
        LinkedValue = (TValue?)value;
        
        if (LinkedValue is not null)
            LinkedKey = LinkedValue.GetKey();
    }

    public override void ClearValue()
    {
        LinkedValue = default;
        LinkedKey = default;
    }

    public override BaseAssignableValue Clone()
    {
        var clone = (LinkedValueBaseAssignableValue<TValue, TKey>)MemberwiseClone();
        clone.LinkedValue = LinkedValue;
        clone.LinkedKey = LinkedKey;
        return clone;
    }

    public override List<IComparable?> GetValues()
    {
        throw new Exception($"{nameof(GetValues)} is not " +
            $"supported for {nameof(LinkedValueBaseAssignableValue<TValue, TKey>)}s.");
    }

    public override void ReplaceValue(IComparable value, int? index = null)
    {
        throw new Exception($"{nameof(ReplaceValue)} is not " +
            $"supported for {nameof(LinkedValueBaseAssignableValue<TValue, TKey>)}s.");
    }

    public override void ConvertAndReplaceValue(IComparable value, int? index = null)
    {
        throw new Exception($"{nameof(ConvertAndReplaceValue)} is not " +
            $"supported for {nameof(LinkedValueBaseAssignableValue<TValue, TKey>)}s.");
    }

    public override void ConvertAndAddValue(IComparable value)
    {
        throw new Exception($"{nameof(ConvertAndAddValue)} is not " +
            $"supported for {nameof(LinkedValueBaseAssignableValue<TValue, TKey>)}s.");
    }
}
