﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Assignable.Value;

public class DateTimeAssignableValue : ValueBaseAssignableValue<DateTime>
{
    
}

public class DateOnlyAssignableValue : ValueBaseAssignableValue<DateOnly>
{

}

public class TimeOnlyAssignableValue : ValueBaseAssignableValue<TimeOnly>
{

}
