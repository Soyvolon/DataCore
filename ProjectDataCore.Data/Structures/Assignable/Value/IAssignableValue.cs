﻿using ProjectDataCore.Data.Structures.Util.Interfaces;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Assignable.Value;

public interface IAssignableValue<T>
{
    public List<T> SetValue { get; set; }
}

public interface ILinkedAssignableValue<TValue, TKey>
    where TKey : unmanaged, IComparable, IEquatable<TKey>
    where TValue : IKeyable<TKey>
{
    public TValue? LinkedValue { get; set; }
    public TKey? LinkedKey { get; set; }
}
