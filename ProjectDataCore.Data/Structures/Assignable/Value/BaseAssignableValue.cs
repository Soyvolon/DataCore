﻿using ProjectDataCore.Data.Account;
using ProjectDataCore.Data.Structures.Assignable.Configuration;
using ProjectDataCore.Data.Structures.Form;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Assignable.Value;

public class BaseAssignableValue : DataObject<Guid>
{
    public DataCoreUser? ForUser { get; set; }
    public Guid? ForUserId { get; set; }

    public DynamicFormResponse? FormResponse { get; set; }
    public Guid? FormResponseKey { get; set; }

    public BaseAssignableConfiguration AssignableConfiguration { get; set; }
    public Guid AssignableConfigurationId { get; set; }

    public DataCoreUser? UserSteamLink { get; set; }
    public Guid? UserSteamLinkKey { get; set; }
    public DataCoreUser? UserDiscordLink { get; set; }
    public Guid? UserDiscordLinkKey { get; set; }
    public DataCoreUser? UserAccessCode { get; set; }
    public Guid? UserAccessCodeKey { get; set; }

    public virtual IComparable? GetValue()
    {
        return null;
    }

    public virtual List<IComparable?> GetValues()
    {
        return Array.Empty<IComparable?>().ToList();
    }

    public virtual void ReplaceValue(IComparable value, int? index = null)
    {
        // Do nothing.
    }

    public virtual void AddValue(IComparable value)
    {
        // Do nothing.
    }

    public virtual void ConvertAndReplaceValue(IComparable value, int? index = null)
    {
        // Do nothing.
    }

    public virtual void ConvertAndAddValue(IComparable value)
    {
        // Do nothing.
    }

    public virtual void ClearValue()
    {
        // Do nothing.
    }

    public virtual void UpdateBound(IServiceProvider serviceProvider, IComparable value, int? index = null)
    {
        // Do nothing.
    }

    public virtual BaseAssignableValue Clone()
    {
        return this;
    }
}
