﻿using ProjectDataCore.Data.Structures.Routing;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Model.Page;

public class CustomPageSettingsEditModel
{
    public string? Name { get; set; }
    public RouteRoot? Route { get; set; }
}
