﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Model.Roster;

public class RosterDisplaySettingsEditModel
{
    public string? Name { get; set; }
    public Guid? HostRosterId { get; set; }
}
