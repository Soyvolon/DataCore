﻿using ProjectDataCore.Data.Services.Form;
using ProjectDataCore.Data.Structures.Form.Bucket;
using ProjectDataCore.Data.Structures.Page.Components.Parameters.Form;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Form;
public class DynamicFormBucket : DataObject<Guid>
{
    public string Name { get; set; } = "New Bucket";
    public bool RequireApproval { get; set; } = false;

    public DynamicForm Form { get; set; }
    public Guid FormKey { get; set; }

    public DynamicForm? IsDefaultFor { get; set; }
    public Guid? IsDefaultForKey { get; set; }

    public List<DynamicFormResponse> Responses { get; set; } = new();
    public List<BucketChangeData> ResponsesEnteringBucket { get; set; } = new();
    public List<BucketChangeData> ResponsesExitingBucket { get; set; } = new();
    public List<BucketAction> Actions { get; set; } = new();

    public List<BucketAction> ActionsMovingToThisBucket { get; set; } = new();

    /// <summary>
    /// Moves a response into this bucket.
    /// </summary>
    /// <param name="responses">The response to move.</param>
    /// <param name="fromBucket">The bucket the response came from. May be null.</param>
    /// <param name="submitter">The submitter of this response. May be null. The Default or Admin user
    /// should be used for system actions. Null should mean a public, non-registered user is submitting
    /// a form/performing a move.</param>
    /// <param name="reason">The reason for this move.</param>
    public void MoveResponsesIn( 
        DynamicFormBucket? fromBucket, 
        DataCoreUser? submitter, 
        string reason,
        params DynamicFormResponse[] responses)
    {
        fromBucket?.MoveResponseOut(this, submitter, reason, responses);

        foreach (var r in responses)
        {
            BucketChangeData change = new()
            {
                FromBucketKey = fromBucket?.Key,
                ToBucketKey = Key,
                TriggeredById = submitter?.GetKey(),
                TriggeredByReason = reason
            };

            r.BucketChanges.Add(change);

            r.BucketKey = Key;
            r.Bucket = null!;

            Actions.ForEach(e => e.OnEnterTrigger(r));
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <remarks>
    /// Does not actually perform any deletions. The response still
    /// needs to be flagged in the database for removal.
    /// </remarks>
    /// <param name="responses"></param>
    public void RunDeleteActions(params DynamicFormResponse[] responses)
    {
        foreach (var r in responses)
        {
            if (!Responses.Remove(r))
                return;

            Actions.ForEach(e => e.OnDeleteTrigger(r));
        }
    }

    public void RunTagUpdateActions(params DynamicFormResponse[] responses)
    {
        foreach (var r in responses)
        {
            if (!Responses.Contains(r))
                return;

            Actions.ForEach(e => e.OnTagsChangedTrigger(r));
        }
    }

    /// <summary>
    /// A method called only when a response is
    /// being moved from one bucket to another. This
    /// should always be called from within the bucket object,
    /// usually in <see cref="MoveResponseIn(DynamicFormResponse, DynamicFormBucket?, DataCoreUser?, string)"/>.
    /// </summary>
    /// <param name="responses">The response leaving the bucket.</param>
    /// <param name="toBucket">The bucket the response is moving to.</param>
    /// <param name="submitter">The person executing this move.</param>
    /// <param name="reason">The reason for the move.</param>
    protected void MoveResponseOut(
        DynamicFormBucket toBucket,
        DataCoreUser? submitter,
        string reason,
        params DynamicFormResponse[] responses)
    {
        foreach(var r in responses)
            Actions.ForEach(e => e.OnExitTrigger(r));
    }
}
