﻿using ProjectDataCore.Data.Structures.Assignable;
using ProjectDataCore.Data.Structures.Assignable.Configuration;
using ProjectDataCore.Data.Structures.Assignable.Value;
using ProjectDataCore.Data.Structures.Form.Bucket;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Form;
public class DynamicFormResponse : DataObject<Guid>, IAssignable
{
    [NoObjectTreeDiscovery]
    public DataCoreUser? Submitter { get; set; }
    public Guid? SubmitterId { get; set; }

    public List<DynamicFormTag> Tags { get; set; } = new();
    public List<DynamicFormTagResponseBinding> TagBindings { get; set; } = new();

    public List<BaseAssignableValue> AssignableValues { get; set; } = new();

    [NoObjectTreeDiscovery]
    public DynamicFormBucket Bucket { get; set; }
    public Guid BucketKey { get; set; }

    [NoObjectTreeDiscovery]
    public DynamicForm Form { get; set; }
    public Guid FormKey { get; set; }

    public List<BucketChangeData> BucketChanges { get; set; } = new();

    #region Action Flags
    /// <summary>
    /// If true, this response should be deleted.
    /// </summary>
    public bool DeleteRequested { get; set; } = false;
    /// <summary>
    /// A set of objects that will also need a database update
    /// during the save action.
    /// </summary>
    public HashSet<object> NeedsDbUpdate { get; set; } = new();
    #endregion

    public BaseAssignableConfiguration.InternalAssignableType GetInternalAssignableType()
        => BaseAssignableConfiguration.InternalAssignableType.FormProperty;
}
