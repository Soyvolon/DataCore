﻿using ProjectDataCore.Data.Structures.Form;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Form.Logic;
public class LogicGroup
{
    public bool CompareWithAnd { get; set; } = true;
    public List<Func<DynamicFormResponse, bool>> Values { get; set; } = new();
    public List<LogicGroup> Groups { get; set; } = new();

    public bool GetValue(DynamicFormResponse response)
    {
        // TODO
        bool cur = true;
        foreach (var g in Groups)
        {
            var next = g.GetValue(response);
            if (CompareWithAnd)
            {
                cur = next && cur;
                if (!cur)
                    return false;
            }
            else
            {
                cur = next || cur;
                if (cur)
                    return true;
            }
        }

        return cur;
    }
}
