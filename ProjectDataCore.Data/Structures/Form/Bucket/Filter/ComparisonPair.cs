﻿using ProjectDataCore.Data.Structures.Assignable;
using ProjectDataCore.Data.Structures.Assignable.Configuration;
using ProjectDataCore.Data.Structures.Assignable.Value;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Form.Bucket.Filter;
public class ComparisonPair : DataObject<Guid>
{
    public BucketActionFilter? FilterCompareFrom { get; set; }
    public Guid? FilterCompareFromKey { get; set; }

    [NoObjectTreeDiscovery]
    public BaseAssignableConfiguration Configuration { get; set; }
    public Guid ConfigurationKey { get; set; }

    /// <summary>
    /// The order used for nested searches.
    /// </summary>
    public int Order { get; set; }

    public BaseAssignableValue? GetValue(IAssignable value)
        => value.GetAssignablePropertyContainer(Configuration.PropertyName);
}

/*
 * base.Configure(builder);

        var valueType = GetAssignableValueType();
        var propName = nameof(ILinkedAssignableValue<TValue, TKey>.LinkedValue);
        var keyName = nameof(ILinkedAssignableValue<TValue, TKey>.LinkedKey);

        var obj = builder.Entity<TAV>();

        obj.HasOne(e => e.LinkedValue)
            .WithMany()
            .HasForeignKey(e => e.LinkedKey);

        var cfgType = GetType();
        propName = nameof(LinkFlag);

        var cfg = builder.Entity<TAC>();
        cfg.Property(e => e.LinkFlag)
            .HasColumnName($"{cfgType.Name}_{propName}");
 */