﻿using ProjectDataCore.Data.Structures.Assignable;
using ProjectDataCore.Data.Structures.Assignable.Configuration;
using ProjectDataCore.Data.Structures.Util.List;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Form.Bucket.Action;
public class ApplyValuesConfiguration : DataObject<Guid>
{
    public BucketAction Action { get; set; }
    public Guid ActionKey { get; set; }

    [NoObjectTreeDiscovery]
    public BaseAssignableConfiguration AssignableToApplyTo { get; set; }
    public Guid AssignableToApplyToKey { get; set; }

    public List<ApplyToValueMap> ApplyToValueMappings { get; set; } = new List<ApplyToValueMap>();

    public IAssignable? ApplyResponse(DynamicFormResponse response)
    {
        var container = response.GetAssignablePropertyContainer(AssignableToApplyTo.PropertyName);

        if (container is null)
            return null;

        var value = (container.GetValue() as IAssignable)!;

        foreach (var map in ApplyToValueMappings)
            map.Apply(response, value);

        return value;
    }

    #region Editing
    private MultiListSelectorSingleValueWrapper<BaseAssignableConfiguration>? _multiApplyTo = null;
    public MultiListSelectorSingleValueWrapper<BaseAssignableConfiguration> MultiApplyTo
    {
        get => _multiApplyTo ??= new(() => AssignableToApplyTo,
                (e) =>
                {
                    AssignableToApplyTo = e!;
                    AssignableToApplyToKey = e?.Key ?? default;
                });
    }
    #endregion
}
