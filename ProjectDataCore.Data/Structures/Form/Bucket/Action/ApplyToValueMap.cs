﻿using ProjectDataCore.Data.Structures.Assignable;
using ProjectDataCore.Data.Structures.Assignable.Configuration;
using ProjectDataCore.Data.Structures.Util.List;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Form.Bucket.Action;
public class ApplyToValueMap : DataObject<Guid>
{
    public ApplyValuesConfiguration Configuration { get; set; }
    public Guid ConfigurationKey { get; set; }

    [NoObjectTreeDiscovery]
    public BaseAssignableConfiguration ValueFrom { get; set; }
    public Guid ValueFromKey { get; set; }

    [NoObjectTreeDiscovery]
    public BaseAssignableConfiguration ValueTo { get; set; }
    public Guid ValueToKey { get; set; }

    public void Apply(IAssignable from, IAssignable to)
    {
        var fromValue = ValueFrom.GetAssignableValue(from);
        var toValue = ValueTo.GetAssignableValue(to);

        if (fromValue is null || toValue is null)
            return;

        var fromList = fromValue.GetValues();

        toValue.ClearValue();
        foreach (var item in fromList)
            toValue.AddValue(item!);
    }

    #region Editor
    private Optional<Type?>? _valueToFilter = null;
    public Optional<Type?> ValueToFilter 
    {
        get => _valueToFilter ??= Optional.FromValue(ValueFrom?.GetValueType());
        set => _valueToFilter = value;
    }

    public void SetValueFrom(BaseAssignableConfiguration? config)
    {
        ValueFrom = config!;
        ValueFromKey = ValueFrom?.Key ?? default;

        ValueToFilter = Optional.FromValue(ValueFrom?.GetValueType());
    }

    public void SetValueTo(BaseAssignableConfiguration? config)
    {
        ValueTo = config!;
        ValueToKey = ValueTo?.Key ?? default;
    }
    #endregion
}
