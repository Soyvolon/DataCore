﻿using ProjectDataCore.Data.Structures.Assignable;
using ProjectDataCore.Data.Structures.Assignable.Configuration;
using ProjectDataCore.Data.Structures.Util.List;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Form.Bucket.Action;
public class AssignToConfiguration : DataObject<Guid>
{
    public BucketAction Action { get; set; }
    public Guid ActionKey { get; set; }

    [NoObjectTreeDiscovery]
    public BaseAssignableConfiguration UserToAssign { get; set; }
    public Guid UserToAssignKey { get; set; }

    public List<BaseAssignableConfiguration> AssignTo { get; set; } = new();
    public List<BaseAssignableConfigAssignToConfigurationBinding> AssignToConfigurations { get; set; } = new();

    public List<IUserAssignable> AssignResponse(DynamicFormResponse response)
    {
        List<IUserAssignable> toUpdate = new();

        var container = UserToAssign.GetAssignableValue(response);

        if (container is null)
            return toUpdate;

        if (container.GetValue() is not DataCoreUser user)
            return toUpdate;

        foreach (var assign in AssignTo)
        {
            var assignToContainer = assign.GetAssignableValue(response);

            if (assignToContainer is null)
                continue;

            if (assignToContainer.GetValue() is not IUserAssignable toAssignTo)
                continue;

            toAssignTo.AssignUser(user);
            toUpdate.Add(toAssignTo);
        }

        return toUpdate;
    }

    #region Editing
    private MultiListSelectorSingleValueWrapper<BaseAssignableConfiguration>? _multiAssignTo = null;
    public MultiListSelectorSingleValueWrapper<BaseAssignableConfiguration> MultiAssignTo
    {
        get => _multiAssignTo ??= new(() => UserToAssign,
                (e) =>
                {
                    UserToAssign = e!;
                    UserToAssignKey = e?.Key ?? default;
                });
    }
    #endregion
}
