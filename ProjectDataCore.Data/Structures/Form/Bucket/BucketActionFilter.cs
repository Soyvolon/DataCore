﻿using ProjectDataCore.Data.Structures.Assignable;
using ProjectDataCore.Data.Structures.Assignable.Configuration;
using ProjectDataCore.Data.Structures.Assignable.Value;
using ProjectDataCore.Data.Structures.Form.Bucket.Filter;
using ProjectDataCore.Data.Structures.Util.Interfaces;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Form.Bucket;
public class BucketActionFilter : DataObject<Guid>
{
	public enum ComparisonType
	{
		[Description("==")]
		Equal,
		[Description("!=")]
		NotEqual,
		[Description("<")]
		LessThan,
		[Description("<=")]
		LessThanOrEqual,
		[Description(">")]
		GreaterThan,
		[Description(">=")]
		GreaterThanOrEqual,
		[Description("Has Value")]
		HasValue,
		[Description("No Value")]
		NoValue,
		//[Description("Present In")]
		//PresentIn
	}

	public enum ComparisonLink
	{
		[Description("AND")]
		And,
		[Description("OR")]
		Or
	}


	public BucketAction Action { get; set; }
	public Guid ActionKey { get; set; }

	public int Order { get; set; }
	public bool StartParentheses { get; set; } = false;
	public bool EndParentheses { get; set; } = false;

	public ComparisonLink JoinToPreviousWith { get; set; } = ComparisonLink.And;
	public ComparisonType Comparison { get; set; } = ComparisonType.Equal;

    public List<ComparisonPair> CompareFromFormValue { get; set; } = new();

    public BaseAssignableValue? ValueToCompareTo { get; set; }
    public Guid? ValueToCompareToKey { get; set; }

    public IReadOnlyList<ComparisonPair> SortedCompareFromValues
	{
		get => CompareFromFormValue.OrderBy(e => e.Order).ToList();
	}

	public void ClearComparisonPairs()
	{
		CompareFromFormValue.Clear();
	}

	public void AddAssignableAsPair(int order, 
		BaseAssignableConfiguration config, 
		BaseAssignableValue? toCompareTo = null)
	{
        ComparisonPair pair = new()
        {
            Order = order,
            FilterCompareFromKey = Key,
            Configuration = config,
            ConfigurationKey = config.Key
        };
		
		CompareFromFormValue.Add(pair);

		ValueToCompareTo = toCompareTo;
		ValueToCompareToKey = toCompareTo?.Key;
    }

	/// <summary>
	/// This will get, create, or replace <see cref="ValueToCompareTo"/>
	/// based on if current configuration.
	/// </summary>
	/// <returns>A valid <see cref="BaseAssignableValue"/> or null.</returns>
	public BaseAssignableValue? GetAssignableValueForEditing()
	{
		if (SortedCompareFromValues.Count <= 0)
			return null;

		var pair = SortedCompareFromValues[SortedCompareFromValues.Count - 1];

		if (ValueToCompareTo is null
			|| ValueToCompareTo.AssignableConfiguration.TypeName
				!= pair.Configuration.TypeName)
		{
			ValueToCompareTo = pair.Configuration.CreateInstance();

			if (ValueToCompareTo is null)
				throw new Exception("Failed to create an instance of an existing assignable configuration.");
		}

		ValueToCompareTo.AssignableConfiguration = pair.Configuration;
		ValueToCompareTo.AssignableConfigurationId = pair.ConfigurationKey;

		return ValueToCompareTo;
    }

	/// <summary>
	/// Checks to see if a response passes the filter.
	/// </summary>
	/// <param name="response">The response to check.</param>
	/// <returns>True if the response passes the filter.</returns>
	public bool CheckFilter(DynamicFormResponse response)
	{
		BaseAssignableValue? formValue = null;
		foreach(var formCompare in SortedCompareFromValues)
		{
			if (formValue is null)
				formValue = formCompare.GetValue(response);
			else if (formValue.GetValue() is IAssignable assign)
				formValue = formCompare.GetValue(assign);
			else break;
		}

        IComparable? compareFrom = formValue?.GetValue();

		if (compareFrom is null)
			return Comparison == ComparisonType.NoValue;

		IComparable? compareTo = ValueToCompareTo?.GetValue();

        return Comparison switch
        {
            ComparisonType.Equal => compareFrom.CompareTo(compareTo) == 0,
            ComparisonType.NotEqual => compareFrom.CompareTo(compareTo) != 0,

            ComparisonType.LessThan => compareFrom.CompareTo(compareTo) < 0,
            ComparisonType.LessThanOrEqual => compareFrom.CompareTo(compareTo) <= 0,
            ComparisonType.GreaterThan => compareFrom.CompareTo(compareTo) > 0,
            ComparisonType.GreaterThanOrEqual => compareFrom.CompareTo(compareTo) >= 0,

            ComparisonType.HasValue => true,
            ComparisonType.NoValue => false,
            _ => false,
        };
    }
}
