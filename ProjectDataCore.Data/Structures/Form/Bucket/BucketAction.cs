﻿using Org.BouncyCastle.Asn1.Mozilla;

using ProjectDataCore.Data.Structures.Assignable.Configuration;
using ProjectDataCore.Data.Structures.Form.Bucket.Action;
using ProjectDataCore.Data.Structures.Form.Logic;
using ProjectDataCore.Data.Structures.Util.Interfaces;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Form.Bucket;
public class BucketAction : DataObject<Guid>, ISortable
{
	[Flags]
	public enum ActionType
	{
		[Description("None")]
		None = 0_0000,
		/// <summary>
		/// Move the response to a new bucket.
		/// </summary>
		[Description("Move Response")]
		MoveResponse = 0_0001,
		/// <summary>
		/// Delete a response.
		/// </summary>
		[Description("Delete Response")]
		DeleteResponse = 0_0002,
		/// <summary>
		/// Apply values to an IAssignable.
		/// </summary>
		[Description("Apply Values")]
		ApplyValues = 0_0004,
		/// <summary>
		/// Assign a user to a IUserAssignable.
		/// </summary>
		[Description("Assign User To")]
		AssignUserTo = 0_0008,
		/// <summary>
		/// Modify the tags of a response.
		/// </summary>
		[Description("Modify Tags")]
		ModifyTags = 0_0016
	}

	[Flags]
	public enum ActionTrigger
	{
		[Description("None")]
		None = 0_0000,
		/// <summary>
		/// When a response enters the bucket.
		/// </summary>
		[Description("On Enter")]
		OnEnterBucket = 0_0001,
		/// <summary>
		/// When a response leaves the bucket.
		/// </summary>
		[Description("On Exit")]
		OnExitBucket = 0_0002,
		/// <summary>
		/// When a response is deleted.
		/// </summary>
		[Description("On Delete")]
		OnDelete = 0_0004,
		/// <summary>
		/// When the tags of a response changes.
		/// </summary>
		[Description("On Tags Changed")]
		OnTagsChanged = 0_0008,
		/// <summary>
		/// When this action is manually triggered.
		/// </summary>
		[Description("Manual")]
		Manual = 0_0016
	}

	public string Name { get; set; } = "New Action";

	public DynamicFormBucket Bucket { get; set; }
	public Guid BucketKey { get; set; }

	public List<BucketActionFilter> Filters { get; set; } = new();

	public ActionType ActionTypes { get; set; } = ActionType.None;
	public ActionTrigger Triggers { get; set; } = ActionTrigger.None;

	public DynamicFormBucket? MoveResponse_MoveTo { get; set; }
	public Guid? MoveResponse_MoveToKey { get; set; }

	public List<ApplyValuesConfiguration> ApplyValuesConfigurations { get; set; } = new();

	public List<AssignToConfiguration> AssignToConfigurations { get; set; } = new();

    public List<DynamicFormTag> ModifyTags_AddTags { get; set; } = new();
	public List<DynamicFormTagBucketActionBindingAdd> ModifyTags_AddTagsBindings { get; set; } = new();
	public List<DynamicFormTag> ModifyTags_RemoveTags { get; set; } = new();
    public List<DynamicFormTagBucketActionBindingRemove> ModifyTags_RemoveTagsBindings { get; set; } = new();

    public void RecalculateOrder(int oldIndex, int newIndex)
    {
		if (newIndex < 0)
			return;

		var old = Filters.ElementAtOrDefault(oldIndex);
		if (old is null)
			return;

		Filters.RemoveAt(oldIndex);

		if (newIndex > Filters.Count)
			Filters.Add(old);
		else Filters.Insert(newIndex, old);

		RecalculateOrder();
    }

	public void RecalculateOrder()
	{
        for (int i = 0; i < Filters.Count; i++)
            Filters[i].Order = i;
    }

	public void OnEnterTrigger(DynamicFormResponse response)
	=> OnTrigger(response, ActionTrigger.OnEnterBucket);

	public void OnExitTrigger(DynamicFormResponse response)
        => OnTrigger(response, ActionTrigger.OnExitBucket);

    public void OnDeleteTrigger(DynamicFormResponse response)
        => OnTrigger(response, ActionTrigger.OnDelete);

    public void OnTagsChangedTrigger(DynamicFormResponse response)
        => OnTrigger(response, ActionTrigger.OnTagsChanged);

    public void OnManualTrigger(DynamicFormResponse response)
		=> OnTrigger(response, ActionTrigger.Manual);

	public void OnTrigger(DynamicFormResponse response, ActionTrigger trigger)
    {
        if (!Triggers.HasFlag(trigger)
            || !CheckFilters(response))
            return;

        RunAction(response);
    }

	protected bool CheckFilters(DynamicFormResponse response)
	{
		var group = BuildLogicGroup();

		var test = group.GetValue(response);

		return test;
	}

	protected LogicGroup BuildLogicGroup()
	{
		// parent group.
		LogicGroup parent = new();

		Stack<LogicGroup> groups = new();
		groups.Push(parent);
		bool first = true;

		LogicGroup? prev = null;
		var filters = Filters.OrderBy(e => e.Order).ToArray();
        for (var i = 0; i < filters.Length; i++)
		{
			var filter = filters[i];
			var cur = groups.Peek();

            var and = filter.JoinToPreviousWith == BucketActionFilter.ComparisonLink.And;
            if (first && prev is not null)
            {
                cur.CompareWithAnd = and;
                first = false;
            }

			if (and != cur.CompareWithAnd
				|| filter.StartParentheses)
			{
				var next = new LogicGroup();
				next.Values.Add(filter.CheckFilter);
				cur.Groups.Add(next);
				prev = cur;

				groups.Push(next);
				first = true;
			}
			else
			{
				cur.Values.Add(filter.CheckFilter);

				if (filter.EndParentheses)
				{
					groups.Pop();
				}
			}
		}

		return parent;
	}

	protected void RunAction(DynamicFormResponse response)
	{
        switch (ActionTypes)
        {
            case ActionType.MoveResponse:
                MoveResponse(response);
                break;
            case ActionType.DeleteResponse:
				DeleteResponse(response);
                break;
            case ActionType.ApplyValues:
				ApplyValues(response);
                break;
            case ActionType.AssignUserTo:
				AssignUserTo(response);
                break;
            case ActionType.ModifyTags:
				ModifyTags(response);
                break;

            case ActionType.None:
			default:
				break;
        }
    }

    protected void MoveResponse(DynamicFormResponse response)
	{
		if (MoveResponse_MoveTo is not null)
		{
			MoveResponse_MoveTo.MoveResponsesIn(Bucket, null, "Move Response Action", response);
			response.NeedsDbUpdate.Add(MoveResponse_MoveTo);
		}
	}

    protected static void DeleteResponse(DynamicFormResponse response) 
		=> response.DeleteRequested = true;

    protected void ApplyValues(DynamicFormResponse response)
	{
		foreach(var config in ApplyValuesConfigurations)
		{
			var obj = config.ApplyResponse(response);
			if (obj is not null)
				response.NeedsDbUpdate.Add(obj);
		}
	}

	protected void AssignUserTo(DynamicFormResponse response)
	{
		foreach(var config in AssignToConfigurations)
		{
			var updates = config.AssignResponse(response);
			foreach (var item in updates)
				response.NeedsDbUpdate.Add(item);
		}
	}

	protected void ModifyTags(DynamicFormResponse response)
	{
		foreach (var toRemove in ModifyTags_RemoveTags)
			response.Tags.Remove(toRemove);

		foreach(var toAdd in ModifyTags_AddTags)
			response.Tags.Add(toAdd);

		OnTagsChangedTrigger(response);
	}
}
