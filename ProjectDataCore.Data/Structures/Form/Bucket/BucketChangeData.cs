﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Form.Bucket;
public class BucketChangeData : DataObject<Guid>
{
    public DynamicFormResponse Response { get; set; }
    public Guid ResponseKey { get; set; }

    public DynamicFormBucket? FromBucket { get; set; }
    public Guid? FromBucketKey { get; set; }

    public DynamicFormBucket ToBucket { get; set; }
    public Guid ToBucketKey { get; set; }

    public DataCoreUser? TriggeredBy { get; set; }
    public Guid? TriggeredById { get; set; }
    public string? TriggeredByReason { get; set; }

    public DateTime ChangedOn { get; set; } = DateTime.UtcNow;
}
