﻿using ProjectDataCore.Data.Structures.Assignable.Configuration;
using ProjectDataCore.Data.Structures.Form.Bucket;
using ProjectDataCore.Data.Structures.Form.Bucket.Action;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Form;

public class DynamicFormTagResponseBinding
{
    [NoObjectTreeDiscovery]
    public DynamicFormTag Tag { get; set; }
    public Guid TagKey { get; set; }

    public DynamicFormResponse Response { get; set; }
    public Guid ResponseKey { get; set; }
}

public class DynamicFormTagBucketActionBindingAdd
{
    [NoObjectTreeDiscovery]
    public DynamicFormTag Tag { get; set; }
    public Guid TagKey { get; set; }

    public BucketAction AddAction { get; set; }
    public Guid AddActionKey { get; set; }
}

public class DynamicFormTagBucketActionBindingRemove
{
    [NoObjectTreeDiscovery]
    public DynamicFormTag Tag { get; set; }
    public Guid TagKey { get; set; }

    public BucketAction RemoveAction { get; set; }
    public Guid RemoveActionKey { get; set; }
}

public class BaseAssignableConfigAssignToConfigurationBinding
{
    [NoObjectTreeDiscovery]
    public BaseAssignableConfiguration ToAssignTo { get; set; }
    public Guid ToAssignToKey { get; set; }

    public AssignToConfiguration Configuration { get; set; }
    public Guid ConfigurationKey { get; set; }
}
