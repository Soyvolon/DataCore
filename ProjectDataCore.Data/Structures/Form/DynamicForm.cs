﻿using ProjectDataCore.Data.Services.Form;
using ProjectDataCore.Data.Structures.Assignable;
using ProjectDataCore.Data.Structures.Assignable.Configuration;
using ProjectDataCore.Data.Structures.Assignable.Value;
using ProjectDataCore.Data.Structures.Page.Components.Parameters.Form;
using ProjectDataCore.Data.Structures.Page.Components.Scope;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Form;
public class DynamicForm : DataObject<Guid>
{
    private string _name = "new form";
    public string Name
    {
        get => _name;
        set => _name = value.ToLower().Trim();
    }

    public List<DynamicFormTag> Tags { get; set; } = new();

    public List<BaseAssignableConfiguration> Properties { get; set; } = new();
    public List<DynamicFormBucket> Buckets { get; set; } = new();
    public List<DynamicFormResponse> AllResponses { get; set; } = new();

    public DynamicFormBucket? HasDefaultBucket { get; set; }

    public List<AssignableScope> DefinesScopes { get; set; } = new();

	public bool AddTag(string tagName)
    {
        var name = tagName.ToLower();
        var tag = new DynamicFormTag()
        {
            Name = name
        };

        Tags.Add(tag);
        return true;
    }

    public bool RemoveTag(DynamicFormTag tag)
        => Tags.Remove(tag);

    public bool HasTag(string tag)
    {
        var name = tag.ToLower();
        return Tags.Any(e => e.Name == name);
    }

    public DynamicFormResponse CreateResponse()
    {
        DynamicFormResponse response = new()
        {
            FormKey = Key
        };

        foreach(var cfg in Properties)
        {
            var val = cfg.CreateInstance();

            if (val is not null)
            {
                val.FormResponse = response;

                response.AssignableValues.Add(val);
            }
        }

        return response;
    }

    public bool NeedsBucket()
        => HasDefaultBucket is null;

    public DynamicFormResponse SubmitResponse(
        IEnumerable<BaseAssignableValue> values,
        DynamicFormBucket? bucket,
        DataCoreUser? submitter)
    {
        bucket ??= HasDefaultBucket;

        if (bucket is null)
            throw new ArgumentException("No default bucket set and no bucket provided", nameof(bucket));

        var response = CreateResponse();

        MapValues(values, response);

        bucket.MoveResponsesIn(null, submitter, "Form Submission", response);

        return response;
    }

    private static void MapValues(IEnumerable<BaseAssignableValue> values, DynamicFormResponse response)
    {
        foreach(var value in values)
        {
            var responseValue = response.AssignableValues
                .Find(e => e.AssignableConfiguration == value.AssignableConfiguration);

            var actualValues = value.GetValues();
            for (int i = 0; i < actualValues.Count; i++)
                responseValue?.ConvertAndReplaceValue(actualValues[i]!, i);
        }
    }
}
