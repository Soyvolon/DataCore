﻿using ProjectDataCore.Data.Structures.Form.Bucket;
using ProjectDataCore.Data.Structures.Page.Components.Parameters;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Form;
public class DynamicFormTag : DataObject<Guid>
{
    public string Name { get; set; } = string.Empty;

    [NoObjectTreeDiscovery]
    public DynamicForm Form { get; set; }
    public Guid FormKey { get; set; }

    [NoObjectTreeDiscovery]
    public List<DynamicFormResponse> Responses { get; set; } = new();
    [NoObjectTreeDiscovery]
    public List<DynamicFormTagResponseBinding> ResponseBindings { get; set; } = new();

    [NoObjectTreeDiscovery]
    public List<BucketAction> BucketAddActions { get; set; } = new();
    [NoObjectTreeDiscovery]
    public List<DynamicFormTagBucketActionBindingAdd> BucketAddActionBindings { get; set; } = new();

    [NoObjectTreeDiscovery]
    public List<BucketAction> BucketRemoveActions { get; set; } = new();
    [NoObjectTreeDiscovery]
    public List<DynamicFormTagBucketActionBindingRemove> BucketRemoveActionBindings { get; set; } = new();
}