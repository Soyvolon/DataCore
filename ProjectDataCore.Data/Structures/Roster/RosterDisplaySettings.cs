﻿using ProjectDataCore.Data.Structures.Page.Components.Parameters;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Roster;

/// <summary>
/// Holds settings for a roster display.
/// </summary>
public class RosterDisplaySettings : DataObject<Guid>
{
    /// <summary>
    /// The name to display.
    /// </summary>
    public string Name { get; set; } = "";
    /// <summary>
    /// The roster that is displayed by this roster display settings.
    /// </summary>
    public RosterTree? HostRoster { get; set; }
    /// <summary>
    /// The key for <see cref="HostRoster"/>.
    /// </summary>
    public Guid? HostRosterId { get; set; }

    /// <summary>
    /// The mapping for page configuration.
    /// </summary>
    public List<EditableComponentSettings> EditableComponentsAllowedEditors { get; set; } = new();
}
