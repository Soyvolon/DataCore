﻿using ProjectDataCore.Data.Structures.Auth.Tags;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Roster;

/// <summary>
/// Configures slot based permissions, specifically for
/// editing and assigning users within slots of a persons
/// own roster.
/// </summary>
/// <remarks>
/// These are global permissions and are checked whenever
/// any slot based dynamic permissions beyond just the
/// regular authorization system is needed. Mainly, it's
/// used to ensure someone who is in charge of other members
/// of their part of the roster can edit only the people
/// they are in charge of.
/// </remarks>
public class RosterTagPermission : DataObject<Guid>
{
    /// <summary>
    /// The tag to check if a slot has.
    /// </summary>
    public AuthorizationTag? AuthorizationTag { get; set; }
    /// <summary>
    /// The key for <see cref="AuthorizationTag"/>.
    /// </summary>
    public Guid AuthorizationTagKey { get; set; }

    /// <summary>
    /// If not empty, only slots in the same tree or child trees
    /// that also have these tags are considered a match.
    /// </summary>
    public List<AuthorizationTag> OnlyValidForTags { get; set; } = [];
    /// <summary>
    /// The bindings for <see cref="OnlyValidForTags"/>.
    /// </summary>
    public List<RosterTagPermissionAuthorizationTagWhitelistBinding> OnlyValidForTagsBindings { get; set; } = [];

    /// <summary>
    /// If true, a user with this tag can edit themselves.
    /// </summary>
    public bool CanEditSelf { get; set; } = false;
    /// <summary>
    /// If true, a user with this tag can edit their own tree.
    /// </summary>
    public bool CanEditTree { get; set; } = false;
    /// <summary>
    /// If true, a user with this tag can edit any child trees.
    /// </summary>
    public bool CanEditChildren { get; set; } = false;

    public bool IsAuthorized(DataCoreUser? user, RosterSlot slot)
    {
        if (user is null)
            return false;

        if (!user.TagsList.Contains(AuthorizationTagKey))
            return false;

        Guid[] slotTags = [.. slot.GetTags()];
        if (OnlyValidForTags.Count > 0 
            && !OnlyValidForTags.Any(e => slotTags.Contains(e.Key)))
            return false;

        if (slot.OccupiedById == user.Id)
            return CanEditSelf;

        if (user.RosterSlots.Any(e => e.ParentRosterId == slot.ParentRosterId)
            && CanEditTree)
            return true;

        if (user.RosterSlots.Any(slot.ParentRoster.HasSlotInParent)
            && CanEditChildren)
            return true;

        return false;
    }
}
