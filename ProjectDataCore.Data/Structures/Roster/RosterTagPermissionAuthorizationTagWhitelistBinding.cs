﻿using ProjectDataCore.Data.Structures.Auth.Tags;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Roster;

/// <summary>
/// For binding the <see cref="RosterTagPermission.OnlyValidForTags"/>
/// value to <see cref="Auth.Tags.AuthorizationTag"/>s.
/// </summary>
public class RosterTagPermissionAuthorizationTagWhitelistBinding
{
    /// <summary>
    /// The roster tag permission.
    /// </summary>
    public RosterTagPermission? RosterTagPermission { get; set; }
    /// <summary>
    /// The key for <see cref="RosterTagPermission"/>.
    /// </summary>
    public Guid RosterTagPermissionKey { get; set; }

    /// <summary>
    /// The auth tag.
    /// </summary>
    public AuthorizationTag? AuthorizationTag { get; set; }
    /// <summary>
    /// The key for <see cref="AuthorizationTag"/>.
    /// </summary>
    public Guid AuthorizationTagKey { get; set; }
}
