﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Infrastructure.Internal;

using Org.BouncyCastle.Asn1.Ocsp;

using ProjectDataCore.Data.Structures.Auth;
using ProjectDataCore.Data.Structures.Auth.Tags;
using ProjectDataCore.Data.Structures.Routing;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Roster;

/// <summary>
/// A section on the roster, such as a platoon or squad.
/// </summary>
public class RosterTree : RosterObject, ITaggable
{
    /// <summary>
    /// The roster slots for this tree.
    /// </summary>
    public List<RosterSlot> RosterSlots { get; set; } = new();
    /// <summary>
    /// The child trees for this tree.
    /// </summary>
    public List<RosterTree> ChildRosters { get; set; } = new();

    /// <summary>
    /// The parent trees for this tree.
    /// </summary>
    public List<RosterTree> ParentRosters { get; set; } = new();

    /// <summary>
    /// The order the roster slots are in for this tree.
    /// </summary>
    public List<RosterOrder> Order { get; set; } = new();
    /// <summary>
    /// The order the child rosters are in for this tree.
    /// </summary>
    public List<RosterOrder> OrderChildren { get; set; } = new();

    /// <summary>
    /// The roster displays that use this roster as a starting point.
    /// </summary>
    public List<RosterDisplaySettings> DisplaySettings { get; set; } = new();

    /// <summary>
    /// Authorization bindings that map to the full roster tree.
    /// </summary>
    public List<AuthorizationBinding> FullTreeAuthorizationBindings { get; set; } = new();
    /// <summary>
    /// Authorization bindings that map to just this roster tree.
    /// </summary>
    public List<AuthorizationBinding> AuthorizationBindings { get; set; } = new();
    /// <summary>
    /// Authorization bindings that are based on tag.
    /// </summary>
    public List<AuthorizationTagTreeSlotUserBinding> AuthorizationTagBindings { get; set; } = new();

    /// <summary>
    /// Inserts a new <see cref="RosterTree"/> into <see cref="ChildRosters"/>
    /// </summary>
    /// <param name="name">The name of the new <see cref="RosterTree"/></param>
    /// <param name="order">The order value for this <see cref="RosterTree"/></param>
    /// <returns>The newly inserted <see cref="RosterTree"/></returns>
    public RosterTree InsertTree(string name, int order)
    {
        RosterTree tree = new()
        {
            Name = name
        };

        ChildRosters.Insert(order, tree);
        RecalculateRosterOrder();

        return tree;
    }

    /// <summary>
    /// Inserts a new <see cref="RosterSlot"/> into <see cref="RosterSlots"/>
    /// </summary>
    /// <param name="name">The name of the new <see cref="RosterSlot"/></param>
    /// <returns>The newly inserted <see cref="RosterSlot"/></returns>
    public RosterSlot InsertSlot(string name)
    {
        RosterSlot slot = new()
        {
            Name = name
        };

        RosterSlots.Add(slot);
        RecalculateSlotOrder();

        return slot;
    }

    /// <summary>
    /// Move a roster tree up in the order of roster trees.
    /// </summary>
    /// <param name="child">The roster tree to move.</param>
    public void MoveUp(RosterTree child)
        => Move(ChildRosters, RecalculateRosterOrder, child, -1);

    /// <summary>
    /// Move a roster tree down in the order of roster trees.
    /// </summary>
    /// <param name="child">The roster tree to move.</param>
    public void MoveDown(RosterTree child)
        => Move(ChildRosters, RecalculateRosterOrder, child, 1);

    /// <summary>
    /// Sets all the values of <see cref="RosterOrder.Order"/> for the <see cref="ChildRosters"/> depending on their
    /// order within the list. To preserve proper order, this should only be
    /// called after <see cref="OrderRosterChildren"/>
    /// </summary>
    /// <remarks>
    /// If a value in <see cref="ChildRosters"/> does not have an exisitng <see cref="RosterOrder"/> for this
    /// parent, it will create a new order object add add it to the child.
    /// </remarks>
    public void RecalculateRosterOrder()
    {
        // Adjust the order of the values to match the new insertion.
        for (int i = 0; i < ChildRosters.Count; i++)
        {
            var orderObj = ChildRosters[i].Order.FirstOrDefault(x => x.ParentObject.Equals(this));
            if (orderObj is null)
            {
                orderObj = new()
                {
                    ParentObject = this
                };

                ChildRosters[i].Order.Add(orderObj);
            }

            orderObj.Order = i;
        }
    }

    public void OrderRosterChildren()
    {
        ChildRosters = ChildRosters
            .OrderBy(x => x.Order.FirstOrDefault(y => y.ParentObjectId == Key) ?? new())
            .ToList();
    }

    /// <summary>
    /// Move a slot up in the order of slots.
    /// </summary>
    /// <param name="slot">The slot to move.</param>
    public void MoveUp(RosterSlot slot)
        => Move(RosterSlots, RecalculateSlotOrder, slot, -1);

    /// <summary>
    /// Move a slot down in the order of slots.
    /// </summary>
    /// <param name="slot">The slot to move.</param>
    public void MoveDown(RosterSlot slot)
        => Move(RosterSlots, RecalculateSlotOrder, slot, 1);

    /// <summary>
    /// Moves two items in a list.
    /// </summary>
    /// <typeparam name="T">The type of objects the list contains.</typeparam>
    /// <param name="objects">The list of objects.</param>
    /// <param name="recalc">The recalcuate function.</param>
    /// <param name="child">The object to move.</param>
    /// <param name="moveBy">The ammount to move it by.</param>
    private static void Move<T>(List<T> objects, Action recalc, T child, int moveBy)
    {
        int pos = objects.IndexOf(child);
        int newPos = pos + moveBy;

        // No item was found ...
        if (pos == -1
            // ... the item is already at the top ...
            || newPos <= -1
            // ... the item is already at the bottom ...
            || newPos >= objects.Count)
            return;

        // then switch em around!
        (objects[pos], objects[newPos]) = (objects[newPos], objects[pos]);

        recalc.Invoke();
    }

    /// <summary>
    /// Sets all the values of <see cref="RosterOrder.Order"/> for the <see cref="RosterSlots"/> depending on their
    /// order within the list. To preserve proper order, this should only be
    /// called after <see cref="OrderSlotChildren"/>
    /// </summary>
    /// <remarks>
    /// If a value in <see cref="RosterSlots"/> does not have an exisitng <see cref="RosterOrder"/>
    /// it will create a new order object add add it to the child.
    /// </remarks>
    public void RecalculateSlotOrder()
    {
        // Adjust the order of the values to match the new insertion.
        for (int i = 0; i < RosterSlots.Count; i++)
        {
            var orderObj = RosterSlots[i].Order;
            if (orderObj is null)
            {
                orderObj = new()
                {
                    ParentObject = this
                };

                RosterSlots[i].Order = orderObj;
            }

            orderObj.Order = i;
        }
    }

    public void OrderSlotChildren()
    {
        RosterSlots = RosterSlots
            .OrderBy(x => x.Order)
            .ToList();
    }

    public void OrderSlotAndRosterChildren()
    {
        OrderRosterChildren();
        OrderSlotChildren();
    }

    public bool DeleteChildTree(RosterTree child)
    {
        if (ChildRosters.Remove(child))
        {
            RecalculateRosterOrder();
            return true;
        }

        return false;
    }

    public bool DeleteChildSlot(RosterSlot slot)
    {
        if (RosterSlots.Remove(slot))
        {
            RecalculateSlotOrder();
            return true;
        }

        return false;
    }

    internal List<RosterSlot> GetAllRosterSlots(ApplicationDbContext context)
        => GetAllRosterSlots(context, null);

    private List<RosterSlot> GetAllRosterSlots(ApplicationDbContext context, List<RosterSlot>? currentSlots)
    {
        currentSlots ??= new();

        var entry = context.Entry(this);

        entry.Collection(e => e.RosterSlots).Load();
        currentSlots.AddRange(RosterSlots);

        entry.Collection(e => e.ChildRosters).Load();
        foreach (var tree in ChildRosters)
            tree.GetAllRosterSlots(context, currentSlots);

        return currentSlots;
    }

    public void UpdateTags(ApplicationDbContext context)
    {
        var entry = context.Entry(this);

        entry.Collection(e => e.RosterSlots).Load();

        foreach (var slot in RosterSlots)
            slot.UpdateTags(context);
    }

    public IEnumerable<Guid> GetTags()
    {
        var tags = AuthorizationTagBindings.Select(e => e.TagKey);

        return tags;
    }

    public void LoadTags(ApplicationDbContext context)
    {
        var entry = context.Entry(this);

        entry.Collection(e => e.AuthorizationTagBindings).Load();
    }

    public void Bind(AuthorizationTagTreeSlotUserBinding binding)
    {
        binding.ClearBindings();

        binding.RosterTree = this;
        binding.RosterTreeKey = Key;
    }

    /// <summary>
    /// Finds the user in the roster tree.
    /// </summary>
    /// <param name="user">The user to look for.</param>
    /// <param name="pastDepth">Only look for the user past this depth. Inclusive value.</param>
    /// <param name="beforeDepth">Only look for the user before this depth. Exclusive value.</param>
    public List<(List<RosterTree> trees, int depth)> FindUserInTree(DataCoreUser user, int pastDepth, int beforeDepth = -1)
    {
        List<(List<RosterTree> trees, int depth)> trees = [];

        Stack<(RosterTree tree, int depth)> search = new();
        Stack<RosterTree> depthTree = new();
        search.Push((this, 0));

        while(search.TryPop(out var pair))
        {
            if (pair.depth >= depthTree.Count)
                depthTree.Push(pair.tree);
            else _ = depthTree.TryPop(out _);

            if (pair.depth >= pastDepth 
                && pair.tree.IsUserInTree(user))
                trees.Add((depthTree.Reverse().ToList(), pair.depth));

            if (pair.depth + 1 < beforeDepth)
                foreach(var t in pair.tree.ChildRosters)
                    search.Push((t, pair.depth + 1));
        }

        return trees;
    }

    public bool IsUserInTree(DataCoreUser user)
        => RosterSlots.Any(e => e.OccupiedById == user.Id);

    /// <summary>
    /// Removes all roster trees from lists
    /// after a specified depth.
    /// </summary>
    /// <param name="depth">The depth to drop at.</param>
    public void DropAfter(int depth)
    {
        if (depth == 0)
            ChildRosters.Clear();

        foreach(var c in ChildRosters)
            c.DropAfter(depth - 1);
    }

    public List<DataCoreUser> GetAllUsers(List<DataCoreUser>? users = null)
    {
        users ??= [];

        users.AddRange(RosterSlots
            .Where(e => e.OccupiedBy is not null)
            .Select(e => e.OccupiedBy!));

        foreach (var t in ChildRosters)
            t.GetAllUsers(users);

        return users;
    }

    /// <summary>
    /// Checks all parent rosters to see if they have the provided slot.
    /// </summary>
    /// <remarks>
    /// This checks all <strong>loaded</strong> parents, so if some parent rosters are not
    /// loaded this won't check them.
    /// </remarks>
    /// <param name="slot">The slot to check.</param>
    /// <returns>True if the slot exists.</returns>
    public bool HasSlotInParent(RosterSlot slot)
        => ParentRosters.Any(e => e.HasSlotInParentInternal(slot));

    private bool HasSlotInParentInternal(RosterSlot slot)
    {
        if (RosterSlots.Contains(slot))
            return true;

        // call has slot in parent to call up the chain.
        return HasSlotInParent(slot);
    }
}
