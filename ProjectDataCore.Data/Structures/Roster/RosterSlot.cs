﻿using ProjectDataCore.Data.Account;
using ProjectDataCore.Data.Structures.Assignable;
using ProjectDataCore.Data.Structures.Auth;
using ProjectDataCore.Data.Structures.Auth.Tags;
using ProjectDataCore.Data.Structures.Util.Interfaces;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Roster;

/// <summary>
/// A position on the roster.
/// </summary>
public class RosterSlot : RosterObject, ITaggable, IUserAssignable, IHasSlots
{
    public DataCoreUser? OccupiedBy { get; set; }
    public Guid? OccupiedById { get; set; }
    public RosterOrder Order { get; set; }
    public RosterTree ParentRoster { get; set; }
    public Guid ParentRosterId { get; set; }

    public List<AuthorizationTagTreeSlotUserBinding> AuthorizationTagBindings { get; set; } = new();
    public List<AuthorizationBinding> AuthorizationBindings { get; set; } = new();

    public void AssignUser(DataCoreUser user)
    {
        OccupiedBy = user;
        OccupiedById = user.Id;
    }

    public void Bind(AuthorizationTagTreeSlotUserBinding binding)
    {
        binding.ClearBindings();

        binding.RosterSlot = this;
        binding.RosterSlotKey = Key;
    }

    public void LoadTags(ApplicationDbContext context)
    {
        var entry = context.Entry(this);

        entry.Collection(e => e.AuthorizationTagBindings).Load();
        entry.Reference(e => e.ParentRoster).Load();
        ParentRoster.LoadTags(context);
    }

    public IEnumerable<Guid> GetTags()
    {
        var tags = AuthorizationTagBindings.Select(e => e.TagKey);
        var treeTags = ParentRoster.GetTags();

        return tags.Concat(treeTags);
    }

    public void UpdateTags(ApplicationDbContext context)
    {
        var entry = context.Entry(this);

        entry.Reference(e => e.OccupiedBy).Load();

        OccupiedBy?.UpdateTags(context);
    }

    public IEnumerable<RosterSlot> GetSlots()
        => new RosterSlot[] { this };
}
