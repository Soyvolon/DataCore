﻿using ProjectDataCore.Data.Structures.Assignable.Configuration;
using ProjectDataCore.Data.Structures.Page.Components.Parameters;
using ProjectDataCore.Data.Structures.Page.Components.Parameters.Form;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Roster;

#nullable disable
public class RosterDisplayColumnOrder : DataObject<Guid>
{
    [NoObjectTreeDiscovery]
    public RosterComponentSettings Settings { get; set; }
    public required Guid SettingsKey { get; set; }

    [NoObjectTreeDiscovery]
    public required BaseAssignableConfiguration AssignableConfiguration { get; set; }
    public required Guid ConfigurationKey { get; set; }

    public int Order { get; set; } = -1;
}
#nullable enable
