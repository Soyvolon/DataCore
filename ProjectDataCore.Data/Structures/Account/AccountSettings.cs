﻿using ProjectDataCore.Data.Structures.Assignable.Configuration;
using ProjectDataCore.Data.Structures.Assignable.Value;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Account;
public class AccountSettings : DataObject<Guid>
{
    /// <summary>
    /// Will a user require a link to a Discord account via OAuth2?
    /// </summary>
    public bool RequireDiscordLink { get; set; } = false;
    public BaseAssignableValue? DiscordLinkConfiguration { get; set; }
    public Guid? DiscordLinkConfigurationKey { get; set; }

    /// <summary>
    /// Will a user require a link to a Steam account via OAuth2?
    /// </summary>
    public bool RequireSteamLink { get; set; } = false;
    public BaseAssignableValue? SteamLinkConfiguration { get; set; }
    public Guid? SteamLinkConfigurationKey { get; set; }

    /// <summary>
    /// Does account registration require an access code?
    /// </summary>
    /// <remarks>
    /// This field is used to determine if users can create their own accounts. Settings this to
    /// true will require the use of a new user form display module to create new accounts.
    /// </remarks>
    public bool RequireAccessCodeForRegister { get; set; } = false;
    public BaseAssignableValue? AccessCodeConfiguration { get; set; }
    public Guid? AccessCodeConfigurationKey { get; set; }
}
