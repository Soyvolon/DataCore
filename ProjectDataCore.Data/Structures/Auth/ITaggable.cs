﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectDataCore.Data.Structures.Auth.Tags;

namespace ProjectDataCore.Data.Structures.Auth;
public interface ITaggable
{
    public void UpdateTags(ApplicationDbContext context);
    public void LoadTags(ApplicationDbContext context);
    public IEnumerable<Guid> GetTags();

    public void Bind(AuthorizationTagTreeSlotUserBinding binding);
}

public static class ITaggableExtensions
{
    public static IEnumerable<Guid> GetTags(this ITaggable taggable, ApplicationDbContext context)
    {
        taggable.LoadTags(context);

        return taggable.GetTags();
    }
}
