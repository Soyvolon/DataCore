﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectDataCore.Data.Structures.Auth.Tags;

namespace ProjectDataCore.Data.Structures.Auth;
public class AuthorizationBinding : DataObject<Guid>
{
    public AuthorizationArea AuthorizationArea { get; set; }
    public Guid AuthorizationAreaKey { get; set; }

    public string Scope { get; set; } = "view";

    public RosterTree? FullRosterTree { get; set; }
    public Guid? FullRosterTreeKey { get; set; }

    public RosterTree? RosterTree { get; set; }
    public Guid? RosterTreeKey { get; set; }

    public RosterSlot? RosterSlot { get; set; }
    public Guid? RosterSlotKey { get; set; }

    public DataCoreUser? User { get; set; }
    public Guid? UserKey { get; set; }

    public AuthorizationTag? Tag { get; set; }
    public Guid? TagKey { get; set; }
}
