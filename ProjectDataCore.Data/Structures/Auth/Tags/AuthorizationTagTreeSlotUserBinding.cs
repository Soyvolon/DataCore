﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Auth.Tags;
public class AuthorizationTagTreeSlotUserBinding : DataObject<Guid>
{
    public AuthorizationTag? Tag { get; set; }
    public Guid TagKey { get; set; }

    public RosterTree? RosterTree { get; set; }
    public Guid? RosterTreeKey { get; set; }

    public RosterSlot? RosterSlot { get; set; }
    public Guid? RosterSlotKey { get; set; }

    public DataCoreUser? User { get; set; }
    public Guid? UserId { get; set; }

    public void ClearBindings()
    {
        RosterTree = null;
        RosterTreeKey = null;

        RosterSlot = null;
        RosterSlotKey = null;

        User = null;
        UserId = null;
    }
}
