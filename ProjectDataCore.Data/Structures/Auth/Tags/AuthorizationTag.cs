﻿using ProjectDataCore.Data.Structures.Util.Interfaces;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Auth.Tags;
public class AuthorizationTag : DataObject<Guid>, IMultiSelectFreehandAddable
{
    private string _name = "";
    public string Name
    {
        get => _name;
        set => _name = value.ToLower().Trim();
    }

    public List<RosterTree> RosterTrees { get; set; } = new();
    public List<RosterSlot> RosterSlots { get; set; } = new();
    public List<DataCoreUser> Users { get; set; } = new();

    public List<AuthorizationTagTreeSlotUserBinding> AuthorizationTagBindings { get; set; } = new();
    public List<AuthorizationBinding> AuthorizationBindings { get; set; } = new();

    public List<AuthorizationTagLinkedAssignableInputSettingsBinding> LinkedAssignableInputSettingsBindings { get; set; } = new();

    /// <summary>
    /// The <see cref="RosterTagPermission"/> using this tag as a primary check.
    /// </summary>
    public List<RosterTagPermission> RosterPermissions { get; set; } = [];

    /// <summary>
    /// The bindings for the whitelists of the permissions.
    /// </summary>
    public List<RosterTagPermission> RosterPermissionWhitelists { get; set; } = [];
    /// <summary>
    /// The bindings for <see cref="RosterPermissionWhitelists"/>.
    /// </summary>
    public List<RosterTagPermissionAuthorizationTagWhitelistBinding> RosterPermissionWhitelistBindings { get; set; } = [];

    public Task ConfigureAsync(string value)
    {
        Name = value;

        return Task.CompletedTask;
    }

    #region Non-Database Fields
    private string? _editing = null;
    public string Editing
    {
        get => _editing ??= Name;
        set => _editing = value;
    }
    #endregion
}
