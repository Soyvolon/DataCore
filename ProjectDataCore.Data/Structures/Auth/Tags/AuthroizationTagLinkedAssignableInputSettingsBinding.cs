﻿using ProjectDataCore.Data.Structures.Assignable.Settings;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Auth.Tags;
public class AuthorizationTagLinkedAssignableInputSettingsBinding
{
    public AuthorizationTag? Tag { get; set; }
    public Guid TagKey { get; set; }

    public LinkedAssignableInputSettings? IncludeFilter { get; set; }
    public Guid? IncludeFilterKey { get; set; }

    public LinkedAssignableInputSettings? ExcludeFilter { get; set; }
    public Guid? ExcludeFilterKey { get; set; }
}
