﻿using ProjectDataCore.Data.Services.Auth;
using ProjectDataCore.Data.Structures.Assignable.Settings;
using ProjectDataCore.Data.Structures.Auth.Tags;
using ProjectDataCore.Data.Structures.Page;
using ProjectDataCore.Data.Structures.Page.Components.Layout;
using ProjectDataCore.Data.Structures.Page.Components.Parameters;
using ProjectDataCore.Data.Structures.Routing;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Structures.Auth;
public class AuthorizationArea : DataObject<Guid>
{
    #region Assigned To
    public List<RouteRoot> RouteRoots { get; set; } = [];
    public List<RouteRoot> RouteRootsCanEdit { get; set; } = [];
    public List<CustomPageSettings> CustomPageSettings { get; set; } = [];
    public List<LayoutNode> LayoutNodes { get; set; } = [];
    public List<PageComponentSettingsBase> PageComponentSettings { get; set; } = [];
    public List<LinkedAssignableInputSettings> LinkedAssignableInputSettings { get; set; } = [];
    public List<RosterComponentSettings> EditingAuthSettings { get; set; } = [];
    #endregion

    #region Check Against
    public List<RosterTree> FullTrees { get; set; } = [];
    public List<RosterTree> RosterTrees { get; set; } = [];
    public List<RosterSlot> RosterSlots { get; set; } = [];
    public List<DataCoreUser> Users { get; set; } = [];
    public List<AuthorizationTag> Tags { get; set; } = [];
    #endregion

    public List<AuthorizationBinding> AuthorizationBindings { get; set; } = [];

    /// <summary>
    /// The name of this <see cref="AuthorizationArea"/>.
    /// </summary>
    public string Name { get; set; } = "Default Area";

    /// <summary>
    /// Used for storing <see cref="AuthorizationArea"/>s.
    /// </summary>
    public string Category { get; set; } = "Default";

    /// <summary>
    /// If true, then anyone, including people who are not signed in, pass
    /// this authorization check.
    /// </summary>
    public bool AllowUnauthorized { get; set; } = false;
    /// <summary>
    /// If true, then anyone who has an account pass this
    /// authorization check.
    /// </summary>
    public bool AllowOnlyAuthorized { get; set; } = false;

    internal List<Guid> AuthorizedSlotsList { get; set; } = [];
    private HashSet<Guid>? _authorizedSlots;
    public IReadOnlySet<Guid> AuthorizedSlots
    {
        get => _authorizedSlots ??= new(AuthorizedSlotsList);
    }

    internal List<Guid> AuthorizedUsersList { get; set; } = [];
    private HashSet<Guid>? _authorizedUsers;
    public IReadOnlySet<Guid> AuthorizedUsers
    {
        get => _authorizedUsers ??= new(AuthorizedUsersList);
    }

    internal List<Guid> AuthorizedTagsList { get; set; } = [];
    private HashSet<Guid>? _authorizedTags;
    public IReadOnlySet<Guid> AuthorizedTags
    {
        get => _authorizedTags ??= new(AuthorizedTagsList);
    }

    internal void UpdateAuthorizedLists(ApplicationDbContext context)
    {
        _authorizedSlots ??= [];
        _authorizedSlots.Clear();

        var entry = context.Entry(this);

        entry.Collection(e => e.FullTrees).Load();
        foreach (var tree in FullTrees)
            foreach (var slot in tree.GetAllRosterSlots(context))
                _authorizedSlots.Add(slot.Key);

        entry.Collection(e => e.RosterTrees).Load();
        foreach(var tree in RosterTrees)
            foreach(var slot in tree.RosterSlots)
                _authorizedSlots.Add(slot.Key);

        entry.Collection(e => e.RosterSlots).Load();
        foreach(var slot in RosterSlots)
            _authorizedSlots.Add(slot.Key);

        _authorizedUsers ??= [];
        _authorizedUsers.Clear();

        entry.Collection(e => e.Users).Load();
        foreach (var user in Users)
            _authorizedUsers.Add(user.Id);

        _authorizedTags ??= [];
        _authorizedTags.Clear();

        foreach (var tag in Tags)
            _authorizedTags.Add(tag.Key);

        AuthorizedSlotsList = _authorizedSlots.ToList();
        AuthorizedUsersList = _authorizedUsers.ToList();
        AuthorizedTagsList = _authorizedTags.ToList();
    }

    /// <summary>
    /// Check if a user is authorized to view this area.
    /// </summary>
    /// <param name="user">The user object. Can be null.</param>
    /// <returns><b>true</b> if the user is authorized.</returns>
    public bool IsAuthorized(DataCoreUser? user)
    {
        DataCoreUser? check = user?.AuthorizationCheckUser;

        if (check is null
            && AllowUnauthorized)
            return true;
        else if (check is null)
            return false;

        if (AllowOnlyAuthorized)
            return true;

        if (check.Administrator)
            return true;

        if (AuthorizedUsers.Contains(check.Id))
            return true;

        foreach(var slot in check.RosterSlots)
            if (AuthorizedSlots.Contains(slot.Key))
                return true;

        if (check.Tags.Overlaps(AuthorizedTags))
            return true;

        return false;
    }

    public static bool Authorize(DataCoreUser? user, AuthorizationArea? authorizationArea)
    {
        if (authorizationArea is null)
            return true;

        return authorizationArea.IsAuthorized(user);
    }
}