﻿using ProjectDataCore.Data.Structures.Model.Page;
using ProjectDataCore.Data.Structures.Page;
using ProjectDataCore.Data.Structures.Page.Components;
using ProjectDataCore.Data.Structures.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Services.Page;

/// <summary>
/// Handles creation, editing, and removal of custom
/// pages to be used by the custom router.
/// </summary>
public interface IPageEditService
{
    #region Page Actions
    /// <summary>
    /// Creates a new website page.
    /// </summary>
    /// <param name="name">The name of the page.</param>
    /// <param name="route">The route to access the page at.</param>
    /// <returns>A task with an <see cref="ActionResult"/> for this action.</returns>
    public Task<ActionResult> CreateNewPageAsync(string name, string route);

    /// <summary>
    /// Update an existing page.
    /// </summary>
    /// <param name="page">The ID of the page to update.</param>
    /// <param name="action">The action to perform on the page.</param>
    /// <returns>A task with an <see cref="ActionResult"/> for this action.</returns>
    public Task<ActionResult> UpdatePageAsync(Guid page, Action<CustomPageSettingsEditModel> action);

    /// <summary>
    /// Update an existing page.
    /// </summary>
    /// <param name="settings">The page settings object to update.</param>
    /// <returns>A task with an <see cref="ActionResult"/> for this action.</returns>
    public Task<ActionResult> UpdateOrAddPageAsync(CustomPageSettings settings);

    /// <summary>
    /// Delete an existing page.
    /// </summary>
    /// <param name="page">The ID of the page to delete.</param>
    /// <returns>A task with an <see cref="ActionResult"/> for this action.</returns>
    public Task<ActionResult> DeletePageAsync(Guid page);

    /// <summary>
    /// Get a single page by its <see cref="Guid"/>
    /// </summary>
    /// <remarks>
    /// This operation will load the entire page settings object, including all
    /// of its child objects.
    /// </remarks>
    /// <param name="page">The ID of the page to retrieve.</param>
    /// <returns>A task with an <see cref="ActionResult"/> that contains a 
    /// <see cref="CustomPageSettings"/> for the provided ID.</returns>
    public Task<ActionResult<CustomPageSettings>> GetPageSettingsAsync(Guid page);

    /// <summary>
    /// Get all custom website page setting objects.
    /// </summary>
    /// <returns>A task that contains a list 
    /// of all <see cref="CustomPageSettings"/>.</returns>
    public Task<List<CustomPageSettings>> GetAllPagesAsync();

    /// <summary>
    /// Get all static page configurations.
    /// </summary>
    /// <returns>A task that contains a list of all <see cref="CustomPageSettings"/> that
    /// hold data for static pages.</returns>
    public Task<List<CustomPageSettings>> GetAllStaticPagesAsync();
    /// <summary>
    /// Delete a collection of pages.
    /// </summary>
    /// <param name="pages">The pages to delete.</param>
    /// <returns>A task with an <see cref="ActionResult"/> for this action.</returns>
    public Task<ActionResult> BulkDeletePagesAsync(IEnumerable<CustomPageSettings> pages);
    /// <summary>
    /// Update or add a collection of pages.
    /// </summary>
    /// <param name="pages">The pages to add or update.</param>
    /// <returns>A task with an <see cref="ActionResult"/> for this action.</returns>
    public Task<ActionResult> BulkUpdateOrAddPagesAsync(IEnumerable<CustomPageSettings> pages);
    #endregion

    #region Text Display Component Actions
    /// <summary>
    /// Updates the raw display contents of a text display.
    /// </summary>
    /// <param name="comp">The ID of the component to update.</param>
    /// <param name="rawContents">The raw contnets to update.</param>
    /// <returns>A task that returns a <see cref="ActionResult"/> for this action.</returns>
    public Task<ActionResult> UpdateTextDisplayContentsAsync(Guid comp, string rawContents);
    #endregion
}
