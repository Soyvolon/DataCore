﻿using ProjectDataCore.Data.Services.HTML;
using ProjectDataCore.Data.Services.Nav;
using ProjectDataCore.Data.Services.Routing;
using ProjectDataCore.Data.Structures.Model.Page;
using ProjectDataCore.Data.Structures.Page;
using ProjectDataCore.Data.Structures.Page.Attributes;
using ProjectDataCore.Data.Structures.Page.Components.Layout;
using ProjectDataCore.Data.Structures.Page.Components.Parameters;
using ProjectDataCore.Data.Structures.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Services.Page;

public class PageEditService : IPageEditService
{
    private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;
    private readonly IRoutingService _routingService;
    private readonly IModularRosterService _rosterService;
    private readonly IHTMLService _HTMLService;

    public PageEditService(IDbContextFactory<ApplicationDbContext> dbContextFactory, IRoutingService routingService, 
        IModularRosterService rosterService, IHTMLService HTMLService)
        => (_dbContextFactory, _routingService, _rosterService, _HTMLService) 
        =  (dbContextFactory,   routingService,  rosterService, HTMLService);

    #region Page Actions
    public async Task<ActionResult> CreateNewPageAsync(string name, string route)
    {
        // Get the physical route object for the route string ...
        var routeRes = await _routingService.GetRouteRootFromRootAsync(route);

        // ... if that errors, return the result object with the errors ...
        if (!routeRes.GetResult(out var routeObj, out _))
            return routeRes;

        // ... otherwise create a new DB context ...
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        // ... and create a new page to add ...
        var obj = new CustomPageSettings()
        {
            Name = name,
            RouteRoot = routeObj,
            Layout = new()
        };
        // ... add it ...
        await _dbContext.AddAsync(obj);
        try
        {
            // ... then save.
            await _dbContext.SaveChangesAsync();
            return new(true, null);
        }
        catch (Exception ex)
        {
            // ... if there was a violation of the unique constraint, then
            // return the error.
            return new(false, new List<string>() { "Route name is already in use.", ex.Message });
        }
    }

    public async Task<ActionResult> DeletePageAsync(Guid page)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        var settings = await _dbContext.FindAsync<CustomPageSettings>(page);

        if (settings is null)
            return new(false, new List<string>() { "No page for the provided ID was found." });

        // Load the settings using the currently tracked object
        // so we can do a proper cascade delete ...
        await foreach (var _ in _routingService.LoadPageSettingsAsync(settings)) { }

        var nav = await _dbContext.NavModules
            .Where(e => e.PageId == page)
            .FirstOrDefaultAsync();

        if (nav is not null)
            _dbContext.Remove(nav);

        var objects = GetElements(settings);
        foreach (var item in objects.Values)
            _dbContext.Remove(item);

        await _dbContext.SaveChangesAsync();

        return new(true, null);
    }

    public async Task<List<CustomPageSettings>> GetAllPagesAsync()
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        return await _dbContext.CustomPageSettings.Include(e=>e.Layout).ToListAsync();
    }

    // TODO REWORK THIS
    public async Task<ActionResult<CustomPageSettings>> GetPageSettingsAsync(Guid page)
    {
        // Code here is modified from the IRoutingService load page to load a 
        // full page settings object.

        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();
        // find the settings object...
        var obj = await _dbContext.FindAsync<CustomPageSettings>(page);

        if (obj is null)
            return new(false, new List<string>() { "No settings object was found"}, null);

        // ... get the DB data for it ...
        var dbDat = _dbContext.Entry(obj);
        // ... then load the base layout ...
        await dbDat.Reference(e => e.Layout).LoadAsync();
        // ... if the layout is not null ...
        if (obj.Layout is not null)
        {
            // ... then the base layout to the level queue ...
            Queue<LayoutNode> level = new();
            level.Enqueue(obj.Layout);

            // ... and while we have level data ...
            while (level.TryDequeue(out var levelItem))
            {
                // ... attach the level item ...
                var layoutObj = _dbContext.Attach(levelItem);
                // ... and load its direct children ...
                var children = layoutObj.Collection(x => x.Nodes).Query()
                    .Include(x => x.ParentNode)
                    .AsAsyncEnumerable();

                // ... then for each child ...
                await foreach (var child in children)
                {
                    // ... if the child is a layout component, enqueue it.
                    if (child is LayoutNode childLayout)
                        level.Enqueue(childLayout);
                }
            }
        }

        // ... and return the fully loaded settings object.
        return new(true, null, obj);
    }

    public async Task<ActionResult> UpdatePageAsync(Guid page, Action<CustomPageSettingsEditModel> action)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        var pageData = await _dbContext.FindAsync<CustomPageSettings>(page);

        if (pageData is null)
            return new(false, new List<string>() { "No page settings object was found for the provided ID." });

        // Run update based on the action values that were passed.
        CustomPageSettingsEditModel update = new();
        action.Invoke(update);

        if(update.Name is not null)
            pageData.Name = update.Name;

        if(update.Route is not null)
        {
            pageData.RouteRoot = update.Route;
        }

        try
        {
            // attempt a save...
            await _dbContext.SaveChangesAsync();
            return new(true, null);
        }
        catch (Exception ex)
        {
            // ... if there was a violation of the unique constraint, then
            // return the error.
            return new(false, new List<string>() { "Route name is already in use.", ex.Message });
        }
    }

    /* Might be useful but needs some more testing. Object discovery with this
     * function goes all the down every route without the proper attributes
     * set.
     * 
     * await _dbContext.UpdateObjectTreeAsync(update, _dbContext.CustomPageSettings,
            async (e) =>
            {
                // ... loading the old settings tree ...
                var loader = _routingService.LoadPageSettingsAsync(e);
                await foreach (var _ in loader)
                    continue;
            });
     */

    public async Task<ActionResult> UpdateOrAddPageAsync(CustomPageSettings update)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        //// If the page settings does not exist ...
        //if(update.Key == default)
        //{
        //    // ... add it to the database ...
        //    _dbContext.Update(update);
        //}
        //else
        //{
        //    // ... if it does exists, then get the original settings object ...
        //    var settings = await _dbContext.CustomPageSettings
        //        .Where(x => x.Key == update.Key)
        //        .AsNoTracking()
        //        .FirstOrDefaultAsync();

        //    // ... get the old elements ...
        //    Dictionary<Guid, object> oldElements = new();
        //    if (settings is not null)
        //    {
        //        // ... staring by loading the old settings tree ...
        //        var loader = _routingService.LoadPageSettingsAsync(settings);
        //        await foreach (var _ in loader)
        //            continue;

        //        // ... and getting the elements ...
        //        oldElements = GetElements(settings);
        //    }

        //    // ... then registering the new data for an update ...
        //    _dbContext.Update(update);

        //    // ... then we get the new elements ...
        //    var newElements = GetElements(update);

        //    // ... and compare the differences in keys between the old and new ...
        //    var keyDiff = oldElements.Keys.ToHashSet<Guid>();
        //    // ... taking only those in the old and not in the new ...
        //    keyDiff.ExceptWith(newElements.Keys.ToHashSet());

        //    // ... then for each key ...
        //    foreach(var key in keyDiff)
        //    {
        //        // ... we get the element ...
        //        var element = oldElements[key];
        //        // ... and mark it for deletion ....
        //        _dbContext.Entry(element).State = EntityState.Deleted;
        //    }
        //}

        try
        {
            // ... finally, attempt a save...
            //await _dbContext.SaveChangesAsync();

            await _dbContext.UpdateObjectTreeAsync(update, _dbContext.CustomPageSettings,
                async (e) =>
                {
                    // ... loading the old settings tree ...
                    var loader = _routingService.LoadPageSettingsAsync(e);
                    await foreach (var _ in loader)
                        continue;
                });

            return new(true, null);
        }
        catch (Exception ex)
        {
            // ... if there was a violation of the unique constraint, then
            // return the error.
            return new(false, new List<string>() { "Failed to add or update the page.", ex.Message });
        }
    }
    #endregion

    public async Task<ActionResult> UpdateTextDisplayContentsAsync(Guid comp, string rawContents)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        var compData = await _dbContext.TextDisplayComponentSettings
            .Where(x => x.Key == comp)
            .FirstOrDefaultAsync();

        if (compData is null)
            return new(false, new List<string>() { "No display component was found for the provided ID." });

        if (rawContents is not null)
            compData.RawContents = rawContents;
        else compData.RawContents = "";

        await _dbContext.SaveChangesAsync();
        return new(true, null);
    }

    public async Task<List<CustomPageSettings>> GetAllStaticPagesAsync()
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();
        var pages = await _dbContext.CustomPageSettings
            .Where(x => x.IsStaticPage)
            .Include(x => x.Layout)
            .ThenInclude(x => x.Component)
            .ToListAsync();

        foreach(var elem in pages)
            if (elem.Layout is not null)
                await _dbContext.Entry(elem)
                    .Reference(x => x.Layout)
                    .LoadAsync();

        return pages;
    }

    public async Task<ActionResult> BulkDeletePagesAsync(IEnumerable<CustomPageSettings> pages)
    {
        List<string> err = new();
        int c = -1;
        foreach (var page in pages)
        {
            c++;

            try
            {
                await DeletePageAsync(page.Key);
            }
            catch (Exception ex)
            {
                err.Add($"Failed to delete page at index {c}: {ex.Message}");
                continue;
            }
        }

        if (err.Count > 0)
            return new(false, err);

        return new(true, null);
    }

    public async Task<ActionResult> BulkUpdateOrAddPagesAsync(IEnumerable<CustomPageSettings> pages)
    {
        List<string> err = new();
        int c = -1;
        foreach (var page in pages)
        {
            c++;

            try
            {
                await UpdateOrAddPageAsync(page);
            }
            catch (Exception ex)
            {
                err.Add($"Failed to add/update page at index {c}: {ex.Message}");
                continue;
            }
        }

        if (err.Count > 0)
            return new(false, err);

        return new(true, null);
    }

    // This method gets a key/object tree to use for deletes/adds later ...
    private Dictionary<Guid, object> GetElements(CustomPageSettings settings)
    {
        // ... by starting at the inital node ...
        Stack<LayoutNode> nodes = new();
        if (settings.Layout is not null)
            nodes.Push(settings.Layout);

        // ... we collect the element dict ...
        Dictionary<Guid, object> elements = new();
        // ... one node at a time ...
        while (nodes.TryPop(out var node))
        {
            // ... saving the node ...
            elements.Add(node.Key, node);
            // ... its component ...
            if (node.Component is not null)
                elements.Add(node.Component.Key, node.Component);

            // ... and its page settings ...
            if (node.PageSettings is not null)
            {
                elements.Add(node.PageSettings.Key, node.PageSettings);

                // ... and the page settings scopes ...
                foreach (var scope in node.PageSettings.AssignableScopes)
                    elements.Add(scope.Key, scope);
            }

            // ... then we collect any more nodes
            // for the next loop ...
            foreach (var n in node.Nodes)
                nodes.Push(n);
        }

        // ...then we return the object dict.
        return elements;
    }
}