﻿using Microsoft.AspNetCore.Connections.Features;
using Microsoft.AspNetCore.Mvc.TagHelpers;

using ProjectDataCore.Data.Services.User;
using ProjectDataCore.Data.Structures.Assignable.Settings;
using ProjectDataCore.Data.Structures.Auth;
using ProjectDataCore.Data.Structures.Util.Interfaces;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace ProjectDataCore.Data.Services.Assignable;
public class LinkedAssignableService : ILinkedAssignableService
{
    private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;
    private readonly ILocalUserService _localUserService;
    private readonly IModularRosterService _modularRosterService;

    public LinkedAssignableService(IDbContextFactory<ApplicationDbContext> dbContextFactory, 
        ILocalUserService localUserService, IModularRosterService modularRosterService)
    {
        _dbContextFactory = dbContextFactory;
        _localUserService = localUserService;
        _modularRosterService = modularRosterService;
    }

    public async Task<List<RosterSlot>> GetPassingRosterSlots(LinkedAssignableInputSettings settings)
    {
        // Get the context ...
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();
        // ... and the local user ...
        var local = _localUserService.LocalUser;

        // ... then start the query ...
        IQueryable<RosterSlot> query = _dbContext.RosterSlots;
        // ... if there is an auth area ...
        if (settings.DataAuthorizationArea is not null)
        {
            // ... see if the slot is in the auth area ...
            query = query.Where(e => settings.DataAuthorizationArea.AuthorizedSlots.Contains(e.Key));
        }

        // ... if there are no other settings to apply ...
        if (local is null
            && settings.IncludeTags.Count <= 0
            && settings.ExcludeTags.Count <= 0)
            // ... return the query ...
                return await query.ToListAsync();

        // ... otherwise, if there are tags to check ...
        if (settings.IncludeTags.Count > 0
            || settings.ExcludeTags.Count > 0)
        {
            // ... get all tags for the slots ...
            query = query.Include(e => e.AuthorizationTagBindings)
                .Include(e => e.ParentRoster)
                .ThenInclude(e => e.AuthorizationTagBindings);
        }

        return await GetItems(settings, local, query);
    }

    public async Task<List<DataCoreUser>> GetPassingUsers(LinkedAssignableInputSettings settings)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        var local = _localUserService.LocalUser;

        IQueryable<DataCoreUser> query = _dbContext.Users;

        return await GetItems(settings, local, query, (e) =>
        {
            if (settings.DataAuthorizationArea is not null)
                return settings.DataAuthorizationArea.IsAuthorized(e);

            return true;
        });
    }

    private async Task<List<T>> GetItems<T>(LinkedAssignableInputSettings settings, DataCoreUser? local, IQueryable<T> query,
        Func<T, bool>? initialCheck = null)
        where T : ITaggable, IHasSlots
    {
        // ... and if there are roster tree checks ...
        Dictionary<Guid, RosterTree>? allTrees = null;
        if (settings.OnlyLocalUsersRosterTree
            || settings.OnlyLocalUsersRosterChildren
            || settings.OnlyLocalUsersRosterTreeAndChildTrees)
        {
            // ... get all top level trees ...
            var res = await _modularRosterService.GetTopLevelRosterTreesAsync();
            if (!res.GetResult(out var topLevel, out var err))
                return new();

            // ... load them fully ...
            Stack<RosterTree> treeStack = new();
            foreach (var t in topLevel)
            {
                await foreach (var _ in _modularRosterService.LoadFullRosterTreeAsync(t))
                    continue;

                // ... and add the top level to the stack ...
                treeStack.Push(t);
            }

            // ... then for all trees ...
            allTrees = new();
            while (treeStack.TryPop(out var tree))
            {
                // ... add them to the dict (for unique values) ...
                if (!allTrees.TryAdd(tree.Key, tree))
                    continue;

                // ... and push the children to the stack ...
                foreach (var t in tree.ChildRosters)
                    treeStack.Push(t);
            }
        }

        // ... then for each slot in the query ...
        List<T> items = new();
        await foreach (var item in query.AsAsyncEnumerable())
        {
            // ... start add at true...
            bool add = true;

            if (initialCheck is not null)
                add &= initialCheck.Invoke(item);

            // ... if there were no matches ...
            if (!add)
                // ... go to the next slot ...
                continue;

            // ... then if we have roster trees to check ...
            if (local is not null && allTrees is not null)
            {
                bool check = false;

                // ... for each tree in the local user ...
                foreach (var tree in local.RosterSlots.Select(e => e.ParentRoster))
                {
                    // ... try and get it from the actual trees ...
                    if (!allTrees.TryGetValue(tree.Key, out var actualTree))
                        continue;

                    foreach (var itemSlot in item.GetSlots())
                    {
                        // ... and if it matches ...
                        check = MatchesSlot(itemSlot, actualTree,
                            settings.OnlyLocalUsersRosterTree || settings.OnlyLocalUsersRosterTreeAndChildTrees,
                            settings.OnlyLocalUsersRosterChildren || settings.OnlyLocalUsersRosterTreeAndChildTrees);

                        // ... exit the loop ...
                        if (check)
                            break;
                    }

                    // ... exit the loop ...
                    if (check)
                        break;
                }

                add &= check;
            }

            // ... if there were no matches ...
            if (!add)
                // ... go to the next slot ...
                continue;

            // ... otherwise see if it contains tags that are included ...
            if (settings.IncludeTags.Count > 0)
            {
                add &= MatchesTags(item.GetTags(),
                    settings.IncludeTags.Select(e => e.Key),
                    settings.IncludeIfObjectHasAnyTag);
            }

            // ... if it doesn't, go to the next ...
            if (!add)
                continue;

            // ... then check to see if it contains any tags that are excluded ...
            if (settings.ExcludeTags.Count > 0)
            {
                add &= !MatchesTags(item.GetTags(),
                    settings.ExcludeTags.Select(e => e.Key),
                    settings.ExcludeIfObjectHasAnyTag);
            }

            // ... if it doesn't, add the slot ...
            if (add)
                items.Add(item);
        }

        // ... finally, return the slots.
        return items;
    }

    private static bool MatchesSlot(RosterSlot slot, RosterTree topLevel, bool canMatchTop, bool canMatchChild)
    {
        Stack<RosterTree> treeStack = new();
        treeStack.Push(topLevel);

        int c = 0;
        while(treeStack.TryPop(out var tree))
        {
            // if it's the first item and can match top ...
            if ((c == 0 && canMatchTop)
                //  or any other item and can match child ...
                || (c > 0 && canMatchChild))
                // ... and the slot is in the tree ...
                if (tree.RosterSlots.Contains(slot))
                    // ... return true ...
                    return true;

            c += 1;
            // ... otherwise, if we can match children ...
            if (canMatchChild)
                // ... take all children ...
                foreach (var t in tree.ChildRosters)
                    // ... and push to the stack.
                    treeStack.Push(t);
        }

        return false;
    }

    private static bool MatchesTags(IEnumerable<Guid> tags, IEnumerable<Guid> compareTo, bool matchesAny)
    {
        var intersect = tags.Intersect(compareTo);

        return intersect.Any() && (intersect.Count() == compareTo.Count() || matchesAny);
    }
}
