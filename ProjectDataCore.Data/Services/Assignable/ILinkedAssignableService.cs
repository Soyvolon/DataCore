﻿using ProjectDataCore.Data.Structures.Assignable.Settings;
using ProjectDataCore.Data.Structures.Auth;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Services.Assignable;
public interface ILinkedAssignableService
{
    /// <summary>
    /// Gets all roster slots that pass the provided <paramref name="settings"/>'s check.
    /// </summary>
    /// <param name="settings">The <see cref="LinkedAssignableInputSettings"/> to test <see cref="RosterSlot"/>s against</param>
    /// <returns>A <see cref="Task{TResult}"/> with a <see cref="List{T}"/> of <see cref="RosterSlot"/>s
    /// that pass the check from the provided <paramref name="settings"/>.</returns>
    public Task<List<RosterSlot>> GetPassingRosterSlots(LinkedAssignableInputSettings settings);

    /// <summary>
    /// Gets all data core users that pass the provided <paramref name="settings"/>'s check.
    /// </summary>
    /// <remarks>
    /// <param name="settings">The <see cref="LinkedAssignableInputSettings"/> to test <see cref="DataCoreUser"/>s against</param>
    /// <returns>A <see cref="Task{TResult}"/> with a <see cref="List{T}"/> of <see cref="DataCoreUser"/>s
    /// that pass the check from the provided <paramref name="settings"/>.</returns>
    public Task<List<DataCoreUser>> GetPassingUsers(LinkedAssignableInputSettings settings);
}
