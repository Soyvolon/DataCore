﻿using ProjectDataCore.Data.Structures.Form;
using ProjectDataCore.Data.Structures.Form.Bucket;
using ProjectDataCore.Data.Structures.Page.Components.Parameters.Form;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.RateLimiting;
using System.Threading.Tasks;

using static System.Collections.Specialized.BitVector32;

namespace ProjectDataCore.Data.Services.Form;
public class FormService : IFormService
{
    private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;

    public FormService(IDbContextFactory<ApplicationDbContext> dbContextFactory)
    {
        _dbContextFactory = dbContextFactory;
    }

    #region Forms
    public async Task<List<DynamicForm>> GetAllFormsAsync()
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        return await _dbContext.DynamicForms
            .ToListAsync();
    }

    public async Task<ActionResult> UpdateOrAddFormAsync(DynamicForm form)
    {
        try
        {
            await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

            if (_dbContext.DynamicForms
                .Where(e => e.Name == form.Name)
                .Where(e => e.Key != form.Key)
                .Any())
                return new(false, new List<string> { $"The name {form.Name} is already in use." });

            await _dbContext.UpdateObjectTreeAsync(form, _dbContext.DynamicForms);

            return new(true);
        }
        catch (Exception ex)
        {
            return new(false, new List<string> { "Failed to update or add a form.", ex.Message });
        }
    }

    public async Task<ActionResult> DeleteFormAsync(DynamicForm form)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        _dbContext.Remove(form);

        await _dbContext.SaveChangesAsync();

        return new(true);
    }

    public async Task RefreshFormAsync(DynamicForm form)
    {
		await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

		var attach = _dbContext.Attach(form);
		await attach.ReloadAsync();
    }

    public async Task<ActionResult<DynamicForm>> GetFormFromBucketKeyAsync(Guid bucketKey)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        var bucket = await _dbContext.DynamicFormBuckets
            .Where(e => e.Key == bucketKey)
            .Include(e => e.Form)
            .FirstOrDefaultAsync();

        if (bucket?.Form is null)
            return new(false, ["The form was unable to be loaded."]);

        return new(true, null, bucket.Form);
    }
    #endregion

    #region Buckets
    public async Task<ActionResult> LoadFormBucketsAsync(DynamicForm form)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        if (form.Key == default)
            return new(false, new List<string> { $"The {nameof(form)} must already be" +
                $" saved to the database to load buckets."});

        try
        {
            var attach = _dbContext.Attach(form);
            await attach.Collection(e => e.Buckets).LoadAsync();

            return new(true);
        }
        catch (Exception ex)
        {
            return new(false, new List<string> { "Failed to load buckets.", ex.Message });
        }
    }

    public async Task<ActionResult> UpdateOrAddFormBucketAsync(DynamicFormBucket bucket)
    {
        try
        {
            await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

            if (_dbContext.DynamicFormBuckets
                .Where(e => e.Name == bucket.Name)
                .Where(e => e.Key != bucket.Key)
                .Where(e => e.FormKey == bucket.FormKey)
                .Any())
                return new(false, new List<string> { $"The name {bucket.Name} is already in use." });

            await _dbContext.UpdateObjectTreeAsync(bucket, _dbContext.DynamicFormBuckets);

            return new(true);
		}
		catch (Exception ex)
		{
			return new(false, new List<string> { "Failed to update or add a form bucket.", ex.Message });
		}
	}

    public async Task<ActionResult> DeleteFormBucketAsync(DynamicFormBucket bucket)
	{
		await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

		_dbContext.Remove(bucket);

		await _dbContext.SaveChangesAsync();

		return new(true);
	}

    public async Task<ActionResult> SetAsDefaultBucketAsync(DynamicFormBucket bucket)
    {
        if (bucket.FormKey == default)
            return new(false, new List<string> { "Invalid bucket - no parent form. If this is a new bucket, please" +
                " save it."});
        
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        var update = _dbContext.Update(bucket);
		await update.Reference(e => e.Form).LoadAsync();

        bucket.IsDefaultFor = bucket.Form;
		bucket.IsDefaultForKey = bucket.FormKey;

        if (bucket.Form.HasDefaultBucket is not null)
        {
			bucket.Form.HasDefaultBucket.IsDefaultFor = null;
			bucket.Form.HasDefaultBucket.IsDefaultForKey = null;
        }

        bucket.Form.HasDefaultBucket = bucket;

        await _dbContext.SaveChangesAsync();

        await update.ReloadAsync();

        return new(true);
	}

	public async Task RefreshBucketAsync(DynamicFormBucket bucket)
    {
		await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

		var attach = _dbContext.Attach(bucket);
		await attach.ReloadAsync();
    }

    public async Task<ActionResult<DynamicFormBucket>> GetBucketFromIDAsync(Guid id)
    {
        try
        {
            await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

            var res = await _dbContext.DynamicFormBuckets
                .Where(e => e.Key == id)
                .FirstOrDefaultAsync();

            if (res is null)
                return new(false, new List<string> { "Failed to find a bucket by the provided ID." });

            return new(true, null, res);
        }
        catch (Exception ex)
        {
            return new(false, new List<string> {
                $"An unexpected error occurred in ${nameof(GetBucketFromIDAsync)}",
                ex.Message
            });
        }
    }
    #endregion

    #region Form Actions
    public async Task<ActionResult> UpdateOrAddBucketActionAsync(BucketAction action)
    {
        try
        {
            await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

            await _dbContext.UpdateObjectTreeAsync(action, _dbContext.BucketActions);

            return new(true);
        }
        catch (Exception ex)
        {
            return new(false, new List<string> { "Failed to update or add a bucket action.", ex.Message });
        }
	}

	public async Task<ActionResult> DeleteBucketActionAsync(BucketAction action)
    {
		await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

		_dbContext.Remove(action);

		await _dbContext.SaveChangesAsync();

		return new(true);
	}
    #endregion

    //#region Form Buttons
    //public async Task LoadButtonFormRelationsAsync(ButtonComponentSettings button)
    //{
    //    await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

    //    var attach = _dbContext.Attach(button);
    //    await attach.Reference(e => e.Form)
    //        .LoadAsync();
    //    await attach.Reference(e => e.Bucket)
    //        .LoadAsync();
    //}
    //#endregion
}
