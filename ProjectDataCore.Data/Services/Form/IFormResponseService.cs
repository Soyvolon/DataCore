﻿using ProjectDataCore.Data.Structures.Assignable.Value;
using ProjectDataCore.Data.Structures.Form;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Services.Form;
public interface IFormResponseService
{
    public Task<ActionResult<DynamicFormResponse>> SubmitFormResponseAsync(
        DynamicForm form,
        DynamicFormBucket? bucket, 
        List<BaseAssignableValue> assignables);

    public Task<ActionResult> MoveResponsesAsync(
        DynamicFormBucket newBucket,
        string reason,
        params DynamicFormResponse[] response);

    public Task<ActionResult> UpdateResponseTagsAsync(
        params DynamicFormResponse[] response);

    public Task<ActionResult> DeleteResponsesAsync(
        params DynamicFormResponse[] response);

    public Task<ActionResult> LoadResponsesForBucketAsync(
        DynamicFormBucket bucket);
}
