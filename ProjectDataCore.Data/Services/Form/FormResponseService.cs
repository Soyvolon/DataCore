﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

using ProjectDataCore.Data.Services.User;
using ProjectDataCore.Data.Structures.Assignable;
using ProjectDataCore.Data.Structures.Assignable.Value;
using ProjectDataCore.Data.Structures.Form;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Services.Form;
public class FormResponseService : IFormResponseService
{
    private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;
    private readonly ILocalUserService _localUserService;

    public FormResponseService(IDbContextFactory<ApplicationDbContext> dbContextFactory, 
        ILocalUserService localUserService)
    {
        _dbContextFactory = dbContextFactory;
        _localUserService = localUserService;
    }

    public async Task<ActionResult<DynamicFormResponse>> SubmitFormResponseAsync(
        DynamicForm form, 
        DynamicFormBucket? bucket, 
        List<BaseAssignableValue> assignables)
    {
        try
        {
            await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

            if (bucket is null && form.NeedsBucket())
                return new(false, new List<string> { 
                    "The provided form does not have a default bucket and no bucket was provided." 
                });

            var local = _localUserService.LocalUser;

            var response = form.SubmitResponse(assignables, bucket, local);
            var delete = response.DeleteRequested;

            // ... update the form ...
            _dbContext.Update(form);
            // ... the bucket ...
            if (bucket is null)
                _dbContext.Update(form.HasDefaultBucket!);
            else _dbContext.Update(bucket);
            // ... and the response ...
            var track = _dbContext.Update(response);

            await _dbContext.SaveChangesAsync();
            await track.ReloadAsync();

            if (delete)
                await DeleteResponsesAsync(response);

            return new(true, null, response);
        }
        catch (Exception ex)
        {
            return new(false, new List<string> { 
                $"An unexpected error occurred in ${nameof(SubmitFormResponseAsync)}", 
                ex.Message 
            });
        }
    }

    public async Task<ActionResult> MoveResponsesAsync(
        DynamicFormBucket newBucket, 
        string reason,
        params DynamicFormResponse[] response)
    {
        try
        {
            List<DynamicFormResponse> toDelete = [];
            List<EntityEntry<DynamicFormResponse>> entries = [];

            await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();
            var local = _localUserService.LocalUser;

            foreach (var r in response)
            {
                if ((r.Bucket is null && r.BucketKey != Guid.Empty)
                    || (r.Bucket is not null && r.BucketKey != r.Bucket.Key))
                {
                    await _dbContext.Entry(r).Reference(e => e.Bucket).LoadAsync();
                }
            }

            foreach (var r in response)
            {
                var track = _dbContext.Update(r);
                entries.Add(track);
            }

            var buckets = response.GroupBy(e => e.Bucket);

            foreach (var p in buckets)
            {
                newBucket.MoveResponsesIn(p.Key, local, reason, [.. p]);

                toDelete.AddRange(p.Where(e => e.DeleteRequested));
            }

            await _dbContext.SaveChangesAsync();

            foreach(var track in entries)
                await track.ReloadAsync();

            await DeleteResponsesAsync([.. toDelete]);

            return new(false, null);
        }
        catch (Exception ex)
        {
            return new(false, [
                $"An unexpected error occurred in ${nameof(MoveResponsesAsync)}",
                ex.Message
            ]);
        }
    }

    public async Task<ActionResult> DeleteResponsesAsync(params DynamicFormResponse[] response)
    {
        try
        {
            if (response.Length == 0)
                return new(false, ["There must be at least one response provided."]);

            await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

            foreach(var r in response)
            {
                if ((r.Bucket is null && r.BucketKey != Guid.Empty)
                    || (r.Bucket is not null && r.BucketKey != r.Bucket.Key))
                {
                    await _dbContext.Entry(r).Reference(e => e.Bucket).LoadAsync();
                }
            }

            var buckets = response.GroupBy(e => e.Bucket);

            foreach (var p in buckets)
                p.Key.RunDeleteActions([.. p]);
            
            foreach(var r in response)
                _dbContext.Remove(r);

            await _dbContext.SaveChangesAsync();

            return new(true, null);
        }
        catch (Exception ex)
        {
            return new(false, [
                $"An unexpected error occurred in ${nameof(DeleteResponsesAsync)}",
                ex.Message
            ]);
        }
    }

    public async Task<ActionResult> UpdateResponseTagsAsync(params DynamicFormResponse[] response)
    {
        try
        {
            await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

            foreach (var r in response)
            {
                if ((r.Bucket is null && r.BucketKey != Guid.Empty)
                    || (r.Bucket is not null && r.BucketKey != r.Bucket.Key))
                {
                    await _dbContext.Entry(r).Reference(e => e.Bucket).LoadAsync();
                }
            }

            var buckets = response.GroupBy(e => e.Bucket);

            foreach (var p in buckets)
                p.Key.RunTagUpdateActions([.. p]);

            await _dbContext.UpdateObjectTreeAsync(response, _dbContext.DynamicFormResponses);

            foreach (var r in response)
            {
                await _dbContext.Entry(r).Collection(e => e.Tags).LoadAsync();
            }

            return new(true, null);
        }
        catch (Exception ex)
        {
            return new(false, [
                $"An unexpected error occurred in ${nameof(UpdateResponseTagsAsync)}",
                ex.Message
            ]);
        }
    }

    public async Task<ActionResult> LoadResponsesForBucketAsync(DynamicFormBucket bucket)
    {
        try
        {
            await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

            var track = _dbContext.Update(bucket);
            await track.Reference(e => e.Form).LoadAsync();
            await track.Collection(e => e.Responses).LoadAsync();

            return new(true, null);
        }
        catch (Exception ex)
        {
            return new(false, new List<string> {
                $"An unexpected error occurred in ${nameof(LoadResponsesForBucketAsync)}",
                ex.Message
            });
        }
    }
}
