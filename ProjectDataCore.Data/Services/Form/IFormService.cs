﻿using ProjectDataCore.Data.Structures.Form;
using ProjectDataCore.Data.Structures.Form.Bucket;
using ProjectDataCore.Data.Structures.Page.Components.Parameters.Form;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Services.Form;
public interface IFormService
{
    #region Forms
    public Task<List<DynamicForm>> GetAllFormsAsync();
    public Task<ActionResult> UpdateOrAddFormAsync(DynamicForm form);
    public Task<ActionResult> DeleteFormAsync(DynamicForm form);
    public Task RefreshFormAsync(DynamicForm form);
    public Task<ActionResult<DynamicForm>> GetFormFromBucketKeyAsync(Guid bucketKey);
	#endregion

	#region Form Buckets
	public Task<ActionResult> LoadFormBucketsAsync(DynamicForm form);
    public Task<ActionResult> UpdateOrAddFormBucketAsync(DynamicFormBucket bucket);
    public Task<ActionResult> DeleteFormBucketAsync(DynamicFormBucket bucket);
    public Task<ActionResult> SetAsDefaultBucketAsync(DynamicFormBucket bucket);
	public Task RefreshBucketAsync(DynamicFormBucket bucket);
    public Task<ActionResult<DynamicFormBucket>> GetBucketFromIDAsync(Guid id);
    #endregion

    #region Form Actions
    public Task<ActionResult> UpdateOrAddBucketActionAsync(BucketAction action);
    public Task<ActionResult> DeleteBucketActionAsync(BucketAction action);
    #endregion

    //#region Form Buttons
    //public Task LoadButtonFormRelationsAsync(ButtonComponentSettings button);
    //#endregion
}
