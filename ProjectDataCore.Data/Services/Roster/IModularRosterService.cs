﻿using ProjectDataCore.Data.Account;
using ProjectDataCore.Data.Structures.Auth;
using ProjectDataCore.Data.Structures.Page.Components.Parameters;
using ProjectDataCore.Data.Structures.Result;
using ProjectDataCore.Data.Structures.Roster;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Services;

/// <summary>
/// A service for maintaining the module roster.
/// </summary>
public interface IModularRosterService
{
    #region Roster Tree
    public Task<ActionResult> RemoveRosterTreeAsync(Guid tree);
    public Task<ActionResult<Guid>> UpdateOrAddRosterTreeAsync(RosterTree editTree);
    #endregion

    #region Roster Position
    public Task<ActionResult> LoadExistingSlotsAsync(DataCoreUser activeUser);
    public Task<ActionResult> AssignUserToSlotAsync(DataCoreUser? user, RosterSlot slot);
    #endregion

    #region Roster Display Settings
    public Task<ActionResult> AddRosterDisplaySettingsAsync(string name, Guid host);
    public Task<ActionResult> RemoveRosterDisplaySettingsAsync(Guid settings);
    #endregion

    #region Get Roster Display
    public IAsyncEnumerable<bool> LoadFullRosterTreeAsync(RosterTree tree, List<DataCoreUser>? userList = null, bool includeDisplays = false);
    public Task<ActionResult> LoadRosterTreeAsync(RosterTree tree);
    public Task<ActionResult<RosterTree>> GetRosterTreeForSettingsAsync(Guid settings);
    public Task<ActionResult<RosterTree>> GetRosterTreeByIdAsync(Guid tree);
    public Task<ActionResult<List<RosterDisplaySettings>>> GetAvailableRosterDisplaysAsync();
    public Task<ActionResult<List<RosterTree>>> GetOrphanedRosterTreesAsync();
    public Task<ActionResult<List<RosterTree>>> GetTopLevelRosterTreesAsync();
    public Task<ActionResult<List<RosterTree>>> GetAllRosterTreesAsync();
    public Task<ActionResult<HashSet<RosterTree>>> GetLoadedRosterTreesForUser(DataCoreUser user);
    #endregion
}
