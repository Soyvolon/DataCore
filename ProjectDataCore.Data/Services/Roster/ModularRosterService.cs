﻿using Microsoft.EntityFrameworkCore;

using Npgsql;
using Npgsql.Replication;

using ProjectDataCore.Data.Account;
using ProjectDataCore.Data.Structures.Auth;
using ProjectDataCore.Data.Structures.Page.Components.Parameters;

using System.Collections.Concurrent;
using System.Runtime.Serialization;

namespace ProjectDataCore.Data.Services;

public class ModularRosterService : IModularRosterService
{
    private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;

    public ModularRosterService(IDbContextFactory<ApplicationDbContext> dbContextFactory)
        => (_dbContextFactory) = (dbContextFactory);

    #region Roster Tree
    public async Task<ActionResult> RemoveRosterTreeAsync(Guid tree)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        var rosterTree = await _dbContext.FindAsync<RosterTree>(tree);

        if (rosterTree is null)
            return new(false, new List<string> { "No roster tree was found." });

        _dbContext.Remove(rosterTree);

        await _dbContext.SaveChangesAsync();

        return new(true, null);
    }

    public async Task<ActionResult<Guid>> UpdateOrAddRosterTreeAsync(RosterTree update)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        if(update.Key == default)
        {
            await _dbContext.AddAsync(update);
        }
        else
        {
            var roster = await _dbContext.RosterTrees
                .Where(x => x.Key == update.Key)
                .OrderBy(x => x.Key)
                .AsNoTracking()
                .FirstOrDefaultAsync();

            Dictionary<Type, Dictionary<Guid, object>> oldElements = new();
            if (roster is not null)
            {
                var loader = LoadFullRosterTreeAsync(roster);
                await foreach (var _ in loader) { }

                oldElements = ApplicationDbContext.GetExistingElementTree(roster);
            }

            var newElements = ApplicationDbContext.GetExistingElementTree(update);

            _dbContext.Update(update);

            _dbContext.MarkDanglingObjectsForDelete(oldElements, newElements);
        }

        try
        {
            // ... finally, attempt a save...
            await _dbContext.SaveChangesAsync();

            return new(true, null, update.Key);
        }
        catch (Exception ex)
        {
            // ... if there was a violation of the unique constraint, then
            // return the error.
            return new(false, new List<string>() { "Failed to add or update the roster.", ex.Message });
        }
    }
    #endregion

    #region Roster Position

    public async Task<ActionResult> LoadExistingSlotsAsync(DataCoreUser activeUser)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        await _dbContext.Attach(activeUser)
            .Collection(e => e.RosterSlots)
            .Query()
            .Include(e => e.ParentRoster)
            .LoadAsync();

        return new(true, null);
    }

    public async Task<ActionResult> AssignUserToSlotAsync(DataCoreUser? user, RosterSlot slot)
    {
        try
        {
            await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

            var track = _dbContext.Update(slot);

            if (user is not null)
            {
                slot.OccupiedBy = null;
                slot.OccupiedById = user.Id;
            }
            else
            {
                slot.OccupiedBy = null;
                slot.OccupiedById = null;
            }

            await _dbContext.SaveChangesAsync();

            await track.ReloadAsync();

            return new(true);
        }
        catch (Exception ex)
        {
            return new(false, [$"An unexpected error occurred in {nameof(AssignUserToSlotAsync)}.", ex.Message]);
        }
    }
    #endregion

    #region Roster Display Settings
    public async Task<ActionResult> AddRosterDisplaySettingsAsync(string name, Guid host)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        var settings = new RosterDisplaySettings()
        {
            Name = name,
            HostRosterId = host
        };

        await _dbContext.AddAsync(settings);
        await _dbContext.SaveChangesAsync();

        return new(true, null);
    }

    public async Task<ActionResult> RemoveRosterDisplaySettingsAsync(Guid settings)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        var settingsObject = await _dbContext.FindAsync<RosterDisplaySettings>(settings);

        if (settingsObject is null)
            return new(false, new List<string> { "No settings object found for the provided ID" });

        _dbContext.Remove(settingsObject);
        await _dbContext.SaveChangesAsync();

        return new(true, null);
    }
    #endregion

    #region Get Roster Display
    public async IAsyncEnumerable<bool> LoadFullRosterTreeAsync(RosterTree tree, 
        List<DataCoreUser>? userList = null, bool includeDisplays = false)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        // attach the tree object...
        var obj = _dbContext.Attach(tree)
            ?? throw new MissingRosterTreeException("The base roster tree can not be loaded.");

        // ... and place the child rosters into a queue ...
        Queue<RosterTree> rosters = new();
        rosters.Enqueue(tree);

        // ... then for each roster ...
        while(rosters.TryDequeue(out RosterTree? roster))
        {
            obj = _dbContext.Entry(roster);

            // ... then load the roster positions ...
            await obj.Collection(e => e.RosterSlots)
                .Query()
                .OrderBy(e => e.Order.Order)
                .Include(e => e.AuthorizationTagBindings)
                .LoadAsync();

            // ... then add the loaded trooperes to the user list ...
            userList?.AddRange(roster.RosterSlots
                .Where(x => x.OccupiedBy is not null)
                .Select(x => x.OccupiedBy!));

            // ... let the page know a new roster has been loaded ...
            yield return true;

            // ... and load further child rosters ...
            await obj.Collection(e => e.ChildRosters).LoadAsync();

            // ... and if requested ...
            if (includeDisplays)
            {
                // ... load the displays ...
                await obj.Collection(e => e.DisplaySettings).LoadAsync();
            }

            // ... then load the roster tags ...
            await obj.Collection(e => e.AuthorizationTagBindings)
                .LoadAsync();

            // ... finally, enqueue them ...
            foreach (var t in roster.ChildRosters)
                rosters.Enqueue(t);
        }

        // ... then let the page know to refresh one more time ...
        yield return true;
    }

    public async Task<ActionResult> LoadRosterTreeAsync(RosterTree tree)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        var attach = _dbContext.Attach(tree);
        await attach.Collection(e => e.RosterSlots).LoadAsync();
        await attach.Collection(e => e.ChildRosters).LoadAsync();

        return new(true, null);
    }

    public async Task<ActionResult<RosterTree>> GetRosterTreeForSettingsAsync(Guid settings)
    {
        // TODO rework this to load from a roster tree object insated of requiring the component
        // to sort the values. (see the loading of a component tree in the RoutingService).

        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        var settingsObject = await _dbContext.RosterDisplaySettings
            .Where(x => x.Key == settings)
            .Include(x => x.HostRoster)
            .FirstOrDefaultAsync();

        if (settingsObject is null)
            return new(false, new List<string>() { "No settings object was found for the provided ID." });

        return new(true, null, settingsObject.HostRoster);
    }

    public async Task<ActionResult<List<RosterDisplaySettings>>> GetAvailableRosterDisplaysAsync()
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        var rosterDisplays = await _dbContext
            .RosterDisplaySettings
            .ToListAsync();

        if(rosterDisplays is null)
            return new(false, new List<string> { "Unable to get a roster display settings list."}, null);

        return new(true, null, rosterDisplays);
    }

	public async Task<ActionResult<List<RosterTree>>> GetOrphanedRosterTreesAsync()
	{
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        var trees = await _dbContext
            .RosterTrees
            .Include(x => x.ParentRosters)
            .Include(x => x.DisplaySettings)
            .Where(x => x.ParentRosters.Count <= 0)
            .Where(x => x.DisplaySettings.Count <= 0)
            .ToListAsync();

        if (trees is null)
            return new(false, new List<string> { "Unable to get a roster tree list." });

        return new(true, null, trees);
	}

	public async Task<ActionResult<List<RosterTree>>> GetTopLevelRosterTreesAsync()
	{
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        var trees = await _dbContext
            .RosterTrees
            .Include(x => x.ParentRosters)
            .Where(x => x.ParentRosters.Count <= 0)
            .ToListAsync();

        if (trees is null)
            return new(false, new List<string> { "Unable to get a roster tree list." });

        return new(true, null, trees);
    }

	public async Task<ActionResult<List<RosterTree>>> GetAllRosterTreesAsync()
	{
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        var trees = await _dbContext.RosterTrees.ToListAsync();

        if (trees is null)
            return new(false, new List<string> { "Unable to get a roster tree list." });

        return new(true, null, trees);
    }

    public async Task<ActionResult<RosterTree>> GetRosterTreeByIdAsync(Guid tree)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        var treeObject = await _dbContext.RosterTrees
            .Where(x => x.Key == tree)
            .FirstOrDefaultAsync();

        if (treeObject is null)
            return new(false, new List<string>() { "No roster tree object was found for the provided ID." });

        return new(true, null, treeObject);
    }

    public async Task<ActionResult<HashSet<RosterTree>>> GetLoadedRosterTreesForUser(DataCoreUser user)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();
        var slots = await _dbContext.Attach(user)
            .Collection(x => x.RosterSlots)
            .Query()
            .Include(x => x.ParentRoster)
            .ToListAsync();

        List<RosterTree> rootTrees = new();
        foreach (var slot in slots)
            rootTrees.Add(slot.ParentRoster);

        HashSet<RosterTree> trees = new();
        foreach(var tree in rootTrees)
        {
            var loader = LoadFullRosterTreeAsync(tree);
            while (await loader.GetAsyncEnumerator().MoveNextAsync()) { /*Loading the tree*/ }

            Stack<RosterTree> navStack = new();
            navStack.Push(tree);
            while (navStack.TryPop(out var curTree))
            {
                trees.Add(curTree);
                foreach (var child in curTree.ChildRosters)
                    navStack.Push(child);
            }
        }

        return new(true, null, trees);
    }
    #endregion
}

/// <summary>
/// Exception for when a roster tree can not be loaded.
/// </summary>
public class MissingRosterTreeException : Exception
{
    public MissingRosterTreeException() { }
    public MissingRosterTreeException(string? message) : base(message) { }
    public MissingRosterTreeException(string? message, Exception? innerException) : base(message, innerException) { }
    protected MissingRosterTreeException(SerializationInfo info, StreamingContext context) : base(info, context) { }
}