﻿using ProjectDataCore.Data.Account;
using ProjectDataCore.Data.Services.Account;
using ProjectDataCore.Data.Structures.Assignable;
using ProjectDataCore.Data.Structures.Assignable.Configuration;
using ProjectDataCore.Data.Structures.Assignable.Value;
using ProjectDataCore.Data.Structures.Form;
using ProjectDataCore.Data.Structures.Model.Assignable;
using ProjectDataCore.Data.Structures.Util.Interfaces;

using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Services.Roster;

public class AssignableDataService : IAssignableDataService
{
    private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;
    private readonly IAccountLinkService _accountLinkService;

    public AssignableDataService(IDbContextFactory<ApplicationDbContext> dbContextFactory,
        IAccountLinkService accountLinkService)
    {
        _dbContextFactory = dbContextFactory;
        _accountLinkService = accountLinkService;
    }

    public async Task<ActionResult> AddNewAssignableConfiguration(BaseAssignableConfiguration config)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        // Get the assignable configuration attribute for this config ...
        var configType = config.GetType();
        var attr = configType.GetCustomAttributes<AssignableConfigurationAttribute>()
            .FirstOrDefault();
        // ... and ensure it exists ...
        if (attr is null)
            return new(false, new List<string>() { "No assignable configuration attribute was found for the provided config." });

        // ... then use the attribute to set the type name ...
        config.TypeName = attr.Name;

        config.NormalizedPropertyName = config.PropertyName.Normalize();

        int curCount = 0;
        if (config.AssignableType == BaseAssignableConfiguration.InternalAssignableType.UserProperty)
        {
            curCount = await _dbContext.AssignableConfigurations
                .Where(x => x.NormalizedPropertyName == config.NormalizedPropertyName)
                .Where(x => x.AssignableType == BaseAssignableConfiguration.InternalAssignableType.UserProperty)
                .CountAsync();
        }
        else if (config.AssignableType == BaseAssignableConfiguration.InternalAssignableType.FormProperty)
        {
            curCount = await _dbContext.AssignableConfigurations
                .Where(x => x.NormalizedPropertyName == config.NormalizedPropertyName)
                .Where(x => x.AssignableType == BaseAssignableConfiguration.InternalAssignableType.FormProperty)
                .Where(x => x.FormKey == config.FormKey)
                .CountAsync();
        }

        if (curCount > 0)
            return new(false, new List<string>() { "A property of this name already exists." });

        // ... then save the new config to the database ...
        var res = _dbContext.Update(config);
        await _dbContext.SaveChangesAsync();

        List<BaseAssignableValue> valueObjects = new();

        // ... then restore the config object to include the new key ...
        await res.ReloadAsync();

        // ... and if this is updating a user ...
        if (config.AssignableType == BaseAssignableConfiguration.InternalAssignableType.UserProperty)
        {
            // ... then for all users ...
            var users = _dbContext.Users.AsAsyncEnumerable();
            await foreach (var user in users)
            {
                // ... create a new instance of the assignable value ...
                if (Activator.CreateInstance(attr.Configures) is BaseAssignableValue assignable)
                {
                    // ... set the links ...
                    assignable.AssignableConfigurationId = config.Key;
                    assignable.ForUserId = user.Id;
                    // ... then add it to the list of objects ...
                    valueObjects.Add(assignable);
                }
                else
                {
                    // ... if an error occours, immedietly exit ...
                    return new(false, new List<string>() { "Unable to create an assignable value from the provided configuration." });
                }
            }

            // ... then for each value object ...
            foreach (var i in valueObjects)
                // ... add it to the database ...
                await _dbContext.AddAsync(i);

			// ... and save changes ...
			await _dbContext.SaveChangesAsync();
		}

        // ... then reurn a success.
        return new(true, null);
    }

    public async Task<ActionResult> DeleteAssignableConfiguration(Guid configKey)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        // ... find the object ...
        var obj = await _dbContext.FindAsync<BaseAssignableConfiguration>(configKey);

        if (obj is null)
            return new(false, new List<string>() { "Configuration for the provided key was not found." });

        // ... then delete it ...
        _dbContext.Remove(obj);
        await _dbContext.SaveChangesAsync();

        return new(true, null);
    }

    public async Task<ActionResult<List<BaseAssignableConfiguration>>> GetAllAssignableConfigurationsAsyncForUsers()
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        var data = await _dbContext.AssignableConfigurations
            .Where(e => e.AssignableType == BaseAssignableConfiguration.InternalAssignableType.UserProperty)
            .ToListAsync();

        return new(true, null, data);
    }

    public async Task<ActionResult> UpdateAssignableConfiguration<T>(Guid configKey, Action<AssignableConfigurationEditModel<T>> update)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        // ... find the object ...
        var obj = await _dbContext.FindAsync<BaseAssignableConfiguration>(configKey);

        if (obj is null)
            return new(false, new List<string>() { "Configuration for the provided key was not found." });

        // ... then save the update information ...
        AssignableConfigurationEditModel<T> editModel = new();
        update.Invoke(editModel);

        if (editModel.AllowMultiple is not null)
            obj.AllowMultiple = editModel.AllowMultiple.Value;

        if(editModel.AllowedInput is not null)
            obj.AllowedInput = editModel.AllowedInput.Value;

        if (!string.IsNullOrWhiteSpace(editModel.PropertyName))
            obj.PropertyName = editModel.PropertyName;

        if (editModel.AllowedValues is not null)
        {
            // ... then find the configuration type ...
            switch (obj)
            {
#pragma warning disable CS8605 // Unboxing a possibly null value.
                case DateTimeValueAssignableConfiguration c:
                    c.AllowedValues = editModel.AllowedValues.ToList(x => (DateTime)Convert.ChangeType(x, typeof(DateTime)));
                    break;
                case DateOnlyValueAssignableConfiguration c:
                    c.AllowedValues = editModel.AllowedValues.ToList(x => (DateOnly)Convert.ChangeType(x, typeof(DateOnly)));
                    break;
                case TimeOnlyValueAssignableConfiguration c:
                    c.AllowedValues = editModel.AllowedValues.ToList(x => (TimeOnly)Convert.ChangeType(x, typeof(TimeOnly)));
                    break;


                case IntegerValueAssignableConfiguration c:
                    c.AllowedValues = editModel.AllowedValues.ToList(x => (int)Convert.ChangeType(x, typeof(int)));
                    break;
                case DoubleValueAssignableConfiguration c:
                    c.AllowedValues = editModel.AllowedValues.ToList(x => (double)Convert.ChangeType(x, typeof(double)));
                    break;


                case StringValueAssignableConfiguration c:
                    c.AllowedValues = editModel.AllowedValues.ToList(x => (string?)Convert.ChangeType(x, typeof(string)) ?? "");
                    break;
                
                case BooleanValueAssignableConfiguration c:
                    c.AllowedValues = editModel.AllowedValues.ToList(x => (bool)Convert.ChangeType(x, typeof(bool)));
                    break;
#pragma warning restore CS8605 // Unboxing a possibly null value.
            }
        }

        await _dbContext.SaveChangesAsync();

        return new(true, null);
    }

//    public async Task<ActionResult> UpdateAssignableValue<T>(Guid user, Guid config, List<T> value)
//    {
//        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

//        var assignable = await _dbContext.AssignableValues
//            .Where(x => x.ForUserId == user)
//            .Where(x => x.AssignableConfigurationId == config)
//            .Include(x => x.AssignableConfiguration)
//            .FirstOrDefaultAsync();

//        if (assignable is null)
//            return new(false, new List<string>() { "No assignable value was found." });

//        switch (assignable)
//        {
//#pragma warning disable CS8605 // Unboxing a possibly null value.
//            case DateTimeAssignableValue c:
//                if (c.SetValue.GetType() != value.GetType())
//                    return new(false, new List<string>() { "Assignable value type missmatch." });

//                c.SetValue = value.ToList(x => (DateTime)Convert.ChangeType(x, typeof(DateTime)));
//                break;
//            case DateOnlyAssignableValue c:
//                if (c.SetValue.GetType() != value.GetType())
//                    return new(false, new List<string>() { "Assignable value type missmatch." });

//                c.SetValue = value.ToList(x => (DateOnly)Convert.ChangeType(x, typeof(DateOnly)));
//                break;
//            case TimeOnlyAssignableValue c:
//                if (c.SetValue.GetType() != value.GetType())
//                    return new(false, new List<string>() { "Assignable value type missmatch." });

//                c.SetValue = value.ToList(x => (TimeOnly)Convert.ChangeType(x, typeof(TimeOnly)));
//                break;


//            case IntegerAssignableValue c:
//                if (c.SetValue.GetType() != value.GetType())
//                    return new(false, new List<string>() { "Assignable value type missmatch." });

//                c.SetValue = value.ToList(x => (int)Convert.ChangeType(x, typeof(int)));
//                break;
//            case DoubleAssignableValue c:
//                if (c.SetValue.GetType() != value.GetType())
//                    return new(false, new List<string>() { "Assignable value type missmatch." });

//                c.SetValue = value.ToList(x => (double)Convert.ChangeType(x, typeof(double)));
//                break;


//            case StringAssignableValue c:
//                if (c.SetValue.GetType() != value.GetType())
//                    return new(false, new List<string>() { "Assignable value type missmatch." });

//                c.SetValue = value.ToList(x => (string?)Convert.ChangeType(x, typeof(string)) ?? "");
//                break;
            
//            case BooleanAssignableValue c:
//                if (c.SetValue.GetType() != value.GetType())
//                    return new(false, new List<string>() {"Assignable value type mismatch"});

//                c.SetValue = value.ToList(x => (bool)Convert.ChangeType(x, typeof(bool)));
//                break;
//#pragma warning restore CS8605 // Unboxing a possibly null value.
//        }

//        await _dbContext.SaveChangesAsync();

//        return new(true, null);
//    }

    public async Task<ActionResult> EnsureAssignableValuesAsync<T, TKey>(T assignableObj)
        where T : IAssignable, IKeyable<TKey>
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        // Reload the assignable values.
        await _dbContext.Attach(assignableObj as IAssignable)
            .Collection(e => e.AssignableValues)
            .LoadAsync();

        var res = await EnsureAssignableValuesAsync<T, TKey>(assignableObj, true, _dbContext);

        if (!res.GetResult(out var err))
            return res;

        await _dbContext.SaveChangesAsync();

        await _dbContext.Entry(assignableObj).ReloadAsync();

        return new(true, null);
    }

    public async Task<ActionResult<T>> GetMockAssignable<T, TKey>()
        where T : IAssignable, IKeyable<TKey>
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        var assignable = Activator.CreateInstance<T>();

        var res = await EnsureAssignableValuesAsync<T, TKey>(assignable, false, _dbContext);

        if (!res.GetResult(out var err))
            return new(false, err, default);

        return new(true, null, assignable);
    }

    private async Task<ActionResult> EnsureAssignableValuesAsync<T, TKey>(T assignableObj, bool doAdd, ApplicationDbContext _dbContext)
        where T : IAssignable, IKeyable<TKey>
    {
        var keyMap = new HashSet<Guid>();
        foreach (var i in assignableObj.AssignableValues)
            keyMap.Add(i.AssignableConfigurationId);

        var assignType = assignableObj.GetInternalAssignableType();

        // Validate that there are no values missing ...
        var query = _dbContext.AssignableConfigurations
            .Where(x => !keyMap.Contains(x.Key))
            .Where(x => x.AssignableType == assignType);

        if (assignType == BaseAssignableConfiguration.InternalAssignableType.FormProperty)
        {
            var keyMatch = (assignableObj as IKeyable<Guid>)?.GetKey();
            query = query.Where(e => e.FormKey == keyMatch);
        }

        var missing = await query.ToListAsync();

        // ... then get the account settings ...
        var accountSettings = await _accountLinkService.GetLinkSettingsAsync();

        // ... then for each value that is missing ...
        foreach (var val in missing)
        {
            // ... get the attribute information ...
            var attr = val.GetType()
                .GetCustomAttributes<AssignableConfigurationAttribute>()
                .FirstOrDefault();

            if (attr is null)
                continue;

            // ... create the assignable value ...
            if (Activator.CreateInstance(attr.Configures) is BaseAssignableValue assignable)
            {
                // ... set the links ...
                assignable.AssignableConfigurationId = val.Key;
                if (!doAdd)
                    assignable.AssignableConfiguration = val;

                if (assignType == BaseAssignableConfiguration.InternalAssignableType.UserProperty)
                {
                    var key = (assignableObj as IKeyable<Guid>)?.GetKey();
                    assignable.ForUserId = key;

                    // ... clear any existing links ...
                    assignable.UserSteamLink = null;
                    assignable.UserSteamLinkKey = null;

                    assignable.UserDiscordLink = null;
                    assignable.UserDiscordLinkKey = null;

                    assignable.UserAccessCode = null;
                    assignable.UserAccessCodeKey = null;

                    // ... and set them back up ...
                    if (val.Key == accountSettings.SteamLinkConfigurationKey)
                        assignable.UserSteamLinkKey = key;
                    else if (val.Key == accountSettings.DiscordLinkConfigurationKey)
                        assignable.UserDiscordLinkKey = key;
                    else if (val.Key == accountSettings.AccessCodeConfigurationKey)
                        assignable.UserAccessCodeKey = key;
                }
                else if (assignType == BaseAssignableConfiguration.InternalAssignableType.FormProperty)
                {
                    assignable.FormResponseKey = (assignableObj as IKeyable<Guid>)?.GetKey();
                }

                if (doAdd)
                {
                    // ... then add it to the list of objects ...
                    await _dbContext.AddAsync(assignable);
                }
                else
                {
                    assignableObj.AssignableValues.Add(assignable);
                    
                    // try assign values
                    assignable.ForUser = assignableObj as DataCoreUser;
                    assignable.FormResponse = assignableObj as DynamicFormResponse;
                }
            }
            else
            {
                // ... if an error occours, immedietly exit ...
                return new(false, new List<string>() { "Unable to create an assignable value from the provided configuration." });
            }
        }

        return new(true, null);
    }

    //public async Task<ActionResult<DataCoreUser>> GetMockUserWithAssignablesAsync()
    //{
    //    await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

    //    // Validate that there are no values missing ...
    //    var missing = await _dbContext.AssignableConfigurations
    //        .Where(x => x.AssignableType == BaseAssignableConfiguration.InternalAssignableType.UserProperty)
    //        .ToListAsync();

    //    DataCoreUser mock = new();
    //    // ... then for each value that is missing ...
    //    foreach (var val in missing)
    //    {
    //        // ... get the attribute information ...
    //        var attr = val.GetType().GetCustomAttributes<AssignableConfigurationAttribute>()
    //            .FirstOrDefault();

    //        if (attr is null)
    //            continue;

    //        // ... create the assignable value ...
    //        if (Activator.CreateInstance(attr.Configures) is BaseAssignableValue assignable)
    //        {
    //            assignable.AssignableConfiguration = val;
    //            // ... set the links ...
    //            mock.AssignableValues.Add(assignable);
    //        }
    //        else
    //        {
    //            // ... if an error occours, immedietly exit ...
    //            return new(false, new List<string>() { "Unable to create an assignable value from the provided configuration." });
    //        }
    //    }

    //    return new(true, null, mock);
    //}
}