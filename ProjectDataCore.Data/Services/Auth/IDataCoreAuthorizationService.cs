﻿using ProjectDataCore.Data.Structures.Auth;
using ProjectDataCore.Data.Structures.Auth.Tags;
using ProjectDataCore.Data.Structures.Routing;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Services.Auth;
public interface IDataCoreAuthorizationService
{
    public Task<List<AuthorizationArea>> GetAllAuthorizationAreasAsync();
    public Task<ActionResult> UpdateOrAddAuthorizationAreaAsync(AuthorizationArea area);
    public Task<ActionResult> DeleteAuthorizationAreaAsync(AuthorizationArea area);
    public Task<bool> AuthorizeRouteRootAsync(DataCoreUser? user, RouteRoot root);

    #region Tags
    public Task<List<AuthorizationTag>> GetAllTagsAsync();
    public Task ReloadTagAsync(AuthorizationTag tag);
    public Task LoadAllTagsAsync(IEnumerable<AuthorizationTagTreeSlotUserBinding> bindings);
    public Task<ActionResult> UpdateOrAddTagAsync(AuthorizationTag tag);
    public Task<ActionResult> DeleteTagAsync(AuthorizationTag tag);
    #endregion

    #region Roster/Tag Bindings
    /// <summary>
    /// Gets all configured roster tag permissions on the site.
    /// </summary>
    /// <returns>A <see cref="List{T}"/> of configured <see cref="RosterTagPermission"/>s.</returns>
    public Task<List<RosterTagPermission>> GetAllRosterTagPermissionsAsync();
    /// <summary>
    /// Loads the whitelist for a tag permissions.
    /// </summary>
    /// <param name="permission">The permissions to load.</param>
    /// <returns>A task for this action.</returns>
    public Task LoadWhitelistForTagPermissionsAsync(RosterTagPermission permission);
    /// <summary>
    /// Creates a new permission from an existing tag.
    /// </summary>
    /// <param name="authorizationTag">The tag to create a permission for.</param>
    /// <returns>An <see cref="ActionResult{T}"/> with the newly created <see cref="RosterTagPermission"/>.</returns>
    public Task<ActionResult<RosterTagPermission>> CreatePermissionFromTagAsync(AuthorizationTag authorizationTag);
    /// <summary>
    /// Deletes a roster permission.
    /// </summary>
    /// <param name="permission">The permission to delete.</param>
    /// <returns>An <see cref="ActionResult"/> for this action.</returns>
    public Task<ActionResult> DeleteRosterPermissionAsync(RosterTagPermission permission);
    /// <summary>
    /// Updates an existing roster tag permission.
    /// </summary>
    /// <param name="permission">The permission to update.</param>
    /// <returns>A <see cref="ActionResult"/> for this action.</returns>
    public Task<ActionResult> UpdateRosterTagPermissionAsync(RosterTagPermission permission);
    #endregion
}
