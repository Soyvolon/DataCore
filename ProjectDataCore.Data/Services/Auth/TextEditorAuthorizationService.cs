﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Services.Auth;
public class TextEditorAuthorizationService : ITextEditorAuthorizationService
{
    private ConcurrentDictionary<string, TextEditAuthorizationType> Tokens { get; set; } = new();

    public bool CheckToken(string token, TextEditAuthorizationType scope)
    {
        if (Tokens.TryGetValue(token, out var registeredScope))
            return scope == registeredScope;

        return false;
    }

    public void RegisterToken(string token, TextEditAuthorizationType scope)
    {
        _ = Tokens.TryAdd(token, scope);
    }

    public void UnregisterToken(string token)
    {
        _ = Tokens.TryRemove(token, out _);
    }
}
