﻿using Microsoft.AspNetCore.Identity.UI;

using ProjectDataCore.Data.Services.User;
using ProjectDataCore.Data.Structures.Auth;
using ProjectDataCore.Data.Structures.Auth.Tags;
using ProjectDataCore.Data.Structures.Routing;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Services.Auth;
public class DataCoreAuthorizationService : IDataCoreAuthorizationService
{
    private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;

    public DataCoreAuthorizationService(IDbContextFactory<ApplicationDbContext> dbContextFactory)
    {
        _dbContextFactory = dbContextFactory;
    }

    public async Task<ActionResult> UpdateOrAddAuthorizationAreaAsync(AuthorizationArea area)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        var attach = _dbContext.Update(area);

        await _dbContext.SaveChangesAsync();

        await attach.ReloadAsync();

        return new(true, null);
    }

    public async Task<ActionResult> DeleteAuthorizationAreaAsync(AuthorizationArea area)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        _dbContext.Remove(area);

        await _dbContext.SaveChangesAsync();

        return new(true, null);
    }

    public async Task<List<AuthorizationArea>> GetAllAuthorizationAreasAsync()
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        return await _dbContext.AuthorizationAreas
            .Include(e => e.FullTrees)
            .Include(e => e.RosterTrees)
            .Include(e => e.RosterSlots)
            .Include(e => e.Users)
            .ToListAsync();
    }

    public async Task<bool> AuthorizeRouteRootAsync(DataCoreUser? user, RouteRoot? root)
    {
        if (root is null)
            return true;

        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        var checks = _dbContext.AuthorizationAreas
            .Where(e => root.OnNavigationChecks.Contains(e.Key))
            .AsAsyncEnumerable();

        await foreach(var check in checks)
        {
            if (!AuthorizationArea.Authorize(user, check))
                return false;
        }

        return true;
    }

    #region Tags
    public async Task<List<AuthorizationTag>> GetAllTagsAsync()
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        return await _dbContext
            .AuthorizationTags
            .OrderBy(e => e.Name)
            .ToListAsync();
    }

    public async Task ReloadTagAsync(AuthorizationTag tag)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        var tracker = _dbContext.Attach(tag);
        await tracker.ReloadAsync();
    }

    public async Task LoadAllTagsAsync(IEnumerable<AuthorizationTagTreeSlotUserBinding> bindings)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        // ensure unique values.
        foreach (var b in bindings.ToHashSet())
        {
            await _dbContext.Attach(b).Reference(e => e.Tag).LoadAsync();
        }
    }

    public async Task<ActionResult> UpdateOrAddTagAsync(AuthorizationTag tag)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        if (await _dbContext.AuthorizationTags
            .Where(e => e.Key != tag.Key)
            .Where(e => e.Name == tag.Name)
            .AnyAsync())
            return new(false, ["A tag with this name already exists."]);

        var tracker = _dbContext.Update(tag);

        await _dbContext.SaveChangesAsync();

        await tracker.ReloadAsync();

        return new(true);
    }

    public async Task<ActionResult> DeleteTagAsync(AuthorizationTag tag)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        _dbContext.Remove(tag);

        await _dbContext.SaveChangesAsync();

        return new(true);
    }
    #endregion

    #region Roster/Tag Bindings
    ///<inheritdoc/>
    public async Task<List<RosterTagPermission>> GetAllRosterTagPermissionsAsync()
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        return await _dbContext
            .RosterTagPermissions
            .OrderBy(e => e.AuthorizationTag!.Name)
            .ToListAsync();
    }

    ///<inheritdoc/>
    public async Task LoadWhitelistForTagPermissionsAsync(RosterTagPermission permission)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        await _dbContext.Attach(permission)
            .Collection(e => e.OnlyValidForTags)
            .LoadAsync();
    }

    ///<inheritdoc/>
    public async Task<ActionResult<RosterTagPermission>> CreatePermissionFromTagAsync(AuthorizationTag authorizationTag)
    {
        try
        {
            await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

            if (await _dbContext
                .RosterTagPermissions
                .AnyAsync(e => e.AuthorizationTagKey == authorizationTag.Key))
                return new(false, ["The provided tag already has a permission created for it."]);

            var perm = new RosterTagPermission()
            {
                AuthorizationTag = authorizationTag
            };

            _dbContext.Update(perm);

            await _dbContext.SaveChangesAsync();

            return new(true, null, perm);
        }
        catch (Exception ex)
        {
            return new(false, [$"An unexpected error occurred in {nameof(CreatePermissionFromTagAsync)}.", ex.Message]);
        }
    }

    ///<inheritdoc/>
    public async Task<ActionResult> DeleteRosterPermissionAsync(RosterTagPermission permission)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        _dbContext.Remove(permission);

        await _dbContext.SaveChangesAsync();

        return new(true);
    }

    ///<inheritdoc/>
    public async Task<ActionResult> UpdateRosterTagPermissionAsync(RosterTagPermission permission)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        if (permission.Key == default)
            return new(false, ["The permission can not be updated because it has not been added to the database."]);

        _dbContext.Update(permission);

        await _dbContext.SaveChangesAsync();

        return new(true);
    }
    #endregion
}
