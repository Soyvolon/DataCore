﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Services.Auth;
public interface ITextEditorAuthorizationService
{
    public void RegisterToken(string token, TextEditAuthorizationType scope);
    public void UnregisterToken(string token);
    public bool CheckToken(string token, TextEditAuthorizationType scope);
}

public enum TextEditAuthorizationType
{
    ImageUpload
}