﻿using ProjectDataCore.Data.Structures.Nav;
using ProjectDataCore.Data.Structures.Page;

namespace ProjectDataCore.Data.Services.Nav;

public interface INavModuleService
{
    public Task<ActionResult> CreateNavModuleAsync(NavModule module);
    public Task<List<NavModule>> GetAllModules();
    public Task<List<NavModule>> GetAllModulesWithChildren();
    public Task<NavModule> GetNavModuleFromPage(CustomPageSettings page);
    public Task<ActionResult> UpdateNavModuleAsync(NavModule navModule);
    public Task<ActionResult> DeleteNavModule(Guid key);
    public Task<ActionResult> RecalculateSortOrder(NavModule navModule, bool recursive = false);
    public Task<ActionResult> SortChildren(NavModule navModule, bool recursive = false);
}