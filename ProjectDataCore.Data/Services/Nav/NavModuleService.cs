﻿using ProjectDataCore.Data.Structures.Nav;
using ProjectDataCore.Data.Structures.Page;

namespace ProjectDataCore.Data.Services.Nav;

public class NavModuleService : INavModuleService
{
    private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;

    public NavModuleService(IDbContextFactory<ApplicationDbContext> dbContextFactory) => _dbContextFactory = dbContextFactory;
    
    public async Task<List<NavModule>> GetAllModules()
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();
        return await _dbContext.NavModules.ToListAsync();
    }

    public async Task<List<NavModule>> GetAllModulesWithChildren()
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();
        var modules =  await _dbContext.NavModules
            .Where(e => e.ParentId == null)
            .ToListAsync();
         foreach(var module in modules)
         {
             await LoadNavModule(module);
             await SortChildren(module);
         }
        return modules;
    }

    public async Task<NavModule> GetNavModuleFromPage(CustomPageSettings page)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();
        return await _dbContext.NavModules.Where(e => e.Page == page).FirstOrDefaultAsync();
    }

    private async Task LoadNavModule(NavModule item)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();
        var obj = _dbContext.Attach(item);
        await obj.Collection(e => e.SubModules)
            .LoadAsync();

        Queue<NavModule> navModules = new();
        foreach (var t in item.SubModules)
            navModules.Enqueue(t);

        while (navModules.TryDequeue(out var module))
        {
            obj = _dbContext.Entry(module);
            await obj.Collection(e => e.SubModules)
                .LoadAsync();

            foreach (var t in module.SubModules)
                navModules.Enqueue(t);
        }
    }

    public async Task<ActionResult> CreateNavModuleAsync(NavModule module)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        try
        {
            // if (module.PageId != null)
            //     module.Href = (await _dbContext.CustomPageSettings.FindAsync(module.PageId))!.RouteRoot.FullRoute;
            _dbContext.Add(module);
            await _dbContext.SaveChangesAsync();
            return new(true);
        }
        catch (Exception ex)
        {
            return new(false, new() { ex.Message });
        }
    }

    public async Task<ActionResult> DeleteNavModule(Guid key)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();
        NavModule module = await _dbContext.NavModules.FindAsync(key);
        if (module.StaticPage)
            return new ActionResult(false, new(){"Cannot delete static pages."});
        await LoadNavModule(module);
        if (module.SubModules.Any())
            foreach (var subModule in module.SubModules)
            {
                _dbContext.Remove(subModule);
            }
        _dbContext.Remove(module);
        _dbContext.SaveChanges();
        return new(true);
    }

    public async Task<ActionResult> RecalculateSortOrder(NavModule navModule, bool recursive = false)
    {
        if (navModule.SubModules.Count == 0)
            return new(false, new(){"No Children"});
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();
        _dbContext.Attach(navModule);
        navModule.SubModules.Sort((a,b) => a.Order.CompareTo(b.Order));
        for (int i = 0; i < navModule.SubModules.Count; i++)
        {
            navModule.SubModules[i].Order = i;
        }

        await _dbContext.SaveChangesAsync();
        return new(true);
    }

    public async Task<ActionResult> SortChildren(NavModule navModule, bool recursive = false)
    {
        if (navModule.SubModules.Count == 0)
            return new(false, new() { "No Children" });
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();
        _dbContext.Attach(navModule);
        navModule.SubModules.Sort((a,b) => a.Order.CompareTo(b.Order));
        await _dbContext.SaveChangesAsync();
        return new(true);
    }

    public async Task<ActionResult> UpdateNavModuleAsync(NavModule navModule)
    {
        if (navModule == null)
        {
            return new(false, new() {"Parameter is null"});
        }
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();
        NavModule module = await _dbContext.FindAsync<NavModule>(navModule.Key);
        if (module is null)
        {
            return new(false, new() { "Module not found" });
        }
        if (module.HasMainPage != navModule.HasMainPage)
            module.HasMainPage = navModule.HasMainPage;
        if (module.ParentId != navModule.ParentId)
            module.ParentId = navModule.ParentId;
        if (module.DisplayName != navModule.DisplayName)
            module.DisplayName = navModule.DisplayName;
        if (module.LinkToPage != navModule.LinkToPage)
            module.LinkToPage = navModule.LinkToPage;

        if (module.LinkToPage)
        {
            if (module.PageId != navModule.PageId)
                module.PageId = navModule.PageId;
            if (module.DisplayName != navModule.Page.Name)
                module.DisplayName = navModule.Page.Name;
            module.Href = null;
        }
        else
        {
            if (module.Href != navModule.Href)
                module.Href = navModule.Href;
        }
        
        await _dbContext.SaveChangesAsync();
        return new(true);
    }
}