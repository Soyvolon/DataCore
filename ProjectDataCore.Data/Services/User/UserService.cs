﻿using Microsoft.AspNetCore.Identity;

using ProjectDataCore.Data.Account;
using ProjectDataCore.Data.Services.Roster;
using ProjectDataCore.Data.Structures.Model.User;
using ProjectDataCore.Data.Structures.Page.Components;
using ProjectDataCore.Data.Structures.Page.Components.Layout;
using ProjectDataCore.Data.Structures.Util;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Services.User;

public class UserService : IUserService
{
    private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;
	private readonly IAssignableDataService _assignableDataService;
    private readonly UserManager<DataCoreUser> _userManager;

	public UserService(IDbContextFactory<ApplicationDbContext> dbContextFactory, 
		IAssignableDataService assignableDataService, UserManager<DataCoreUser> userManager)
		=> (_dbContextFactory, _assignableDataService, _userManager)
		=  (dbContextFactory, assignableDataService, userManager);

    public async Task<ActionResult> CreateOrUpdateAccountAsync(string? accessCode, string username, string password)
	{
		DataCoreUser user;
		if (accessCode is null)
		{
			user = new()
			{
				UserName = username,
			};

			var res = await _userManager.CreateAsync(user, password);

			if (!res.Succeeded)
				return new(false, res.Errors.ToList().ToList(x => x.Description));

			user = await _userManager.FindByNameAsync(user.UserName);

			await _assignableDataService.EnsureAssignableValuesAsync<DataCoreUser, Guid>(user);

			return new(true, null);
		}
		else
        {
			user = await _userManager.FindByNameAsync(username);

			// if user is not null and access code is null
			if (user is null || user.AccessCode is not null)
			{
				// TODO change to read account settings instead of access code status.
				if (user is null || user.AccessCode?.GetValue() as string != accessCode)
					return new(false, new List<string>() { "Access codes did not match." });
			}

			var res = await _userManager.RemovePasswordAsync(user);
			if (!res.Succeeded)
				return new(false, res.Errors.ToList().ToList(x => x.Description));


			res = await _userManager.AddPasswordAsync(user, password);
			if (!res.Succeeded)
				return new(false, res.Errors.ToList().ToList(x => x.Description));


			res = await _userManager.SetUserNameAsync(user, username);
			if (!res.Succeeded)
				return new(false, res.Errors.ToList().ToList(x => x.Description));

			user.AccessCode?.ClearValue();

			res = await _userManager.UpdateAsync(user);
			if (!res.Succeeded)
				return new(false, res.Errors.ToList().ToList(x => x.Description));


			return new(true, null);
		}
    }

    public async Task<ActionResult> CreateUserAsync(DataCoreUser user, Action<DataCoreUserEditModel> model)
    {
		var token = Guid.NewGuid().ToString();

		if(string.IsNullOrWhiteSpace(user.AccessCode?.GetValue() as string))
			user.AccessCode?.ReplaceValue(token);

		if(string.IsNullOrWhiteSpace(user.UserName))
			user.UserName = token;

		user.AssignableValues.Clear();

		var res = await _userManager.CreateAsync(user, token);

		if(res.Succeeded)
        {
			user = (await _userManager.FindByNameAsync(user.UserName))!;

			var updateRes = await _assignableDataService.EnsureAssignableValuesAsync<DataCoreUser, Guid>(user);

			if (updateRes.GetResult(out _))
			{
				updateRes = await UpdateUserAsync(user.Id, model);
			}

			return updateRes;
        }
		else
        {
			var err = new List<string>() { "Failed to create a new user account." };
			err.AddRange(res.Errors.ToList().ToList(x => x.Description));
			return new(false, err);
        }
    }

    public async Task<List<DataCoreUser>> GetAllUnregisteredUsersAsync()
    {
		await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

		return await _dbContext.Users
			.Where(x => !x.Registered)
			.Include(x => x.AssignableValues)
			.ThenInclude(x => x.AssignableConfiguration)
			.ToListAsync();
    }

    public async Task<DataCoreUser> GetDefaultUserAsync()
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        return await _dbContext.Users
			.Where(e => e.Default)
			.FirstAsync();
    }

    public async Task<List<DataCoreUser>> GetAllUsersAsync()
    {
		await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

		return await _dbContext.Users.ToListAsync();
    }

    public async Task<DataCoreUser?> GetUserFromClaimsPrinciaplAsync(ClaimsPrincipal claims)
	{
		_ = Guid.TryParse(_userManager.GetUserId(claims), out Guid id);
		return await GetUserFromIdAsync(id);
	}

	public async Task<DataCoreUser?> GetUserFromIdAsync(Guid id)
	{
		await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

		var user = await _dbContext.Users.Where(e => e.Id == id)
			.Include(e => e.CurrentlyImpersonating)
			.FirstOrDefaultAsync();

		return user;
	}

    public async Task<ActionResult> UpdateUserAsync(Guid user, Action<DataCoreUserEditModel> action)
    {
		await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

		var userData = await _dbContext.FindAsync<DataCoreUser>(user);

		if (userData is null)
			return new(false, new List<string>() { "No user was found for the provided ID." });

		DataCoreUserEditModel model = new();
		action.Invoke(model);

		try
		{
			model.ApplyStaticValues(userData);
		}
		catch (Exception ex)
        {
			return new(false, new List<string>() { "Failed to apply static value updates.", ex.Message });
        }

		foreach (var item in model.AssignableValues)
        {
			var property = userData.AssignableValues
				.Find(x => x.AssignableConfiguration.PropertyName == item.Key);

			if(property is not null && item.Value is not null)
				property.ReplaceValue(item.Value);
        }

		if(model.Slots is not null)
		{
			foreach(var slot in model.Slots)
			{
				_dbContext.Attach(slot);

				slot.OccupiedById = userData.Id;
			}
		}

		await _dbContext.SaveChangesAsync();

		return new(true, null);
    }

	public async Task<ActionResult> ImpersonateUser(DataCoreUser user, DataCoreUser toImpersonate)
	{
		await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();
		_dbContext.Attach(user);

		user.ImpersonateUser(toImpersonate);

		await _dbContext.SaveChangesAsync();

		return new(true, null);
	}

	public async Task<ActionResult> StopImpersonating(DataCoreUser user)
	{
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();
        _dbContext.Attach(user);

        user.StopImpersonating();

        await _dbContext.SaveChangesAsync();

        return new(true, null);
    }
}
