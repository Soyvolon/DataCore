﻿using ProjectDataCore.Data.Account;
using ProjectDataCore.Data.Structures.Events.Parameters;
using ProjectDataCore.Data.Structures.Keybindings;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Services.User;
public interface ILocalUserService : IDisposable
{
    public DataCoreUser? LocalUser { get; protected set; }

    public Task InitalizeAsync(Guid userId);
    public Task<bool> InitalizeIfDeinitalizedAsync(Guid userId);
    public void Deinitalize();
    public void DeinitalizeIfInitalized();
    public Dictionary<OnPressEventArgs, Keybinding> GetCustomKeybindings();
    public Task ReloadKeybindingsAsync();
}
