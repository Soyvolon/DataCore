﻿using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;

using ProjectDataCore.Data.Structures.Auth;
using ProjectDataCore.Data.Structures.Page;
using ProjectDataCore.Data.Structures.Page.Components;
using ProjectDataCore.Data.Structures.Page.Components.Layout;
using ProjectDataCore.Data.Structures.Routing;

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Services.Routing;

public class RoutingService : IRoutingService
{
    public class RoutingServiceSettings
    {
        public Assembly ComponentAssembly { get; init; }

        public RoutingServiceSettings(Assembly componentAsm)
            => ComponentAssembly = componentAsm;
    }

    private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;

    private ConcurrentDictionary<PageComponentListing, Type> ComponentCache { get; init; }
    private Assembly ComponentAssembly { get; init; }

    public RoutingService(RoutingServiceSettings settings, IDbContextFactory<ApplicationDbContext> dbContextFactory)
    {
        _dbContextFactory = dbContextFactory;

        ComponentCache = new ConcurrentDictionary<PageComponentListing, Type>();
        ComponentAssembly = settings.ComponentAssembly;
    }

	#region Listings
	public void ReloadCache()
    {
        ComponentCache.Clear();
        foreach(PageComponentListing val in Enum.GetValues(typeof(PageComponentListing)))
        {
            var t = LookupListingType(val);
            if (t is not null)
            {
                if (!ComponentCache.TryAdd(val, t))
                {
                    throw new Exception($"The component listing {val} was used more than once. " +
                        $"A component listing can only be used one time. Make a new component " +
                        $"listing if needed or change existing listing values.");
                }
            }
        }
    }

    public Type GetComponentType(PageComponentListing listing, bool forceUpdate = false)
    {
        // If there is an update requested of the item is not in the cache...
        if (forceUpdate || !ComponentCache.TryGetValue(listing, out Type? type))
        {
            // ... then lookup the listing type ...
            type = LookupListingType(listing);
            // ... if it does not exist, throw an error ...
            if (type is null)
                throw new MissingComponentException("The provided listing was not found in the components assembly.");
            // ... otherwise add it to the cache ...
            ComponentCache[listing] = type;
        }
        // ... then return the type.
        return type;
    }

    private Type? LookupListingType(PageComponentListing listing)
    {
        foreach (var t in ComponentAssembly.GetTypes())
        {
            var attr = t.GetCustomAttribute<BaseComponentAttribute>();
			if (attr is not null 
                && attr.PageComponentListing == listing)
            {
                return t;
            }
        }

        return null;
    }
	#endregion

	public async Task<CustomPageSettings?> GetPageSettingsFromRouteAsync(string route)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();
        // Find the page settings for the provided route.
        var pageSettings = await _dbContext.CustomPageSettings
            .Where(x => x.RouteRoot.FullRoute == route)
            .FirstOrDefaultAsync();

        return pageSettings;
    }

    public async IAsyncEnumerable<bool> LoadPageSettingsAsync(CustomPageSettings settings, ApplicationDbContext? context = null)
    {
        var _dbContext = context;
        try
        {
            // If we dont have a context, get one.
            _dbContext ??= await _dbContextFactory.CreateDbContextAsync();

            // attach the settings object...
            var obj = _dbContext.Attach(settings);

            // ... then load the base layout ...
            await obj.Reference(e => e.Layout).LoadAsync();

            // ... then load the scopes ...
            await obj.Collection(e => e.AssignableScopes).LoadAsync();
            // ... and component pools ...
            await obj.Collection(e => e.ComponentPools).LoadAsync();

            // ... if the layout is not null ...
            if (settings.Layout is not null)
            {
                // ... yield the first value so the display can be refreshed ...
                yield return true;

                // ... then the base layout to the level queue ...
                Queue<LayoutNode> level = new();
                level.Enqueue(settings.Layout);

                // ... and while we have level data ...
                while (level.TryDequeue(out var levelItem))
                {
                    // ... attach the level item ...
                    var layoutObj = _dbContext.Attach(levelItem);

                    // ... and load the levels component/parent ...
                    await layoutObj.Reference(e => e.Component)
                        .Query()
                        .Include(e => e.ScopeProviders)
                        .Include(e => e.ScopeListeners)
                        .Include(e => e.ComponentPools)
                        .LoadAsync();

                    await layoutObj.Reference(e => e.ParentNode)
                        .LoadAsync();

                    // ... notify a load has completed ...
                    yield return true;

                    // ... and load its direct children ...
                    var children = layoutObj.Collection(x => x.Nodes)
                        .Query()
                        .AsAsyncEnumerable();

                    // ... then for each child ...
                    await foreach (var child in children)
                    {
                        // ... notify that it has been loaded ...
                        yield return true;

                        // ... and if the child is a layout component, enqueue it.
                        if (child is LayoutNode childLayout)
                            level.Enqueue(childLayout);
                    }
                }
            }

            // return one last time for loading purposes.
            yield return true;
        }
        finally
        {
            // Only dispose of this if its not a provided context.
            if (context is null && _dbContext is not null)
                await _dbContext.DisposeAsync();
        }
    }

    #region Route Root
    public async Task<ActionResult<RouteRoot>> AddRouteRootAsync(string routeUrl)
    {
        var allRootsRes = await GetAllRouteRootsAsync();
        if (!allRootsRes.GetResult(out var allRoots, out var err))
            return new(false, err, null);

        List<string> routeParts = new();
        string remainingRoute = routeUrl.Trim('/');
        RouteRoot? parent;
        do
        {
            if (!allRoots.TryGetValue('/' + remainingRoute, out parent))
            {
                var lastIndex = remainingRoute.LastIndexOf('/');
                if (lastIndex < 0)
                {
                    routeParts.Add(remainingRoute);
                    break;
                }

                var part = remainingRoute[lastIndex..];
                routeParts.Add(part);

                remainingRoute = remainingRoute[..lastIndex];
            }
        } while (parent is null);

        parent ??= await GetOrCreateDefaultRouteRoot();

        routeParts.Reverse();

        foreach(var p in routeParts)
        {
            var rootRes = await AddRouteRootAsync(p, parent, true);

            if (!rootRes.GetResult(out var nextParent, out var rootErr))
                return new(false, rootErr, parent);

            parent = nextParent;
        }

        return new(true, null, parent);
    }

    public async Task<ActionResult<RouteRoot>> AddRouteRootAsync(string routePart, 
        RouteRoot parent, bool editable)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        var route = routePart.Trim().Trim('/').ToLower();

        if (route.Contains('/'))
            return new(false, new() { "This method cannot take routes that contain a / character." });

        var child = new RouteRoot()
        {
            Route = route,
            Editable = editable,
            ParentRoot = parent,
        };

        var tracker = _dbContext.Update(child);

        await _dbContext.SaveChangesAsync();

        await tracker.ReloadAsync();

        return new(true, null, child);
    }

    public async Task<ActionResult> UpdateOrAddRouteRootAsync(RouteRoot root)
    {
        // Get the db context ...
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        // ... then add the route root object ...
        var tracker = _dbContext.Update(root);

        // ... save the database ...
        await _dbContext.SaveChangesAsync();

        // ... and get the updated values (mainly, the key) ...
        await tracker.ReloadAsync();

        // ... and let the caller know we succeded.
        return new(true, null);
    }

    public async Task ReloadRouteRootAsync(RouteRoot root)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        var tracker = _dbContext.Attach(root);
        await tracker.ReloadAsync();
    }

    public async Task<ActionResult> SwapRouteRootPagesAsync(RouteRoot one, RouteRoot two)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        var trackOne = _dbContext.Attach(one);
        var trackTwo = _dbContext.Attach(two);

        await trackOne.ReloadAsync();
        await trackTwo.ReloadAsync();

        var onePage = one.Page;
        var twoPage = two.Page;

        if (onePage is null && twoPage is null)
            return new(true);

        if (onePage is null)
        {
            two.Page = null;
        }

        if (twoPage is null)
        {
            one.Page = null;
        }

        if (onePage is not null
            && twoPage is not null)
        {
            one.Page = two.Page;
            two.Page = null;

            await _dbContext.SaveChangesAsync();

            two.Page = onePage;
        }

        try
        {
            await _dbContext.SaveChangesAsync();

            await trackOne.ReloadAsync();
            await trackTwo.ReloadAsync();

            return new(true);
        }
        catch (Exception ex)
        {
            return new(false, new List<string> { "Failed to save page swap.", ex.Message });
        }
    }

    public async Task<ActionResult> DeleteRouteRootAsync(RouteRoot root)
    {
        // Assume input is good input.

        // ... get the db context ...
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        // ... find the route based on the object that was passed to the method ...
        var routeRoot = await _dbContext.RouteRoots
            .Where(x => x.Key == root.Key)
            .Include(x => x.Page)
            .FirstOrDefaultAsync();

        // ... if it doesnt exist, let the user know ...
        if (routeRoot is null)
            return new(false, new List<string> { $"No route found for the key {root.Key} in the database." });

        // ... then remove the page on the route ...
        _dbContext.Remove(routeRoot.Page);

        // ... and remove the route root ...
        _dbContext.Remove(routeRoot);

        // ... then save the database ...
        await _dbContext.SaveChangesAsync();

        // ... and inform the caller.
        return new(true);
    }

    public async Task<ActionResult<Dictionary<string, RouteRoot>>> GetAllRouteRootsAsync()
    {
        // ... get the db context ...
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        // ... get all route roots ...
        var routes = await _dbContext.RouteRoots.ToDictionaryAsync(x => x.FullRoute);

        // ... and return their values.
        return new(true, null, routes);
    }

    public async Task<ActionResult<List<RouteRoot>>> GetRouteRootsForUserAsync(DataCoreUser? user)
    {
        // ... get the db context ...
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        // ... then query all route roots ...
        var routes = _dbContext.RouteRoots
            .Include(e => e.Page)
            .AsAsyncEnumerable();

        // ... then for each root ...
        List<RouteRoot> roots = new();
        await foreach(var route in routes)
        {
            if (AuthorizationArea.Authorize(user, route.AuthorizationArea))
                roots.Add(route);
        }

        // ... then return the authorized route roots.
        return new(true, null, roots);
    }

    public async Task<ActionResult<RouteRoot>> GetRouteRootFromRootAsync(string root)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        var allRoots = await _dbContext.RouteRoots
            .ToListAsync();

        var selectedRoot = allRoots.Where(x => x.FullRoute == root).FirstOrDefault();

        if (selectedRoot is not null)
            return new(true, null, selectedRoot);

        return new(false, new() { "No Root found." }, null);
    }

    public async Task<RouteRoot> GetOrCreateDefaultRouteRoot(bool includePage = false)
    {
        await using var _dbContext = await _dbContextFactory.CreateDbContextAsync();

        var route = await _dbContext.RouteRoots
            .Where(e => e.Route == "")
            .FirstOrDefaultAsync();

        if (route is not null)
        {
            if (includePage)
            {
                await _dbContext.Entry(route)
                    .Reference(e => e.Page)
                    .LoadAsync();
            }

            return route;
        }

        route = new()
        {
            DefaultRoute = true
        };

        var tracker = _dbContext.Add(route);
        await _dbContext.SaveChangesAsync();

        await tracker.ReloadAsync();

        return route;
    }
	#endregion
}

/// <summary>
/// Exception for when a component type can not be loaded.
/// </summary>
public class MissingComponentException : Exception
{
    public MissingComponentException() { }
    public MissingComponentException(string? message) : base(message) { }
    public MissingComponentException(string? message, Exception? innerException) : base(message, innerException) { }
    protected MissingComponentException(SerializationInfo info, StreamingContext context) : base(info, context) { }
}