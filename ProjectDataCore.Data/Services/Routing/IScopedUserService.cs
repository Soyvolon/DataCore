﻿using ProjectDataCore.Data.Account;
using ProjectDataCore.Data.Structures.Page.Components.Scope;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Services.Routing;

/// <summary>
/// Service for <see cref="AssignableScope" utlities./>.
/// </summary>
public interface IScopedUserService
{

}
