﻿using ProjectDataCore.Data.Structures.Page;
using ProjectDataCore.Data.Structures.Routing;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Services.Routing;

/// <summary>
/// Handles custom routing and delivering route information
/// to the page generator.
/// </summary>
/// <remarks>
/// Due to the nature of loading larger page values, this service
/// should be run as a singleton so the cache systems will operate
/// properly accross all users.
/// </remarks>
public interface IRoutingService
{
    /// <summary>
    /// Reloads the cache of component types.
    /// </summary>
    public void ReloadCache();

	/// <summary>
	/// Gets the type of a component by its name.
	/// </summary>
	/// <param name="listing">The listing of the component.</param>
	/// <param name="forceUpdate">If true, this will get the newest version of the type data, and updates the cache.</param>
	/// <returns>The <see cref="Type"/> for the provided <paramref name="qualifiedName"/></returns>
	public Type GetComponentType(PageComponentListing listing, bool forceUpdate = false);

    /// <summary>
    /// Gets the top level page settings object from a provided route.
    /// </summary>
    /// <remarks>
    /// To reduce load times, this method will only return the <see cref="CustomPageSettings"/> and
    /// none of the connected entities. To load the rest of the settings object pass the returned 
    /// object into the <see cref="LoadPageSettingsAsync(CustomPageSettings)"/> method.
    /// </remarks>
    /// <param name="route">The route to get a settings object for.</param>
    /// <returns>A <see cref="CustomPageSettings"/> object if the route is valid, otherwise null.</returns>
    public Task<CustomPageSettings?> GetPageSettingsFromRouteAsync(string route);

    /// <summary>
    /// Loads the full settings for a page.
    /// </summary>
    /// <param name="settings">The page settings object to load.</param>
    /// <param name="context">An existing context to use when loading the page settings
    /// is needed for change tracking.</param>
    /// <returns>An <see cref="IAsyncEnumerable{T}"/> where the boolean value is true.</returns>
    public IAsyncEnumerable<bool> LoadPageSettingsAsync(CustomPageSettings settings, ApplicationDbContext? context = null);

    #region Route Roots
    /// <summary>
    /// Create a new <see cref="RouteRoot"/> from a full URL.
    /// </summary>
    /// <param name="routeUrl">The full route URL.</param>
    /// <remarks>
    /// This method takes a full url string and automatically discoverys any needed parent routes.
    /// </remarks>
    /// <returns>An <see cref="ActionResult{T}"/> continging the <see cref="RouteRoot"/> that is
    /// at the end of the URL. For example, if /this/example/route was passed to <paramref name="routeUrl"/>,
    /// and all three parts were made into <see cref="RouteRoot"/> objects, only the <see cref="RouteRoot"/>
    /// object for /route would be returned.</returns>
    public Task<ActionResult<RouteRoot>> AddRouteRootAsync(string routeUrl);
    /// <summary>
    /// Create a new <see cref="RouteRoot"/> from a single route url part.
    /// </summary>
    /// <param name="routeUrl">The url of the relative route.</param>
    /// <param name="parent">The parent route if applicable. If left blank, a parent route
    /// will be searched for. In the event no parent route is found, it uses the index route of '/'.</param>
    /// <param name="editable">The value of <see cref="RouteRoot.Editable"/></param>
    /// <remarks>
    /// This method requires a single route part to be passed - for example, you can only pass /route/ and not
    /// /route/that/is/longer. Anything more than one URL part will cause this method to return an error. Adding
    /// to a url needs to be done by passing a <see cref="RouteRoot"/> object to the <paramref name="parent"/>
    /// parameter.
    /// </remarks>
    /// <returns>An <see cref="ActionResult{T}"/> containing the new <see cref="RouteRoot"/> object.</returns>
    public Task<ActionResult<RouteRoot>> AddRouteRootAsync(string routePart, RouteRoot parent, bool editable);
    /// <summary>
    /// Gets a <see cref="RouteRoot"/> object from its raw root string.
    /// </summary>
    /// <param name="root">The raw root string.</param>
    /// <returns>An <see cref="ActionResult{T}"/> contaning the <see cref="RouteRoot"/>.</returns>
    public Task<ActionResult<RouteRoot>> GetRouteRootFromRootAsync(string root);
    /// <summary>
    /// Create or update a <see cref="RouteRoot"/>
    /// </summary>
    /// <param name="root">The root object</param>
    /// <remarks>
    /// As this method adds a new route root, any existing pages that
    /// match this root will be automatically added to this root.
    /// <br /><br />
    /// Otherwise, this page updates a route root.
    /// </remarks>
    /// <returns>An <see cref="ActionResult"/> for this action.</returns>
    public Task<ActionResult> UpdateOrAddRouteRootAsync(RouteRoot root);
    /// <summary>
    /// Swaps the pages between two route roots.
    /// </summary>
    /// <remarks>
    /// As this is just swapping values, one or both of the route roots
    /// can have null values for their page and this method will suucced.
    /// </remarks>
    /// <param name="one">The first <see cref="RouteRoot"/>.</param>
    /// <param name="two">The second <see cref="RouteRoot"/>.</param>
    /// <returns>A <see cref="ActionResult"/> for this action.</returns>
    public Task<ActionResult> SwapRouteRootPagesAsync(RouteRoot one, RouteRoot two);
    /// <summary>
    /// Reloads a <see cref="RouteRoot"/> with data from the database.
    /// </summary>
    /// <param name="root">The <see cref="RouteRoot"/> to reload.</param>
    /// <returns>A task for this action.</returns>
    public Task ReloadRouteRootAsync(RouteRoot root);
    /// <summary>
    /// Deletes a route root. Moves all pages to the base root of '/'.
    /// </summary>
    /// <param name="root">The <see cref="RouteRoot"/> object to delete.</param>
    /// <returns>A <see cref="ActionResult"/> for this action.</returns>
    public Task<ActionResult> DeleteRouteRootAsync(RouteRoot root);
    /// <summary>
    /// Gets all route roots.
    /// </summary>
    /// <returns>A <see cref="ActionResult{T}"/> with a <see cref="List{T}"/> of all <see cref="RouteRoot"/>s
    /// in the database.</returns>
    public Task<ActionResult<Dictionary<string, RouteRoot>>> GetAllRouteRootsAsync();
    /// <summary>
    /// Gets all route roots a user has access to modify.
    /// </summary>
    /// <param name="user">The <see cref="DataCoreUser"/> to get roots for.</param>
    /// <returns>A <see cref="ActionResult{T}"/> with a <see cref="List{T}"/> of all <see cref="RouteRoot"/>s
    /// the <paramref name="user"/> has access to edit.</returns>
    public Task<ActionResult<List<RouteRoot>>> GetRouteRootsForUserAsync(DataCoreUser? user);
    /// <summary>
    /// Gets or creates the default <see cref="RouteRoot"/>
    /// </summary>
    /// <param name="includePage">If true, will include page data for this <see cref="RouteRoot"/>.
    /// Defaults to <strong>false</strong>.</param>
    /// <returns>A <see cref="RouteRoot"/> with 
    /// <see cref="RouteRoot.DefaultRoute"/> equal to <strong>true</strong>.</returns>
    public Task<RouteRoot> GetOrCreateDefaultRouteRoot(bool includePage = false);
	#endregion
}
