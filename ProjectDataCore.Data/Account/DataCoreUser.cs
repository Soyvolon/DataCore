﻿using Microsoft.AspNetCore.Identity;

using MimeKit.Cryptography;

using ProjectDataCore.Data.Structures.Assignable;
using ProjectDataCore.Data.Structures.Assignable.Configuration;
using ProjectDataCore.Data.Structures.Assignable.Value;
using ProjectDataCore.Data.Structures.Auth;
using ProjectDataCore.Data.Structures.Auth.Tags;
using ProjectDataCore.Data.Structures.Form;
using ProjectDataCore.Data.Structures.Form.Bucket;
using ProjectDataCore.Data.Structures.Keybindings;
using ProjectDataCore.Data.Structures.Util.Interfaces;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Account;

public class DataCoreUser : IdentityUser<Guid>, ITaggable, IAssignable, IKeyable<Guid>, IComparable, IHasSlots
{
    public bool Administrator { get; set; } = false;
    /// <summary>
    /// Used to identity to user account that is pulled when
    /// assignable are needed for configuration.
    /// </summary>
    public bool Default { get; set; } = false;
    /// <summary>
    /// If true, the user has registered.
    /// </summary>
    /// <remarks>
    /// This is only set to false when a user requires an access code to register.
    /// In that case, the account is created, an access code saved, and the user
    /// "registers" by changing their username/password.
    /// </remarks>
    public bool Registered { get; set; } = true;

    public List<BaseAssignableValue> AssignableValues { get; set; } = new();
	
    public BaseAssignableValue? SteamLink { get; set; }
    public BaseAssignableValue? DiscordLink { get; set; }
    public BaseAssignableValue? AccessCode { get; set; }

    public List<RosterSlot> RosterSlots { get; set; } = new();

    public List<DynamicFormResponse> DynamicFormResponses { get; set; } = new();
    public List<BucketChangeData> BucketChanges { get; set; } = new();

    public List<UserKeybinding> KeyBindings { get; set; } = new();

    public List<AuthorizationBinding> AuthorizationBindings { get; set; } = new();
    public List<AuthorizationTagTreeSlotUserBinding> AuthorizationTagBindings { get; set; } = new();

    internal List<Guid> TagsList { get; set; } = new();
    private HashSet<Guid>? _tags;
    public IReadOnlySet<Guid> Tags
    {
        get => _tags ??= new(TagsList);
    }

    public bool Impersonating { get; set; } = false;
    public DataCoreUser? CurrentlyImpersonating { get; set; } = null;
    public Guid? CurrentlyImpersonatingKey { get; set; } = null;

    public DataCoreUser? AuthorizationCheckUser
    {
        get
        {
            if (Impersonating)
                return CurrentlyImpersonating;

            return this;
        }
    }

	internal void ImpersonateUser(DataCoreUser? toImpersonate)
    {
        Impersonating = true;
        CurrentlyImpersonating = toImpersonate;
        CurrentlyImpersonatingKey = toImpersonate?.Id;
    }

    internal void StopImpersonating()
    {
        Impersonating = false;
        CurrentlyImpersonating = null;
        CurrentlyImpersonatingKey = null;
    }

    [Obsolete("Use Assignable Value Renderers instead.", false)]
    public string GetStaticProperty(string property, string? format = null)
    {
        var s = this.GetStaticPropertyContainer(property)?.GetValue();
        if (s is not null)
        {
            return string.Format(format ?? "{0}", s);
        }

        return string.Empty;
    }

    [Obsolete("Use Assignable Value Renderers instead.", false)]
    public string GetAssignableProperty(string property, string? format = null)
    {
        var data = this.GetAssignablePropertyContainer(property);

        if (data is null)
            return string.Empty;

        if (data.AssignableConfiguration.AllowMultiple)
        {
            var vals = data.GetValues();
            List<string> parts = new();
            foreach (var v in vals)
                if (v is not null)
                    parts.Add(string.Format(format ?? "{0}", v));

            return string.Join(", ", parts);
        }
        else
        {
            var val = data.GetValue();
            if (val is not null)
                return string.Format(format ?? "{0}", val);

            return "";
        }
    }

    public void UpdateTags(ApplicationDbContext context)
    {
        LoadTags(context);

        _tags ??= new();
        _tags.Clear();

        foreach (var binding in AuthorizationTagBindings)
            _tags.Add(binding.TagKey);

        foreach (var slot in RosterSlots)
            foreach (var tag in slot.GetTags(context))
                _tags.Add(tag);

        TagsList = _tags.ToList();
    }

    public void LoadTags(ApplicationDbContext context)
    {
        var entry = context.Entry(this);

        entry.Collection(e => e.AuthorizationTagBindings).Load();
        entry.Collection(e => e.RosterSlots).Load();
    }

    public IEnumerable<Guid> GetTags()
        => TagsList;

    public void Bind(AuthorizationTagTreeSlotUserBinding binding)
    {
        binding.ClearBindings();

        binding.User = this;
        binding.UserId = Id;
    }

    public Guid GetKey()
        => Id;

    public BaseAssignableConfiguration.InternalAssignableType GetInternalAssignableType()
        => BaseAssignableConfiguration.InternalAssignableType.UserProperty;

    public int CompareTo(object? obj)
    {
        if (obj is DataCoreUser user)
            return Id.CompareTo(user.Id);

        return GetHashCode().CompareTo(obj?.GetHashCode());
    }

    public IEnumerable<RosterSlot> GetSlots()
        => RosterSlots;
}
