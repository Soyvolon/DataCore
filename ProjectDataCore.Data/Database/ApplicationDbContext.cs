﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Routing.Patterns;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

using ProjectDataCore.Data.Structures;
using ProjectDataCore.Data.Structures.Account;
using ProjectDataCore.Data.Structures.Assignable.Configuration;
using ProjectDataCore.Data.Structures.Assignable.Render;
using ProjectDataCore.Data.Structures.Assignable.Settings;
using ProjectDataCore.Data.Structures.Assignable.Value;
using ProjectDataCore.Data.Structures.Auth;
using ProjectDataCore.Data.Structures.Auth.Tags;
using ProjectDataCore.Data.Structures.Form;
using ProjectDataCore.Data.Structures.Form.Bucket;
using ProjectDataCore.Data.Structures.Form.Bucket.Action;
using ProjectDataCore.Data.Structures.Form.Bucket.Filter;
using ProjectDataCore.Data.Structures.Keybindings;
using ProjectDataCore.Data.Structures.Nav;
using ProjectDataCore.Data.Structures.Page;
using ProjectDataCore.Data.Structures.Page.Components.Layout;
using ProjectDataCore.Data.Structures.Page.Components.Parameters;
using ProjectDataCore.Data.Structures.Page.Components.Parameters.Form;
using ProjectDataCore.Data.Structures.Page.Components.Pool;
using ProjectDataCore.Data.Structures.Page.Components.Scope;
using ProjectDataCore.Data.Structures.Page.Data;
using ProjectDataCore.Data.Structures.Routing;
using ProjectDataCore.Data.Structures.Selector.User;
using ProjectDataCore.Data.Structures.Util.Converters;

using System.Reflection;

namespace ProjectDataCore.Data.Database;

public class ApplicationDbContext : IdentityDbContext<DataCoreUser, DataCoreRole, Guid>
{
    #region Roster
    public DbSet<RosterTree> RosterTrees { get; internal set; }
    public DbSet<RosterSlot> RosterSlots { get; internal set; }
    public DbSet<RosterDisplaySettings> RosterDisplaySettings { get; internal set; }
    public DbSet<RosterOrder> RosterOrders { get; internal set; }
    public DbSet<RosterTagPermission> RosterTagPermissions { get; internal set; }
    #endregion

    #region Page
    public DbSet<DisplayComponentSettings> DisplayComponentSettings { get; internal set; }
    public DbSet<LayoutNode> LayoutNodes { get; internal set; }
    public DbSet<EditableComponentSettings> EditableComponentSettings { get; internal set; }
    public DbSet<CustomPageSettings> CustomPageSettings { get; internal set; }
    public DbSet<RosterComponentSettings> RosterComponentSettings { get; internal set; }
    public DbSet<ButtonComponentSettings> ButtonComponentSettings { get; internal set; }
    public DbSet<TextDisplayComponentSettings> TextDisplayComponentSettings { get; internal set; }
    public DbSet<UserSelectComponentSettings> UserSelectComponentSettings { get; internal set; }
    public DbSet<AssignableScope> UserScopes { get; internal set; }
    #endregion

    #region Forms
    public DbSet<DynamicForm> DynamicForms { get; internal set; }
    public DbSet<DynamicFormBucket> DynamicFormBuckets { get; internal set; }
    public DbSet<DynamicFormResponse> DynamicFormResponses { get; internal set; }
    public DbSet<BucketChangeData> BucketChangeData { get; internal set; }
    public DbSet<BucketAction> BucketActions { get; internal set; }
    public DbSet<BucketActionFilter> BucketActionFilters { get; internal set; }
    #endregion

    #region Assignable Values
    // Configuration Sets
    public DbSet<BaseAssignableConfiguration> AssignableConfigurations { get; internal set; }
    public DbSet<StringValueAssignableConfiguration> StringValueAssignableConfigurations { get; internal set; }
    public DbSet<IntegerValueAssignableConfiguration> IntegerValueAssignableConfigurations { get;internal set; }
    public DbSet<DoubleValueAssignableConfiguration> DoubleValueAssignableConfigurations { get; internal set; }
    public DbSet<DateTimeValueAssignableConfiguration> DateTimeValueAssignableConfigurations { get; internal set; }
    public DbSet<DateOnlyValueAssignableConfiguration> DateOnlyValueAssignableConfigurations { get; internal set; }
    public DbSet<TimeOnlyValueAssignableConfiguration> TimeOnlyValueAssignableConfigurations{ get; internal set; }
    public DbSet<BooleanValueAssignableConfiguration> BooleanValueAssignableConfigurations { get; internal set; }

    // Assigned Value Setes
    public DbSet<BaseAssignableValue> AssignableValues { get; internal set; }
    public DbSet<StringAssignableValue> StringAssignableValues { get; internal set; }
    public DbSet<IntegerAssignableValue> IntegerAssignableValues { get; internal set; }
    public DbSet<DoubleAssignableValue> DoubleAssignableValues { get; internal set; }
    public DbSet<DateTimeAssignableValue> DateTimeAssignableValues { get; internal set; }
    public DbSet<DateOnlyAssignableValue> DateOnlyAssignableValues { get; internal set; }
    public DbSet<TimeOnlyAssignableValue> TimeOnlyAssignableValues { get; internal set; }
    public DbSet<BooleanAssignableValue> BooleanAssignableValues { get; internal set; }

    // Assignable Value Rendering
    public DbSet<AssignableValueRenderer> AssignableValueRenderers { get; internal set; }
    public DbSet<AssignableValueConversion> AssignableValueConversions { get; internal set; }
    #endregion

    #region Account Link
    public DbSet<AccountSettings> LinkSettings { get; internal set; }
    #endregion

    #region Nav
    public DbSet<NavModule> NavModules { get; internal set; }
    public DbSet<RouteRoot> RouteRoots { get; internal set; }
    #endregion

    #region Auth
    public DbSet<AuthorizationArea> AuthorizationAreas { get; internal set; }
    public DbSet<AuthorizationBinding> AuthorizationBindings { get; internal set; }
    public DbSet<AuthorizationTag> AuthorizationTags { get; internal set; }
    public DbSet<AuthorizationTagTreeSlotUserBinding> AuthorizationTagBindings { get; internal set; }
    #endregion

    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
    {
        SavingChanges += OnSave_UpdateRouteRoots;
        SavingChanges += OnSave_UpdateAuthorizationGrants;
        SavingChanges += OnSave_UpdateITaggables;
        SavingChanges += OnSave_UpdateTimestamps;
    }

    private void OnSave_UpdateRouteRoots(object? sender, SavingChangesEventArgs e)
    {
        var objs = ChangeTracker.Entries()
            .Where(x => x.Entity is RouteRoot 
                && (x.State == EntityState.Modified || x.State == EntityState.Added))
            .ToArray();

        for (int i = 0; i < objs.Length; i++)
        {
            var entity = objs[i].Entity as RouteRoot;

            entity!.UpdateFullRoute(this);
            entity!.UpdateOnNavigateChecks(this);
        }
    }

    private void OnSave_UpdateAuthorizationGrants(object? sender, SavingChangesEventArgs e)
    {
        var objs = ChangeTracker.Entries()
            .Where(x => x.Entity is AuthorizationArea
                && (x.State == EntityState.Modified || x.State == EntityState.Added));

        foreach (var obj in objs)
        {
            var entity = obj.Entity as AuthorizationArea;

            entity!.UpdateAuthorizedLists(this);
        }
    }

    private void OnSave_UpdateITaggables(object? sender, SavingChangesEventArgs e)
    {
        var objs = ChangeTracker.Entries()
            .Where(x => (x.Entity is ITaggable)
                && (x.State == EntityState.Modified || x.State == EntityState.Added));

        foreach(var obj in objs)
        {
            var item = obj.Entity as ITaggable;
            item?.UpdateTags(this);
        }
    }

    private void OnSave_UpdateTimestamps(object? sender, SavingChangesEventArgs e)
    {
        var now = DateTime.UtcNow;
        foreach (var entry in ChangeTracker.Entries())
            if (entry.Entity is DataObject data)
                data.LastEdit = now;
    }

    public void MarkDanglingObjectsForDelete<T>(Dictionary<Type, Dictionary<T, object>> oldElements,
        Dictionary<Type, Dictionary<T, object>> newElements)
            where T : unmanaged, IComparable, IEquatable<T>
    {
        var keys = oldElements.Keys.Union(newElements.Keys);
        // ... for each data type ...
        foreach (var key in keys)
        {
            // ... try get the new and old elements ...
            if (newElements.TryGetValue(key, out var newE)
                && oldElements.TryGetValue(key, out var oldE))
            {
                // ... if old and new exist, get the old keys ...
                var keyDiff = oldE.Keys.ToHashSet();
                // ... and the difference with the new keys ...
                keyDiff.ExceptWith(newE.Keys.ToHashSet());

                // ... then for all missing keys ...
                foreach (var elemKey in keyDiff)
                {
                    // ... delete them, as they are old objects ...
                    var element = oldE[elemKey];
                    Entry(element).State = EntityState.Deleted;
                }
            }
            // ... if the type is null ...
            else if (newE is null
                // ... but the old type has values ...
                && oldElements.TryGetValue(key, out oldE))
            {
                // ... delete them all ...
                foreach (var element in oldE.Values)
                {
                    Entry(element).State = EntityState.Deleted;
                }
            }
        }
    }

    public async Task UpdateObjectTreeAsync<T, TEntity>(DataObject<T> root, DbSet<TEntity> dbSet, bool limit = true)
        where T : unmanaged, IComparable, IEquatable<T>
        where TEntity : DataObject<T>
        => await UpdateObjectTreeAsync(root, dbSet, (e) => Task.CompletedTask, limit);

    public async Task UpdateObjectTreeAsync<T, TEntity>(DataObject<T> root, DbSet<TEntity> dbSet,
        Func<TEntity, Task> loader, bool limit = true)
        where T : unmanaged, IComparable, IEquatable<T>
        where TEntity : DataObject<T>
        => await UpdateObjectTreeAsync([root], dbSet, loader, limit);

    public async Task UpdateObjectTreeAsync<T, TEntity>(IEnumerable<DataObject<T>> root, DbSet<TEntity> dbSet, bool limit = true)
        where T : unmanaged, IComparable, IEquatable<T>
        where TEntity : DataObject<T>
        => await UpdateObjectTreeAsync(root, dbSet, (e) => Task.CompletedTask, limit);

    public async Task UpdateObjectTreeAsync<T, TEntity>(IEnumerable<DataObject<T>> root, DbSet<TEntity> dbSet,
        Func<TEntity, Task> loader, bool limit = true)
        where T : unmanaged, IComparable, IEquatable<T>
        where TEntity : DataObject<T>
    {
        foreach(var r in root)
            await UpdateObjectTreeElementAsync(r, dbSet, loader, limit);

        await SaveChangesAsync();

        foreach(var r in root)
            await Entry(r).ReloadAsync();
    }

    private async Task UpdateObjectTreeElementAsync<T, TEntity>(DataObject<T> root, DbSet<TEntity> dbSet,
        Func<TEntity, Task> loader, bool limit = true)
        where T : unmanaged, IComparable, IEquatable<T>
        where TEntity : DataObject<T>
    {
        var actual = await dbSet
            .AsNoTrackingWithIdentityResolution()
            .OrderBy(x => x.Key)
            .Where(x => x.Key.Equals(root.Key))
            .FirstOrDefaultAsync();

        var oldElements = new Dictionary<Type, Dictionary<T, object>>();
        if (actual is not null)
        {
            await loader.Invoke(actual);
            oldElements = GetExistingElementTree(actual, limit);
        }

        var newElements = GetExistingElementTree(root, limit);

        Update(root);
        MarkDanglingObjectsForDelete(oldElements, newElements);
    }

    public static Dictionary<Type, Dictionary<T, object>> GetExistingElementTree<T>(DataObject<T> root, bool limit = true)
        where T : unmanaged, IComparable, IEquatable<T>
    {
        Dictionary<Type, Dictionary<T, object>> elems = new();
        var baseType = typeof(DataObject<T>);
        var enumerableBaseType = typeof(IEnumerable<DataObject<T>>);

        Stack<DataObject<T>> objectsToCheck = new();
        objectsToCheck.Push(root);

        HashSet<object> added = new();

        while (objectsToCheck.TryPop(out var obj))
        {
            if (obj is null
                || obj.GetKey().Equals(default))
                continue;

            // already have this one ....
            if (!added.Add(obj))
                continue;

            var props = obj.GetType().GetProperties();
            foreach (var p in props)
            {
                if (limit
                    && p.GetCustomAttributes<NoObjectTreeDiscoveryAttribute>().Any())
                    continue;

                if (p.PropertyType.IsAssignableTo(baseType))
                {
                    if (p.GetValue(obj) is DataObject<T> next)
                        objectsToCheck.Push(next);
                }
                else if (p.PropertyType.IsAssignableTo(enumerableBaseType))
                {
                    if (p.GetValue(obj) is IEnumerable<DataObject<T>> nextSet)
                        foreach (var next in nextSet)
                            objectsToCheck.Push(next);
                }
            }

            _ = elems.TryAddSet(obj.GetType(), obj.GetKey(), obj);
        }

        return elems;
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);

        #region Roster
        var rosterObject = builder.Entity<RosterObject>();
        rosterObject.HasKey(e => e.Key);

        var rosterOrder = builder.Entity<RosterOrder>();
        rosterOrder.HasKey(e => e.Key);

        var rosterDisplaySettings = builder.Entity<RosterDisplaySettings>();
        rosterDisplaySettings.HasKey(e => e.Key);
        rosterDisplaySettings.HasOne(e => e.HostRoster)
            .WithMany(p => p.DisplaySettings)
            .HasForeignKey(e => e.HostRosterId);

        var rosterTree = builder.Entity<RosterTree>();
        rosterTree.HasMany(e => e.ChildRosters)
            .WithMany(p => p.ParentRosters)
            .UsingEntity<RosterOrder>(
                r => r.HasOne(e => e.TreeToOrder)
                    .WithMany(p => p.Order)
                    .HasForeignKey(e => e.TreeToOrderId),
                l => l.HasOne(e => e.ParentObject)
                    .WithMany(p => p.OrderChildren)
                    .HasForeignKey(e => e.ParentObjectId));

        rosterTree.HasMany(e => e.RosterSlots)
            .WithOne(p => p.ParentRoster)
            .HasForeignKey(p => p.ParentRosterId);

        var rosterSlot = builder.Entity<RosterSlot>();
        rosterSlot.HasOne(e => e.Order)
            .WithOne(p => p.SlotToOrder)
            .HasForeignKey<RosterOrder>(p => p.SlotToOrderId);

        rosterSlot.Navigation(e => e.Order)
            .AutoInclude(true);
        rosterSlot.Navigation(e => e.OccupiedBy)
            .AutoInclude(true);

        var rosterTagPermissions = builder.Entity<RosterTagPermission>();
        rosterTagPermissions.HasKey(e => e.Key);

        rosterTagPermissions.HasOne(d => d.AuthorizationTag)
            .WithMany(p => p.RosterPermissions)
            .HasForeignKey(d => d.AuthorizationTagKey);

        rosterTagPermissions.HasMany(d => d.OnlyValidForTags)
            .WithMany(p => p.RosterPermissionWhitelists)
            .UsingEntity<RosterTagPermissionAuthorizationTagWhitelistBinding>(
                r => r.HasOne(d => d.AuthorizationTag)
                    .WithMany(p => p.RosterPermissionWhitelistBindings)
                    .HasForeignKey(d => d.AuthorizationTagKey),
                l => l.HasOne(d => d.RosterTagPermission)
                    .WithMany(p => p.OnlyValidForTagsBindings)
                    .HasForeignKey(d => d.RosterTagPermissionKey)
            );

        rosterTagPermissions.Navigation(e => e.AuthorizationTag)
            .AutoInclude(true);
        #endregion

        #region Page
        var customPageSettings = builder.Entity<CustomPageSettings>();
        customPageSettings.HasKey(e => e.Key);
        customPageSettings.HasOne(e => e.Layout)
            .WithOne(p => p.PageSettings)
            .HasForeignKey<LayoutNode>(e => e.PageSettingsId);
        customPageSettings.HasMany(e => e.AssignableScopes)
            .WithOne(p => p.Page)
            .HasForeignKey(p => p.PageId);

        customPageSettings.Navigation(e => e.RouteRoot)
            .AutoInclude(true);

        var layoutNodes = builder.Entity<LayoutNode>();
        layoutNodes.HasKey(e => e.Key);

        layoutNodes.HasOne(e => e.ParentNode)
            .WithMany(p => p.Nodes)
            .HasForeignKey(e => e.ParentNodeId);

        layoutNodes.HasOne(e => e.Component)
            .WithOne(p => p.ParentNode)
            .HasForeignKey<PageComponentSettingsBase>(p => p.ParentNodeId)
            .IsRequired(false);

        layoutNodes.HasOne(e => e.AuthorizationArea)
            .WithMany(p => p.LayoutNodes)
            .HasForeignKey(e => e.AuthorizationAreaKey)
            .IsRequired(false);

        layoutNodes.Ignore(e => e.NodeWidths)
            .Ignore(e => e.EditorKey);

        layoutNodes.Navigation(e => e.AuthorizationArea)
            .AutoInclude(true);

        var userScopes = builder.Entity<AssignableScope>();
        userScopes.HasKey(e => e.Key);

        userScopes.HasOne(e => e.FormScopeDefinition)
            .WithMany(p => p.DefinesScopes)
            .HasForeignKey(e => e.FormScopeDefinitionKey);

        userScopes.Navigation(e => e.FormScopeDefinition)
            .AutoInclude(true);

        var userScopeProviderContainers = builder.Entity<AssignableScopeProviderContainer>();
        userScopeProviderContainers.HasKey(e => e.Key);
        userScopeProviderContainers.HasOne(e => e.ProvidingComponent)
            .WithMany(p => p.ScopeListeners)
            .HasForeignKey(e => e.ProvidingComponentId);
        userScopeProviderContainers.HasOne(e => e.ListeningScope)
            .WithMany(p => p.ScopeProviders)
            .HasForeignKey(e => e.ListeningScopeId);

        var userScopeListenerContainers = builder.Entity<AssignableScopeListenerContainer>();
        userScopeListenerContainers.HasKey(e => e.Key);
        userScopeListenerContainers.HasOne(e => e.ListeningComponent)
            .WithMany(p => p.ScopeProviders)
            .HasForeignKey(e => e.ListeningComponentId);
        userScopeListenerContainers.HasOne(e => e.ProvidingScope)
            .WithMany(p => p.ScopeListeners)
            .HasForeignKey(e => e.ProvidingScopeId);

        var componentPool = builder.Entity<ComponentPool>();
        componentPool.HasKey(e => e.Key);

        componentPool.HasOne(e => e.PageSettings)
            .WithMany(p => p.ComponentPools)
            .HasForeignKey(e => e.PageSettingsKey);

        componentPool.HasMany(e => e.Components)
            .WithMany(p => p.ComponentPools)
            .UsingEntity<ComponentPoolBinding>(
                r => r.HasOne(e => e.Settings)
                    .WithMany(p => p.ComponentPoolBindings)
                    .HasForeignKey(e => e.SettingsKey),
                l => l.HasOne(e => e.Pool)
                    .WithMany(p => p.ComponentPoolBindings)
                    .HasForeignKey(e => e.PoolKey)
            );

        var pageComponentSettingsBase = builder.Entity<PageComponentSettingsBase>();
        pageComponentSettingsBase.HasKey(e => e.Key);

        var parametererComponentSettingsBase = builder.Entity<ParameterComponentSettingsBase>();
        parametererComponentSettingsBase.HasMany(e => e.AssignableValueRenderers)
            .WithOne(p => p.ParameterComponentSettings)
            .HasForeignKey(p => p.ParameterComponentSettingsId);

        parametererComponentSettingsBase.Navigation(e => e.AssignableValueRenderers)
            .AutoInclude();

        var editableComponentSettings = builder.Entity<EditableComponentSettings>();
        editableComponentSettings.HasMany(e => e.EditableDisplays)
            .WithMany(e => e.EditableComponentsAllowedEditors);

        var rosterComponentSettings = builder.Entity<RosterComponentSettings>();

        rosterComponentSettings.HasOne(e => e.RosterTreeDisplayComponentData)
            .WithOne(p => p.RosterComponentSettings)
            .HasForeignKey<DisplayComponentData>(p => p.RosterComponentSettingsKey);
        rosterComponentSettings.Navigation(e => e.RosterTreeDisplayComponentData)
            .AutoInclude(true);

        rosterComponentSettings.Navigation(e => e.UserListDisplayedProperties)
            .AutoInclude(true);

        rosterComponentSettings.HasOne(d => d.EditingAuthArea)
            .WithMany(p => p.EditingAuthSettings)
            .HasForeignKey(d => d.EditingAuthAreaKey)
            .IsRequired(false);

        rosterComponentSettings.Navigation(e => e.EditingAuthArea)
            .AutoInclude(true);

        var rosterDisplayColumnOrder = builder.Entity<RosterDisplayColumnOrder>();
        rosterDisplayColumnOrder.HasKey(e => e.Key);

        rosterDisplayColumnOrder.HasOne(e => e.Settings)
            .WithMany(p => p.UserListDisplayedProperties)
            .HasForeignKey(e => e.SettingsKey);

        rosterDisplayColumnOrder.HasOne(e => e.AssignableConfiguration)
            .WithMany(p => p.RosterDisplayColumnOrders)
            .HasForeignKey(e => e.ConfigurationKey);
        rosterDisplayColumnOrder.Navigation(e => e.AssignableConfiguration)
            .AutoInclude(true);

        var textDisplayComponentSettings = builder.Entity<TextDisplayComponentSettings>();
        textDisplayComponentSettings.Ignore(e => e.Display);

        var userSelectComponentSettings = builder.Entity<UserSelectComponentSettings>();
        userSelectComponentSettings.HasKey(e => e.Key);

        var formComponentSettings = builder.Entity<FormComponentSettings>();

        formComponentSettings.Navigation(e => e.AssignableConfiguration)
            .AutoInclude(true);

        formComponentSettings.HasOne(e => e.LinkedAssignableInputSettings)
            .WithMany(p => p.FormComponents)
            .HasForeignKey(e => e.LinkedAssignableInputSettingsKey)
            .IsRequired(false);

        formComponentSettings.Navigation(e => e.LinkedAssignableInputSettings)
            .AutoInclude(true);

        var buttonComponentSettings = builder.Entity<ButtonComponentSettings>();

        buttonComponentSettings.Navigation(e => e.Data)
            .AutoInclude(true);

        var buttonComponentData = builder.Entity<ButtonComponentData>();
        buttonComponentData.HasKey(e => e.Key);

        buttonComponentData.HasOne(e => e.ButtonComponentSettings)
            .WithOne(p => p.Data)
            .HasForeignKey<ButtonComponentData>(e => e.ButtonComponentSettingsKey)
            .IsRequired(false);

        buttonComponentData.HasOne(e => e.BucketComponentSettings)
            .WithMany(p => p.Actions)
            .HasForeignKey(e => e.BucketComponentSettingsKey)
            .IsRequired(false);

        var displayComponentData = builder.Entity<DisplayComponentData>();
        displayComponentData.HasKey(e => e.Key);

        displayComponentData.HasOne(e => e.DisplayComponentSettings)
            .WithOne(p => p.Data)
            .HasForeignKey<DisplayComponentData>(e => e.DisplayComponentSettingsKey)
            .IsRequired(false);

        displayComponentData.HasOne(e => e.BucketComponentSettings)
            .WithOne(p => p.ItemSummaryDisplayConfig)
            .HasForeignKey<DisplayComponentData>(e => e.BucketComponentSettingsKey)
            .IsRequired(false);

        displayComponentData.Navigation(e => e.DisplayComponentSettings)
            .AutoInclude(true);

        var bucketComponentSettings = builder.Entity<BucketComponentSettings>();
        bucketComponentSettings.HasOne(e => e.ItemSummaryDisplayConfig)
            .WithOne(e => e.BucketComponentSettings)
            .HasForeignKey<DisplayComponentData>(e => e.BucketComponentSettingsKey);

        bucketComponentSettings.HasMany(e => e.Actions)
            .WithOne(e => e.BucketComponentSettings)
            .HasForeignKey(e => e.BucketComponentSettingsKey);

        bucketComponentSettings.Navigation(e => e.ItemSummaryDisplayConfig)
            .AutoInclude();
        bucketComponentSettings.Navigation(e => e.Actions)
            .AutoInclude();
        bucketComponentSettings.Navigation(e => e.DisplayOrders)
            .AutoInclude();

        var bucketValueDisplayOrder = builder.Entity<BucketValueDisplayOrder>();
        bucketValueDisplayOrder.HasKey(e => e.Key);

        bucketValueDisplayOrder.HasOne(e => e.AssignableConfiguration)
            .WithMany(p => p.BucketValueDisplayOrders)
            .HasForeignKey(e => e.ConfigurationKey);

        bucketValueDisplayOrder.HasOne(e => e.Settings)
            .WithMany(p => p.DisplayOrders)
            .HasForeignKey(e => e.SettingsKey);

        bucketValueDisplayOrder.Navigation(e => e.AssignableConfiguration)
            .AutoInclude();
        #endregion

        #region Forms
        var dynamicForm = builder.Entity<DynamicForm>();
        dynamicForm.HasKey(e => e.Key);
        dynamicForm.HasIndex(e => e.Name);

        dynamicForm.HasMany(e => e.Buckets)
            .WithOne(p => p.Form)
            .HasForeignKey(p => p.FormKey);

        dynamicForm.HasMany(e => e.AllResponses)
            .WithOne(p => p.Form)
            .HasForeignKey(p => p.FormKey);

        dynamicForm.HasMany(e => e.Properties)
            .WithOne(p => p.Form)
            .HasForeignKey(p => p.FormKey);

        dynamicForm.HasMany(e => e.DefinesScopes)
            .WithOne(p => p.FormScopeDefinition)
            .HasForeignKey(p => p.FormScopeDefinitionKey);

        dynamicForm.Navigation(e => e.HasDefaultBucket)
            .AutoInclude(true);
        dynamicForm.Navigation(e => e.Properties)
            .AutoInclude(true);
        dynamicForm.Navigation(e => e.Tags)
            .AutoInclude(true);

        var dynamicFormBucket = builder.Entity<DynamicFormBucket>();
        dynamicFormBucket.HasKey(e => e.Key);

        dynamicFormBucket.HasMany(e => e.Responses)
            .WithOne(p => p.Bucket)
            .HasForeignKey(p => p.BucketKey);

        dynamicFormBucket.HasMany(e => e.ResponsesEnteringBucket)
            .WithOne(p => p.ToBucket)
            .HasForeignKey(p => p.ToBucketKey);

        dynamicFormBucket.HasMany(e => e.ResponsesExitingBucket)
            .WithOne(p => p.FromBucket)
            .HasForeignKey(p => p.FromBucketKey);

        dynamicFormBucket.HasMany(e => e.Actions)
            .WithOne(p => p.Bucket)
            .HasForeignKey(p => p.BucketKey);
        dynamicFormBucket.HasMany(e => e.ActionsMovingToThisBucket)
            .WithOne(p => p.MoveResponse_MoveTo)
            .HasForeignKey(p => p.MoveResponse_MoveToKey);

        dynamicFormBucket.Navigation(e => e.Actions)
            .AutoInclude(true);

        var dynamicFormResponse = builder.Entity<DynamicFormResponse>();
        dynamicFormResponse.HasKey(e => e.Key);

        dynamicFormResponse.HasOne(p => p.Submitter)
            .WithMany(e => e.DynamicFormResponses)
            .HasForeignKey(p => p.SubmitterId)
            .IsRequired(false);

        dynamicFormResponse.HasMany(e => e.BucketChanges)
            .WithOne(p => p.Response)
            .HasForeignKey(p => p.ResponseKey);

        dynamicFormResponse.Ignore(e => e.DeleteRequested)
            .Ignore(e => e.NeedsDbUpdate);

        dynamicFormResponse.Navigation(e => e.AssignableValues)
            .AutoInclude(true);
        dynamicFormResponse.Navigation(e => e.Tags)
            .AutoInclude(true);

        var dynamicFormTag = builder.Entity<DynamicFormTag>();
        dynamicFormTag.HasKey(e => e.Key);

        dynamicFormTag.HasOne(e => e.Form)
            .WithMany(p => p.Tags)
            .HasForeignKey(e => e.FormKey);

        dynamicFormTag.HasMany(e => e.Responses)
            .WithMany(p => p.Tags)
            .UsingEntity<DynamicFormTagResponseBinding>(
                r => r.HasOne(e => e.Response)
                    .WithMany(p => p.TagBindings)
                    .HasForeignKey(e => e.ResponseKey),
                l => l.HasOne(e => e.Tag)
                    .WithMany(p => p.ResponseBindings)
                    .HasForeignKey(e => e.TagKey)
            );

        dynamicFormTag.HasMany(e => e.BucketAddActions)
            .WithMany(p => p.ModifyTags_AddTags)
            .UsingEntity<DynamicFormTagBucketActionBindingAdd>(
                l => l.HasOne(e => e.AddAction)
                    .WithMany(p => p.ModifyTags_AddTagsBindings)
                    .HasForeignKey(e => e.AddActionKey),
                r => r.HasOne(e => e.Tag)
                    .WithMany(p => p.BucketAddActionBindings)
                    .HasForeignKey(e => e.TagKey)
            );

        dynamicFormTag.HasMany(e => e.BucketRemoveActions)
            .WithMany(p => p.ModifyTags_RemoveTags)
            .UsingEntity<DynamicFormTagBucketActionBindingRemove>(
                l => l.HasOne(e => e.RemoveAction)
                    .WithMany(p => p.ModifyTags_RemoveTagsBindings)
                    .HasForeignKey(e => e.RemoveActionKey),
                r => r.HasOne(e => e.Tag)
                    .WithMany(p => p.BucketRemoveActionBindings)
                    .HasForeignKey(e => e.TagKey)
            );

        var bucketChangeData = builder.Entity<BucketChangeData>();
        bucketChangeData.HasKey(e => e.Key);

        bucketChangeData.HasOne(p => p.TriggeredBy)
            .WithMany(e => e.BucketChanges)
            .HasForeignKey(p => p.TriggeredById)
            .IsRequired(false);

        var bucketAction = builder.Entity<BucketAction>();
        bucketAction.HasKey(e => e.Key);

        bucketAction.HasMany(e => e.Filters)
            .WithOne(p => p.Action)
            .HasForeignKey(p => p.ActionKey);

        bucketAction.Navigation(e => e.Filters)
            .AutoInclude(true);

        bucketAction.Navigation(e => e.ModifyTags_AddTags)
            .AutoInclude(true);
        bucketAction.Navigation(e => e.ModifyTags_RemoveTags)
            .AutoInclude(true);
        bucketAction.Navigation(e => e.AssignToConfigurations)
            .AutoInclude(true);
        bucketAction.Navigation(e => e.ApplyValuesConfigurations)
            .AutoInclude(true);

        var bucketActionFilter = builder.Entity<BucketActionFilter>();
        bucketActionFilter.HasKey(e => e.Key);

        bucketActionFilter.HasMany(e => e.CompareFromFormValue)
            .WithOne(p => p.FilterCompareFrom)
            .HasForeignKey(p => p.FilterCompareFromKey)
            .IsRequired(false);

        bucketActionFilter.HasOne(e => e.ValueToCompareTo)
            .WithMany()
            .HasForeignKey(e => e.ValueToCompareToKey)
            .IsRequired(false);

        bucketActionFilter.Navigation(e => e.CompareFromFormValue)
            .AutoInclude(true);

        bucketActionFilter.Navigation(e => e.ValueToCompareTo)
            .AutoInclude(true);

        bucketActionFilter.Ignore(e => e.SortedCompareFromValues);

        var comparisonPair = builder.Entity<ComparisonPair>();
        comparisonPair.HasKey(e => e.Key);

        comparisonPair.HasOne(e => e.Configuration)
            .WithMany()
            .HasForeignKey(e => e.ConfigurationKey);

        comparisonPair.Navigation(e => e.Configuration)
            .AutoInclude(true);

        var applyValuesConfiguration = builder.Entity<ApplyValuesConfiguration>();
        applyValuesConfiguration.HasKey(e => e.Key);

        applyValuesConfiguration.HasOne(e => e.Action)
            .WithMany(p => p.ApplyValuesConfigurations)
            .HasForeignKey(e => e.ActionKey);

        applyValuesConfiguration.HasMany(e => e.ApplyToValueMappings)
            .WithOne(p => p.Configuration)
            .HasForeignKey(e => e.ConfigurationKey);

        applyValuesConfiguration.HasOne(e => e.AssignableToApplyTo)
            .WithMany()
            .HasForeignKey(e => e.AssignableToApplyToKey);

        applyValuesConfiguration.Navigation(e => e.ApplyToValueMappings)
            .AutoInclude(true);
        applyValuesConfiguration.Navigation(e => e.AssignableToApplyTo)
            .AutoInclude(true);

        applyValuesConfiguration.Ignore(e => e.MultiApplyTo);

        var applyToValueMap = builder.Entity<ApplyToValueMap>();
        applyToValueMap.HasKey(e => e.Key);

        applyToValueMap.HasOne(e => e.ValueFrom)
            .WithMany()
            .HasForeignKey(e => e.ValueFromKey);

        applyToValueMap.HasOne(e => e.ValueTo)
            .WithMany()
            .HasForeignKey(e => e.ValueToKey);

        applyToValueMap.Navigation(e => e.ValueFrom)
            .AutoInclude(true);
        applyToValueMap.Navigation(e => e.ValueTo)
            .AutoInclude(true);

        applyToValueMap.Ignore(e => e.ValueToFilter);

        var assignToConfiguration = builder.Entity<AssignToConfiguration>();
        assignToConfiguration.HasKey(e => e.Key);

        assignToConfiguration.HasOne(e => e.Action)
            .WithMany(p => p.AssignToConfigurations)
            .HasForeignKey(e => e.ActionKey);

        assignToConfiguration.HasMany(e => e.AssignTo)
            .WithMany(p => p.ValueToAssignToAction)
            .UsingEntity<BaseAssignableConfigAssignToConfigurationBinding>(
                r => r.HasOne(e => e.ToAssignTo)
                    .WithMany(p => p.ValueToAssignToActionActionBindings)
                    .HasForeignKey(e => e.ToAssignToKey),
                l => l.HasOne(e => e.Configuration)
                    .WithMany(p => p.AssignToConfigurations)
                    .HasForeignKey(e => e.ConfigurationKey)
            );

        assignToConfiguration.HasOne(e => e.UserToAssign)
            .WithMany()
            .HasForeignKey(e => e.UserToAssignKey);

        assignToConfiguration.Navigation(e => e.AssignTo)
            .AutoInclude(true);
        assignToConfiguration.Navigation(e => e.UserToAssign)
            .AutoInclude(true);

        assignToConfiguration.Ignore(e => e.MultiAssignTo);
        #endregion

        #region Assignable Values
        var baseAssignableConfiguration = builder.Entity<BaseAssignableConfiguration>();
        baseAssignableConfiguration.HasKey(e => e.Key);

        baseAssignableConfiguration.HasMany(e => e.FormComponents)
            .WithOne(p => p.AssignableConfiguration)
            .HasForeignKey(p => p.AssignableConfigurationKey)
            .IsRequired(false);

        var baseAssignableValues = builder.Entity<BaseAssignableValue>();
        baseAssignableValues.HasKey(e => e.Key);
        baseAssignableValues.HasOne(e => e.AssignableConfiguration)
            .WithMany(p => p.AssignableValues)
            .HasForeignKey(e => e.AssignableConfigurationId);
        baseAssignableValues.HasOne(e => e.ForUser)
            .WithMany(p => p.AssignableValues)
            .HasForeignKey(e => e.ForUserId);

        baseAssignableValues.HasOne(p => p.FormResponse)
            .WithMany(p => p.AssignableValues)
            .HasForeignKey(p => p.FormResponseKey);
        
        baseAssignableValues.Navigation(e => e.AssignableConfiguration)
            .AutoInclude();

        baseAssignableValues.Navigation(e => e.ForUser)
            .IsRequired(false);
        baseAssignableValues.Navigation(e => e.FormResponse)
            .IsRequired(false);

        var assignableValueRenderers = builder.Entity<AssignableValueRenderer>();
        assignableValueRenderers.HasKey(e => e.Key);
        assignableValueRenderers.HasMany(e => e.Conversions)
            .WithOne(p => p.Renderer)
            .HasForeignKey(p => p.RendererId);

        assignableValueRenderers.Navigation(e => e.Conversions)
            .AutoInclude(true);

        var assignableValueConversions = builder.Entity<AssignableValueConversion>();
        assignableValueConversions.HasKey(e => e.Key);

        var linkedAssignableInputSettings = builder.Entity<LinkedAssignableInputSettings>();
        linkedAssignableInputSettings.HasKey(e => e.Key);

        linkedAssignableInputSettings.HasOne(e => e.DataAuthorizationArea)
            .WithMany(p => p.LinkedAssignableInputSettings)
            .HasForeignKey(e => e.DataAuthorizationAreaKey)
            .IsRequired(false);

        linkedAssignableInputSettings.HasMany(e => e.IncludeTags)
            .WithMany()
            .UsingEntity<AuthorizationTagLinkedAssignableInputSettingsBinding>(
                r => r.HasOne(e => e.Tag)
                    .WithMany(p => p.LinkedAssignableInputSettingsBindings)
                    .HasForeignKey(e => e.TagKey),
                l => l.HasOne(e => e.IncludeFilter)
                    .WithMany(p => p.IncludeTagBindings)
                    .HasForeignKey(e => e.IncludeFilterKey)
            );

        linkedAssignableInputSettings.HasMany(e => e.ExcludeTags)
            .WithMany()
            .UsingEntity<AuthorizationTagLinkedAssignableInputSettingsBinding>(
                r => r.HasOne(e => e.Tag)
                    .WithMany(p => p.LinkedAssignableInputSettingsBindings)
                    .HasForeignKey(e => e.TagKey),
                l => l.HasOne(e => e.ExcludeFilter)
                    .WithMany(p => p.ExcludeTagBindings)
                    .HasForeignKey(e => e.ExcludeFilterKey)
            );

        linkedAssignableInputSettings.Navigation(e => e.DataAuthorizationArea)
            .AutoInclude(true);
        #endregion

        #region User
        var dataCoreUser = builder.Entity<DataCoreUser>();
        dataCoreUser.HasMany(e => e.RosterSlots)
            .WithOne(e => e.OccupiedBy)
            .HasForeignKey(e => e.OccupiedById);
        dataCoreUser.HasMany(e => e.KeyBindings)
            .WithOne(e => e.DataCoreUser)
            .HasForeignKey(e => e.DataCoreUserId);

        dataCoreUser.HasOne(e => e.SteamLink)
            .WithOne(p => p.UserSteamLink)
            .HasForeignKey<BaseAssignableValue>(p => p.UserSteamLinkKey);

        dataCoreUser.HasOne(e => e.DiscordLink)
            .WithOne(p => p.UserDiscordLink)
            .HasForeignKey<BaseAssignableValue>(p => p.UserDiscordLinkKey);

        dataCoreUser.HasOne(e => e.AccessCode)
            .WithOne(p => p.UserAccessCode)
            .HasForeignKey<BaseAssignableValue>(p => p.UserAccessCodeKey);

        dataCoreUser.Navigation(e => e.AssignableValues)
            .AutoInclude(true);
        dataCoreUser.Navigation(e => e.RosterSlots)
            .AutoInclude(true);
        dataCoreUser.Navigation(e => e.SteamLink)
            .AutoInclude(true);
        dataCoreUser.Navigation(e => e.DiscordLink)
            .AutoInclude(true);
        dataCoreUser.Navigation(e => e.AccessCode)
            .AutoInclude(true);

        dataCoreUser.HasOne(e => e.CurrentlyImpersonating)
            .WithMany()
            .HasForeignKey(e => e.CurrentlyImpersonatingKey);

        dataCoreUser.Ignore(e => e.AuthorizationCheckUser);

        var userKeybindings = builder.Entity<UserKeybinding>();
        userKeybindings.HasKey(e => e.KeyPressed);
        #endregion

        #region Account Link
        var accountLinkSettings = builder.Entity<AccountSettings>();
        accountLinkSettings.HasKey(e => e.Key);

        accountLinkSettings.HasOne(e => e.DiscordLinkConfiguration)
            .WithMany()
            .HasForeignKey(e => e.DiscordLinkConfigurationKey)
            .IsRequired(false);

        accountLinkSettings.HasOne(e => e.SteamLinkConfiguration)
            .WithMany()
            .HasForeignKey(e => e.SteamLinkConfigurationKey)
            .IsRequired(false);

        accountLinkSettings.HasOne(e => e.AccessCodeConfiguration)
            .WithMany()
            .HasForeignKey(e => e.AccessCodeConfigurationKey)
            .IsRequired(false);
        #endregion

        #region Nav
        var navModules = builder.Entity<NavModule>();
        navModules.HasKey(e => e.Key);
        navModules.HasMany(e => e.SubModules)
            .WithOne(e => e.Parent)
            .HasForeignKey(e => e.ParentId);
        navModules.Navigation(e => e.Page)
            .AutoInclude(true);
        navModules.Property(e => e.Href).IsRequired(false);
        navModules.HasOne(e => e.Page)
            .WithMany()
            .HasForeignKey(e => e.PageId);

        var routeRoot = builder.Entity<RouteRoot>();
        routeRoot.HasKey(e => e.Key);
        routeRoot.HasOne(e => e.Page)
            .WithOne(p => p.RouteRoot)
            .HasForeignKey<CustomPageSettings>(p => p.RouteRootKey)
            .IsRequired(false);

        routeRoot.HasOne(p => p.ParentRoot)
            .WithMany(e => e.ChildRoots)
            .HasForeignKey(p => p.ParentRootKey)
            .IsRequired(false);

        routeRoot.HasIndex(e => e.FullRoute)
            .IsUnique(true);

        routeRoot.HasOne(e => e.AuthorizationArea)
            .WithMany(p => p.RouteRoots)
            .HasForeignKey(e => e.AuthorizationAreaKey)
            .IsRequired(false);
        routeRoot.HasOne(e => e.CanEditAuthorizationArea)
            .WithMany(p => p.RouteRootsCanEdit)
            .HasForeignKey(e => e.CanEditAuthorizationAreaKey)
            .IsRequired(false);

        routeRoot.Navigation(e => e.AuthorizationArea)
            .AutoInclude(true);
        routeRoot.Navigation(e => e.CanEditAuthorizationArea)
            .AutoInclude(true);
        #endregion

        #region Auth

        // AREAS

        var authorizationArea = builder.Entity<AuthorizationArea>();
        authorizationArea.HasKey(e => e.Key);
        authorizationArea.HasMany(e => e.FullTrees)
            .WithMany()
            .UsingEntity<AuthorizationBinding>(
                r => r.HasOne(e => e.FullRosterTree)
                    .WithMany(p => p.FullTreeAuthorizationBindings)
                    .HasForeignKey(e => e.FullRosterTreeKey),
                l => l.HasOne(e => e.AuthorizationArea)
                    .WithMany(p => p.AuthorizationBindings)
                    .HasForeignKey(e => e.AuthorizationAreaKey));

        authorizationArea.HasMany(e => e.RosterTrees)
            .WithMany()
            .UsingEntity<AuthorizationBinding>(
                r => r.HasOne(e => e.RosterTree)
                    .WithMany(p => p.AuthorizationBindings)
                    .HasForeignKey(e => e.RosterTreeKey),
                l => l.HasOne(e => e.AuthorizationArea)
                    .WithMany(p => p.AuthorizationBindings)
                    .HasForeignKey(e => e.AuthorizationAreaKey));

        authorizationArea.HasMany(e => e.RosterSlots)
            .WithMany()
            .UsingEntity<AuthorizationBinding>(
                r => r.HasOne(e => e.RosterSlot)
                    .WithMany(p => p.AuthorizationBindings)
                    .HasForeignKey(e => e.RosterSlotKey),
                l => l.HasOne(e => e.AuthorizationArea)
                    .WithMany(p => p.AuthorizationBindings)
                    .HasForeignKey(e => e.AuthorizationAreaKey));

        authorizationArea.HasMany(e => e.Users)
            .WithMany()
            .UsingEntity<AuthorizationBinding>(
                r => r.HasOne(e => e.User)
                    .WithMany(p => p.AuthorizationBindings)
                    .HasForeignKey(e => e.UserKey),
                l => l.HasOne(e => e.AuthorizationArea)
                    .WithMany(p => p.AuthorizationBindings)
                    .HasForeignKey(e => e.AuthorizationAreaKey));

        authorizationArea.HasMany(e => e.Tags)
            .WithMany()
            .UsingEntity<AuthorizationBinding>(
                r => r.HasOne(e => e.Tag)
                    .WithMany(p => p.AuthorizationBindings)
                    .HasForeignKey(e => e.TagKey),
                l => l.HasOne(e => e.AuthorizationArea)
                    .WithMany(p => p.AuthorizationBindings)
                    .HasForeignKey(e => e.AuthorizationAreaKey));

        authorizationArea.HasMany(p => p.CustomPageSettings)
            .WithOne(e => e.AuthorizationArea)
            .HasForeignKey(p => p.AuthorizationAreaKey)
            .IsRequired(false);
        authorizationArea.HasMany(p => p.PageComponentSettings)
            .WithOne(e => e.AuthorizationArea)
            .HasForeignKey(p => p.AuthorizationAreaKey)
            .IsRequired(false);

        authorizationArea.Ignore(e => e.AuthorizedSlots);
        authorizationArea.Ignore(e => e.AuthorizedUsers);

        var authorizationBinding = builder.Entity<AuthorizationBinding>();
        authorizationBinding.HasKey(e => e.Key);

        authorizationBinding.Navigation(e => e.FullRosterTree)
            .IsRequired(false);
        authorizationBinding.Navigation(e => e.RosterTree)
            .IsRequired(false);
        authorizationBinding.Navigation(e => e.RosterSlot)
            .IsRequired(false);
        authorizationBinding.Navigation(e => e.User)
            .IsRequired(false);
        authorizationBinding.Navigation(e => e.Tag)
            .IsRequired(false);

        authorizationBinding.Navigation(e => e.AuthorizationArea)
            .AutoInclude(true);

        // TAGS

        var authorizationTag = builder.Entity<AuthorizationTag>();
        authorizationTag.HasKey(e => e.Key);

        authorizationTag.HasAlternateKey(e => e.Name);

        authorizationTag.HasMany(e => e.RosterTrees)
            .WithMany()
            .UsingEntity<AuthorizationTagTreeSlotUserBinding>(
                r => r.HasOne(e => e.RosterTree)
                    .WithMany(p => p.AuthorizationTagBindings)
                    .HasForeignKey(e => e.RosterTreeKey),
                l => l.HasOne(e => e.Tag)
                    .WithMany(p => p.AuthorizationTagBindings)
                    .HasForeignKey(e => e.TagKey));

        authorizationTag.HasMany(e => e.RosterSlots)
            .WithMany()
            .UsingEntity<AuthorizationTagTreeSlotUserBinding>(
                r => r.HasOne(e => e.RosterSlot)
                    .WithMany(p => p.AuthorizationTagBindings)
                    .HasForeignKey(e => e.RosterSlotKey),
                l => l.HasOne(e => e.Tag)
                    .WithMany(p => p.AuthorizationTagBindings)
                    .HasForeignKey(e => e.TagKey));

        authorizationTag.HasMany(e => e.Users)
            .WithMany()
            .UsingEntity<AuthorizationTagTreeSlotUserBinding>(
                r => r.HasOne(e => e.User)
                    .WithMany(p => p.AuthorizationTagBindings)
                    .HasForeignKey(e => e.UserId),
                l => l.HasOne(e => e.Tag)
                    .WithMany(p => p.AuthorizationTagBindings)
                    .HasForeignKey(e => e.TagKey));

        var authorizationTagBinding = builder.Entity<AuthorizationTagTreeSlotUserBinding>();
        authorizationTagBinding.HasKey(e => e.Key);

        authorizationTagBinding.Navigation(e => e.RosterTree)
            .IsRequired(false);
        authorizationTagBinding.Navigation(e => e.RosterSlot)
            .IsRequired(false);
        authorizationTagBinding.Navigation(e => e.User)
            .IsRequired(false);

        authorizationTagBinding.Navigation(e => e.Tag)
            .IsRequired(true);

        authorizationTag.Ignore(e => e.Editing);
        #endregion

        #region Assignable Configs
        var types = Assembly.GetAssembly(typeof(BaseAssignableConfiguration))?
            .GetTypes().Where(e => e.IsSubclassOf(typeof(BaseAssignableConfiguration)));

        if (types is not null)
        {
            foreach(var t in types)
            {
                if (t.ContainsGenericParameters)
                    continue;

                var cfg = (BaseAssignableConfiguration?)Activator.CreateInstance(t);

                if (cfg is null)
                    continue;

                cfg.Configure(builder);
            }
        }
        #endregion
    }

    public override void Dispose()
    {
        SavingChanges -= OnSave_UpdateRouteRoots;
        SavingChanges -= OnSave_UpdateAuthorizationGrants;
        SavingChanges -= OnSave_UpdateITaggables;
        SavingChanges -= OnSave_UpdateTimestamps;

        base.Dispose();
    }
}