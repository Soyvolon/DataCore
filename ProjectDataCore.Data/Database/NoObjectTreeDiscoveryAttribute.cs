﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Data.Database;

[AttributeUsage(AttributeTargets.Property)]
internal class NoObjectTreeDiscoveryAttribute : Attribute
{

}
