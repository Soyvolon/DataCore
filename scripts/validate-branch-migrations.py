import subprocess
import sys

try:
    res = sys.argv[1]
    
    # process = subprocess.run(
    #     [
    #         "git",
    #         "diff",
    #         "development.." + branch,
    #         "--compact-summary",
    #         "--",
    #         "ProjectDataCore.Data\Migrations\**"
    #     ],
    #     stdout=subprocess.PIPE,
    #     stderr=subprocess.PIPE,
    #     text=True)

    # err = process.stderr
    if 'fatal' in res:
        raise Exception(res)

    # res = process.stdout
    print(res)

    if res.count("(new)") > 2:
        raise Exception("Too many migrations in checked branch.")
    
    print("Checks complete. Branch migrations good to merge.")

    sys.exit(0)
except Exception as ex:
    print(str(ex))
    sys.exit(1)