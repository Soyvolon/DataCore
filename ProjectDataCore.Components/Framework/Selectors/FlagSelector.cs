﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Components.Framework.Selectors;
public class FlagSelector<TEnum> : MultiListSelectSelector<int>
    where TEnum : Enum
{
    [Parameter, EditorRequired]
    public required TEnum Flags { get; set; }

    [Parameter, EditorRequired]
    public required Func<TEnum, Task> OnFlagUpdatedAsync { get; set; }

    private int FlagInts { get; set; }
    private Type? FlagType { get; set; }

    protected override async Task OnParametersSetAsync()
    {
        await base.OnParametersSetAsync();

        FlagType = typeof(TEnum);
        FlagInts = Convert.ToInt32(Flags);

        Values.Clear();
        SelectedItems.Clear();
        DisplayValues.Clear();

        foreach (TEnum name in Enum.GetValues(FlagType))
        {
            var disp = name.AsFull();
            var val = Convert.ToInt32(name);

            if (val == 0)
                continue;

            Values.Add(val);

            if (Flags.HasFlag(name))
                SelectedItems.Add(val);

            DisplayValues.Add(disp);
        }

        OnValueChangedAsync = ValueChangedAsync;
    }

    private async Task ValueChangedAsync()
    {
        if (FlagType is null)
            return;

        foreach (TEnum name in Enum.GetValues(FlagType))
        {
            var val = Convert.ToInt32(name);

            if (SelectedItems.Contains(val))
                FlagInts |= val;
            else FlagInts &= ~val;
        }

        Flags = (TEnum)Enum.ToObject(FlagType, FlagInts);

        await OnFlagUpdatedAsync(Flags);
    }
}
