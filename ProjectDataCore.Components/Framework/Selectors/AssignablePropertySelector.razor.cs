﻿using ProjectDataCore.Data.Services.Alert;
using ProjectDataCore.Data.Services.Roster;
using ProjectDataCore.Data.Structures.Assignable.Configuration;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Components.Framework.Selectors;
public partial class AssignablePropertySelector
{
    [Inject]
    public required IAssignableDataService AssignableDataService { get; set; }
    [Inject]
    public required IAlertService AlertService { get; set; }


    [Parameter, EditorRequired]
    public required IAssignable Assignable { get; set; }

    [Parameter]
    public Action<string>? SetPropertyName { get; set; }

    [Parameter]
    public Action<bool>? SetPropertyType { get; set; }

    [Parameter]
    public Action<BaseAssignableConfiguration?>? SetConfiguration { get; set; }

    [Parameter]
    public bool UseStaticProperties { get; set; }
    [Parameter]
    public string PropertyName { get; set; } = string.Empty;
    [Parameter]
    public BaseAssignableConfiguration? Configuration { get; set; }
    [Parameter]
    public Type[] AllowedStaticTypes { get; set; } = Array.Empty<Type>();
    [Parameter]
    public bool AllowStatic { get; set; } = true;

    [Parameter]
    public Optional<Type?> FilterByType { get; set; } = Optional.FromNoValue<Type?>();

    [Parameter]
    public string Label { get; set; } = "Select Property";

    public List<(string, string)> Properties { get; set; } = new();

    protected List<BaseAssignableConfiguration> AssignableConfigurations { get; set; } = new();
    protected bool FirstRender = false;

    protected override async Task OnParametersSetAsync()
    {
        await base.OnParametersSetAsync();

        if (!FirstRender)
            return;

        if (UseStaticProperties)
        {
            ReloadStaticProperties();
        }
        else
        {
            ReloadAssignableProperties();
        }
    }

    protected override async Task OnAfterRenderAsync(bool firstRender)
	{
		await base.OnAfterRenderAsync(firstRender);

        if (firstRender)
        {
            if (!AllowStatic && UseStaticProperties)
            {
                OnStaticSelectorChanged(false);
            }
            else
            {
                if (UseStaticProperties)
                {
                    ReloadStaticProperties();
                }
                else
                {
                    ReloadAssignableProperties();
                }
            }

            if (Configuration is not null)
            {
                PropertyName = Configuration.NormalizedPropertyName;
            }

            StateHasChanged();

            FirstRender = true;
        }
	}

    protected void ReloadStaticProperties()
	{
        Properties.Clear();

        var typ = Assignable.GetType();

        var propertyList = typ.GetProperties();

        foreach(var property in propertyList)
		{
            if (FilterByType.HasValue)
            {
                if (FilterByType.Value != property.PropertyType)
                    continue;
            }

            if (AllowedStaticTypes.Contains(property.PropertyType))
            {
                var disp = property.GetCustomAttributes(false)
                    .FirstOrDefault(x => x is DescriptionAttribute)
                    as DescriptionAttribute;

                Properties.Add((property.Name, disp?.Description ?? property.Name));
            }
		}
	}

    protected void ReloadAssignableProperties()
	{
        Properties.Clear();

        if (Assignable is null)
            return;

		AssignableConfigurations = Assignable.AssignableValues
            .Select(e => e.AssignableConfiguration)
            .Where(e =>
            {
                if (FilterByType.HasValue)
                {
                    if (FilterByType.Value != e.GetValueType())
                        return false;
                }

                return true;
            })
            .ToList();

        foreach (var prop in AssignableConfigurations)
            Properties.Add((prop.NormalizedPropertyName, prop.PropertyName));

        if (AssignableConfigurations.Count <= 0)
            OnNameChanged(string.Empty);
	}

	protected void OnNameChanged(string e)
	{
        PropertyName = e;
        SetPropertyName?.Invoke(e);

        if (UseStaticProperties
            || SetConfiguration is null)
            return;

        Configuration = AssignableConfigurations
            .Where(e => e.NormalizedPropertyName == PropertyName)
            .FirstOrDefault();

        SetConfiguration.Invoke(Configuration);
    }

    protected void OnStaticSelectorChanged(bool e)
	{
        UseStaticProperties = e;
        SetPropertyType?.Invoke(e);

        if (UseStaticProperties)
		{
            ReloadStaticProperties();
		}
        else
		{
            ReloadAssignableProperties();
		}
	}
}
