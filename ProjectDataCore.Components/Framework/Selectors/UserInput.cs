﻿using Microsoft.AspNetCore.Identity;

using ProjectDataCore.Data.Services.User;
using ProjectDataCore.Data.Structures.Selector.User;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Components.Framework.Selectors;
public class UserInput : MultiListSelectSelector<DataCoreUser>
{
#pragma warning disable CS8618 // Injections are never null.
	[Inject]
	public IUserService UserService { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

	[Parameter]
	public UserSelectComponentSettings? UserSettings { get; set; }

	protected override async Task OnParametersSetAsync()
	{
		await base.OnParametersSetAsync();

		var res = await UserService.GetAllUsersAsync();
		Values = res;
	}

	protected override bool GetClosenessMatch(DataCoreUser user)
	{
		if (UserSettings is not null && UserSettings.Properties.Count > 0)
		{
			for (int i = 0; i < UserSettings.Properties.Count; i++)
			{
				string val;
				if (UserSettings.IsStaticList[i])
					val = user.GetStaticProperty(UserSettings.Properties[i], UserSettings.Formats[i]);
				else
					val = user.GetAssignableProperty(UserSettings.Properties[i], UserSettings.Formats[i]);

				if (val.StartsWith(SearchRaw, StringComparison.OrdinalIgnoreCase))
					return true;
			}

			return false;
		}

		return user.UserName?.StartsWith(SearchRaw, StringComparison.OrdinalIgnoreCase) ?? false;
	}

	protected override string GetDisplayValue(DataCoreUser user)
	{
		if (UserSettings is not null && UserSettings.Properties.Count > 0)
		{
			List<string> values = [];
			for (int i = 0; i < UserSettings.Properties.Count; i++)
			{
				string val;
				if (UserSettings.IsStaticList[i])
					val = user.GetStaticProperty(UserSettings.Properties[i], UserSettings.Formats[i]);
				else
					val = user.GetAssignableProperty(UserSettings.Properties[i], UserSettings.Formats[i]);

				values.Add(val);
			}

			return string.Join(" ", values);
		}

		return user.UserName ?? "";
	}
}
