﻿using Microsoft.AspNetCore.Components.Web;
using Microsoft.EntityFrameworkCore.Metadata;

using ProjectDataCore.Data.Structures.Util.Interfaces;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Components.Framework.Selectors;
public partial class MultiListSelectSelector<TValue> : ComponentBase
{
    private readonly string _id = Guid.NewGuid().ToString();

    /// <summary>
    /// An action to trigger when the value changes.
    /// </summary>
    [Parameter]
    public Func<Task>? OnValueChangedAsync { get; set; }

    [Parameter]
    public Action? OnValueChanged { get; set; }

    /// <summary>
    /// The currently selected items.
    /// </summary>
    [Parameter]
    public IList<TValue> SelectedItems { get; set; } = new List<TValue>();

    /// <summary>
    /// A list of values to select from.
    /// </summary>
    [Parameter]
    public IList<TValue> Values { get; set; } = new List<TValue>();

    /// <summary>
    /// A list of values to display insated of the value in <see cref="Values"/>.
    /// </summary>
    [Parameter]
    public IList<string> DisplayValues { get; set; } = new List<string>();
    /// <summary>
    /// A function to populate <see cref="DisplayValues"/>. Only works when
    /// not null and <see cref="DisplayValues.Count"/> is 0.
    /// </summary>
    [Parameter]
    public Func<TValue, string>? DisplayFunc { get; set; } = null;
    [Parameter]
    public string DisplayName { get; set; } = "Value Selector";
    [Parameter]
    public int DisplayedPotentialOptions { get; set; } = 5;
    [Parameter]
    public int MaxSelections { get; set; } = -1;
    private bool NoSelectionLimit { get { return MaxSelections < 0; } }

    [Parameter]
    public bool DisplayAllWhenEmpty { get; set; } = true;
    [Parameter]
    public bool DontSaveSelections { get; set; } = false;

    [Parameter]
    public bool AllowFreehand { get; set; } = false;

	[CascadingParameter(Name = "DisableEdits")]
	public bool DisableEdits { get; set; }

	public string SearchRaw { get; set; } = "";
    public List<TValue> SuggestedItems { get; set; } = new();

    public List<int> SelectedDisplays { get; set; } = new();

    private List<TValue> ToAddOnInit { get; set; } = new();

    protected override async Task OnInitializedAsync()
    {
        await base.OnInitializedAsync();

        var ttype = typeof(TValue);
        var isFreehandable = ttype.IsAssignableTo(typeof(IMultiSelectFreehandAddable));
        var isConvertable = ttype.IsAssignableTo(typeof(IConvertible));

        if (AllowFreehand && !(isConvertable || isFreehandable))
            throw new Exception($"{nameof(AllowFreehand)} can only be used when " +
                $"{nameof(TValue)} is {nameof(IMultiSelectFreehandAddable)} or " +
                $"{nameof(IConvertible)}");

        if (DisplayValues.Count == 0 && DisplayFunc is not null)
            DisplayValues = Values.Select(DisplayFunc.Invoke).ToList();
    }

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (firstRender)
        {
            StateHasChanged();

            ToAddOnInit.AddRange(SelectedItems);
            SelectedItems.Clear();

            foreach (var i in ToAddOnInit)
                await OnAddSelectionAsync(i, true);

            ToAddOnInit.Clear();
        }
    }

    protected void OnSearchUpdated(string? value)
    {
        SearchRaw = value ?? "";

        if (string.IsNullOrWhiteSpace(SearchRaw))
        {
            if (DisplayAllWhenEmpty)
            {

                if (DisplayValues.Count > 0)
                {
                    var sugDisp = OrderValues(DisplayValues);

                    AddSugDispToSuggestedItems(sugDisp);
                }
                else
                {
                    SuggestedItems = OrderValues(Values)
                        .ToList();
                }
            }
            else
            {
                SuggestedItems.Clear();
            }
            return;
        }

        if (DisplayValues.Count > 0)
        {
            var sugDisp = OrderValues(
                DisplayValues
                    .Where(x => x.StartsWith(SearchRaw, StringComparison.OrdinalIgnoreCase))
                    .Take(DisplayedPotentialOptions)
            );

            AddSugDispToSuggestedItems(sugDisp);
        }
        else
        {
            SuggestedItems = OrderValues(
                    Values
                        .Where(GetClosenessMatch)
                        .Take(DisplayedPotentialOptions)
                )
                .ToList();
        }

        StateHasChanged();
    }

	private void AddSugDispToSuggestedItems(IEnumerable<string> sugDisp)
    {
        SuggestedItems.Clear();
        foreach (var s in sugDisp)
        {
            try
            {
                SuggestedItems.Add(Values[DisplayValues.IndexOf(s)]);
            }
            catch
            {
                // Do nothing.
            }
        }
    }

    protected async Task AddFreehandInputAsync()
    {
        var newValue = SearchRaw;

        var valueType = typeof(TValue);
        Optional<TValue> valueContainer = Optional.FromNoValue<TValue>();

        if (valueType.IsAssignableTo(typeof(IConvertible)))
        {
            valueContainer = Optional.FromValue((TValue)Convert.ChangeType(newValue, valueType));
        }
        else if (valueType.IsAssignableTo(typeof(IMultiSelectFreehandAddable)))
        {
            valueContainer = Optional.FromValue(Activator.CreateInstance<TValue>());
            var freehand = valueContainer.Value as IMultiSelectFreehandAddable;
            await freehand!.ConfigureAsync(newValue);
        }

        if (!valueContainer.HasValue)
            return;

        if (DisplayValues.Count == Values.Count)
			DisplayValues.Add(newValue);

		Values.Add(valueContainer.Value);
        await OnAddSelectionAsync(valueContainer.Value);
    }

    protected async Task OnAddSelectionAsync(TValue value, bool init = false)
    {
        SelectedItems.Add(value);
        if (!init)
        {
            OnValueChanged?.Invoke();

            if (OnValueChangedAsync is not null)
                await OnValueChangedAsync.Invoke();
        }

        SuggestedItems.Clear();
        SearchRaw = "";

        if (DontSaveSelections)
        {
            SelectedItems.Clear();
        }

        StateHasChanged();
    }

    protected async Task OnRemoveSelectionAsync(TValue value)
    {
        SelectedItems.Remove(value);

        OnValueChanged?.Invoke();

        if (OnValueChangedAsync is not null)
            await OnValueChangedAsync.Invoke();

        StateHasChanged();
    }

    protected virtual bool GetClosenessMatch(TValue value)
        => value?.ToString()?
            .StartsWith(SearchRaw, StringComparison.OrdinalIgnoreCase)
            ?? false;

    protected virtual string GetDisplayValue(TValue item)
    {
        if (DisplayValues.Count > 0)
        {
            try
            {
                var i = Values.IndexOf(item);
                return DisplayValues[i];
            }
            catch
            {
                return "err: no display";
            }
        }
        else
        {
            return item?.ToString() ?? "err: no display";
        }
    }

    protected virtual IOrderedEnumerable<TValue> OrderValues(IEnumerable<TValue> values)
    {
        var comp = new MultiListSelectorOrderer(Values, DisplayValues);
        return values.OrderBy(x => x, comp);
    }

    protected virtual IOrderedEnumerable<string> OrderValues(IEnumerable<string> values)
    {
        return values.OrderBy(x => x);
    }

    private class MultiListSelectorOrderer : IComparer<TValue>
    {
        private readonly IList<TValue> _values;
        private readonly IList<string> _displays;

        public MultiListSelectorOrderer(IList<TValue> values, IList<string> displays)
        {
            _values = values;
            _displays = displays;
        }

        public int Compare(TValue? x, TValue? y)
        {
#nullable disable
            string xdisp;
            try
            {
                xdisp = _displays[_values.IndexOf(x)];
            }
            catch
            {
                xdisp = "err: no display";
            }

            string ydisp;
            try
            {
                ydisp = _displays[_values.IndexOf(y)];
            }
            catch
            {
                ydisp = "err: no display";
            }

            return xdisp.CompareTo(ydisp);
#nullable enable
        }
    }
}
