using ProjectDataCore.Data.Services.Auth;
using ProjectDataCore.Data.Services.Bus.Scoped;
using ProjectDataCore.Data.Structures.Auth;
using ProjectDataCore.Data.Structures.Auth.Tags;

namespace ProjectDataCore.Components.Framework.Selectors.Auth;

public partial class AuthorizationTagSelector
{
	[Inject]
    public required IDataCoreAuthorizationService AuthorizationService { get; set; }
    [Inject]
    public required IScopedDataBus ScopedDataBus { get; set; }

    [Parameter]
    public List<AuthorizationTagTreeSlotUserBinding> TagBindings { get; set; } = new();
    [Parameter]
    public ITaggable Taggable { get; set; }
    [Parameter]
    public string Class { get; set; } = "";
    [Parameter]
    public string Title { get; set; } = "";
    
    protected List<AuthorizationTag> Tags { get; set; } = new();
	protected List<AuthorizationTag> ExistingTags { get; set; } = new();
    protected List<string> ExistingTagNames { get; set; } = new();

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (firstRender)
        {
            await AuthorizationService.LoadAllTagsAsync(TagBindings);
            ExistingTags = await AuthorizationService.GetAllTagsAsync();

            Tags = TagBindings
                .Where(e => e.Tag is not null)
                .Select(e => e.Tag!)
                .ToList();

            ExistingTagNames = ExistingTags.Select(e => e.Name).ToList();

            StateHasChanged();
        }
    }

    protected async Task OnCloseAsync()
    {
        var oldTags = TagBindings.Select(e => e.Tag).ToList();
        
        var toRemoveTags = oldTags.Except(Tags);

        var toRemove = TagBindings.Where(x => 
            toRemoveTags.Any(e => 
                e.AuthorizationTagBindings.Contains(x)))
            .ToList();

        foreach (var i in toRemove)
            TagBindings.Remove(i);

        var toAddTags = Tags.Except(oldTags);

        foreach(var tag in toAddTags)
        {
            AuthorizationTagTreeSlotUserBinding binding = new()
            {
                TagKey = tag.Key
            };

            Taggable.Bind(binding);
            TagBindings.Add(binding);
        }

        foreach (var b in TagBindings)
            b.Tag = null;

		await ScopedDataBus.CloseMenuAsync(this, nameof(AuthorizationTagSelector));
	}
}