﻿using ProjectDataCore.Data.Services.Auth;
using ProjectDataCore.Data.Structures.Auth;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Components.Framework.Selectors.Auth;
public class AuthorizationAreaSelector : MultiListSelectSelector<AuthorizationArea>
{
    #region Hide Auto Filled Parameters
    public new Func<Task>? OnValueChangedAsync { get; set; }
    public new List<AuthorizationArea> SelectedItems { get; set; }
    public new List<AuthorizationArea> Values { get; set; }
    public new List<string> DisplayValues { get; set; }
    public new bool DontSaveSelections { get; set; } = true;
    public new int MaxSelections { get; set; } = 1;
    #endregion

#pragma warning disable CS8618 // Injections are never null.
    [Inject]
    public IDataCoreAuthorizationService AuthorizationService { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

    [Parameter]
    public Action<AuthorizationArea>? OnAuthAreaSelected { get; set; }
    [Parameter]
    public Func<AuthorizationArea, Task>? OnAuthAreaSelectedAsync { get; set; }

    protected override async Task OnParametersSetAsync()
    {
        await base.OnParametersSetAsync();

        base.DontSaveSelections = true;
        base.MaxSelections = 1;

        base.OnValueChangedAsync = async () =>
        {
            var item = base.SelectedItems.FirstOrDefault();
            if (item is not null)
            {
                OnAuthAreaSelected?.Invoke(item);
                if (OnAuthAreaSelectedAsync is not null)
                    await OnAuthAreaSelectedAsync.Invoke(item);
            }
        };

        base.Values = await AuthorizationService.GetAllAuthorizationAreasAsync();
        base.DisplayValues = base.Values.Select(e => $"{e.Name} [{e.Category}]").ToList();
    }
}
