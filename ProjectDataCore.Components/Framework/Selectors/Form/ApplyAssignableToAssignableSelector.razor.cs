using ProjectDataCore.Data.Structures.Form.Bucket.Action;

namespace ProjectDataCore.Components.Framework.Selectors.Form;

public partial class ApplyAssignableToAssignableSelector
{
    [Parameter, EditorRequired]
    public required ApplyValuesConfiguration Configuration { get; set; }
    [Parameter, EditorRequired]
    public required DynamicForm Form { get; set; }

    [Parameter]
    public string Class { get; set; } = string.Empty;
    [Parameter]
    public string ButtonClass { get; set; } = string.Empty;
}