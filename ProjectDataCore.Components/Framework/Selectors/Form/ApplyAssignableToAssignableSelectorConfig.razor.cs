using ProjectDataCore.Data.Services.Alert;
using ProjectDataCore.Data.Services.User;
using ProjectDataCore.Data.Structures.Assignable.Configuration;
using ProjectDataCore.Data.Structures.Form.Bucket.Action;
using ProjectDataCore.Data.Structures.Util.List;

namespace ProjectDataCore.Components.Framework.Selectors.Form;

public partial class ApplyAssignableToAssignableSelectorConfig
{
    [Inject]
    public required IFormService FormService { get; set; }
    [Inject]
    public required IUserService UserService { get; set; }
    [Inject]
    public required IAlertService AlertService { get; set; }

    [Parameter, EditorRequired]
    public required ApplyValuesConfiguration Configuration { get; set; }
    [Parameter, EditorRequired]
    public required DynamicForm Form { get; set; }

    protected IAssignable? Left { get; set; }
    /// <summary>
    /// The object to assign to.
    /// </summary>
    protected IAssignable? Right { get; set; }

    protected Optional<Type?> RightFilter { get; set; } = Optional.FromValue<Type?>(null);

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (firstRender)
        {
            await ReloadAssignables();
        }
    }

    protected async Task ReloadAssignables()
    {
        Left = Form.CreateResponse();

        var rightContainer = Configuration.AssignableToApplyTo;
        var rightType = rightContainer.GetValueType();

        if (rightType == typeof(DataCoreUser))
        {
            Right = await UserService.GetDefaultUserAsync();
        }
        else
        {
            AlertService.CreateErrorAlert($"Type: {rightType.Name} is not configured.");
        }

        await InvokeAsync(StateHasChanged);
    }

    protected void AddMapping()
    {
        Configuration.ApplyToValueMappings.Add(new());
        StateHasChanged();
    }

    protected void RemoveMapping(ApplyToValueMap map)
    {
        Configuration.ApplyToValueMappings.Remove(map);
        StateHasChanged();
    }

    protected void UpdateLeftProperty(ApplyToValueMap map, BaseAssignableConfiguration cfg)
    {
        map.SetValueFrom(cfg);
        StateHasChanged();
    }
}