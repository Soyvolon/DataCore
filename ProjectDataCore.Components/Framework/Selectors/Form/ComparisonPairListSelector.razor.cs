using ProjectDataCore.Components.Framework.Selectors.Auth;
using ProjectDataCore.Data.Services.Alert;
using ProjectDataCore.Data.Services.Bus.Scoped;
using ProjectDataCore.Data.Services.Roster;
using ProjectDataCore.Data.Structures.Assignable.Configuration;
using ProjectDataCore.Data.Structures.Assignable.Value;
using ProjectDataCore.Data.Structures.Form.Bucket;
using ProjectDataCore.Data.Structures.Form.Bucket.Filter;

namespace ProjectDataCore.Components.Framework.Selectors.Form;

public partial class ComparisonPairListSelector
{
    [Inject]
    public required IScopedDataBus ScopedDataBus { get; set; }
    [Inject]
    public required IAssignableDataService AssignableDataService { get; set; }
    [Inject]
    public required IAlertService AlertService { get; set; }

    [Parameter, EditorRequired]
    public required DynamicForm Form { get; set; }
    [Parameter, EditorRequired]
    public required IReadOnlyList<ComparisonPair> ComparisonPairs { get; set; }
    [Parameter, EditorRequired]
    public required BucketActionFilter Filter { get; set; }
    [Parameter, EditorRequired]
    public bool CompareTo { get; set; } = false;
    [Parameter]
    public BaseAssignableValue? AssignableValue { get; set; }

    [Parameter]
    public string Class { get; set; } = "";
    [Parameter]
    public string ButtonClass { get; set; } = "";

    private List<BaseAssignableConfiguration> SelectionChain { get; set; } = new();
    private BaseAssignableConfiguration? RootConfiguration { get; set; }
    private BaseAssignableValue? ValueToCompareTo { get; set; }

    protected async Task AssignableSelected(int depth, BaseAssignableConfiguration config)
    {
        RootConfiguration = null;

        if(SelectionChain.Count > depth)
        {
            SelectionChain.RemoveRange(depth, SelectionChain.Count - depth);
        }

        SelectionChain.Add(config);

        OnCompile();

        await InvokeAsync(StateHasChanged);
    }

    protected async Task RootSelected(int depth, BaseAssignableConfiguration config)
    {
        await AssignableSelected(depth, config);

        RootConfiguration = config;

        if (CompareTo)
        {
            ValueToCompareTo = RootConfiguration.CreateInstance();

            if (ValueToCompareTo is null)
            {
                AlertService.CreateErrorAlert("Failed to create an instance for the selected assignable value type.");
            }
        }

        OnCompile();

        await InvokeAsync(StateHasChanged);
    }

    protected void OnCompile()
    {
        if (CompareTo)
        {

        }
        else
        {
            Filter.ClearComparisonPairs();

            int order = 0;
            foreach (var selection in SelectionChain)
            {
                Filter.AddAssignableAsPair(order++, selection, ValueToCompareTo);
            }
        }
    }

    protected async Task OnCompileAndClose()
    {
        OnCompile();

        await ScopedDataBus.CloseMenuAsync(this, nameof(ComparisonPairListSelector));
    }
}