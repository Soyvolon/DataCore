using ProjectDataCore.Data.Services.Alert;
using ProjectDataCore.Data.Services.Roster;
using ProjectDataCore.Data.Structures.Assignable.Configuration;
using ProjectDataCore.Data.Structures.Form.Bucket.Filter;

namespace ProjectDataCore.Components.Framework.Selectors.Form;

public partial class ComparisonPairListSelectorPart
{
	[Inject]
	public required IAssignableDataService AssignableDataService { get; set; }
	[Inject]
	public required IAlertService AlertService { get; set; }

	[Parameter, EditorRequired]
	public required int Depth { get; set; }
	[Parameter, EditorRequired]
	public required IReadOnlyList<ComparisonPair> Values { get; set; } = Array.Empty<ComparisonPair>();

	[Parameter]
	public BaseAssignableConfiguration? ParentConfig { get; set; }
	[Parameter]
	public DynamicForm? Form { get; set; }

    [CascadingParameter(Name = "OnAssignableSelected")]
	public Func<int, BaseAssignableConfiguration, Task>? AssignableSelected { get; set; }
	[CascadingParameter(Name = "OnRootAssignableSelected")]
	public Func<int, BaseAssignableConfiguration, Task>? RootAssignableSelected { get; set; }
	[CascadingParameter(Name = "SelectionChain")]
	public List<BaseAssignableConfiguration> SelectionChain { get; set; } = new();

	private string DisplayName { get; set; } = "";
	private List<string> DisplayValues { get; set; } = new();
	private List<BaseAssignableConfiguration> ConfigValues { get; set; } = new();
	private List<BaseAssignableConfiguration> ListSelection { get; set; } = new();
	private BaseAssignableConfiguration? CurrentSelection { get; set; }

    protected override async Task OnAfterRenderAsync(bool firstRender)
	{
		await base.OnAfterRenderAsync(firstRender);

		if (firstRender)
		{
			if (Form is not null)
            {
                DisplayName = Form.Name;
                ConfigValues = Form.Properties;
			}
			else if (ParentConfig is not null)
            {
                DisplayName = ParentConfig.GetPropConfigDisplay();

                if (ParentConfig.GetValueType().IsAssignableTo(typeof(DataCoreUser)))
				{
					var userConfigsRes = await AssignableDataService.GetAllAssignableConfigurationsAsyncForUsers();
					if (!userConfigsRes.GetResult(out var res, out var err))
					{
						AlertService.CreateErrorAlert(err);
						return;
					}

					ConfigValues = res;
				}
            }

			DisplayValues = ConfigValues.Select(e => e.GetPropConfigDisplay()).ToList();

			if (Values.Count > (Depth))
			{
				var val = Values[Depth];
				CurrentSelection = val.Configuration;
			}

			StateHasChanged();
		}
	}

	protected async Task OnSelectionChanged()
	{
		var sel = ListSelection.FirstOrDefault();
        CurrentSelection = sel;

        if (sel is null)
		{
			StateHasChanged();
			return;
		}

        if (sel.GetValueType().IsAssignableTo(typeof(IAssignable)))
		{
			if (sel.GetValueType().IsAssignableTo(typeof(DataCoreUser)))
			{
				if (AssignableSelected is not null)
					await AssignableSelected(Depth, sel);
			}
			else
			{
				AlertService.CreateWarnAlert($"The configuration type selected," +
					$" {sel.GetValueType().FullName}, is not currently supported.");

				CurrentSelection = null;
			}
		}
		else
        {
            if (RootAssignableSelected is not null)
				await RootAssignableSelected(Depth, sel);
		}

		StateHasChanged();
	}
}