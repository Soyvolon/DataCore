﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components.Routing;

using ProjectDataCore.Data.Services.Alert;
using ProjectDataCore.Data.Services.Auth;
using ProjectDataCore.Data.Services.Routing;
using ProjectDataCore.Data.Structures.Auth.Tags;
using ProjectDataCore.Data.Structures.Page.Components.Parameters;
using ProjectDataCore.Data.Structures.Routing;

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Components.Framework.Roster;
public partial class RosterEditComponent : ComponentBase, IDisposable
{
#pragma warning disable CS8618 // Injects are non-nullable.
    [Inject]
    public IModularRosterService ModularRosterService { get; set; }
    [Inject]
    public IDataCoreAuthorizationService AuthorizationService { get; set; }
    [Inject]
    public IAlertService AlertService { get; set; }
    [Inject]
    public NavigationManager NavigationManager { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

    private IDisposable? LocationChangeRegistration { get; set; }

    public RosterComponentSettings ComponentSettings { get; set; } = new();

    public RosterDisplaySettings? EditDisplay { get; set; }
    public RosterTree? EditTree { get; set; }
    public Action<bool>? FullReloadListener { get; set; }

    public ConcurrentDictionary<Guid, bool> OpenRosterEdits { get; set; } = new();

    #region States
    public enum LeftMenu
    {
        RosterSelection,
        Editing,
        GlobalPermissions
    }

    public enum RightMenu
    {
        Empty,
        RosterEditor,
        GlobalPermissions
    }
    #endregion

    protected LeftMenu LeftMenuState { get; set; } = LeftMenu.RosterSelection;
    protected RightMenu RightMenuState { get; set; } = RightMenu.Empty;

    protected override async Task OnParametersSetAsync()
    {
        await base.OnParametersSetAsync();

        FullReloadListener = new(CallFullReload);
    }

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (firstRender)
        {
            await ReloadRosterListingsAsync();
            LocationChangeRegistration = NavigationManager.RegisterLocationChangingHandler(OnLocationChanging);
        }
    }

    #region Utility
    public void RequestStateHasChanged()
    {
        StateHasChanged();
    }

    public async Task RequestStateHasChangedAsync()
    {
        await InvokeAsync(StateHasChanged);
    }
    #endregion

    protected async Task DeleteRosterDisplayAsync(RosterDisplaySettings display)
    {
        var res = await ModularRosterService.RemoveRosterDisplaySettingsAsync(display.Key);

        if (!res.GetResult(out var err))
            AlertService.CreateErrorAlert(err);

        await ReloadRosterListingsAsync();
    }

    protected async Task DeleteRosterTreeAsync(RosterTree tree)
    {
        var res = await ModularRosterService.RemoveRosterTreeAsync(tree.Key);

        if (!res.GetResult(out var err))
            AlertService.CreateErrorAlert(err);

        CallFullReload(true);
    }

    protected async Task LoadRosterAsync(bool singleRefresh = false)
    {
        if (EditTree is not null)
        {
            var editRes = await ModularRosterService.GetRosterTreeByIdAsync(EditTree.Key);

            if (editRes.GetResult(out var editTree, out _))
            {
                var loader = ModularRosterService.LoadFullRosterTreeAsync(editTree, includeDisplays: true);

                if (!singleRefresh)
                {
                    await foreach (var _ in loader)
                        await InvokeAsync(StateHasChanged);
                }
                else
                {
                    await foreach (var _ in loader) { }
                    await InvokeAsync(StateHasChanged);
                }

                EditTree = editTree;
            }
        }
    }

    protected void CallFullReload(bool singleRefresh)
    {
        _ = Task.Run(async () => {
            NewRosterName = "";
            TopLevelSectionName = "";

            await LoadRosterAsync(singleRefresh);

            await ReloadRosterListingsAsync();
        });
    }

    #region Left Menu - Roster Selection
    public List<RosterDisplaySettings> AllRosterDisplays { get; set; } = new();
    public List<RosterTree> AllOrphanedRosterTrees { get; set; } = new();
    public List<RosterTree> AllTopLevelRosterTrees { get; set; } = new();

    public bool DisplayNewDisplay { get; set; } = false;
    public bool DisplayNewTree { get; set; } = false;
    public bool DisplaySearch { get; set; } = false;

    public string NewRosterName { get; set; } = "";
    public string TopLevelSectionName { get; set; } = "";

    protected async Task AddNewRosterDisplay()
    {
        if (!string.IsNullOrWhiteSpace(NewRosterName)
            && !string.IsNullOrWhiteSpace(TopLevelSectionName))
        {
            var keyRes = await ModularRosterService.UpdateOrAddRosterTreeAsync(new RosterTree()
            {
                Name = TopLevelSectionName
            });

            if (keyRes.GetResult(out var key, out var err))
            {
                var res = await ModularRosterService.AddRosterDisplaySettingsAsync(NewRosterName, key);

                _ = res.GetResult(out err);
            }

            if (err is not null)
            {
                AlertService.CreateErrorAlert(err);
                return;
            }

            CallFullReload(true);
		}
        else
        {
            AlertService.CreateErrorAlert("Both roster display name and roster tree name must be set.");
        }
    }

    protected async Task AddNewRosterTree()
    {
        if (!string.IsNullOrWhiteSpace(TopLevelSectionName))
        {
            var res = await ModularRosterService.UpdateOrAddRosterTreeAsync(new()
            {
                Name = TopLevelSectionName
            });

            if (!res.GetResult(out var err))
                AlertService.CreateErrorAlert(err);
            else
                await ReloadRosterListingsAsync();
        }
        else
        {
            AlertService.CreateErrorAlert("Roster Tree name must have a value.");
        }
    }

    public async Task ReloadRosterListingsAsync()
    {
        // Get all avalible rosters to display ...
        var displayRes = await ModularRosterService.GetAvailableRosterDisplaysAsync();

        if (displayRes.GetResult(out var displays, out var err))
        {
            // ... and add them as options.
            AllRosterDisplays = displays;
        }
        else
        {
            AlertService.CreateErrorAlert(err);
            return;
        }

        var orphanRes = await ModularRosterService.GetOrphanedRosterTreesAsync();

        if (orphanRes.GetResult(out var trees, out err))
        {
            // ... and add them as options.
            AllOrphanedRosterTrees = trees;
        }
        else
        {
            AlertService.CreateErrorAlert(err);
            return;
        }

        var topRes = await ModularRosterService.GetTopLevelRosterTreesAsync();

        if (topRes.GetResult(out trees, out err))
        {
            // ... and add them as options.
            AllTopLevelRosterTrees = trees;
        }
        else
        {
            AlertService.CreateErrorAlert(err);
            return;
        }

        ReloadDisplaysInEditTree();

        await InvokeAsync(StateHasChanged);
    }

    protected async Task EditRosterFromDisplayAsync(RosterDisplaySettings display)
    {
        var res = await ModularRosterService.GetRosterTreeForSettingsAsync(display.Key);

        if (res.GetResult(out var tree, out var err))
        {
            EditDisplay = display;

            EditRosterTree(tree);
        }
        else
        {
            AlertService.CreateErrorAlert(err);
        }
    }

    protected void EditRosterTree(RosterTree tree)
    {
        EditTree = tree;

        LeftMenuState = LeftMenu.Editing;
        RightMenuState = RightMenu.RosterEditor;

        _ = Task.Run(async () =>
        {
            await LoadRosterAsync();

            ReloadDisplaysInEditTree();
            await RequestStateHasChangedAsync();
        });
    }

    protected void StopEdit()
    {
        EditTree = null;
        EditDisplay = null;

        LeftMenuState = LeftMenu.RosterSelection;
        RightMenuState = RightMenu.RosterEditor;
    }

    private ValueTask OnLocationChanging(LocationChangingContext context)
    {
        if (LeftMenuState == LeftMenu.Editing)
        {
            context.PreventNavigation();

            if (!context.IsNavigationIntercepted)
                AlertService.CreateWarnAlert("Currently editing the roster. Please exit editing before going to a new page.", true);
        }

        return ValueTask.CompletedTask;
    }
    #endregion

    #region Left Menu - Active Edit
    public RosterDisplaySettings? DisplayEdit { get; set; } = null;
    public Guid DisplayEditKey { get; set; }
    public List<RosterDisplaySettings> AllDisplaysInEditTree { get; set; } = new();

    protected async Task SaveRosterAsync()
    {
        if (EditTree is not null)
        {
            var res = await ModularRosterService.UpdateOrAddRosterTreeAsync(EditTree);

            if (!res.GetResult(out var err))
                AlertService.CreateErrorAlert(err);
            else
            {
                await LoadRosterAsync(true);

                AlertService.CreateSuccessAlert("Roster Saved.", true, 2000);
            }
        }
    }

    protected void StartRosterDisplayEdit()
    {
        DisplayEdit = AllDisplaysInEditTree.FirstOrDefault(x => x.Key == DisplayEditKey);

        StateHasChanged();
    }

    protected void CloseRosterDisplayEdits()
    {
        DisplayEdit = null;

        StateHasChanged();
    }

    protected void ReloadDisplaysInEditTree()
    {
        if (EditTree is null)
            return;

        AllDisplaysInEditTree.Clear();

        Stack<RosterTree> trees = new();
        trees.Push(EditTree);

        while(trees.TryPop(out var tree))
        {
            AllDisplaysInEditTree.AddRange(tree.DisplaySettings);

            foreach (var child in tree.ChildRosters)
                trees.Push(child);
        }
    }

    protected async Task AbortRosterEditAsync()
    {
        StopEdit();

        await ReloadRosterListingsAsync();
    }
    #endregion

    #region Left Menu - Global Settings
    private List<RosterTagPermission> TagPermissions { get; set; } = [];
    private List<AuthorizationTag> AuthorizationTags { get; set; } = [];
    private List<AuthorizationTag> AllTags { get; set; } = [];
    private List<AuthorizationTag> SelectedTagsToAdd { get; set; } = [];
    private RosterTagPermission? PermissionToEdit { get; set; } = null;

    protected async Task EditGlobalSettingsAsync()
    {
        AllTags = await AuthorizationService.GetAllTagsAsync();
        TagPermissions = await AuthorizationService.GetAllRosterTagPermissionsAsync();
        await ReloadAuthTagsAsync();

        LeftMenuState = LeftMenu.GlobalPermissions;
        RightMenuState = RightMenu.GlobalPermissions;

        await InvokeAsync(StateHasChanged)
            .ConfigureAwait(false);
    }

    protected async Task ReloadAuthTagsAsync()
    {
        AuthorizationTags = await AuthorizationService.GetAllTagsAsync();
        foreach (var p in TagPermissions)
            AuthorizationTags.RemoveAll(e => e.Key == p.AuthorizationTagKey);
    }

    protected async Task ExitGlobalSettingsAsync()
    {
        if (PermissionToEdit is not null)
            return;

        TagPermissions = [];
        AuthorizationTags = [];

        LeftMenuState = LeftMenu.RosterSelection;
        RightMenuState = RightMenu.Empty;

        await InvokeAsync(StateHasChanged)
            .ConfigureAwait(false);
    }

    protected async Task AddNewSettingsAsync()
    {
        foreach (var t in SelectedTagsToAdd)
        {
            var permRes = await AuthorizationService.CreatePermissionFromTagAsync(t);
            if (!permRes.GetResult(out var perm, out var err))
            {
                AlertService.CreateErrorAlert(err);
                return;
            }

            TagPermissions.Add(perm);
        }

        SelectedTagsToAdd.Clear();
        TagPermissions = [.. TagPermissions.OrderBy(e => e.AuthorizationTag?.Name)];

        await ReloadAuthTagsAsync();

        await InvokeAsync(StateHasChanged)
            .ConfigureAwait(false);
    }

    protected async Task StartSettingsEditAsync(RosterTagPermission permission)
    {
        if (PermissionToEdit is not null)
            return;

        PermissionToEdit = permission;

        await InvokeAsync(StateHasChanged)
            .ConfigureAwait(false);
    }

    protected async Task StopEditingAsync()
    {
        PermissionToEdit = null;

        await InvokeAsync(StateHasChanged)
            .ConfigureAwait(false);
    }

    protected async Task SavePermissionEditsAsync()
    {
        if (PermissionToEdit is null)
            return;

        var res = await AuthorizationService.UpdateRosterTagPermissionAsync(PermissionToEdit);
        if (!res.GetResult(out var err))
        {
            AlertService.CreateErrorAlert(err);
            return;
        }

        AlertService.CreateSuccessAlert("Saved.", true);
    }

    protected async Task DeleteSettingsAsync(RosterTagPermission permission)
    {
        if (PermissionToEdit is not null)
            return;

        var res = await AuthorizationService.DeleteRosterPermissionAsync(permission);
        if (!res.GetResult(out var err))
        {
            AlertService.CreateErrorAlert(err);
            return;
        }

        TagPermissions.RemoveAll(e => e.Key == permission.Key);

        await ReloadAuthTagsAsync();

        await InvokeAsync(StateHasChanged)
            .ConfigureAwait(false);
    }
    #endregion

    #region Page Control
    private bool SidebarVisible { get; set; } = true;

    private void ToggleSidebar()
    {
        SidebarVisible = !SidebarVisible;
        StateHasChanged();
    }
    #endregion

    public void Dispose()
    {
        LocationChangeRegistration?.Dispose();
    }
}
