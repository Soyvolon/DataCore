﻿using Org.BouncyCastle.Asn1.Mozilla;

using ProjectDataCore.Data.Structures.Roster;

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Components.Framework.Roster;
public partial class RosterTreeDisplay
{
#pragma warning disable CS8618 // Injections are not null.
    [Inject]
    public IModularRosterService ModularRosterService { get; set;}
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

    private struct ClassCodes
    {
        public string Outer { get; set; }
        public string Inner { get; set; }
        public string Text { get; set; }
        
        public ClassCodes(string outer, string inner, string text)
        {
            Outer = outer;
            Inner = inner;
            Text = text;
        }
    }

    private readonly ClassCodes[] classStrings = new ClassCodes[3];
    private int depthCode = 0;

    [Parameter]
    public RosterTree? Tree { get; set; }
    [Parameter]
    public int Depth { get; set; } = 0;
    [Parameter]
    public RosterTree? Parent { get; set; }

    [CascadingParameter(Name = "RosterEdit")]
    public bool Editing { get; set; } = false;
    [CascadingParameter(Name = "RosterEditComponent")]
    public RosterEditComponent? EditingComponent { get; set; }

    [CascadingParameter(Name = "OpenEdits")]
    public ConcurrentDictionary<Guid, bool> OpenEdits { get; set; } = new();

    protected override async Task OnParametersSetAsync()
    {
        await base.OnParametersSetAsync();

        if(Tree is not null)
        {
            Tree!.OrderSlotAndRosterChildren();

            if(OpenEdits.TryGetValue(Tree.Key, out var edit))
            {
                StartRosterSectionEdit();
            }
            else
            {
                EditingRosterSection = false;
            }
        }

        depthCode = Depth % classStrings.Length;
    }

    #region Add Roster Section
    public int PendingAdd { get; set; } = -1;
    public string NewRosterSectionName { get; set; } = "";
    protected void AddNewRosterSection()
    {
        if(Tree is not null 
            && !string.IsNullOrWhiteSpace(NewRosterSectionName))
        {
            _ = Tree.InsertTree(NewRosterSectionName, PendingAdd);            

            SetPendingAdd(-1);

            EditingComponent?.RequestStateHasChanged();
        }
    }
    
    protected void SetPendingAdd(int pos)
    {
        PendingAdd = pos;
        NewRosterSectionName = "";
    }
    #endregion

    #region Edit Roster Section
    public bool EditingRosterSection { get; set; } = false;

    public string EditSectionName { get; set; } = "";
    public bool AddingDisplay { get; set; } = false;
    public string NewDisplayName { get; set; } = "";

    public bool ConfirmDelete { get; set; } = false;

    protected void ResetInnerDisplays()
    {
        AddingDisplay = false;
        ConfirmDelete = false;
    }

    protected void StartRosterSectionEdit()
    {
        ResetInnerDisplays();

        EditSectionName = Tree!.Name;
        EditingRosterSection = true;
    }

    protected void CancelRosterSectionEdit()
    {
        ResetInnerDisplays();

        EditSectionName = "";
        EditingRosterSection = false;
        _ = OpenEdits.TryRemove(Tree!.Key, out _);
    }

    protected void SaveRosterSectionEdit()
    {
        if (Tree is not null
            && !string.IsNullOrWhiteSpace(EditSectionName))
        {
            Tree.Name = EditSectionName;

            CancelRosterSectionEdit();

            EditingComponent?.RequestStateHasChanged();
        }
    }

    protected void ToggleAddDisplay()
    {
        if(!AddingDisplay)
            ResetInnerDisplays();

        AddingDisplay = !AddingDisplay;
        NewDisplayName = "";
    }

    protected async Task AddDisplayAsync()
    {
        if(Tree is not null
            && !string.IsNullOrWhiteSpace(NewDisplayName))
        {
            var res = await ModularRosterService.AddRosterDisplaySettingsAsync(NewDisplayName, Tree.Key);

            // TODO handle errors.

            CancelRosterSectionEdit();

            EditingComponent?.RequestStateHasChanged();
            EditingComponent?.ReloadRosterListingsAsync();
        }
    }

    protected void DeleteRosterTree()
    {
        if(Tree is not null && Parent is not null)
        {
            Parent.DeleteChildTree(Tree);

            EditingComponent?.RequestStateHasChanged();
        }
    }

    protected void OnRosterMoveUp()
    {
        if(Tree is not null
            && Parent is not null)
        {
            Parent.MoveUp(Tree);

            CancelRosterSectionEdit();

            OpenEdits[Tree.Key] = true;

            EditingComponent?.RequestStateHasChanged();
        }
    }

    protected void OnRosterMoveDown()
    {
        if (Tree is not null 
            && Parent is not null)
        {
            Parent.MoveDown(Tree);

            CancelRosterSectionEdit();

            OpenEdits[Tree.Key] = true;

            EditingComponent?.RequestStateHasChanged();
        }
    }
    #endregion


}
