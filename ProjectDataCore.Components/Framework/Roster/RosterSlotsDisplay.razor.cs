﻿using ProjectDataCore.Data.Services.Auth;
using ProjectDataCore.Data.Services.Bus.Scoped;
using ProjectDataCore.Data.Structures.Assignable.Value;
using ProjectDataCore.Data.Structures.Page.Components.Layout;
using ProjectDataCore.Data.Structures.Page.Components.Parameters;
using ProjectDataCore.Data.Structures.Page.Components.Scope;
using ProjectDataCore.Data.Structures.Page.Data;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Components.Framework.Roster;
public partial class RosterSlotsDisplay
{
#pragma warning disable CS8618 // Injections are not null.
    [Inject]
    public IModularRosterService ModularRosterService { get; set; }
    [Inject]
    public required IDataCoreAuthorizationService AuthorizationService { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

    [Parameter]
    public List<RosterSlot> Slots { get; set; } = new();
    [Parameter]
    public int Depth { get; set; }
    [Parameter]
    public RosterTree? Parent { get; set; }

    [CascadingParameter(Name = "RosterEdit")]
    public bool Editing { get; set; } = false;

    [CascadingParameter(Name = "RosterEditComponent")]
    public RosterEditComponent? EditingComponent { get; set; }
    [CascadingParameter(Name = "RosterComponent")]
    public RosterComponentSettings? ComponentData { get; set; }

    [CascadingParameter(Name = "ActiveUser")]
    public DataCoreUser? ActiveUser { get; set; }
    [CascadingParameter(Name = "RosterPermissions")]
    public List<RosterTagPermission> Permissions { get; set; } = [];

    public string[] DisplayValues { get; set; } = [];
    public DisplayComponentSettings[] DisplaySettings { get; set; } = [];
    public LayoutNode Node { get; set; } = new();

    public string NewRosterSlotName { get; set; } = "";
    public RosterSlot? SlotToEdit { get; set; }
    public string EditSlotName { get; set; } = "";

    protected override async Task OnParametersSetAsync()
    {
        await base.OnParametersSetAsync();

        LoadDisplayValues();
    }

    protected void LoadDisplayValues()
    {
        if (ComponentData is not null || Editing)
        {
            DisplayValues = new string[Slots.Count];
            DisplaySettings = new DisplayComponentSettings[Slots.Count];

            for (int i = 0; i < Slots.Count; i++)
            {
                if (Slots[i].OccupiedBy is not null)
                {
                    if (Editing)
                    {
                        DisplayValues[i] = Slots[i].OccupiedBy!.UserName + "/" + Slots[i].OccupiedBy!.Id;
                    }
                    else if (ComponentData is not null)
                    {
                        var scope = new AssignableScope();
                        scope.RegisterAssignable(null, Slots[i].OccupiedBy!);

                        var scopeContainer = new AssignableScopeListenerContainer()
                        {
                            ProvidingScope = scope
                        };

                        DisplayComponentData cdata = ComponentData.RosterTreeDisplayComponentData.GetDisplayCopy();
                        cdata.DisplayComponentSettings?.ScopeProviders.Add(scopeContainer);

                        var settings = new DisplayComponentSettings()
                        {
                            Data = cdata
                        };

                        DisplaySettings[i] = settings;
                    }
                }
                else
                {
                    DisplayValues[i] = "TBD";
                }
            }
        }
    }

    private async Task ReloadSlotUIAsync()
    {
        DisplaySettings = new DisplayComponentSettings[Slots.Count];

        await InvokeAsync(StateHasChanged)
            .ConfigureAwait(false);

        LoadDisplayValues();

        await InvokeAsync(StateHasChanged)
            .ConfigureAwait(false);
    }

    protected bool CanEditSlotAssignable(RosterSlot slot)
    {
        if (ComponentData?.EditingAuthArea?.IsAuthorized(ActiveUser) ?? false)
            return true;

        return Permissions.Any(e => e.IsAuthorized(ActiveUser, slot));
    }

    protected void OnSlotMoveUp(RosterSlot slot)
    {
        if(Parent is not null)
        {
            Parent.MoveUp(slot);

            EditingComponent?.RequestStateHasChanged();
        }
    }

    protected void OnSlotMoveDown(RosterSlot slot)
    {
        if(Parent is not null)
        {
            Parent.MoveDown(slot);

            EditingComponent?.RequestStateHasChanged();
        }
    }

    protected void OnAddRosterSlot()
    {
        if(Parent is not null
            && !string.IsNullOrWhiteSpace(NewRosterSlotName))
        {
            Parent.InsertSlot(NewRosterSlotName);

            EditingComponent?.RequestStateHasChanged();
        }
    }

    protected void OnEditSlot(RosterSlot? slot)
    {
        SlotToEdit = slot;
        EditSlotName = slot?.Name ?? "";
    }

    protected void OnSaveSlotEdit()
    {
        if(SlotToEdit is not null
            && !string.IsNullOrWhiteSpace(EditSlotName))
        {
            SlotToEdit.Name = EditSlotName;

            EditingComponent?.RequestStateHasChanged();

            OnEditSlot(null);
        }
    }

    protected void OnDeleteSlot(RosterSlot slot)
    {
        if(Parent is not null)
        {
            Parent.DeleteChildSlot(slot);

            EditingComponent?.RequestStateHasChanged();
        }
    }
}
