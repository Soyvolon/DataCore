﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using ProjectDataCore.Data.Structures.Page.Components.Layout;
using ProjectDataCore.Data.Structures.Page.Components.Parameters.Form;
using ProjectDataCore.Data.Structures.Page.Components.Pool;
using ProjectDataCore.Data.Structures.Page.Components.Pool.Callback;

namespace ProjectDataCore.Components.Framework.Components;
public abstract class CustomComponentBase : ComponentBase, IDisposable
{
    private bool disposedValue;

    [CascadingParameter(Name = "PageEditComponent")]
    public PageEditComponent? EditComponent { get; set; }
    [CascadingParameter(Name = "PageEdit")]
    public bool PageEdit { get; set; } = false;

    [Parameter]
    public PageComponentSettingsBase? ComponentData { get; set; }
    [Parameter]
    public LayoutNode? Node { get; set; }

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (!PageEdit && firstRender)
        {
            RegisterPoolEvents();
            RegisterAssignableScopeEvents();
        }
    }

    private void RegisterPoolEvents()
    {
        if (ComponentData is not null)
        {
            foreach (var pool in ComponentData.ComponentPools)
            {
                pool.ValuesRequested += Pool_ValuesRequested;
                pool.ClearValuesRequested += Pool_ClearValuesRequested;
            }
        }
    }

    private void DeregisterPoolEvents()
    {
        if (ComponentData is not null)
        {
            foreach (var pool in ComponentData.ComponentPools)
            {
                pool.ValuesRequested -= Pool_ValuesRequested;
                pool.ClearValuesRequested -= Pool_ClearValuesRequested;
            }
        }
	}

	protected virtual void Pool_ValuesRequested(PageComponentSettingsBase sender,
		Action<ValuesRequestedEventCallbackArgs> callback,
		ValuesRequestedEventArgs args)
	{
		// Do nothing.
	}

	protected virtual void Pool_ClearValuesRequested(PageComponentSettingsBase sender)
	{
		// Do nothing.
	}

	private void RegisterAssignableScopeEvents()
    {
		if (ComponentData is not null)
		{
			foreach (var scope in ComponentData.ScopeProviders)
			{
				scope.ProvidingScope.AssignablesChanged += AssignableScope_AssignablesChanged;
			}
		}
	}

	private void DeregisterAssignableScopeEvents()
    {
		if (ComponentData is not null)
		{
			foreach (var scope in ComponentData.ScopeProviders)
			{
				scope.ProvidingScope.AssignablesChanged -= AssignableScope_AssignablesChanged;
			}
		}
	}

	protected virtual void AssignableScope_AssignablesChanged(PageComponentSettingsBase sender)
	{
		// Do nothing.
	}

	protected virtual void AddAssignableToScopes(IAssignable assignable)
	{
		if (ComponentData is not null)
		{
			foreach (var scope in ComponentData.ScopeListeners)
			{
				scope.ListeningScope.RegisterAssignable(ComponentData, assignable);
			}
		}
	}

	protected virtual void RemoveAssignableFromScopes(IAssignable assignable)
	{
		if (ComponentData is not null)
		{
			foreach (var scope in ComponentData.ScopeListeners)
			{
				scope.ListeningScope.RemoveAssignable(ComponentData, assignable);
			}
		}
	}

	protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                DeregisterPoolEvents();
                DeregisterAssignableScopeEvents();
            }

            disposedValue = true;
        }
    }

    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
}
