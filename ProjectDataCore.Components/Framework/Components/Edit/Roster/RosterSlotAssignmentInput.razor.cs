using ProjectDataCore.Data.Services.Alert;
using ProjectDataCore.Data.Services.Bus.Scoped;
using ProjectDataCore.Data.Structures.Selector.User;

namespace ProjectDataCore.Components.Framework.Components.Edit.Roster;

public partial class RosterSlotAssignmentInput : ComponentBase
{
    [Inject]
    public required IModularRosterService RosterService { get; set; }
    [Inject]
    public required IAlertService AlertService { get; set; }
    [Inject]
    public required IScopedDataBus ScopedDataBus { get; set; }

    [CascadingParameter(Name = "Identifier")]
    public string? PopupId { get; set; }
    [CascadingParameter(Name = "ReloadSlotUI")]
    public Func<Task>? ReloadSlotUI { get; set; }

    [Parameter, EditorRequired]
    public required RosterSlot RosterSlot { get; set; }

    private UserSelectComponentSettings UserSelectComponentSettings { get; set; } = new();

    private List<DataCoreUser> SelectedUsers { get; set; } = [];
    private DataCoreUser? SelectedUser { get; set; } = null;


    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (firstRender)
        {
            SelectedUser = RosterSlot.OccupiedBy;
        }
    }

    private async Task UserSelectionChangedAsync()
    {
        SelectedUser = SelectedUsers.FirstOrDefault();

        await InvokeAsync(StateHasChanged)
            .ConfigureAwait(false);
    }

    private async Task ConfirmAssignmentAsync()
    {
        var res = await RosterService.AssignUserToSlotAsync(SelectedUser, RosterSlot);
        if (!res.GetResult(out var err))
        {
            AlertService.CreateErrorAlert(err);
            return;
        }

        AlertService.CreateSuccessAlert("User Assigned.", true);

        if (ReloadSlotUI is not null)
            await ReloadSlotUI();

        if (!string.IsNullOrWhiteSpace(PopupId))
            await ScopedDataBus.CloseMenuAsync(this, PopupId);
    }
}