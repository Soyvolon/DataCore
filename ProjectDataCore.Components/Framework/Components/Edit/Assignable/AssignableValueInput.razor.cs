using ProjectDataCore.Components.Page.Form;
using ProjectDataCore.Data.Structures.Assignable.Configuration;
using ProjectDataCore.Data.Structures.Assignable.Settings;
using ProjectDataCore.Data.Structures.Assignable.Value;
using ProjectDataCore.Data.Structures.Model.Assignable;

using static ProjectDataCore.Components.Page.Form.AssignableValueEditComponent;

namespace ProjectDataCore.Components.Framework.Components.Edit.Assignable;

public partial class AssignableValueInput : IDisposable
{
	[Parameter, EditorRequired]
	public required BaseAssignableValue Value { get; set; }
	/// <summary>
	/// The index of a value in a list of values.
	/// </summary>
	[Parameter]
	public int ValueIndex { get; set; } = 0;
	[Parameter]
	public string Label { get; set; } = "";
	[Parameter]
	public AssignableValueEditComponent? AssignableValueEditComponent { get; set; }
	[Parameter]
	public LinkedAssignableInputSettings? LinkedAssignableInputSettings { get; set; }
	[Parameter]
	public Action? ValueChanged { get; set; }

	protected AssignableConfigurationValueEditModel? EditModel { get; set; }

	protected override async Task OnAfterRenderAsync(bool firstRender)
	{
		await base.OnAfterRenderAsync(firstRender);

		if (firstRender)
		{
			EditModel = new(Value, ValueIndex);
			RegisterClearEvents();
			StateHasChanged();
		}
	}

	protected void RegisterClearEvents()
	{
		if (AssignableValueEditComponent is not null)
            AssignableValueEditComponent.AssignableClear += AssignableValueEditComponent_AssignableClear;
	}

    private void AssignableValueEditComponent_AssignableClear(object sender)
    {
		Value.ClearValue();
		EditModel = new(Value, ValueIndex);

		ValueChanged?.Invoke();

		_ = InvokeAsync(StateHasChanged);
    }

    protected void OnEditModelChanged()
	{
		if (EditModel is null)
			return;

		switch (Value)
		{
			case ValueBaseAssignableValue<DateTime> c:
				var dateTime = EditModel.DateValue;
				dateTime += EditModel.TimeValue;
				c.ReplaceValue(dateTime, ValueIndex);
				break;
			case ValueBaseAssignableValue<DateOnly> c:
				c.ReplaceValue(DateOnly.FromDateTime(EditModel.DateValue), ValueIndex);
				break;
			case ValueBaseAssignableValue<TimeOnly> c:
				c.ReplaceValue(TimeOnly.FromTimeSpan(EditModel.TimeValue), ValueIndex);
				break;

			case ValueBaseAssignableValue<int> c:
				c.ReplaceValue(EditModel.IntValue, ValueIndex);
				break;
			case ValueBaseAssignableValue<double> c:
				c.ReplaceValue(EditModel.DoubleValue, ValueIndex);
				break;

			case ValueBaseAssignableValue<string> c:
				c.ReplaceValue(EditModel.StringValue, ValueIndex);
				break;

			case LinkedValueBaseAssignableValue<DataCoreUser, Guid> c:
				if (EditModel.UserValue is not null)
					c.AddValue(EditModel.UserValue);
				else c.ClearValue();
				break;
			case LinkedValueBaseAssignableValue<RosterSlot, Guid> c:
				if (EditModel.RosterSlotValue is not null)
					c.AddValue(EditModel.RosterSlotValue);
				else c.ClearValue();
				break;
		}

		ValueChanged?.Invoke();
	}

	protected void DeregisterClearEvents()
	{
		if (AssignableValueEditComponent is not null)
			AssignableValueEditComponent.AssignableClear -= AssignableValueEditComponent_AssignableClear;

    }

    public void Dispose()
    {
		DeregisterClearEvents();
    }
}