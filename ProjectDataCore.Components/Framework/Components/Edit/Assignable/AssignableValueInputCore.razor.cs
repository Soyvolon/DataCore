using ProjectDataCore.Data.Services.Assignable;
using ProjectDataCore.Data.Services.Auth;
using ProjectDataCore.Data.Services.User;
using ProjectDataCore.Data.Structures.Assignable.Configuration;
using ProjectDataCore.Data.Structures.Assignable.Settings;
using ProjectDataCore.Data.Structures.Auth;
using ProjectDataCore.Data.Structures.Model.Assignable;

namespace ProjectDataCore.Components.Framework.Components.Edit.Assignable;

public partial class AssignableValueInputCore
{
	[Inject]
	public required ILinkedAssignableService LinkedAssignableService { get; set; }

	[CascadingParameter(Name = "ActiveUser")]
	public DataCoreUser? ActiveUser { get; set; }

	[Parameter, EditorRequired]
	public required AssignableConfigurationValueEditModel EditModel { get; set; }
	[Parameter]
	public string Label { get; set; } = "";

	[Parameter]
	public BaseAssignableConfiguration? ToEdit { get; set; }
	[Parameter]
	public Action? SingleValueUpdate { get; set; }

	[Parameter]
	public LinkedAssignableInputSettings? LinkedAssignableSettings { get; set; }

	[CascadingParameter(Name = "DisableEdits")]
	public bool DisableEdits { get; set; }


    public bool Loaded { get; set; } = false;

	protected List<DataCoreUser>? UserData { get; set; }
	protected List<string>? UserDataDisplays { get; set; }
	protected List<DataCoreUser>? UserDataSelection { get; set; }

	protected List<RosterSlot>? SlotData { get; set; }
	protected List<string>? SlotDataDisplays { get; set; }
	protected List<RosterSlot>? SlotDataSelection { get; set; }

	protected override async Task OnAfterRenderAsync(bool firstRender)
	{
		await base.OnAfterRenderAsync(firstRender);

		if (firstRender)
		{
			if (ToEdit is ILinkedAssignableConfiguration)
			{
				await LoadLinkedInputData();
			}

			Loaded = true;
			StateHasChanged();
		}
	}

	private async Task LoadLinkedInputData()
	{
		switch(ToEdit)
		{
			case DataCoreUserValueAssignableConfiguration:
				await LoadDataCoreUserData();
				break;
			case RosterSlotValueAssignableConfiguration:
				await LoadRosterSlotData();
				break;
		}
	}

	private async Task LoadDataCoreUserData()
	{
		UserDataSelection ??= new();

		UserDataSelection.Clear();

		if (LinkedAssignableSettings is null)
			return;

		UserData = await LinkedAssignableService.GetPassingUsers(LinkedAssignableSettings);
		UserDataDisplays = UserData.Select(e => e.UserName ?? "n/a")
			.ToList();
	}

	private async Task LoadRosterSlotData()
	{
		SlotDataSelection ??= new();

		SlotDataSelection.Clear();

		if (LinkedAssignableSettings is null)
            return;

        SlotData = await LinkedAssignableService.GetPassingRosterSlots(LinkedAssignableSettings);
		SlotDataDisplays = SlotData.Select(e => e.Name)
			.ToList();
    }

	private void ValueChanged(Action updateValue)
	{
		updateValue.Invoke();

		SingleValueUpdate?.Invoke();
	}
}