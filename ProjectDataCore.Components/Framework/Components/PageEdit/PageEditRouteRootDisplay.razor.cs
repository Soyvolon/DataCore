﻿using ProjectDataCore.Data.Services.History;
using ProjectDataCore.Data.Structures.Auth;
using ProjectDataCore.Data.Structures.History.PageEdit.General;
using ProjectDataCore.Data.Structures.Routing;

namespace ProjectDataCore.Components.Framework.Components.PageEdit;
public partial class PageEditRouteRootDisplay
{
#pragma warning disable CS8618 // Parameters/Injections should never be null.
    // Currently, the edit history system only works when a page edit is in progress,
    // not where these elements would be concerend.
    //[Inject]
    //public IEditHistoryService EditHistory { get; set; }

    [Parameter, EditorRequired]
    public RouteRootTree Tree { get; set; }

    [Parameter, EditorRequired]
    public Func<Task> RefreshRequested { get; set; }

    [Parameter, EditorRequired]
    public Func<RouteRoot, Task>? OnViewRouteDetails { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

    private bool DisplayChildRoutes { get; set; } = false;

    private void SwitchChildDisplay()
    {
        DisplayChildRoutes = !DisplayChildRoutes;

        StateHasChanged();
    }
}
