﻿using ProjectDataCore.Data.Structures.Page.Components.Layout;
using ProjectDataCore.Data.Structures.Page.Components.Parameters;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Components.Framework.Components;
public class StaticComponentBase : ComponentBase
{
    [Parameter]
    public PageComponentSettingsBase? ComponentData { get; set; }
    [CascadingParameter(Name = "PageEditComponent")]
    public PageEditComponent? EditComponent { get; set; }
    [Parameter]
    public LayoutNode? Node { get; set; }
}
