﻿using ProjectDataCore.Data.Structures.Page.Components.Layout;
using ProjectDataCore.Data.Structures.Page.Components.Parameters.Form;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Components.Framework.Components;
public class FormComponentBase : CustomComponentBase
{
    public FormComponentSettings? FormSettings { get => ComponentData as FormComponentSettings; }
}
