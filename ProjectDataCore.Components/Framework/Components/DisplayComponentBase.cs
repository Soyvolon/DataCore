﻿using ProjectDataCore.Data.Structures.Page.Components.Layout;
using ProjectDataCore.Data.Structures.Page.Components.Parameters;

namespace ProjectDataCore.Components.Framework.Components;
public class DisplayComponentBase : CustomComponentBase
{
    public DisplayComponentSettings? DisplaySettings { get => ComponentData as DisplayComponentSettings; }
}
