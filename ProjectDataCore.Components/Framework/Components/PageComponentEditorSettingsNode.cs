using Microsoft.AspNetCore.Components;

namespace ProjectDataCore.Components.Framework.Components;
public class PageComponentEditorSettingsNode : ComponentBase, IDisposable
{
    public class NodeSettings
    {
        public string Name { get; set; } = string.Empty;
        public required RenderFragment ChildContent { get; set; }
        public bool Remove { get; set; } = false;

        public Action? OnOpen { get; set; }
        public Func<Task>? OnOpenAsync { get; set; }

        public Action? OnClose { get; set; }
        public Func<Task>? OnCloseAsync { get; set; }
    }

    [Parameter]
    public RenderFragment ChildContent { get; set; }
    [Parameter]
    public string Name { get; set; }
    [Parameter]
    public Action? OnOpen { get; set; }
    [Parameter]
    public Func<Task>? OnOpenAsync { get; set; }
    [Parameter]
    public Action? OnClose { get; set; }
    [Parameter]
    public Func<Task>? OnCloseAsync { get; set; }

    [CascadingParameter(Name = "EditorSettingsNodeHook")]
    public Func<NodeSettings, Task> PushNode { get; set; }

    public void Dispose()
    {
        PushNode?.Invoke(new() {
            Name = Name, 
            ChildContent = ChildContent, 
            Remove = true
        });
    }

    protected override async Task OnInitializedAsync()
    {
        await base.OnInitializedAsync();

        if (PushNode is not null)
            await PushNode.Invoke(new()
            {
                Name = Name,
                ChildContent = ChildContent,
                Remove = false,
                OnOpen = OnOpen,
                OnOpenAsync = OnOpenAsync,
                OnClose = OnClose,
                OnCloseAsync = OnCloseAsync,
            });
    }
}
