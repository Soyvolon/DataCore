﻿using Microsoft.AspNetCore.Mvc.ModelBinding;

using Npgsql;

using ProjectDataCore.Data.Services.Alert;
using ProjectDataCore.Data.Services.Logging;
using ProjectDataCore.Data.Structures.Auth;
using ProjectDataCore.Data.Structures.Selector.User;

using System.ComponentModel;

namespace ProjectDataCore.Components.Framework.Components.Auth.Edit;
public partial class AuthorizationEditComponentGrantEditor
{
#pragma warning disable CS8618 // Injections are never null.
    [Inject]
    public IModularRosterService ModularRosterService { get; set; }
    [Inject]
    public IAlertService AlertService { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

    public enum GrantType
    {
        [Description("Roster Grant")]
        Roster,
        [Description("User Grant")]
        User
    }

    [CascadingParameter(Name = "AuthArea")]
    public AuthorizationArea AuthorizationArea { get; set; }
    [CascadingParameter(Name = "RequestStateChange")]
    public Func<Task> RequestStateChange { get; set; }

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (firstRender)
        {
            await ReloadActiveRosterAsync();
        }
    }

    #region Roster
    private List<RosterTree> TopLevelRosterTrees { get; set; } = new();
    private List<RosterTree> SelectedRosterTrees { get; set; } = new();
    private Stack<RosterTree> TreeStack { get; set; } = new();

    private List<RosterTree> RostersToList { get; set; } = new();
    private List<string> RostersToListDisplays { get; set; } = new();

    protected async Task ReloadActiveRosterAsync()
    {
        if (TreeStack.Count == 0)
        {
            var res = await ModularRosterService.GetTopLevelRosterTreesAsync();
            if (res.GetResult(out var values, out var err))
                TopLevelRosterTrees = values;
            else AlertService.CreateErrorAlert(err);

            RostersToList = TopLevelRosterTrees;
        }
        else
        {
            if (!TreeStack.TryPeek(out var tree))
                return;

            var res = await ModularRosterService.LoadRosterTreeAsync(tree);

            if (!res.GetResult(out var err))
                AlertService.CreateErrorAlert(err);

            RostersToList = tree.ChildRosters;
        }

        RostersToListDisplays = RostersToList.Select(x => x.Name).ToList();

        await InvokeAsync(StateHasChanged);
    }

    protected async Task MoveUpTreeStackAsync()
    {
        _ = TreeStack.TryPop(out _);
        await ReloadActiveRosterAsync();
        StateHasChanged();
    }

    protected async Task RosterSelectionChangedAsync()
    {
        var elem = SelectedRosterTrees.FirstOrDefault();

        if (elem is not null)
        {
            TreeStack.Push(elem);

            await ReloadActiveRosterAsync();

            await InvokeAsync(StateHasChanged);
        }
    }
    #endregion

    #region User
    public GrantType GrantSelection { get; set; } = GrantType.Roster;

    private List<DataCoreUser> SelectedUsersForGrants { get; set; } = new();
    // TODO: Configure this from an admin page?
    private UserSelectComponentSettings UserSelectComponentSettings { get; set; } = new();

    protected async Task UserSelectionChangedAsync()
    {
        var user = SelectedUsersForGrants.FirstOrDefault();

        if (user is not null)
        {
            await AddGrantAsync(user);

            await InvokeAsync(StateHasChanged);
        }
    }

    #endregion
    protected async Task AddGrantAsync(object grant, bool full = true)
    {
        AuthorizationBinding? binding = new()
        {
            AuthorizationArea = AuthorizationArea
        };

        switch(grant)
        {
            case RosterTree t:
                if (full)
                {
                    AuthorizationArea.FullTrees.Add(t);
                    binding.FullRosterTree = t;
                }
                else
                {
                    AuthorizationArea.RosterTrees.Add(t);
                    binding.RosterTree = t;
                }
                break;
            case RosterSlot s:
                AuthorizationArea.RosterSlots.Add(s);
                binding.RosterSlot = s;
                break;
            case DataCoreUser u:
                AuthorizationArea.Users.Add(u);
                binding.User = u;
                break;
            default:
                binding = null;
                break;
        }

        if (binding is not null)
            AuthorizationArea.AuthorizationBindings.Add(binding);

        await InvokeAsync(StateHasChanged);
    }

    protected async Task RemoveGrantAsync(object grant, bool full = true)
    {
        switch (grant)
        {
            case RosterTree t:
                if (full)
                    AuthorizationArea.FullTrees.Remove(t);
                else AuthorizationArea.RosterTrees.Remove(t);
                break;
            case RosterSlot s:
                AuthorizationArea.RosterSlots.Remove(s);
                break;
            case DataCoreUser u:
                AuthorizationArea.Users.Remove(u);
                break;
        }

        await InvokeAsync(StateHasChanged);
    }
}
