﻿using ProjectDataCore.Data.Services.Bus.Scoped;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Components.Framework.Components.Auth.Edit;
public partial class AuthorizationGrantRow
{
    [Parameter, EditorRequired]
    public string Type { get; set; } = "";
    [Parameter, EditorRequired]
    public object? Grant { get; set; }
    [Parameter, EditorRequired]
    public Func<Task>? OnButtonPressedAsync { get; set; }

    private async Task OnButtonPressedAsyncInternal()
    {
        if (OnButtonPressedAsync is not null)
            await OnButtonPressedAsync.Invoke();
    }
}
