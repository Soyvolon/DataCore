﻿using ProjectDataCore.Data.Services.Alert;
using ProjectDataCore.Data.Services.Auth;
using ProjectDataCore.Data.Services.Bus.Scoped;
using ProjectDataCore.Data.Structures.Auth;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using static ProjectDataCore.Components.Framework.Components.Auth.AuthorizationEditComponent;

namespace ProjectDataCore.Components.Framework.Components.Auth.Edit;
public partial class AuthorizationEditComponentPopupLayout
{
    private const string OnSuccessfulSaveMessage = "Area: {0} saved succesffuly.";
    public enum TabOptions
    {
        Settings,
        Grants
    }

#pragma warning disable CS8618 // Injections are never null.
    [Inject]
    public IScopedDataBus ScopedDataBus { get; set; }
    [Inject]
    public IDataCoreAuthorizationService AuthorizationService { get; set; }
    [Inject]
    public IAlertService AlertService { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

    [CascadingParameter(Name = "AuthArea")]
    public AuthorizationArea AuthorizationArea { get; set; }
    [CascadingParameter(Name = "Identifier")]
    public string Identifier { get; set; }

    public TabOptions CurrentTab { get; set; } = TabOptions.Settings;

    private Func<Task>? StateChangeRequester { get; set; }

    protected override async Task OnParametersSetAsync()
    {
        await base.OnParametersSetAsync();

        StateChangeRequester = new(OnStateChangeRequested);
    }

    protected void ChangeCurrentTab(TabOptions tabOptions)
    {
        if (tabOptions == TabOptions.Grants
            && (AuthorizationArea.AllowUnauthorized
                || AuthorizationArea.AllowOnlyAuthorized))
        {
            return;
        }

        CurrentTab = tabOptions;

        StateHasChanged();
    }

    protected async Task StopEditAsync()
    {
        await ScopedDataBus.CloseMenuAsync(this, Identifier);
    }

    protected async Task OnStateChangeRequested()
    {
        await InvokeAsync(StateHasChanged);
    }

    protected async Task SaveAreaAsync()
    {
        var res = await AuthorizationService
            .UpdateOrAddAuthorizationAreaAsync(AuthorizationArea);

        if (!res.GetResult(out var err))
            AlertService.CreateErrorAlert(err);
        else AlertService.CreateSuccessAlert(
            string.Format(OnSuccessfulSaveMessage, AuthorizationArea.Name), 
            true, 2000);
    }
}
