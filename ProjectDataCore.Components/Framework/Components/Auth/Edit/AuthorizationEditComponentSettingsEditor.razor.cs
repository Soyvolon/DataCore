﻿using ProjectDataCore.Data.Structures.Auth;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Components.Framework.Components.Auth.Edit;
public partial class AuthorizationEditComponentSettingsEditor
{
    [CascadingParameter(Name = "AuthArea")]
    public AuthorizationArea AuthorizationArea { get; set; }
}
