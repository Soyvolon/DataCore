﻿using ProjectDataCore.Data.Services.Auth;
using ProjectDataCore.Data.Structures.Auth;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Components.Framework.Components.Auth;
public partial class AuthorizationEditComponent
{

#pragma warning disable CS8618 // Injections/Parameters are never null.
    [Inject]
    public IDataCoreAuthorizationService AuthorizationService { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

	[Parameter, EditorRequired]
	public AuthorizationArea? AuthorizationArea { get; set; }
    [Parameter]
    public bool ShowDisabled { get; set; }

    private string? Identifier { get; set; } = null;

    protected override Task OnParametersSetAsync()
    {
        Identifier = AuthorizationArea?.Key.ToString();

        return base.OnParametersSetAsync();
    }
}
