﻿using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Routing;
using Microsoft.Extensions.Logging;
using Microsoft.JSInterop;

using ProjectDataCore.Data.Services.Alert;
using ProjectDataCore.Data.Services.History;
using ProjectDataCore.Data.Services.Keybindings;
using ProjectDataCore.Data.Services.Routing;
using ProjectDataCore.Data.Services.User;
using ProjectDataCore.Data.Structures.Auth;
using ProjectDataCore.Data.Structures.Keybindings;
using ProjectDataCore.Data.Structures.Page.Components.Pool;
using ProjectDataCore.Data.Structures.Page.Components.Scope;
using ProjectDataCore.Data.Structures.Routing;

using System.Collections.Concurrent;
using System.Reflection;
using System.Web;

namespace ProjectDataCore.Components.Framework.Components;
public partial class PageEditComponent : IDisposable
{
#pragma warning disable CS8618 // Inject is always non-null.
    [Inject]
    public IPageEditService PageEditService { get; set; }
    [Inject]
    public IAlertService AlertService { get; set; }
    [Inject]
    public IRoutingService RoutingService { get; set; }
    [Inject]
    public IEditHistoryService EditHistoryService { get; set; }
    [Inject]
    public IKeybindingService KeybindingService { get; set; }
    [Inject]
    public ILocalUserService LocalUserService { get; set; }
    [Inject]
    public IFormService FormService { get; set; }
    [Inject]
    public IJSRuntime JSRuntime { get; set; }
    [Inject]
    public NavigationManager NavigationManager { get; set; }
    [CascadingParameter]
    public Task<AuthenticationState> AuthStateTask { get; set; }
    [Inject]
    public ILogger<PageEditComponent> Logger { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

    private IDisposable? LocationChangingRegistration { get; set; }

    public delegate Task DraggableRefreshRequested(object sender);
    public event DraggableRefreshRequested? OnDragRefreshRequested;

    #region States
    public enum LeftMenu
    {
        RouteSelection,
        RouteSettings,
        ComponentSelection,
        SettingsSelection
    }

    public enum RightMenu
    {
        Empty,
        PageEditor,
        SettingsEditor
    }
    #endregion

    protected LeftMenu LeftMenuState { get; set; } = LeftMenu.RouteSelection;
    protected RightMenu RightMenuState { get; set; } = RightMenu.Empty;

    #region Utility
    private List<(ComponentAttribute, PageComponentListing)>? _editorComponents = null;
    public IReadOnlyList<(ComponentAttribute, PageComponentListing)> EditorComponents
    {
        get => _editorComponents ??= GetEditorComponents();
    }

    private static List<(ComponentAttribute component, 
        PageComponentListing listing)> GetEditorComponents()
    {
        var curType = typeof(CustomComponentBase);
        var subclasses = Assembly.GetAssembly(curType)!.GetTypes()
            .Where(x => x.IsSubclassOf(curType))
            .ToList(x => x.GetCustomAttributes(typeof(ComponentAttribute), true).FirstOrDefault() as ComponentAttribute)
			.Where(x => x is not null)
			.ToList(x => (component: x, listing: x!.PageComponentListing));
        
        // We filtered out the null values already.
        return subclasses!.ToList()!;
    }

    public void RequestStateHasChanged()
    {
        StateHasChanged();
    }

    public async Task RequestStateHasChangedAsync()
    {
        await InvokeAsync(StateHasChanged);
    }
    #endregion

    #region Inital Setup
    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (firstRender)
        {
            await RefreshRouteListAsync();

            LocationChangingRegistration = NavigationManager.RegisterLocationChangingHandler(OnLocationChanging);
        }

        if (OpenConfigurationNode is not null)
        {
            if (HideConfigurationOptions &&
                !ShowConfigurationOptions)
            {
                OpenConfigurationNode = null;
                HideConfigurationOptions = false;
                StateHasChanged();
            }
            else if (!ShowConfigurationOptions)
            {
                ShowConfigurationOptions = true;
                StateHasChanged();
            }
        }

        if (RegisterDroppablesOnNextRender)
        {
            RegisterDroppablesOnNextRender = false;
            await RegisterDroppableAsync();
        }
    }

    protected override async Task OnInitializedAsync()
    {
        await base.OnInitializedAsync();

        KeybindingService.RegisterKeybindListener(Keybinding.Undo, OnUndoClickedAsync);
        KeybindingService.RegisterKeybindListener(Keybinding.Redo, OnRedoClickedAsync);
    }
    #endregion

    #region Page Control
    private bool SidebarVisible { get; set; } = true;

    private void ToggleSidebar()
    {
        SidebarVisible = !SidebarVisible;
        StateHasChanged();
    }
    #endregion

    #region Left Menu - Page Selection
    private List<RouteRoot> AllowedRoots { get; set; } = new();
    private RouteRootTree AllowedRootTree { get; set; } = new();

    private bool DisplaySearch { get; set; } = false;
    private string NameSearch { get; set; } = "";
    private string RouteSearch { get; set; } = "";

    private CustomPageSettings? PageToEdit { get; set; }

    private bool DisplayNewRoute { get; set; } = false;
    private string NewRoutePath { get; set; } = "";

    private async Task RefreshRouteListAsync()
    {
        var user = LocalUserService.LocalUser;
        if (user is not null)
        {
            var routeRes = await RoutingService.GetRouteRootsForUserAsync(user);
            if (!routeRes.GetResult(out var res, out var err))
                AlertService.CreateErrorAlert(err);
            else
                AllowedRoots = res;
        }

        foreach (var root in AllowedRoots)
            AllowedRootTree.AddRoot(root);

        StateHasChanged();
    }

    private void OnNameSearchChanged(string search)
    {
        NameSearch = search;

        UpdateSearch();
    }

    private void OnRouteSearchChanged(string search)
    {
        RouteSearch = search;

        UpdateSearch();
    }

    private void UpdateSearch()
    {
        // TODO
    }

    private async Task CreateNewRouteAsync()
    {
        var path = NewRoutePath;
        if (!ValidatePath(ref path))
            return;

        var res = await RoutingService.AddRouteRootAsync(path);

        if (!res.GetResult(out var err))
        {
            AlertService.CreateErrorAlert(err);
        }
        else
        {
            NewRoutePath = "";
        }

        await RefreshRouteListAsync();
    }

    private bool ValidatePath(ref string path)
    {
        if (string.IsNullOrWhiteSpace(path))
        {
            AlertService.CreateErrorAlert("The new route must have a value.", true);
            return false;
        }

        var oldRoute = path;
        path = HttpUtility.HtmlEncode(path);

        if (!oldRoute.Equals(path))
        {
            AlertService.CreateWarnAlert("The route was changed to be HTTP safe. Please verify this " +
                "is the route you would like and attempt to create a new page again.", true);
            return false;
        }

        if (path.Contains('?')
            || path.Contains('&'))
        {
            AlertService.CreateErrorAlert("The route contains one or more of the following invalid characters: ? &");
            return false;
        }

        if (!path.StartsWith("/"))
        {
            path = "/" + path;
        }

        return true;
    }

    private async Task DeleteRouteAsync(RouteRoot route)
    {
        var res = await RoutingService.DeleteRouteRootAsync(route);

        if (!res.GetResult(out var err))
            AlertService.CreateErrorAlert(err);
        else await RefreshRouteListAsync();
    }

    private async ValueTask OnLocationChanging(LocationChangingContext context)
    {
        if (RightMenuState == RightMenu.PageEditor
            || RightMenuState == RightMenu.SettingsEditor
            || LeftMenuState == LeftMenu.RouteSettings)
        {
            context.PreventNavigation();

            if (!context.IsNavigationIntercepted)
                AlertService.CreateWarnAlert("Currently editing a page or route. Please exit editing before going to a new page.", true);
        }
        else
        {
            try
            {
                await DisposeDroppableAsync();
            }
            catch (Exception ex)
            {
                Logger.LogWarning(ex, "Failed to dispose droppables.");
            }
        }
    }
    #endregion

    #region Left Menu - Route Settings
    private RouteRoot? RouteToEdit { get; set; }

    private bool SelectingAuthArea { get; set; }
    private bool SelectingCanEditAuthArea { get; set; }

    private bool MovingPage { get; set; } = false;
    private List<RouteRoot> MoveSelectedValues { get; set; } = new();
    private List<string> RouteRootDisplays { get; set; } = new();
    private RouteRoot? MovePageTo { get; set; }

    private async Task OnViewRouteDetailsAsync(RouteRoot root)
    {
        RouteToEdit = root;
        LeftMenuState = LeftMenu.RouteSettings;

        await InvokeAsync(StateHasChanged);
    }

    private async Task<bool> SaveRouteEditsAsync()
    {
        if (RouteToEdit is null)
            return false;

        var res = await RoutingService.UpdateOrAddRouteRootAsync(RouteToEdit);
        if (res.GetResult(out var err))
        {
            AlertService.CreateSuccessAlert("Saved", true, 2000);
            return true;
        }

        AlertService.CreateErrorAlert(err);
        return false;
    }

    private async Task ReloadRouteRootAsync()
    {
        if (RouteToEdit is null)
            return;

        await RoutingService.ReloadRouteRootAsync(RouteToEdit);

        await InvokeAsync(StateHasChanged);
    }

    public void OnAddAuthArea()
    {
        if (RouteToEdit is null)
            return;

        RouteToEdit.AuthorizationArea = new();
    }

    public void OnSelectAuthArea()
    {
        SelectingAuthArea = !SelectingAuthArea;

        StateHasChanged();
    }

    public void OnAuthAreaSelectionChanged(AuthorizationArea area)
    {
        if (RouteToEdit is null)
            return;

        RouteToEdit.AuthorizationArea = area;
        RouteToEdit.AuthorizationAreaKey = area.Key;

        SelectingAuthArea = false;
        StateHasChanged();
    }

    public void OnDeleteAuthArea()
    {
        if (RouteToEdit is null)
            return;

        RouteToEdit.AuthorizationArea = null;
        RouteToEdit.AuthorizationAreaKey = null;
    }

    public void OnAddCanEditAuthArea()
    {
        if (RouteToEdit is null)
            return;

        RouteToEdit.CanEditAuthorizationArea = new();
    }

    public void OnSelectCanEditAuthArea()
    {
        SelectingCanEditAuthArea = !SelectingCanEditAuthArea;

        StateHasChanged();
    }

    public void OnCanEditAuthAreaSelectionChanged(AuthorizationArea area)
    {
        if (RouteToEdit is null)
            return;

        RouteToEdit.CanEditAuthorizationArea = area;
        RouteToEdit.CanEditAuthorizationAreaKey = area.Key;

        SelectingCanEditAuthArea = false;
        StateHasChanged();
    }

    public void OnDeleteCanEditAuthArea()
    {
        if (RouteToEdit is null)
            return;

        RouteToEdit.CanEditAuthorizationArea = null;
        RouteToEdit.CanEditAuthorizationAreaKey = null;
    }

    public async Task OnAddPageAsync()
    {
        if (RouteToEdit is null)
            return;

        RouteToEdit.CreatePage();
        var res = await RoutingService.UpdateOrAddRouteRootAsync(RouteToEdit);

        if (!res.GetResult(out var err))
        {
            AlertService.CreateErrorAlert(err);
        }

        await ReloadRouteRootAsync();
    }

    public async Task OnEditPageAsync()
    {
        if (RouteToEdit?.Page is not null)
            if (await SaveRouteEditsAsync())
                await StartPageEditAsync(RouteToEdit.Page);
    }

    public async Task OnStartPageMoveAsync()
    {
        if (RouteToEdit?.Page is null)
            return;

        await ReloadRouteRootAsync();

        RouteRootDisplays = AllowedRoots.Select(e => e.FullRoute).ToList();

        MovingPage = true;
        StateHasChanged();
    }

    public async Task OnMoveSelectionChanged()
    {
        MovePageTo = MoveSelectedValues.FirstOrDefault();

        await InvokeAsync(StateHasChanged);
    }

    public async Task OnMovePageAsync()
    {
        if (MovePageTo is null
            || RouteToEdit is null)
            return;

        if (!await SaveRouteEditsAsync())
            return;

        var res = await RoutingService.SwapRouteRootPagesAsync(RouteToEdit, MovePageTo);

        if (!res.GetResult(out var err))
        {
            AlertService.CreateErrorAlert(err);
            return;
        }

        MovingPage = false;
        StateHasChanged();
    }

    public async Task OnDeletePageAsync()
    {
        if (RouteToEdit?.Page is null)
            return;

        var res = await PageEditService.DeletePageAsync(RouteToEdit.Page.Key);
        if (!res.GetResult(out var err))
            AlertService.CreateErrorAlert(err);

        await ReloadRouteRootAsync();

        await InvokeAsync(StateHasChanged);
    }

    public async Task CloseRouteSettingsEditAsync()
    {
        RouteToEdit = null;
        LeftMenuState = LeftMenu.RouteSelection;

        await RefreshRouteListAsync();

        StateHasChanged();
    }
    #endregion

    #region Left Menu - Page Editing
    private async Task StartPageEditAsync(CustomPageSettings settings)
    {
        // New editor? Reset the edit history.
        EditHistoryService.Reset();

        PageToEdit = settings;

        var loader = RoutingService.LoadPageSettingsAsync(settings);

        await foreach (var _ in loader)
            StateHasChanged();

        LeftMenuState = LeftMenu.ComponentSelection;
        RightMenuState = RightMenu.PageEditor;

        RegisterDroppablesOnNextRender = true;

        StateHasChanged();
    }

    private async Task AbortPageEditAsync()
    {
        PageToEdit = null;
        await RefreshRouteListAsync();
        await DisposeDroppableAsync();

        LeftMenuState = LeftMenu.RouteSelection;
        RightMenuState = RightMenu.Empty;
    }

    private async Task SavePageEditAsync()
    {
        if (PageToEdit is not null)
        {
            var res = await PageEditService.UpdateOrAddPageAsync(PageToEdit);

            if (!res.GetResult(out var err))
                AlertService.CreateErrorAlert(err);
            else
                AlertService.CreateSuccessAlert("Saved.", true, 2000);
        }
    }
    #endregion

    #region Component Droppable Management
    public const string DROPPABLE_ID = "page-editor-global-draggable";
    public const string DROPPABLE_DROPZONE = "add-component";
    private DotNetObjectReference<PageEditComponent>? DotNetRef { get; set; }
    protected DotNetObjectReference<PageEditComponent> GetDotNetReference()
    {
        if (DotNetRef is null)
        {
            DotNetRef = DotNetObjectReference.Create(this);
        }

        return DotNetRef;
    }

    private bool DraggingComponent { get; set; } = false;
    private bool RegisterDroppablesOnNextRender { get; set; } = false;

    [JSInvokable]
    public async Task OnDragChanged(bool started, string type)
    {
        DraggingComponent = started;
        await InvokeAsync(StateHasChanged);
    }

    private async Task RegisterDroppableAsync()
    {
        await JSRuntime.InvokeVoidAsync("DropInterop.init", DROPPABLE_ID, GetDotNetReference(), true, true, nameof(OnDragChanged), ".drag-component");
    }

    private async Task DisposeDroppableAsync()
    {
        await JSRuntime.InvokeVoidAsync("DropInterop.destroyDroppable", DROPPABLE_ID);
    }
    #endregion

    #region Left Menu - Page Settings
    public enum SettingState
    {
        Page,
        Node,
        Component
    }

    private ConcurrentDictionary<string, PageComponentEditorSettingsNode.NodeSettings> ConfigurationNodes { get; set; } = new();
    private PageComponentEditorSettingsNode.NodeSettings? OpenConfigurationNode { get; set; } = null;
    private bool ShowConfigurationOptions { get; set; } = false;
    private bool HideConfigurationOptions { get; set; } = false;
    private SettingState SettingMenuState { get; set; } = SettingState.Page;

    public delegate Task OnConfigureMenuClosedEventArgs(PageEditComponent sender);
    public event OnConfigureMenuClosedEventArgs? OnConfigureMenuClosed;

    public async Task OnConfigureNodePushed(PageComponentEditorSettingsNode.NodeSettings settings)
    {
        if (settings.Remove)
        {
            _ = ConfigurationNodes?.TryRemove(settings.Name, out _);
        }
        else
        {
            ConfigurationNodes[settings.Name] = settings;
        }

        await InvokeAsync(StateHasChanged);
    }

    public async Task OpenPageSettingsAsync()
    {
        LeftMenuState = LeftMenu.SettingsSelection;
        RightMenuState = RightMenu.SettingsEditor;
        SettingMenuState = SettingState.Page;
        ConfigurationNodes.Clear();

        await DisposeDroppableAsync();

        await RequestStateHasChangedAsync();
    }

    public async Task OpenNodeSettingsAsync()
    {
        LeftMenuState = LeftMenu.SettingsSelection;
        RightMenuState = RightMenu.SettingsEditor;
        SettingMenuState = SettingState.Node;
        ConfigurationNodes.Clear();

        await DisposeDroppableAsync();

        await RequestStateHasChangedAsync();
    }

    public async Task OpenComponentSettingsAsync()
    {
        LeftMenuState = LeftMenu.SettingsSelection;
        RightMenuState = RightMenu.SettingsEditor;
        SettingMenuState = SettingState.Component;
        ConfigurationNodes.Clear();

        await DisposeDroppableAsync();

        await RequestStateHasChangedAsync();
    }

    private async Task CloseSettingsMenuAsync()
    {
        LeftMenuState = LeftMenu.ComponentSelection;
        RightMenuState = RightMenu.PageEditor;

        StopUserScopeEdit();

        ConfigurationNodes.Clear();

        if (OnConfigureMenuClosed is not null)
            await OnConfigureMenuClosed.Invoke(this);

        RegisterDroppablesOnNextRender = true;

        await RequestStateHasChangedAsync();
    }

    private async Task OpenConfigurationNodeAsync(string? key)
    {
        if (key is not null)
        {
            _ = ConfigurationNodes.TryGetValue(key, out var settings);
            OpenConfigurationNode = settings;

            settings?.OnOpen?.Invoke();

            if (settings?.OnOpenAsync is not null)
                await settings.OnOpenAsync.Invoke();
        }
        else
        {
            HideConfigurationOptions = true;
            ShowConfigurationOptions = false;

            OpenConfigurationNode?.OnClose?.Invoke();

            if (OpenConfigurationNode?.OnCloseAsync is not null)
                await OpenConfigurationNode.OnCloseAsync.Invoke();

            OpenConfigurationNode = null;
        }

        StateHasChanged();
    }

    public async Task CloseConfigurationNodeAsync()
        => await OpenConfigurationNodeAsync(null);
    #endregion

    #region Assignable Scopes
    private AssignableScope? AssignableScopeToEdit { get; set; } = null;
    private int AvailableComponentsForScopeIndex { get; set; } = 0;
    private List<PageComponentSettingsBase> AvailableComponentsForScope { get; set; } = new();
    private List<DynamicForm> AvailableFormsForScope { get; set; } = new();
    private bool ScopeListenToForm { get; set; } = false;

    private void AddUserScope()
    {
        if (PageToEdit is not null)
        {
            PageToEdit.AssignableScopes.Add(new());
            StateHasChanged();
        }
    }

    private async Task EditUserScopeAsync(AssignableScope scope)
    {
        AssignableScopeToEdit = scope;

        AvailableComponentsForScope.Clear();
        AvailableComponentsForScopeIndex = 0;

        await ReloadComponentListsAsync(scope);

        await InvokeAsync(StateHasChanged);
    }

    private async Task ReloadComponentListsAsync(AssignableScope scope)
    {
        if (PageToEdit is not null)
        {
            var allChildren = PageToEdit.GetChildComponents();
            AvailableComponentsForScope = allChildren
                .Except(scope.GetOrderedProvidingComponents())
                .Except(scope.GetOrderedListeningComponents())
                .ToList();

            AvailableFormsForScope = await FormService.GetAllFormsAsync();
        }
    }

    private void DeleteUserScope(AssignableScope scope)
    {
        if (PageToEdit is not null)
        {
            PageToEdit.RemoveScope(scope);

            if (AssignableScopeToEdit == scope)
                StopUserScopeEdit();
            StateHasChanged();
        }
    }

    private void StopUserScopeEdit()
    {
        AssignableScopeToEdit = null;
    }

    private async Task ListenToComponentAsync()
    {
        if (AssignableScopeToEdit is not null
            && AvailableComponentsForScopeIndex >= 0)
        {
            if (AssignableScopeToEdit.UseUserDefinition
                && AvailableComponentsForScopeIndex < AvailableComponentsForScope.Count)
            {
                var component = AvailableComponentsForScope[AvailableComponentsForScopeIndex];
                AssignableScopeToEdit.AttachProvider(component);
            }
            else if (AvailableComponentsForScopeIndex < AvailableFormsForScope.Count)
            {
                var form = AvailableFormsForScope[AvailableComponentsForScopeIndex];
                AssignableScopeToEdit.AttachForm(form);
            }

            await ReloadComponentListsAsync(AssignableScopeToEdit);
        }
    }

    private void StopListeningTo(AssignableScopeProviderContainer component)
    {
        if (AssignableScopeToEdit is not null)
        {
            AssignableScopeToEdit.DetachProvider(component);

            ReloadComponentListsAsync(AssignableScopeToEdit);
        }
    }

    private void ProvideForComponent()
    {
        if (AssignableScopeToEdit is not null
            && AvailableComponentsForScopeIndex >= 0
            && AvailableComponentsForScopeIndex < AvailableComponentsForScope.Count)
        {
            var component = AvailableComponentsForScope[AvailableComponentsForScopeIndex];
            AssignableScopeToEdit.AttachListener(component);

            ReloadComponentListsAsync(AssignableScopeToEdit);
        }
    }

    private void StopProvidingTo(AssignableScopeListenerContainer component)
    {
        if (AssignableScopeToEdit is not null)
        {
            AssignableScopeToEdit.DetachListener(component);

            ReloadComponentListsAsync(AssignableScopeToEdit);
        }
    }

    private async Task StopListeningToFormAsync()
    {
        if (AssignableScopeToEdit is not null)
        {
            AssignableScopeToEdit.FormScopeDefinition = null;
            AssignableScopeToEdit.FormScopeDefinitionKey = null;

            await ReloadComponentListsAsync(AssignableScopeToEdit);
        }
    }
    #endregion

    #region Component Pools
    public ComponentPool? ComponentPoolToEdit { get; set; }

    private void AddComponentPool()
    {
        PageToEdit?.ComponentPools.Add(new());
        StateHasChanged();
    }

    public void EditComponentPool(ComponentPool pool)
    {
        ComponentPoolToEdit = pool;
        StateHasChanged();
    }

    private void RemoveComponentPool(ComponentPool pool)
    {
        PageToEdit?.ComponentPools.Remove(pool);
        StateHasChanged();
    }
    #endregion

    #region Undo/Redo
    private async Task OnUndoClickedAsync()
    {
        await EditHistoryService.UndoAsync();

        await InvokeAsync(StateHasChanged);
    }

    private async Task OnRedoClickedAsync()
    {
        await EditHistoryService.RedoAsync();

        await InvokeAsync(StateHasChanged);
    }
    #endregion

#nullable disable
    private bool asyncDisposed;
    private bool disposedValue;

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                KeybindingService.RemoveKeybindListener(Keybinding.Undo, OnUndoClickedAsync);
                KeybindingService.RemoveKeybindListener(Keybinding.Redo, OnRedoClickedAsync);

                LocationChangingRegistration?.Dispose();
            }

            ConfigurationNodes = null;
            AllowedRoots = null;
            PageToEdit = null;
            RouteToEdit = null;

            disposedValue = true;
        }
    }

    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
#nullable enable
}
