using ProjectDataCore.Data.Services.Alert;
using ProjectDataCore.Data.Services.Roster;
using ProjectDataCore.Data.Structures.Assignable.Configuration;
using ProjectDataCore.Data.Structures.Model.Assignable;

namespace ProjectDataCore.Components.Framework.Components.Assignable;

public partial class AssignableConfigurationEditor
{
	[Inject]
	public required IAssignableDataService AssignableDataService { get; set; }
	[Inject]
	public required IAlertService AlertService { get; set; }

	[Parameter, EditorRequired]
	public required BaseAssignableConfiguration ToEdit { get; set; }

	[Parameter, EditorRequired]
	public required Func<Task> OnStopEditRequestedAsync { get; set; }

	public AssignableConfigurationValueEditModel ValueEditModel { get; set; } = new();
	public List<string> ItemList { get; set; } = new();
	public int MoveIndex { get; set; } = -1;

	protected override async Task OnAfterRenderAsync(bool firstRender)
	{
		await base.OnAfterRenderAsync(firstRender);

		if (firstRender)
		{
			ItemList = ((IAssignableConfiguration)ToEdit).GetDisplayValues();
			StateHasChanged();
		}
	}

	private async Task SaveEditAsync()
	{
		ActionResult? res = null;
		switch (ToEdit)
		{
			case ValueBaseAssignableConfiguration<DateTime> c:
				res = await AssignableDataService.UpdateAssignableConfiguration<DateTime>(ToEdit.Key, x =>
				{
					x.AllowedValues = c.AllowedValues;
					RunUpdate(x, ToEdit);
				});
				break;
			case ValueBaseAssignableConfiguration<DateOnly> c:
				res = await AssignableDataService.UpdateAssignableConfiguration<DateOnly>(ToEdit.Key, x =>
				{
					x.AllowedValues = c.AllowedValues;
					RunUpdate(x, ToEdit);
				});
				break;
			case ValueBaseAssignableConfiguration<TimeOnly> c:
				res = await AssignableDataService.UpdateAssignableConfiguration<TimeOnly>(ToEdit.Key, x =>
				{
					x.AllowedValues = c.AllowedValues;
					RunUpdate(x, ToEdit);
				});
				break;

			case ValueBaseAssignableConfiguration<int> c:
				res = await AssignableDataService.UpdateAssignableConfiguration<int>(ToEdit.Key, x =>
				{
					x.AllowedValues = c.AllowedValues;
					RunUpdate(x, ToEdit);
				});
				break;
			case ValueBaseAssignableConfiguration<double> c:
				res = await AssignableDataService.UpdateAssignableConfiguration<double>(ToEdit.Key, x =>
				{
					x.AllowedValues = c.AllowedValues;
					RunUpdate(x, ToEdit);
				});
				break;

			case ValueBaseAssignableConfiguration<string> c:
				res = await AssignableDataService.UpdateAssignableConfiguration<string>(ToEdit.Key, x =>
				{
					x.AllowedValues = c.AllowedValues;
					RunUpdate(x, ToEdit);
				});
				break;
		}

		if (res is not null)
		{
			if (!res.GetResult(out var err))
				AlertService.CreateErrorAlert(err);
			else AlertService.CreateErrorAlert(
				"Failed to find the ValueBaseAssignableConfiguration to save to.");
		}
	}

	private static void RunUpdate<T>(AssignableConfigurationEditModel<T> cfg, BaseAssignableConfiguration update)
	{
		cfg.AllowedInput = update.AllowedInput;
		cfg.AllowMultiple = update.AllowMultiple;
		cfg.PropertyName = update.PropertyName;
	}

	private void AddNewOption()
	{
		switch (ToEdit)
		{
			case IAssignableConfiguration<DateTime> c:
				var dateTime = ValueEditModel.DateValue;
				dateTime += ValueEditModel.TimeValue;
				c.AddElement(dateTime);
				ItemList.Add(dateTime.ToShortDateString() + " " + dateTime.ToShortTimeString());
				break;
			case IAssignableConfiguration<DateOnly> c:
				c.AddElement(DateOnly.FromDateTime(ValueEditModel.DateValue));
				ItemList.Add(ValueEditModel.DateValue.ToShortDateString());
				break;
			case IAssignableConfiguration<TimeOnly> c:
				c.AddElement(TimeOnly.FromTimeSpan(ValueEditModel.TimeValue));
				ItemList.Add(ValueEditModel.TimeValue.ToString("hh:mm:ss"));
				break;

			case IAssignableConfiguration<int> c:
				c.AddElement(ValueEditModel.IntValue);
				ItemList.Add(ValueEditModel.IntValue.ToString());
				break;
			case IAssignableConfiguration<double> c:
				c.AddElement(ValueEditModel.DoubleValue);
				ItemList.Add(ValueEditModel.DoubleValue.ToString());
				break;

			case IAssignableConfiguration<string> c:
				c.AddElement(ValueEditModel.StringValue);
				ItemList.Add(ValueEditModel.StringValue);
				break;
		}

		StateHasChanged();
	}

	private void OnMoveItem(int newPos)
	{
		if (ToEdit is IAssignableConfiguration config)
		{
			config.MoveElement(MoveIndex, newPos);

			var item = ItemList[MoveIndex];
			ItemList.RemoveAt(MoveIndex);
			ItemList.Insert(newPos, item);

			MoveIndex = -1;
			StateHasChanged();
		}
	}

	private void OnDeleteItem(int index)
	{
		if (ToEdit is IAssignableConfiguration config)
		{
			config.RemoveElement(index);
			ItemList.RemoveAt(index);

			StateHasChanged();
		}
	}

	private async Task DeleteAsync()
	{
		var res = await AssignableDataService.DeleteAssignableConfiguration(ToEdit.Key);

		if (!res.GetResult(out var err))
			AlertService.CreateErrorAlert(err);
	}
}