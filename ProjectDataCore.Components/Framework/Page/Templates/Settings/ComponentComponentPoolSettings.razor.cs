using ProjectDataCore.Data.Structures.Page.Components.Layout;
using ProjectDataCore.Data.Structures.Page.Components.Pool;
using ProjectDataCore.Data.Structures.Page.Components.Scope;

namespace ProjectDataCore.Components.Framework.Page.Templates.Settings;

public partial class ComponentComponentPoolSettings
{
    [Parameter, EditorRequired]
    public required LayoutNode Node { get; set; }
    [CascadingParameter(Name = "PageEditComponent")]
    public PageEditComponent? EditComponent { get; set; }

    private PageComponentSettingsBase? _component;

    private int ComponentIndex { get; set; } = 0;
    private List<ComponentPool> AvailablePools { get; set; } = new();

    protected override async Task OnParametersSetAsync()
    {
        await base.OnParametersSetAsync();

        _component = Node.Component;

        RefreshPools();
    }

    protected void RefreshPools()
    {
        if (_component is not null
            && Node.TryGetPageSettings(out var settings))
        {
            AvailablePools = settings.ComponentPools
                .Where(x => !x.Components.Any(e => e == _component))
                .Where(x => !_component.ComponentPools.Contains(x))
                .ToList();
        }
    }

    protected void AddPool()
    {
        var pool = AvailablePools.ElementAtOrDefault(ComponentIndex);

        if (pool is null)
            return;

        _component?.ComponentPools.Add(pool);
        RefreshPools();

        StateHasChanged();
    }

    protected void RemovePool(ComponentPool pool)
    {
        _component?.ComponentPools.Remove(pool);
        RefreshPools();

        RefreshPools();
    }
}