﻿using HtmlAgilityPack;
using ProjectDataCore.Data.Structures.Page.Components.Layout;
using ProjectDataCore.Data.Structures.Page.Components.Parameters;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Components.Framework.Page.Templates.Settings;
public partial class DisplaySettingsTemplate
{
    [Parameter, EditorRequired]
    public required LayoutNode Node { get; set; }

    [Parameter, EditorRequired]
    public required DisplayComponentSettings DisplaySettings { get; set; }

    [CascadingParameter(Name = "PageEditComponent")]
    public PageEditComponent? EditComponent { get; set; }

    #region Display Values
    private bool EditingAuthorized = false;
    private bool EditingUnathroized = false;

    public Timer? HTMLUpdateTimer { get; set; }
    private MarkupString? Markup { get; set; }
    private HtmlDocument HtmlDocument { get; init; } = new();

    private void ToggleEditAuthorizedDisplay()
    {
        if (EditingUnathroized) return;

        EditingAuthorized = !EditingAuthorized;

        if (!EditingAuthorized && HTMLUpdateTimer is not null)
        {
            HTMLUpdateTimer.Dispose();
            HTMLUpdateTimer = null;
            Markup = null;
        }
        else
        {
            string html = DisplaySettings?.Data.AuthorizedRaw ?? "";
            HtmlDocument.LoadHtml(html);
            if (HtmlDocument.ParseErrors.Count() <= 0)
            {
                Markup = new(html);
            }
        }

        EditComponent?.RequestStateHasChanged();
    }

    private void ToggleEditUnathorizedDisplay()
    {
        if (EditingAuthorized) return;

        EditingUnathroized = !EditingUnathroized;

        if (!EditingUnathroized && HTMLUpdateTimer is not null)
        {
            HTMLUpdateTimer.Dispose();
            HTMLUpdateTimer = null;
            Markup = null;
        }
        else
        {
            string html = DisplaySettings?.Data.UnAuthorizedRaw ?? "";
            HtmlDocument.LoadHtml(html);
            if (!HtmlDocument.ParseErrors.Any())
            {
                Markup = new(html);
            }
        }

        EditComponent?.RequestStateHasChanged();
    }

    private Task OnAuthHTMLChangedAsync(string html)
    {
        if (DisplaySettings is not null)
        {
            DisplaySettings.Data.AuthorizedRaw = html;

            if (HTMLUpdateTimer is null)
            {
                HTMLUpdateTimer = new(async (x) =>
                {
                    string html = DisplaySettings?.Data.AuthorizedRaw ?? "";
                    HtmlDocument.LoadHtml(html);
                    if (!HtmlDocument.ParseErrors.Any())
                    {
                        // await HTMLService.UpdateCustomSiteCSSAsync(html);
                        Markup = new(html);
                    }
                }, null,
                    TimeSpan.FromSeconds(5), Timeout.InfiniteTimeSpan);
            }
            else
            {
                HTMLUpdateTimer.Change(TimeSpan.FromSeconds(5), Timeout.InfiniteTimeSpan);
            }
        }

        return Task.CompletedTask;
    }

    private Task OnUnauthHTMLChangedAsync(string html)
    {
        if (DisplaySettings is not null)
        {
            DisplaySettings.Data.UnAuthorizedRaw = html;

            if (HTMLUpdateTimer is null)
            {
                HTMLUpdateTimer = new(async (x) =>
                {
                    string html = DisplaySettings?.Data.UnAuthorizedRaw ?? "";
                    HtmlDocument.LoadHtml(html);
                    if (!HtmlDocument.ParseErrors.Any())
                    {
                        // await HTMLService.UpdateCustomSiteCSSAsync(html);
                        Markup = new(html);
                    }
                }, null,
                    TimeSpan.FromSeconds(5), Timeout.InfiniteTimeSpan);
            }
            else
            {
                HTMLUpdateTimer.Change(TimeSpan.FromSeconds(5), Timeout.InfiniteTimeSpan);
            }
        }

        return Task.CompletedTask;
    }
    #endregion
}
