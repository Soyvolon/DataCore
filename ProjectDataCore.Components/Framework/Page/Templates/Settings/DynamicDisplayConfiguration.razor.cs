using HtmlAgilityPack;

using ProjectDataCore.Data.Services.HTML;
using ProjectDataCore.Data.Services.User;
using ProjectDataCore.Data.Structures.Assignable.Render;
using ProjectDataCore.Data.Structures.Assignable.Value;
using ProjectDataCore.Data.Structures.Page.Components.Layout;
using ProjectDataCore.Data.Structures.Page.Components.Parameters;
using ProjectDataCore.Data.Structures.Page.Components.Scope;

namespace ProjectDataCore.Components.Framework.Page.Templates.Settings;

public partial class DynamicDisplayConfiguration
{
    [Parameter, EditorRequired]
    public required LayoutNode Node { get; set; }

    [Parameter, EditorRequired]
    public required DisplayComponentSettings DisplaySettings { get; set; }
}