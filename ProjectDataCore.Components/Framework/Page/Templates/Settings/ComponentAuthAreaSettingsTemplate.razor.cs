using ProjectDataCore.Data.Structures.Auth;
using ProjectDataCore.Data.Structures.Page.Components.Layout;

namespace ProjectDataCore.Components.Framework.Page.Templates.Settings;

public partial class ComponentAuthAreaSettingsTemplate
{
	[Parameter, EditorRequired]
	public required LayoutNode Node { get; set; }
	[CascadingParameter(Name = "PageEditComponent")]
	public PageEditComponent? EditComponent { get; set; }

	[Parameter]
	public RenderFragment? CustomAuthArea { get; set; }

	protected void OnAuthAreaChanged(AuthorizationArea area)
	{
		Node.AuthorizationArea = area;
		StateHasChanged();
	}
}