﻿using ProjectDataCore.Data.Services.HTML;
using ProjectDataCore.Data.Services.User;
using ProjectDataCore.Data.Structures.Assignable.Render;
using ProjectDataCore.Data.Structures.Assignable.Value;
using ProjectDataCore.Data.Structures.Page.Components.Layout;
using ProjectDataCore.Data.Structures.Page.Components.Parameters;
using ProjectDataCore.Data.Structures.Page.Components.Scope;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Components.Framework.Page.Templates.Settings;
public partial class AssignableValueSettingsTemplate : ComponentBase
{
    [Inject]
    public required IHTMLService HTMLService { get; set; }
    [Inject]
    public required IUserService UserService { get; set; }

    [Parameter, EditorRequired]
    public required LayoutNode Node { get; set; }

    [Parameter, EditorRequired]
    public required DisplayComponentSettings DisplaySettings { get; set; }

    [Parameter]
    public IAssignable? ForceScope { get; set; }

    [CascadingParameter(Name = "PageEditComponent")]
    public PageEditComponent? EditComponent { get; set; }

    #region Assignable Values
    private static readonly Type[] AllowedStaticTypes = new[]
    {
        // TODO add Guid support for assignable types
        // typeof(Guid),
        typeof(string),
        typeof(ulong)
    };

    private AssignableValueRenderer? AssignableValueRendererToEdit { get; set; }
    private AssignableValueConversion? AssignableValueConversionToEdit { get; set; }
    private BaseAssignableValue? ValueConversionContainer { get; set; }

    protected List<string> ScopeNames { get; set; } = new();
    protected List<AssignableScopeListenerContainer> ScopeToPickFrom { get; set; } = new();
    protected AssignableScope? SelectedScope { get; set; } = null;
    protected IAssignable? ConfigAssignable { get; set; } = null;

    protected override async Task OnParametersSetAsync()
    {
        await base.OnParametersSetAsync();

        if (ForceScope is not null)
            ConfigAssignable = ForceScope;
    }

    private void AddAssignableValueRenderer()
    {
        DisplaySettings?.AssignableValueRenderers.Add(new()
        {
            ParameterComponentSettings = DisplaySettings
        });

        EditComponent?.RequestStateHasChanged();
    }

    private void DeleteAssignableValueRenderer(AssignableValueRenderer renderer)
    {
        if (AssignableValueRendererToEdit == renderer)
        {
            AssignableValueRendererToEdit = null;
        }

        DisplaySettings?.AssignableValueRenderers.Remove(renderer);

        EditComponent?.RequestStateHasChanged();
    }

    private async Task EditAssignableValueRendererAsync(AssignableValueRenderer? renderer)
    {
        if (DisplaySettings is null)
            return;

        AssignableValueRendererToEdit = renderer;

        if (AssignableValueRendererToEdit is not null)
        {
            ScopeNames = DisplaySettings.ScopeProviders
                .Select(e => e.ProvidingScope.DisplayName).ToList() ?? new();

            var scope = DisplaySettings.ScopeProviders
                .Where(e => e.ProvidingScopeId == AssignableValueRendererToEdit.ScopeProviderKey)
                .FirstOrDefault();

            if (scope is not null)
            {
                ScopeToPickFrom.Clear();
                ScopeToPickFrom.Add(scope);

                await ChangeScope();
            }
        }

        EditComponent?.RequestStateHasChanged();
    }

    private void AddAssignableValueConversion()
    {
        AssignableValueRendererToEdit?.Conversions.Add(new()
        {
            Renderer = AssignableValueRendererToEdit
        });

        EditComponent?.RequestStateHasChanged();
    }

    private void DeleteAssignableValueConversion(AssignableValueConversion conversion)
    {
        if (AssignableValueConversionToEdit == conversion)
        {
            AssignableValueConversionToEdit = null;
        }

        AssignableValueRendererToEdit?.Conversions.Remove(conversion);

        EditComponent?.RequestStateHasChanged();
    }

    protected async Task OnScopeToPickChanged()
    {
        await ChangeScope();

        if (EditComponent is not null)
            await EditComponent.RequestStateHasChangedAsync();
    }

    protected async Task ChangeScope()
    {
        if (AssignableValueRendererToEdit is null)
            return;

        SelectedScope = ScopeToPickFrom.FirstOrDefault()?.ProvidingScope;

        AssignableValueRendererToEdit.ScopeProviderKey = SelectedScope?.Key ?? default;

        if (SelectedScope is not null)
            ConfigAssignable = await SelectedScope.GetConfigurationAssignableAsync(UserService);

        if (EditComponent is not null)
            await EditComponent.RequestStateHasChangedAsync();
    }

    private async Task EditAssignableValueConversionAsync(AssignableValueConversion? conversion)
    {
        AssignableValueConversionToEdit = conversion;

        await ReloadValueConversionContainerAsync();

        EditComponent?.RequestStateHasChanged();
    }

    private Task ReloadValueConversionContainerAsync()
    {
        if (AssignableValueRendererToEdit is not null
            && DisplaySettings is not null)
        {
            if (AssignableValueRendererToEdit.Static)
            {
                ValueConversionContainer = ConfigAssignable?.GetStaticPropertyContainer(AssignableValueRendererToEdit.PropertyName);
            }
            else
            {
                ValueConversionContainer = ConfigAssignable?.GetAssignablePropertyContainer(AssignableValueRendererToEdit.PropertyName);
            }
        }

        return Task.CompletedTask;
    }

    private void DateTime_ConvertToTimeSpan_ValueChanged(bool x)
    {
        if (AssignableValueConversionToEdit is not null)
        {
            AssignableValueConversionToEdit.DateTime_ConvertToTimeSpan = x;

            EditComponent?.RequestStateHasChanged();
        }
    }

    private void DateTime_ManualTimeSpanComparision_ValueChanged(bool x)
    {
        if (AssignableValueConversionToEdit is not null)
        {
            AssignableValueConversionToEdit.DateTime_ManualTimeSpanComparision = x;

            EditComponent?.RequestStateHasChanged();
        }
    }
    #endregion

}
