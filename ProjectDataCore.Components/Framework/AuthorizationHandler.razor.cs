﻿using Microsoft.AspNetCore.Components.Authorization;
using ProjectDataCore.Data.Services.Alert;
using ProjectDataCore.Data.Services.Auth;
using ProjectDataCore.Data.Services.User;
using ProjectDataCore.Data.Structures.Auth;
using ProjectDataCore.Data.Structures.Page.Components.Layout;
using ProjectDataCore.Data.Structures.Routing;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Components.Framework;
public partial class AuthorizationHandler
{
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    [Inject]
    public IAlertService AlertService { get; set; }

    [Inject]
    public IDataCoreAuthorizationService AuthorizationService { get; set; }

    [Parameter, EditorRequired]
    public RenderFragment Authorized { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

    [CascadingParameter(Name = "ActiveUser")]
    public DataCoreUser? ActiveUser { get; set; }

    [Parameter]
    public RenderFragment? NotAuthorized { get; set; }

    [Parameter]
    public AuthorizationArea? Area { get; set; }

    [Parameter]
    public RouteRoot? Route { get; set; }

    [CascadingParameter(Name = "PageEdit")]
    public bool Editing { get; set; } = false;

    private bool IsAuthorized { get; set; } = false;

    protected override async Task OnParametersSetAsync()
    {
        await base.OnParametersSetAsync();

        if (!Editing)
        {
            if (Route is not null)
            {
                IsAuthorized = await AuthorizationService.AuthorizeRouteRootAsync(ActiveUser, Route);
            }
            else
            {
                IsAuthorized = AuthorizationArea.Authorize(ActiveUser, Area);
            }
        }
        else
        {
            IsAuthorized = true;
        }
    }
}
