﻿using Microsoft.AspNetCore.Components.Web;
using Microsoft.JSInterop;

using ProjectDataCore.Data.Services.Bus.Scoped;
using ProjectDataCore.Data.Structures.Events.Parameters;
using ProjectDataCore.Data.Structures.Page.ContextMenu;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Components.Framework.Menus;
public partial class ModalMenu : PopupMenuBase, IDisposable
{
#pragma warning disable CS8618 // Injections are never null.
    [Inject]
    public IScopedDataBus ScopedDataBus { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
	[Parameter, EditorRequired]
	public string Identifier { get; set; } = "";
	[Parameter]
	public bool DisplayCloseButton { get; set; } = true;

	private bool _positionReady = false;
	private bool _menuReady = false;

	protected override async Task OnInitializedAsync()
	{
		await base.OnInitializedAsync();

		ScopedDataBus.MenuClosed += ScopedDataBus_MenuClosed;
	}

	private Task OnClick(MouseEventArgs args)
	{
		_positionReady = true;

		StateHasChanged();

		return Task.CompletedTask;
	}

	private async Task OnMenuLoaded()
	{
		if (!_menuReady)
		{
			_menuReady = true;

			await ScopedDataBus.RequestMenuReload(this);
		}
	}

	private async Task OnMenuReady()
	{
		if (_menuReady)
		{
			// TODO?
		}
	}

	private Task ScopedDataBus_MenuClosed(object sender, DisplayMenuEventArgs args)
	{
		if (args.Id != Identifier)
			return Task.CompletedTask;

		_ = Task.Run(AbortMenuAsync);

		return Task.CompletedTask;
	}

	private async Task AbortMenuAsync()
	{
		_positionReady = false;
		_menuReady = false;

		await InvokeAsync(StateHasChanged);
	}

	public void Dispose()
    {
        ScopedDataBus.MenuClosed -= ScopedDataBus_MenuClosed;
    }
}
