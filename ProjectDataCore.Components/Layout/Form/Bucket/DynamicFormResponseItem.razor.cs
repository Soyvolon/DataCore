using ProjectDataCore.Data.Structures.Page.Components.Layout;
using ProjectDataCore.Data.Structures.Page.Components.Parameters;
using ProjectDataCore.Data.Structures.Page.Components.Parameters.Form;
using ProjectDataCore.Data.Structures.Page.Components.Scope;

namespace ProjectDataCore.Components.Layout.Form.Bucket;

public partial class DynamicFormResponseItem
{
    [Parameter, EditorRequired]
    public required DynamicFormResponse Response { get; set; }
    [Parameter, EditorRequired]
    public required BucketComponentSettings Settings { get; set; }

    [Parameter]
    public bool HasActions { get; set; } = false;

    private DisplayComponentSettings? _settings;
    private readonly LayoutNode _node = new();

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (firstRender)
        {
            var scope = new AssignableScope();
            scope.RegisterAssignable(null, Response);

            var scopeContainer = new AssignableScopeListenerContainer()
            {
                ProvidingScope = scope
            };

            Settings.ItemSummaryDisplayConfig.DisplayComponentSettings?.ScopeProviders.Add(scopeContainer);

            _settings = new DisplayComponentSettings() 
            { 
                Data = Settings.ItemSummaryDisplayConfig
            };

            StateHasChanged();
        }
    }
}