using Microsoft.JSInterop;

using ProjectDataCore.Data.Structures.Util.Interfaces;
using ProjectDataCore.Data.Structures.Util.List;

using System.Diagnostics.CodeAnalysis;

namespace ProjectDataCore.Components.Layout.List;

public partial class ListRenderer<TValue> //: IDisposable, IAsyncDisposable
{
    //private bool disposedValue;
    //private bool disposedAsync;

    public class ListRenderContext<T>
	{ 
		public required T Value { get; set; }
		public required int Order { get; set; }
        public required int Index { get; set; }

		[SetsRequiredMembers]
		public ListRenderContext(T value, int order, int index) 
		{ 
			Value = value;
			Order = order;
            Index = index;
		}
	}

    [Inject]
    public required IJSRuntime JSRuntime { get; set; }

    [Parameter, EditorRequired]
	public required RenderFragment<ListRenderContext<TValue>> ChildContent { get; set; }
    private int _itemCount = 0;
    [Parameter, EditorRequired]
    public IList<TValue> Items { get; set; } = [];
    /// <summary>
    /// If true, the values in this list are sortable. Mutually exclusive with
    /// <see cref="Paginate"/>.
    /// </summary>
	[Parameter]
	public bool Sortable { get; set; } = false;
    [Parameter]
    public string Identifier { get; set; } = string.Empty;
    [Parameter]
    public ISortable? SortableParent { get; set; } = null;
    /// <summary>
    /// The number of values to show per page. If <= 0, pagination is disabled. Mutually exclusive
    /// with <see cref="Sortable"/>.
    /// </summary>
	[Parameter]
	public int Paginate { get; set; } = 0;
    [Parameter]
    public int MaxItemsPerPage { get; set; } = 10;
    /// <summary>
    /// The event to call when the pagination is updated.
    /// </summary>
    [Parameter]
    public Action<int, int>? OnPageChanged { get; set; }

    [Parameter]
    public string Class { get; set; } = "";

    /// <summary>
    /// If true, show selection checkboxes on every row of the table.
    /// </summary>
    [Parameter]
    public bool HasSelectionBoxes { get; set; } = false;

    /// <summary>
    /// The selection data for selected rows. Must have
    /// a value if <see cref="HasSelectionBoxes"/> is true.
    /// </summary>
    [Parameter]
    public ListSelectionData? SelectionData { get; set; }

    private int PaginationIndex { get; set; } = 0;
    private int PaginationMaxIndex { get; set; } = 0;

    protected override async Task OnParametersSetAsync()
    {
        await base.OnParametersSetAsync();

        PaginationMaxIndex = PaginationIndex + MaxItemsPerPage;

        if (_itemCount != Items.Count)
        {
            _itemCount = Items.Count;

            if (SelectionData is not null)
            {
                if (Paginate > 0)
                {
                    SelectionData.Range = PaginationMaxIndex > Items.Count
                        ? Items.Count - PaginationIndex
                        : MaxItemsPerPage;
                }
                else
                {
                    SelectionData.Range = Items.Count;
                }

                SelectionData.Clear();
            }
        }
    }

    private void OnSortUp(int index)
	{
		SortableParent?.RecalculateOrder(index, index - 1);
	}

	private void OnSortDown(int index)
    {
        SortableParent?.RecalculateOrder(index, index + 1);
    }

    private void OnPaginationChanged(int index)
    {
        PaginationIndex = index;
        PaginationMaxIndex = PaginationIndex + MaxItemsPerPage;

        if (SelectionData is not null)
        {
            SelectionData.Clear();
            SelectionData.Range = PaginationMaxIndex > Items.Count 
                ? Items.Count - PaginationIndex
                : MaxItemsPerPage;
        }

        OnPageChanged?.Invoke(MaxItemsPerPage, PaginationIndex);

        StateHasChanged();
    }

    // This has some problems with re-rendering when object orders are changed.
    // May need to configure a secondary internal list that tracks the order (or
    // use a secondary list for the display so it wont change).
    #region Old Sortable JS code
    //private DotNetObjectReference<ListRenderer<TValue>>? DotNetRef { get; set; }

    //protected override async Task OnParametersSetAsync()
    //{
    //    await base.OnParametersSetAsync();

    //    if (string.IsNullOrWhiteSpace(Identifier))
    //        Identifier = Guid.NewGuid().ToString().Replace("-", "");

    //    _items = Sortable ? new List<TValue>(Items) : Items;

    //    if (Sortable)
    //    {
    //        Sortable = SortableParent is not null;
    //    }
    //}

    //protected override async Task OnAfterRenderAsync(bool firstRender)
    //{
    //    await base.OnAfterRenderAsync(firstRender);

    //    if (firstRender && Sortable)
    //    {
    //        await RegisterSortable();
    //    }
    //}

    //protected DotNetObjectReference<ListRenderer<TValue>> GetDotNetReference
    //{
    //    get => DotNetRef ??= DotNetObjectReference.Create(this);
    //}

    //private async Task RegisterSortable()
    //{
    //    await JSRuntime.InvokeVoidAsync("SortableInterop.registerSort", Identifier, GetDotNetReference, nameof(OrderChangedAsync));
    //}

    //[JSInvokable]
    //public Task OrderChangedAsync(int oldIndex, int newIndex)
    //{
    //    SortableParent?.RecalculateOrder(oldIndex, newIndex);

    //    return Task.CompletedTask;
    //}

    //private async Task UnregisterSortableAsync()
    //{
    //    await JSRuntime.InvokeVoidAsync("SortableInterop.destroySort", Identifier);
    //}

    //protected virtual void Dispose(bool disposing)
    //{
    //    if (!disposedValue)
    //    {
    //        if (disposing)
    //        {
    //            if (!disposedAsync && Sortable)
    //            {
    //                UnregisterSortableAsync().GetAwaiter().GetResult();
    //            }
    //        }

    //        DotNetRef?.Dispose();
    //        disposedValue = true;
    //    }
    //}

    //public void Dispose()
    //{
    //    // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
    //    Dispose(disposing: true);
    //    GC.SuppressFinalize(this);
    //}

    //public async ValueTask DisposeAsync()
    //{
    //    disposedAsync = true;

    //    if (Sortable)
    //    {
    //        await UnregisterSortableAsync();
    //    }

    //    // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
    //    Dispose(disposing: true);
    //    GC.SuppressFinalize(this);
    //}
    #endregion
}