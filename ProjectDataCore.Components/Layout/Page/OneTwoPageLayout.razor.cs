namespace ProjectDataCore.Components.Layout.Page;

public partial class OneTwoPageLayout
{
	[Parameter, EditorRequired]
	public required RenderFragment Left { get; set; }
	[Parameter, EditorRequired]
	public required RenderFragment Right { get; set; }
}