﻿using ProjectDataCore.Data.Services.Alert;
using ProjectDataCore.Data.Services.Auth;
using ProjectDataCore.Data.Services.Roster;
using ProjectDataCore.Data.Services.User;
using ProjectDataCore.Data.Structures.Assignable.Configuration;
using ProjectDataCore.Data.Structures.Assignable.Value;
using ProjectDataCore.Data.Structures.Auth;
using ProjectDataCore.Data.Structures.Page.Components.Layout;
using ProjectDataCore.Data.Structures.Page.Components.Parameters;
using ProjectDataCore.Data.Structures.Page.Components.Parameters.Form;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Components.Page.Roster;

[Component(typeof(RosterComponentSettings), PageComponentListing.Roster_RosterDisplay,
    Name = "Roster Display",
    ShortName = "Roster",
    IconPath = "svg/mat-icons/account_tree.svg",
    Category = "Roster"
)]
public partial class RosterDisplayComponent : FormComponentBase
{
    [Inject]
    public required IModularRosterService RosterService { get; set; }
    [Inject]
    public required IAlertService AlertService { get; set; }
    [Inject]
    public required IUserService UserService { get; set; }
    [Inject]
    public required IAssignableDataService AssignableDataService { get; set; }
    [Inject]
    public required IDataCoreAuthorizationService AuthorizationService { get; set; }

    [CascadingParameter(Name = "ActiveUser")]
    public DataCoreUser? ActiveUser { get; set; }

    public RosterComponentSettings? RosterSettings { get => ComponentData as RosterComponentSettings; }
    public RosterTree? RosterToDisplay { get; set; }

    private List<RosterTagPermission> TagPermissions { get; set; } = [];

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (firstRender)
        {
            if (EditComponent is null)
                await LoadRosterDisplayForPublishAsync();
            else
            {
                if (RosterSettings is null)
                    return;

                RosterSettings.RosterTreeDisplayComponentData ??= new();
                RosterSettings.RosterTreeDisplayComponentData.DisplayComponentSettings ??= new()
                {
                    Data = RosterSettings.RosterTreeDisplayComponentData
                };

                var userRes = await AssignableDataService.GetMockAssignable<DataCoreUser, Guid>();
                if (!userRes.GetResult(out var mock, out var err))
                {
                    AlertService.CreateErrorAlert(err);
                    return;
                }

                MockAssignable = mock;

                StateHasChanged();
            }
        }
    }

    #region Display
    public bool UserList { get; set; } = false;
    public List<DataCoreUser>? UsersInRoster { get; set; }
    public List<BaseAssignableConfiguration>? ValueConfigs { get; set; }

    protected async Task LoadRosterDisplayForPublishAsync()
    {
        if (RosterSettings is null)
            return;

        var res = await RosterService.GetRosterTreeForSettingsAsync(RosterSettings.RosterToDisplay);

        if (!res.GetResult(out var roster, out var err))
        {
            AlertService.CreateErrorAlert(err);
            return;
        }

        await foreach (var check in RosterService.LoadFullRosterTreeAsync(roster))
            continue;

        if (!RosterSettings.Scoped)
        {
            RosterToDisplay = roster;
        }
        else if (ActiveUser is not null)
        {
            var tree = roster.FindUserInTree(ActiveUser, RosterSettings.LevelFromTop, 
                RosterSettings.LevelFromTop + RosterSettings.Depth);

            var startTreeList = tree.FirstOrDefault().trees;
            var startTree = startTreeList?.ElementAtOrDefault(RosterSettings.LevelFromTop);

            if (startTree is null)
            {
                AlertService.CreateWarnAlert("You are not present in this roster.");
                return;
            }

            startTree.DropAfter(RosterSettings.Depth);

            RosterToDisplay = startTree;
        }

        if (RosterSettings.AllowUserListing)
        {
            UsersInRoster = (RosterToDisplay?.GetAllUsers() ?? [])
                .Distinct()
                .ToList();

            var userRes = await AssignableDataService.GetMockAssignable<DataCoreUser, Guid>();
            if (!userRes.GetResult(out var mock, out err))
            {
                AlertService.CreateErrorAlert(err);
                return;
            }

            var activeKeys = RosterSettings.UserListDisplayedProperties
                .OrderBy(e => e.Order)
                .Select(e => e.ConfigurationKey)
                .ToArray();

            ValueConfigs = mock.AssignableValues
                .Select(e => e.AssignableConfiguration)
                .Where(e => activeKeys.Contains(e.Key))
                .ToList();
        }

        TagPermissions = await AuthorizationService.GetAllRosterTagPermissionsAsync();

        StateHasChanged();
    }

    protected void ToggleUserList()
    {
        UserList = !UserList;

        // TODO: other config.

        StateHasChanged();
    }
    #endregion

    #region Editing
    public List<RosterDisplaySettings>? RosterDisplays { get; set; }
    public DataCoreUser? MockAssignable { get; set; }

    protected List<RosterDisplaySettings> SelectedRoster { get; set; } = [];
    protected List<BaseAssignableValue> SelectedConfigurations { get; set; } = [];

    protected async Task LoadRosterDisplaysAsync()
    {
        var res = await RosterService.GetAvailableRosterDisplaysAsync();
        if (!res.GetResult(out var displays, out var err))
        {
            AlertService.CreateErrorAlert(err);
            return;
        }

        RosterDisplays = displays;

        if (RosterSettings?.RosterToDisplay != default)
        {
            var roster = RosterDisplays.Find(e => e.Key == RosterSettings?.RosterToDisplay);
            if (roster is not null)
                SelectedRoster.Add(roster);
        }

        if (EditComponent is not null)
            await EditComponent.RequestStateHasChangedAsync();
    }

    protected void SelectedRosterChanged()
    {
        if (RosterSettings is null)
            return;

        var selection = SelectedRoster.FirstOrDefault();

        RosterSettings.RosterToDisplay = selection?.Key ?? Guid.Empty;
    }

    protected async Task AddSelectedConfigurationsToDisplayListAsync()
    {
        if (RosterSettings is null)
            return;

        var orderings = SelectedConfigurations.Select(e => new RosterDisplayColumnOrder()
        {
            AssignableConfiguration = e.AssignableConfiguration,
            ConfigurationKey = e.Key,

            SettingsKey = RosterSettings.Key
        });

        RosterSettings.UserListDisplayedProperties.AddRange(orderings);

        RosterSettings.RecalculateOrder();

        SelectedConfigurations.Clear();

        if (EditComponent is not null)
            await EditComponent.RequestStateHasChangedAsync();
    }

    protected async Task RemoveConfigurationFromDisplayAsync(int index)
    {
        if (RosterSettings is null)
            return;

        RosterSettings.UserListDisplayedProperties.RemoveAt(index);

        RosterSettings.RecalculateOrder();

        if (EditComponent is not null)
            await EditComponent.RequestStateHasChangedAsync();
    }

    protected async Task SetEditAuthAreaAsync(AuthorizationArea area)
    {
        if (RosterSettings is null)
            return;

        RosterSettings.EditingAuthArea = area;

        if (EditComponent is not null)
            await EditComponent.RequestStateHasChangedAsync();
    }

    protected async Task ClearEditAuthAreaAsync()
    {
        if (RosterSettings is null)
            return;

        RosterSettings.EditingAuthArea = null;
        RosterSettings.EditingAuthAreaKey = null;

        if (EditComponent is not null)
            await EditComponent.RequestStateHasChangedAsync();
    }
    #endregion
}
