using ProjectDataCore.Data.Services.Alert;
using ProjectDataCore.Data.Services.Auth;
using ProjectDataCore.Data.Structures.Auth.Tags;

namespace ProjectDataCore.Components.Page.Admin.Auth;

[StaticPage(PageComponentListing.Admin_AuthorizationTagEditor,
    Name = "Auth Tag Editor",
    Route = "/admin/auth/tags")]
public partial class AuthorizationTagEditor : StaticComponentBase
{
    [Inject]
    public required IDataCoreAuthorizationService AuthorizationService { get; set; }
    [Inject]
    public required IAlertService AlertService { get; set; }

    protected SortedList<string, AuthorizationTag> Tags { get; set; } = new();
    protected string NewTagName { get; set; } = string.Empty;

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (firstRender)
        {
            await ReloadTagsAsync();
            StateHasChanged();
        }
    }

    protected async Task ReloadTagsAsync(AuthorizationTag? tag = null)
    {
        if (tag is null
            || tag.Key == default)
            Tags = new((await AuthorizationService.GetAllTagsAsync())
                .ToDictionary(e => e.Name));
        else await AuthorizationService.ReloadTagAsync(tag);
    }

    protected async Task DeleteTagAsync(AuthorizationTag tag)
    {
        var res = await AuthorizationService.DeleteTagAsync(tag);
        if (!res.GetResult(out var err))
        {
            AlertService.CreateErrorAlert(err);
            return;
        }

        _ = Tags.Remove(tag.Name);
        StateHasChanged();
    }

    protected async Task<bool> UpdateTagAsync(AuthorizationTag tag, string name)
    {
        tag.Name = name;
        var res = await AuthorizationService.UpdateOrAddTagAsync(tag);
        if (!res.GetResult(out var err))
        {
            AlertService.CreateErrorAlert(err);
            return false;
        }

        StateHasChanged();
        return true;
    }

    protected async Task CreateTagAsync()
    {
        var newTag = new AuthorizationTag();

        var res = await UpdateTagAsync(newTag, NewTagName);

        if (!res)
            return;

        NewTagName = string.Empty;
        Tags.TryAdd(newTag.Name, newTag);
        StateHasChanged();
    }
}