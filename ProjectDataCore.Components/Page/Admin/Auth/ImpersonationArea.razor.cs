﻿using ProjectDataCore.Data.Services.Routing;
using ProjectDataCore.Data.Services.User;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Components.Page.Admin.Auth;

[StaticPage(PageComponentListing.Admin_ImpersonationArea,
	Name = "Impersonation Area",
	Route = "/admin/auth/impersonate")]
public partial class ImpersonationArea : StaticComponentBase
{
#pragma warning disable CS8618 // Injections are never null.
    [Inject]
    public NavigationManager NavigationManager { get; set; }
	[Inject]
	public IUserService UserService { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

    [CascadingParameter(Name = "ActiveUser")]
	public DataCoreUser? ActiveUser { get; set; }

	private List<DataCoreUser> SelectedUsers { get; set; } = new();

	private void ImpersonateUserAsync()
	{
		if (ActiveUser is null)
			return; // how are you here?

		var user = SelectedUsers.FirstOrDefault();

		if (user is null)
			return;

		UserService.ImpersonateUser(ActiveUser, user);

		NavigationManager.NavigateTo("/");
	}
}
