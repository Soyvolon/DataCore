﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Connections.Features;

using ProjectDataCore.Data.Services.Alert;
using ProjectDataCore.Data.Services.Auth;
using ProjectDataCore.Data.Structures.Auth;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Components.Page.Admin.Auth;

[StaticPage(PageComponentListing.Admin_AuthorizationAreaEditor,
    Name = "Auth Area Editor",
    Route = "/admin/auth/edit")]
public partial class AuthorizationAreaEditor : StaticComponentBase
{
    #region Messages
    private const string FailedToAddToSortedListMsg = "Failed to add {0} to the sorted areas list.";
    #endregion

    private const string DefaultCategoryName = "Uncategorized";

#pragma warning disable CS8618 // Injections are never null.
    [Inject]
    public IDataCoreAuthorizationService AuthorizationService { get; set; }
    [Inject]
    public IAlertService AlertService { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

    [CascadingParameter(Name = "ActiveUser")]
    public DataCoreUser? ActiveUser { get; set; }

    private SortedList<string, SortedList<string, AuthorizationArea>> SortedAreas { get; set; } = new();
    private List<string> Categories { get; set; } = new();
    private List<string> SelectedCategories { get; set; } = new();
    private List<string> SelectedFilteredCategories { get; set; } = new();

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (firstRender)
        {
            await LoadAuthAreasAsync();
            StateHasChanged();
        }
    }

    private async Task LoadAuthAreasAsync()
    {
        var allAreas = await AuthorizationService.GetAllAuthorizationAreasAsync();

        SortedAreas.Clear();
        foreach (var a in allAreas)
            InsertIntoSortedAreas(a);

        RebuildCategories();
    }

    private void InsertIntoSortedAreas(AuthorizationArea area)
    {
        var cat = GetAreaCategory(area);

        if (!SortedAreas.TryGetValue(cat, out var areaList))
        {
            areaList = new();
            SortedAreas.Add(cat, areaList);
        }

        areaList.Remove(area.Name);
        if (!areaList.TryAdd(GetAreaKey(area), area))
            _ = AlertService.CreateWarnAlert(
                string.Format(FailedToAddToSortedListMsg, area.Name),
                true, 2500);
    }

    private bool RemoveFromSortedAreas(AuthorizationArea area)
    {
        var cat = GetAreaCategory(area);

        if (SortedAreas.TryGetValue(cat, out var areaList))
        {
            return areaList.Remove(GetAreaKey(area));
        }

        return false;
    }

    private string GetAreaCategory(AuthorizationArea area)
    {
        var cat = area.Category;
        if (string.IsNullOrWhiteSpace(cat))
            cat = DefaultCategoryName;

        return cat;
    }

    private string GetAreaKey(AuthorizationArea area)
        => area.Name + area.Key.ToString();

    private void RebuildCategories()
    {
        Categories.Clear();

        List<string> toRemove = new();
        foreach(var cat in SortedAreas.Keys)
        {
            if (SortedAreas[cat].Count == 0)
                toRemove.Add(cat);
            else Categories.Add(cat);
        }

        foreach (var category in toRemove)
        {
            SortedAreas.Remove(category);
        }

        if (!Categories.Contains(DefaultCategoryName))
            Categories.Add(DefaultCategoryName);
    }

    private async Task OnCatSelectionChange()
    {
        await InvokeAsync(StateHasChanged);
    }

    private async Task AddAreaAsync()
    {
        var cat = SelectedCategories.FirstOrDefault();
        AuthorizationArea area = new()
        {
            Category = string.IsNullOrWhiteSpace(cat)
                ? DefaultCategoryName : cat,
        };

        var res = await AuthorizationService.UpdateOrAddAuthorizationAreaAsync(area);
        if (!res.GetResult(out var err))
        {
            AlertService.CreateErrorAlert(err);
            return;
        }

        InsertIntoSortedAreas(area);
        RebuildCategories();
        StateHasChanged();
    }

    private async Task DeleteAreaAsync(AuthorizationArea area)
    {
        var res = await AuthorizationService.DeleteAuthorizationAreaAsync(area);
        if (!res.GetResult(out var err))
        {
            AlertService.CreateErrorAlert(err);
            return;
        }

        RemoveFromSortedAreas(area);
        RebuildCategories();
        StateHasChanged();
    }

    private async Task UpdateFilterAsync()
    {
        await InvokeAsync(StateHasChanged);
    }

    private void ClearFilter()
    {
        SelectedFilteredCategories.Clear();
        StateHasChanged();
    }
}
