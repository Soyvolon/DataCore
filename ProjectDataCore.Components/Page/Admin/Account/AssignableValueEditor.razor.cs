using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using System.Net.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Components.Routing;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.Web.Virtualization;
using Microsoft.JSInterop;
using ProjectDataCore;
using ProjectDataCore.Components.Framework.Roster;
using ProjectDataCore.Data.Structures.Assignable.Value;
using ProjectDataCore.Data.Structures.Assignable.Configuration;
using ProjectDataCore.Data.Structures.Result;
using ProjectDataCore.Data.Structures.Assignable;
using ProjectDataCore.Data.Structures.Model.Assignable;
using ProjectDataCore.Data.Services.Roster;
using System.Reflection;

namespace ProjectDataCore.Components.Page.Admin.Account
{
    [StaticPage(PageComponentListing.Admin_AdminAssignableValueEditor,
        Name = "Assignable Value Editor", 
        Route="/admin/account/avedit")]
    public partial class AssignableValueEditor : StaticComponentBase
    {
#pragma warning disable CS8618 // Injections are not null.
        [Inject]
        public IAssignableDataService AssignableDataService { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

        public List<BaseAssignableConfiguration> CurrentAssignables { get; set; } = new();

        public BaseAssignableConfiguration? ToEdit { get; set; } = null;

        public string NewAssignable { get; set; } = "";
        public int NewConfigurationType { get; set; } = -1;

        List<(Type type, string name)> ConfigurationTypes { get; set; } = new();

        public bool ConfirmDelete = true;

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            await base.OnAfterRenderAsync(firstRender);

            if (firstRender)
            {
                await RefreshAssignablesAsync();
                await RefreshAssignableTypesAsync();
            }
        }

        private async Task RefreshAssignableTypesAsync()
        {
            ConfigurationTypes = BaseAssignableConfiguration.GetConfigurationTypes(true);

            await InvokeAsync(StateHasChanged);
        }

        private async Task RefreshAssignablesAsync()
        {
            var res = await AssignableDataService.GetAllAssignableConfigurationsAsyncForUsers();

            if(res.GetResult(out var data, out _))
            {
                CurrentAssignables = data;
            }

            ConfirmDelete = true;

            await InvokeAsync(StateHasChanged);
        }

        private async Task OnCreateAssignableAsync()
		{
            if (NewConfigurationType > -1)
            {
                var config = Activator.CreateInstance(ConfigurationTypes[NewConfigurationType].type) as BaseAssignableConfiguration;

                if (config is not null)
                {
                    config.PropertyName = NewAssignable;

                    var res = await AssignableDataService.AddNewAssignableConfiguration(config);

                    if(!res.GetResult(out var err))
                    {
                        // TODO: handle errors.
                    }

                    await RefreshAssignablesAsync();
                }
            }
		}

        private void StartEdit(BaseAssignableConfiguration config)
        {
            ToEdit = config;

            StateHasChanged();
		}

		private async Task DiscardEditAsync()
		{
			ToEdit = null;
			await RefreshAssignablesAsync();
		}
	}
}