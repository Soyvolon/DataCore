﻿
using ProjectDataCore.Data.Services.Alert;
using ProjectDataCore.Data.Services.Auth;
using ProjectDataCore.Data.Services.Nav;
using ProjectDataCore.Data.Structures.Auth;
using ProjectDataCore.Data.Structures.Nav;

namespace ProjectDataCore.Components.Page.Admin.NavBar
{
    [StaticPage(PageComponentListing.Admin_NavbarEditor,
        Name = "Navbar Editor", 
        Route = "/admin/navbar/edit")]
    public partial class ModifyNavBar : StaticComponentBase
    {
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        [Inject]
        public INavModuleService NavModuleService { get; set; }
        [Inject]
        public IPageEditService PageEditService { get; set; }
        [Inject]
        public IAlertService AlertService { get; set; }
        public IDataCoreAuthorizationService AuthorizationService { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

        List<NavModule> navModules = new();
        List<NavModule> allNavModules = new();
        List<CustomPageSettings> allPages = new();
        private List<AuthorizationArea> allAuthAreas = new();

        bool LinkToPage = false;

        private NavModule? Editing { get; set; }

        protected override async Task OnInitializedAsync()
        {
            await base.OnInitializedAsync();


            await GetData();
        }

        private async Task GetData()
        {

            navModules = await NavModuleService.GetAllModulesWithChildren();
            allNavModules = await NavModuleService.GetAllModules();
            allPages = await PageEditService.GetAllPagesAsync();
            allAuthAreas = await AuthorizationService.GetAllAuthorizationAreasAsync();
        }

        private void EditNavModule(NavModule navModule)
        {
            Editing = navModule;
            LinkToPage = true;
            InvokeAsync(StateHasChanged);
        }

        private async Task UpdateModule()
        {
            var _result = await NavModuleService.UpdateNavModuleAsync(Editing);
            if (!_result.GetResult(out var _errors))
                AlertService.CreateErrorAlert(_errors);
            await GetData();
            Editing = null;
            await InvokeAsync(StateHasChanged);
        }

        private async Task HandleForm()
        {
            if (Editing.Key != Guid.Empty)
            {
                await UpdateModule();
            }
            else
            {
                var _result = await NavModuleService.CreateNavModuleAsync(Editing);

                if (!_result.GetResult(out var _errors))
                {
                    AlertService.CreateErrorAlert(_errors);
                    return;
                }
                await GetData();
                await InvokeAsync(StateHasChanged);
            }
        }

        private void NewNavModule()
        {
            EditNavModule(new NavModule(){DisplayName = "New"});
        }

        private async Task DeleteModule()
        {
            ActionResult _result = (await NavModuleService.DeleteNavModule(Editing.Key));

            if (!_result.GetResult(out var errors))
            {
                AlertService.CreateErrorAlert(errors);
                return;
            }
            
            Editing = null;
            navModules = await NavModuleService.GetAllModulesWithChildren();
            await InvokeAsync(StateHasChanged);
        }

        private void PageSelect(Guid id)
        {
            // TODO
            // Editing.AuthKey = allPages.Where(x => x.Key == id).FirstOrDefault()?.Layout?.AuthorizationPolicyKey;
            Editing.PageId = id;
        }

        private async Task SortModules()
        {
            foreach (var _module in navModules)
            {
                await NavModuleService.RecalculateSortOrder(_module);
            }

            await InvokeAsync(StateHasChanged);
        }
    }
}