﻿using ProjectDataCore.Data.Services.Alert;
using ProjectDataCore.Data.Structures.Page.Attributes;
using ProjectDataCore.Data.Structures.Page.Components.Layout;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ProjectDataCore.Components.Page.Admin.Page;

[StaticPage(PageComponentListing.Admin_PageEditor,
    Name = "Page Editor", 
    Route = "/admin/page/edit")]
public partial class PageEditor : StaticComponentBase
{
    
}
