﻿using ProjectDataCore.Data.Structures.Roster;

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Components.Page.Admin.Page;

[StaticPage(PageComponentListing.Admin_RosterEditor,
    Name = "Roster Editor",
    Route = "/admin/roster/edit")]
public partial class RosterEditor : StaticComponentBase
{

}
