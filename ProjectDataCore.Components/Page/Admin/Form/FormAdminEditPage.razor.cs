using Npgsql;

using ProjectDataCore.Data.Services.Alert;
using ProjectDataCore.Data.Services.Roster;
using ProjectDataCore.Data.Structures.Assignable.Configuration;
using ProjectDataCore.Data.Structures.Form.Bucket;
using ProjectDataCore.Data.Structures.Form.Bucket.Action;
using ProjectDataCore.Data.Structures.Form.Bucket.Filter;

using System.Security.Cryptography.X509Certificates;

namespace ProjectDataCore.Components.Page.Admin.Form;

[StaticPage(PageComponentListing.Admin_FormEditor,
    Name = "Form Editor",
	Route = "/admin/form/edit")]
public partial class FormAdminEditPage : StaticComponentBase
{
    public enum LeftMenuState
    {
        Main,
        EditForm,
        EditBucket,
        EditAction
    }

    [Inject]
    public required IFormService FormService { get; set; }
    [Inject]
    public required IAlertService AlertService { get; set; }
    [Inject]
    public required IAssignableDataService AssignableDataService { get; set; }

	protected SortedList<string, DynamicForm> Forms { get; set; } = new();
    protected LeftMenuState LeftMenu { get; set; } = LeftMenuState.Main;

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (firstRender)
        {
            await ReloadFormsAsync();
        }
    }

    protected async Task ReloadFormsAsync()
    {
        Forms = new((await FormService.GetAllFormsAsync())
            .ToDictionary(e => e.Name));

        StateHasChanged();
    }

    #region Home
    protected string NewFormName { get; set; } = string.Empty;

    protected async Task CreateFormAsync()
    {
        if (string.IsNullOrWhiteSpace(NewFormName))
        {
            AlertService.CreateErrorAlert("You must give the new form a name!", true);
            return;
        }

        DynamicForm form = new()
        {
            Name = NewFormName
        };

		var res = await FormService.UpdateOrAddFormAsync(form);

        if (!res.GetResult(out var err))
        {
            AlertService.CreateErrorAlert(err);
        }
        else
        {
            AlertService.CreateSuccessAlert($"Form: {NewFormName} created.", true, 1600);
            _ = Forms.TryAdd(form.Name, form);

            NewFormName = string.Empty;

            await ReloadFormsAsync();
        }
    }

    protected async Task DeleteFormAsync(DynamicForm form)
    {
        var res = await FormService.DeleteFormAsync(form);

        if (!res.GetResult(out var err))
            AlertService.CreateErrorAlert(err);
        else
            _ = Forms.Remove(form.Name);
    }
    #endregion

    #region Edit Form
    private enum EditFormTab
    { 
        Settings,
        Tags,
        Assignable
	}
	private EditFormTab CurrentEditFormTab { get; set; } = EditFormTab.Settings;

	protected DynamicForm? FormToEdit { get; set; } = null;
    protected List<string> FormBucketNames { get; set; } = new();
    protected string NewBucketName { get; set; } = string.Empty;
    List<(Type type, string name)> AssignableConfigurationTypes { get; set; } = new();
    protected string NewFormTag { get; set; } = string.Empty;

    private void ChangeEditFormTab(EditFormTab tab)
    {
        CurrentEditFormTab = tab;
        StateHasChanged();
    }

	protected async Task StartEditFormAsync(DynamicForm form)
	{
        FormToEdit = form;
        LeftMenu = LeftMenuState.EditForm;
        await LoadFormBucketsAsync();

		AssignableConfigurationTypes = BaseAssignableConfiguration.GetConfigurationTypes(false);
		await InvokeAsync(StateHasChanged);
	}

    protected async Task LoadFormBucketsAsync()
    {
        if (FormToEdit is null)
            return;

		await FormService.RefreshFormAsync(FormToEdit);
		var res = await FormService.LoadFormBucketsAsync(FormToEdit);

        if (!res.GetResult(out var err))
            AlertService.CreateErrorAlert(err);
    }

    protected async Task SaveFormAsync()
    {
        if (FormToEdit is null)
            return;

        var res = await FormService.UpdateOrAddFormAsync(FormToEdit);

        if (!res.GetResult(out var err))
            AlertService.CreateErrorAlert(err);
		else AlertService.CreateSuccessAlert("Saved.", true, 1600);
	}

    protected async Task CreateBucketAsync()
    {
        if (FormToEdit is null)
            return;

        if (string.IsNullOrWhiteSpace(NewBucketName))
        {
            AlertService.CreateErrorAlert("You must provide a name for the bucket!", true);
            return;
        }

        DynamicFormBucket bucket = new()
        {
            FormKey = FormToEdit.Key,
            Name = NewBucketName
        };

        var res = await FormService.UpdateOrAddFormBucketAsync(bucket);

        if (!res.GetResult(out var err))
        {
            AlertService.CreateErrorAlert(err);
            return;
        }
        else
        {
            await LoadFormBucketsAsync();

			AlertService.CreateSuccessAlert($"Bucket: {NewBucketName} created.", true, 1600);

			NewBucketName = string.Empty;

			await InvokeAsync(StateHasChanged);
        }
	}

    protected async Task DeleteBucketAsync(DynamicFormBucket bucket)
    {
        var res = await FormService.DeleteFormBucketAsync(bucket);

        if (!res.GetResult(out var err))
        {
            AlertService.CreateErrorAlert(err);
        }
        else
        {
            await LoadFormBucketsAsync();
            await InvokeAsync(StateHasChanged);
        }
	}

	protected void AddFormTag()
	{
		if (FormToEdit is null)
			return;

        if (string.IsNullOrWhiteSpace(NewFormTag))
        {
            AlertService.CreateErrorAlert("The form tag must have a name.", true);
            return;
        }

        if (FormToEdit.HasTag(NewFormTag))
		{
			AlertService.CreateWarnAlert($"The form tag: {NewFormTag} already exists.", true);
			return;
        }

		FormToEdit.AddTag(NewFormTag);
        NewFormName = string.Empty;

		StateHasChanged();
	}

	protected void RemoveFormTag(DynamicFormTag tag)
    {
        if (FormToEdit is null)
            return;

        FormToEdit.RemoveTag(tag);

        StateHasChanged();
    }

	protected async Task StopFormEditAsync()
    {
        FormToEdit = null;
        LeftMenu = LeftMenuState.Main;
        await InvokeAsync(StateHasChanged);
    }
    #endregion

    #region Form Assignables
    protected BaseAssignableConfiguration? BaseAssignableConfigurationToEdit { get; set; }
    protected string NewBaseConfigName { get; set; } = string.Empty;
	public int NewConfigurationType { get; set; } = -1;

	protected async Task CreateBaseAssignableConfigurationAsync()
    {
        if (FormToEdit is null)
            return;

        if (NewConfigurationType < 0)
            NewConfigurationType = 0;

        var cfg = Activator.CreateInstance(AssignableConfigurationTypes[NewConfigurationType].type) as BaseAssignableConfiguration;

        if (cfg is null)
        {
            AlertService.CreateErrorAlert($"Failed to create an instance of" +
                $" {AssignableConfigurationTypes[NewConfigurationType].name}.");
            return;
        }

        cfg.AssignableType = BaseAssignableConfiguration.InternalAssignableType.FormProperty;
        cfg.PropertyName = NewBaseConfigName;
        cfg.Form = FormToEdit;
        cfg.FormKey = FormToEdit.Key;

        var res = await AssignableDataService.AddNewAssignableConfiguration(cfg);

        if (!res.GetResult(out var err))
        {
            AlertService.CreateErrorAlert(err);
        }
        else
        {
            await FormService.RefreshFormAsync(FormToEdit);

			AlertService.CreateSuccessAlert($"Assignable Configuration: {NewBaseConfigName}" +
                $" of type {AssignableConfigurationTypes[NewConfigurationType].name}" +
                $" created.", true, 1600);

            NewBaseConfigName = string.Empty;

			await InvokeAsync(StateHasChanged);
        }
	}

    protected async Task StartEditBaseAssignableAsync(BaseAssignableConfiguration config)
    {
        BaseAssignableConfigurationToEdit = config;
        await InvokeAsync(StateHasChanged);
    }

	protected async Task StopBaseAssignableConfigurationEdit()
    {
        BaseAssignableConfigurationToEdit = null;
        await InvokeAsync(StateHasChanged);
    }

    protected async Task DeleteBaseAssignableAsync(BaseAssignableConfiguration config)
	{
		if (FormToEdit is null)
			return;

		var res = await AssignableDataService.DeleteAssignableConfiguration(config.Key);

        if (!res.GetResult(out var err))
        {
            AlertService.CreateErrorAlert(err);
        }
        else
        {
            await FormService.RefreshFormAsync(FormToEdit);
            await InvokeAsync(StateHasChanged);
        }
    }
    #endregion

    #region Buckets
    protected DynamicFormBucket? BucketToEdit { get; set; } = null;

    protected List<DynamicFormBucket> SelectedDefaultBucket { get; set; } = new(1);

	protected async Task StartEditBucketAsync(DynamicFormBucket bucket)
	{
        SelectedDefaultBucket.Clear();
		BucketToEdit = bucket;

        if (FormToEdit is not null)
            FormTagNames = FormToEdit.Tags.Select(e => e.Name).ToList();

        LoadBucketActionOptions();

        LeftMenu = LeftMenuState.EditBucket;
		await InvokeAsync(StateHasChanged);
	}

	protected async Task SaveBucketAsync()
	{
		if (BucketToEdit is null
            || FormToEdit is null)
			return;

		var res = await FormService.UpdateOrAddFormBucketAsync(BucketToEdit);

        if (!res.GetResult(out var err))
        {
            AlertService.CreateErrorAlert(err);
        }
        else
        {
            AlertService.CreateSuccessAlert("Saved.", true, 1600);
            await InvokeAsync(StateHasChanged);
        }
	}

    protected async Task SetDefaultBucketAsync()
    {
		if (BucketToEdit is null
			|| FormToEdit is null)
			return;

		var res = await FormService.SetAsDefaultBucketAsync(BucketToEdit);

		if (!res.GetResult(out var err))
		{
			AlertService.CreateErrorAlert(err);
		}
		else
		{
			await InvokeAsync(StateHasChanged);
		}
	}

	protected async Task StopBucketEditAsync()
	{
		BucketToEdit = null;
		LeftMenu = LeftMenuState.EditForm;
		await InvokeAsync(StateHasChanged);
	}
	#endregion

	#region Bucket Actions
	private enum BucketActionTabs
	{
		Settings,
		Filters,
        MoveResponse,
        DeleteResponse,
        ApplyValues,
        AssignUserTo,
        ModifyTags
    }
	private BucketActionTabs CurrentActionTab { get; set; } = BucketActionTabs.Settings;
	protected BucketAction? ActionToEdit { get; set; } = null;
	protected string NewActionName { get; set; } = string.Empty;
    protected List<DynamicFormBucket> ActionMoveBuckets { get; set; } = new(1);

    private void ChangeEditActionTab(BucketActionTabs action)
    {
        CurrentActionTab = action;
        StateHasChanged();
    }


    protected async Task CreateActionAsync()
	{
        if (BucketToEdit is null)
            return;

        if (string.IsNullOrWhiteSpace(NewActionName))
        {
            AlertService.CreateErrorAlert("The new action requires a name.", true);
            return;
        }

        BucketAction action = new()
        {
            BucketKey = BucketToEdit.Key,
            Bucket = BucketToEdit,
            Name = NewActionName
        }; 
        
        var res = await FormService.UpdateOrAddBucketActionAsync(action);

		if (!res.GetResult(out var err))
		{
			AlertService.CreateErrorAlert(err);
		}
		else
		{
			await FormService.RefreshBucketAsync(BucketToEdit);

			AlertService.CreateSuccessAlert($"Bucket Action: {NewActionName} created.", true, 1600);

			NewActionName = string.Empty;

			await InvokeAsync(StateHasChanged);
		}
	}

    protected async Task DeleteActionAsync(BucketAction action)
    {
		if (BucketToEdit is null)
			return;

		var res = await FormService.DeleteBucketActionAsync(action);

		if (!res.GetResult(out var err))
		{
			AlertService.CreateErrorAlert(err);
		}
		else
		{
			await FormService.RefreshBucketAsync(BucketToEdit);

			await InvokeAsync(StateHasChanged);
		}
	}

    protected async Task StartEditActionAsync(BucketAction action)
	{
		ActionToEdit = action;
		LeftMenu = LeftMenuState.EditAction;

		await InvokeAsync(StateHasChanged);
	}

    protected async Task ActionTypesUpdatedAsync(BucketAction.ActionType actionType)
    {
        if (ActionToEdit is null) 
            return;

        ActionToEdit.ActionTypes = actionType;
        await InvokeAsync(StateHasChanged);
    }

    protected async Task ActionTriggersUpdatedAsync(BucketAction.ActionTrigger actionTrigger)
    {
        if (ActionToEdit is null) 
            return;

        ActionToEdit.Triggers = actionTrigger;
        await InvokeAsync(StateHasChanged);
    }

    protected async Task ActionMoveBucketValueChanged()
    {
        if (ActionToEdit is null)
            return;

        ActionToEdit.MoveResponse_MoveTo = ActionMoveBuckets.FirstOrDefault();
        ActionToEdit.MoveResponse_MoveToKey = ActionToEdit.MoveResponse_MoveTo?.Key;

        await InvokeAsync(StateHasChanged);
    }

    protected void AddActionFilter()
    {
        if (ActionToEdit is null)
            return;

        ActionToEdit.Filters.Add(new()
        {
            Action = ActionToEdit,
            ActionKey = ActionToEdit.Key
        });

        StateHasChanged();
    }

    protected async Task SaveBucketActionAsync()
    {
		if (ActionToEdit is null
            || BucketToEdit is null)
			return;

		var res = await FormService.UpdateOrAddBucketActionAsync(ActionToEdit);

		if (!res.GetResult(out var err))
		{
			AlertService.CreateErrorAlert(err);
		}
		else
		{
			await FormService.RefreshBucketAsync(BucketToEdit);

			AlertService.CreateSuccessAlert($"Saved.", true, 1600);

			await InvokeAsync(StateHasChanged);
		}
	}

    protected async Task StopActionEditAsync()
    {
		ActionToEdit = null;
		LeftMenu = LeftMenuState.EditBucket;
		await InvokeAsync(StateHasChanged);
	}
    #endregion


    #region Bucket Action Settings
    protected List<string> FormTagNames { get; set; } = new();
    protected List<BaseAssignableConfiguration> CanApplyTo { get; set; } = new();
    protected List<string> CanApplyToNames { get; set; } = new();

    protected List<BaseAssignableConfiguration> CanAssign { get; set; } = new();
    protected List<string> CanAssignNames { get; set; } = new();

    protected List<BaseAssignableConfiguration> CanAssignTo { get; set; } = new();
    protected List<string> CanAssignToNames { get; set; } = new();

    protected void LoadBucketActionOptions()
    {
        if (FormToEdit is null)
            return;

        FormBucketNames = FormToEdit.Buckets.Select(e => e.Name).ToList();

        // Apply values to another assignable object
        CanApplyTo = FormToEdit.Properties
            .Where(e => e is ILinkedAssignableConfiguration)
            .Where(e => e.GetValueType().IsAssignableTo(typeof(IAssignable)))
            .ToList();
        CanApplyToNames = CanApplyTo
            .Select(e => e.GetPropConfigDisplay())
            .ToList();

        // Users that can be assigned places
        CanAssign = FormToEdit.Properties
            .Where(e => e is ILinkedAssignableConfiguration)
            .Where(e => e.GetValueType().IsAssignableTo(typeof(DataCoreUser)))
            .ToList();
        CanAssignNames = CanAssign
            .Select(e => e.GetPropConfigDisplay())
            .ToList();

        // Apply a user to an IUserAssignable
        CanAssignTo = FormToEdit.Properties
            .Where(e => e is ILinkedAssignableConfiguration)
            .Where(e => e.GetValueType().IsAssignableTo(typeof(IUserAssignable)))
            .ToList();
        CanAssignToNames = CanAssignTo
            .Select(e => e.GetPropConfigDisplay())
            .ToList();
    }

    protected void AddAssignToConfiguration()
    {
        ActionToEdit?.AssignToConfigurations.Add(new());
        EditComponent?.RequestStateHasChanged();
    }

    protected void RemoveAssignToConfiguration(AssignToConfiguration cfg)
    {
        ActionToEdit?.AssignToConfigurations.Remove(cfg);
        EditComponent?.RequestStateHasChanged();
    }

    protected void AddApplyToConfiguration()
    {
        ActionToEdit?.ApplyValuesConfigurations.Add(new());
        EditComponent?.RequestStateHasChanged();
    }

    protected void RemoveApplyToConfiguration(ApplyValuesConfiguration cfg)
    {
        ActionToEdit?.ApplyValuesConfigurations.Remove(cfg);
        EditComponent?.RequestStateHasChanged();
    }
    #endregion

    #region Action Filters
    protected void ToggleStartParens(BucketActionFilter filter)
    {
        filter.StartParentheses = !filter.StartParentheses;
        StateHasChanged();
    }

    protected void ToggleEndParens(BucketActionFilter filter)
    {
        filter.EndParentheses = !filter.EndParentheses;
        StateHasChanged();
    }
    #endregion
}