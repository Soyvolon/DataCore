﻿using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Routing;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;

using ProjectDataCore.Components.Framework.Page;
using ProjectDataCore.Data.Services.Routing;
using ProjectDataCore.Data.Services.User;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RouteData = Microsoft.AspNetCore.Components.RouteData;

namespace ProjectDataCore.Components.Page;
public class DataCoreRouter : IComponent, IHandleAfterRender, IDisposable
{
	private bool disposedValue;
	private RenderHandle _renderHandle;
	private string _location = "";
	private Uri? _absLoc;
	private string _localLoc = "";

	private bool _interceptingNav = false;

	private bool _firstRender = true;
	private CustomPageSettings? _customPageSettings;

#pragma warning disable CS8618 // Injections/Parameters are never null.
	[Inject]
	private NavigationManager NavigationManager { get; set; }
	[Inject]
	private INavigationInterception NavigationInterception { get; set; }
	[Inject]
	private IRoutingService RoutingService { get; set; }
	[Inject]
	private ILogger<DataCoreRouter> Logger { get; set; }
	[Inject]
	private ILocalUserService LocalUserService { get; set; }
    [Inject]
    public required IUserService UserService { get; set; }

    [Parameter, EditorRequired]
	public RenderFragment<(Func<Task>, RouteData)> HasComponent { get; set; }

	[Parameter, EditorRequired]
	public RenderFragment NoComponent { get; set; }

    [CascadingParameter]
    public Task<AuthenticationState>? AuthStateTask { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

    public delegate Task OnComponentRenderedAsyncEventArgs(CustomPageSettings settings);
	public event OnComponentRenderedAsyncEventArgs? OnComponentRenderedAsync;

	public void Attach(RenderHandle renderHandle)
	{
		_renderHandle = renderHandle;

		NavigationManager.LocationChanged += OnLocationChanged;

		ReloadLocationVars();
	}

	private void ReloadLocationVars()
	{
		_location = NavigationManager.Uri;
		_absLoc = NavigationManager.ToAbsoluteUri(NavigationManager.Uri);
		_localLoc = _absLoc.LocalPath;
	}

	public Task OnAfterRenderAsync()
	{
		if (!_interceptingNav)
		{
			_interceptingNav = true;
			return NavigationInterception.EnableNavigationInterceptionAsync();
		}

		return Task.CompletedTask;
	}

	public async Task SetParametersAsync(ParameterView parameters)
	{
		var oldLoc = _localLoc;

		parameters.SetParameterProperties(this);

		ReloadLocationVars();

		if (HasComponent is null)
		{
			throw new InvalidOperationException($"The {nameof(HasComponent)} can not be null");
		}

		if (NoComponent is null)
		{
			throw new InvalidOperationException($"The {nameof(NoComponent)} can not be null");
		}

		if (_firstRender || oldLoc != _localLoc)
		{
			_firstRender = false;

			await RefreshAsync();
		}
	}

	private async void OnLocationChanged(object? sender, LocationChangedEventArgs e)
	{
		if (_location != NavigationManager.Uri)
		{
			ReloadLocationVars();
			await RefreshAsync();
		}
	}

	private async Task RefreshAsync()
	{
		_customPageSettings = await RoutingService.GetPageSettingsFromRouteAsync(_localLoc);

		if (_customPageSettings is null)
		{
			// TODO manaully get a not-found page.

			if (_customPageSettings is null)
				_renderHandle.Render(NoComponent);
		}

		await RenderCustomPageSettingsAsync();
	}

	private async Task RenderCustomPageSettingsAsync()
	{
		if (_customPageSettings is null)
			return;

		try
		{
			if (AuthStateTask is not null)
			{
				var user = (await AuthStateTask).User;
				var newUser = await UserService.GetUserFromClaimsPrinciaplAsync(user);

				if (newUser is not null)
					await LocalUserService.InitalizeIfDeinitalizedAsync(newUser.Id);
			}

			await RefreshComponentOnly();

			if (_customPageSettings.RedirectTo is not null)
			{
				NavigationManager.NavigateTo(_customPageSettings.RedirectTo);
			}
			else
			{
                var data = new RouteData(
					typeof(LayoutNodeTreeLoader),
					new Dictionary<string, object>
					{
						{ "ParentNode", _customPageSettings.Layout! },
						{ "TopLevelNode", true }
					});

                _renderHandle.Render(HasComponent((RefreshComponentOnly, data)));
			}
		}
		catch (Exception ex)
		{
			Logger.LogError(ex, "Renderer failed to render component.");
		}
	}

	private async Task RefreshComponentOnly()
	{
		if (_customPageSettings is null)
			return;

		// Clear any existing data if it exisits ...
		_customPageSettings.Layout = null;
		
		// ... otherwise, for each setp in the load method ...
		await foreach (bool _ in RoutingService.LoadPageSettingsAsync(_customPageSettings)) { }
	}

	protected virtual void Dispose(bool disposing)
	{
		if (!disposedValue)
		{
			if (disposing)
			{
                LocalUserService.DeinitalizeIfInitalized();
            }

			NavigationManager.LocationChanged -= OnLocationChanged;

			disposedValue = true;
		}
	}

	public void Dispose()
	{
		// Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
		Dispose(disposing: true);
		GC.SuppressFinalize(this);
	}
}
