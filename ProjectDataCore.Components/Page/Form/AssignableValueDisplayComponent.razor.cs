﻿using HtmlAgilityPack;

using ProjectDataCore.Components.Page.Admin.Page;
using ProjectDataCore.Data.Services.Alert;
using ProjectDataCore.Data.Services.HTML;
using ProjectDataCore.Data.Services.User;
using ProjectDataCore.Data.Structures.Assignable;
using ProjectDataCore.Data.Structures.Assignable.Render;
using ProjectDataCore.Data.Structures.Assignable.Value;
using ProjectDataCore.Data.Structures.Page.Components.Parameters;
using ProjectDataCore.Data.Structures.Page.Components.Scope;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Components.Page.Form;

[Component(typeof(DisplayComponentSettings), PageComponentListing.Forms_AssignableValueDisplay, 
    Name = "Assignable Value Display",
    ShortName = "Value Display",
    IconPath = "svg/mat-icons/text_fields.svg",
    Category = "Forms"
)]
public partial class AssignableValueDisplayComponent : DisplayComponentBase
{
    private MarkupString Authorized { get; set; } = new();
    private MarkupString NotAuthorized { get; set; } = new();

    #region Rendering
    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (firstRender)
        {
            if (DisplaySettings?.Data is not null)
            {
                Authorized = DisplaySettings.Data.GetAuthorizedRawMarkup();
                NotAuthorized = DisplaySettings.Data.GetUnAuthorizedRawMarkup();

                StateHasChanged();
            }
        }
    }

	protected override void AssignableScope_AssignablesChanged(PageComponentSettingsBase sender)
	{
        if (DisplaySettings is not null)
        {
            Authorized = DisplaySettings.Data.GetAuthorizedRawMarkup();
            NotAuthorized = DisplaySettings.Data.GetUnAuthorizedRawMarkup();

            StateHasChanged();
        }
	}
	#endregion
}
