﻿using ProjectDataCore.Data.Structures.Assignable.Value;
using ProjectDataCore.Data.Structures.Page.Components.Parameters.Form;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Components.Page.Form.Bucket;
public partial class ResponseFullViewComponent : ComponentBase
{
    [Parameter, EditorRequired]
    public required BucketComponentSettings Settings { get; set; }
    [Parameter, EditorRequired]
    public required DynamicFormResponse Response { get; set; }

    protected List<(BaseAssignableValue assignable, string label)> Values { get; set; } = new();

    protected override async Task OnParametersSetAsync()
    {
        await base.OnParametersSetAsync();

        await GetValueListAsync();
    }

    protected Task GetValueListAsync()
    {
        Values.Clear();

        foreach(var order in Settings.DisplayOrders)
        {
            var value = Response.AssignableValues.FirstOrDefault(e => e.AssignableConfigurationId == order.ConfigurationKey);

            if (value is not null)
                Values.Add((value, order.AssignableConfiguration.PropertyName));
        }

        return Task.CompletedTask;
    }
}
