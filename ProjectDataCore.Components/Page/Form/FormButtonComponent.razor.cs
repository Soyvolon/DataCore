using ProjectDataCore.Data.Services.Alert;
using ProjectDataCore.Data.Structures.Assignable.Value;
using ProjectDataCore.Data.Structures.Page.Components.Parameters.Form;
using ProjectDataCore.Data.Structures.Page.Components.Pool;
using ProjectDataCore.Data.Structures.Page.Components.Pool.Callback;
using ProjectDataCore.Data.Structures.Page.Components.Scope;

using System.ComponentModel;

namespace ProjectDataCore.Components.Page.Form;

[Component(typeof(ButtonComponentSettings), PageComponentListing.Forms_FormButton,
	Name = "Form Button",
	ShortName = "Button",
	IconPath = "svg/mat-icons/left_click.svg",
	Category = "Forms"
)]
public partial class FormButtonComponent : FormComponentBase
{
    [Inject]
    public required IFormService FormService { get; set; }
    [Inject]
    public required IAlertService AlertService { get; set; }
    [Inject]
    public required IFormResponseService FormResponseService { get; set; }

    [CascadingParameter(Name = "ActiveUser")]
    public DataCoreUser? ActiveUser { get; set; }

	protected ButtonComponentSettings? ButtonSettings { get => ComponentData as ButtonComponentSettings; }

    #region Button
    protected void LoadButtonFormSettings()
    {
        if (ButtonSettings is null)
            return;

        if (PageEdit)
        {
            ReloadScopes();
            ReloadTags();
            ReloadBuckets();
        }
    }

    protected async Task OnButtonClickedAsync()
    {
        if (ComponentData is null
            || ButtonSettings is null)
            return;

        var pool = ComponentData.ComponentPools.FirstOrDefault();

        if (pool is null)
        {
            AlertService.CreateWarnAlert("This submit action isn't configured: no component pool found. " +
                "Please contact a site administrator if you believe this to be an error.");
            return;
        }

        if (ButtonSettings.Data.InvokeSubmit)
        {
            await OnSubmitFormAsync(pool);
        }

        if (ButtonSettings.Data.InvokeReset)
        {
            OnResetForm(pool);
        }
    }

    protected async Task OnSubmitFormAsync(ComponentPool pool)
    {
        if (ComponentData is null
            || ButtonSettings is null)
            return;

        var values = pool.RequestValues(ComponentData, new());
        var assignables = values
            .Where(e => e is AssignableValueRequestedCallbackArgs)!
            .Select(e => (e as AssignableValueRequestedCallbackArgs)!.Value!)
            .Where(e => e is not null)
            .ToList();

        var scope = ButtonSettings.ScopeProviders
            .Where(e => e.ProvidingScopeId == ButtonSettings.ProvidingScopeId)
            .FirstOrDefault();

        if (scope is null)
        {
            AlertService.CreateWarnAlert("This submit action isn't configured: no scope provider found. " +
                "Please contact a site administrator if you believe this to be an error.");
            return;
        }

        var form = scope.ProvidingScope.FormScopeDefinition;
        if (form is null)
        {
            AlertService.CreateWarnAlert("This submit action isn't configured: no form found in scope. " +
                "Please contact a site administrator if you believe this to be an error.");
            return;
        }

        var bucket = form.HasDefaultBucket;
        if (ButtonSettings.Data.BucketKey is not null)
        {
            bucket = form.Buckets.Find(e => e.Key == ButtonSettings.Data.BucketKey);
        }

        if (form.NeedsBucket() && bucket is null)
        {
            AlertService.CreateWarnAlert("This submit action isn't configured: no configured or default bucket. " +
                "Please contact a site administrator if you believe this to be an error.");
            return;
        }

        var res = await FormResponseService.SubmitFormResponseAsync(form, bucket, assignables);
        
        if (res.GetResult(out var response, out var err))
        {
            AlertService.CreateSuccessAlert("Response submitted successfully", true);
        }
        else
        {
            // Do some message cleanup.
            err.Insert(0, $"Failed to submit a response to {form.Name}");
            err.RemoveAt(1);

            AlertService.CreateErrorAlert(err);
        }
    }

    protected void OnResetForm(ComponentPool pool)
    {
        if (ComponentData is null
            || ButtonSettings is null)
            return;

        pool.RequestClearValues(ComponentData);
    }
    #endregion

    #region Settings
    protected List<AssignableScopeListenerContainer> ValidScopes { get; set; } = new();
    protected List<string> ValidScopeNames { get; set; } = new();
    protected List<AssignableScopeListenerContainer> SelectedScopes { get; set; } = new();
    protected DynamicForm? SelectedForm { get; set; }

    protected List<string> TagNames { get; set; } = new();
    protected List<DynamicFormTag> SelectedTags { get; set; } = new();

    protected List<string> BucketNames { get; set; } = new();
    protected List<DynamicFormBucket> SelectedBuckets { get; set; } = new();

    protected void ReloadScopes()
    {
        if (ComponentData is null
            || ButtonSettings is null)
            return;

        ValidScopes = ComponentData.ScopeProviders
            .Where(e => !e.ProvidingScope.UseUserDefinition)
            .ToList();
        ValidScopeNames = ValidScopes.Select(e => e.ProvidingScope.DisplayName)
            .ToList();

        SelectedScopes = ValidScopes
            .Where(e => e.ProvidingScopeId == ButtonSettings.ProvidingScopeId)
            .ToList();

        SelectedForm = SelectedScopes.FirstOrDefault()
            ?.ProvidingScope.FormScopeDefinition;
    }

    protected async Task OnScopeChangedAsync()
    {
        if (ButtonSettings is null)
            return;

        var scope = SelectedScopes.FirstOrDefault();
        ButtonSettings.ProvidingScopeId = scope?.ProvidingScopeId ?? default;

        SelectedForm = scope?.ProvidingScope.FormScopeDefinition;

        ReloadTags();
        ReloadBuckets();

        if (EditComponent is not null)
            await EditComponent.RequestStateHasChangedAsync();
    }

    protected async Task OnInvokeSubmitChanged(bool e)
    {
        if (ButtonSettings is null)
            return;

        ButtonSettings.Data.InvokeSubmit = e;

        if (EditComponent is not null)
            await EditComponent.RequestStateHasChangedAsync();
    }

    protected void ReloadTags()
    {
        if (SelectedForm is null
            || ButtonSettings is null)
            return;

        TagNames = SelectedForm.Tags
            .Select(t => t.Name)
            .ToList();

        SelectedTags = SelectedForm.Tags
            .Where(e => ButtonSettings.Data.TagsToApplyOnSubmit.Contains(e.Key))
            .ToList();
    }

    protected async Task OnTagChangedAsync()
    {
        if (ButtonSettings is null)
            return;

        ButtonSettings.Data.TagsToApplyOnSubmit = SelectedTags.Select(e => e.Key)
            .ToList();

        if (EditComponent is not null)
            await EditComponent.RequestStateHasChangedAsync();
    }

    protected void ReloadBuckets()
    {
        if (ButtonSettings is null
            || SelectedForm is null)
            return;

        BucketNames = SelectedForm.Buckets
            .Select(e => e.Name).ToList();

        SelectedBuckets.Clear();

        if (ButtonSettings.Data.BucketKey != default
            && SelectedForm.Buckets
                .Any(e => e.Key == ButtonSettings.Data.BucketKey))
        {
            SelectedBuckets.Add(SelectedForm.Buckets.First(e => e.Key == ButtonSettings.Data.BucketKey));
        }
        else
        {
            ButtonSettings.Data.BucketKey = default;
        }
    }

    protected async Task OnBucketChangedAsync()
    {
        if (ButtonSettings is null)
            return;

        var bucket = SelectedBuckets.FirstOrDefault();
        ButtonSettings.Data.BucketKey = bucket?.Key ?? default;

        if (EditComponent is not null)
            await EditComponent.RequestStateHasChangedAsync();
    }
    #endregion
}