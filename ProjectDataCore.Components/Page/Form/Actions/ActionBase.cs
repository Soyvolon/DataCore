﻿using ProjectDataCore.Data.Services.Alert;
using ProjectDataCore.Data.Services.Bus.Scoped;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDataCore.Components.Page.Form.Actions;
public class ActionBase : ComponentBase
{
    [Inject]
    public required IFormResponseService FormResponseService { get; set; }
    [Inject]
    public required IAlertService AlertService { get; set; }
    [Inject]
    public required IScopedDataBus ScopedDataBus { get; set; }

    [Parameter, EditorRequired]
    public required FormBucketComponent Bucket { get; set; }

    [CascadingParameter(Name = "Identifier")]
    public required string Identifier { get; set; } = "";

	public List<DynamicFormResponse> SelectedResponses { get; set; } = [];

    protected override async Task OnParametersSetAsync()
    {
        await base.OnParametersSetAsync();

        SelectedResponses.Clear();
        foreach(var i in Bucket.SelectionData)
            SelectedResponses.Add(Bucket.ActiveResponses[i]);
    }
}
