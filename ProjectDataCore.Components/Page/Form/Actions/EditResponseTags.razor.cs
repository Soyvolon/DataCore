namespace ProjectDataCore.Components.Page.Form.Actions;

public partial class EditResponseTags : ActionBase
{
    protected List<DynamicFormTag> UnionedTags { get; set; } = [];

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (firstRender)
        {
            UnionedTags.AddRange(SelectedResponses.SelectMany(e => e.Tags).ToHashSet());
            StateHasChanged();
        }
    }

    protected async Task OnChangeTagsAsync()
    {
        SelectedResponses.ForEach(e =>
        {
            e.Tags.Clear();
            e.Tags = UnionedTags;
        });

        var res = await FormResponseService.UpdateResponseTagsAsync([.. SelectedResponses]);
        if (!res.GetResult(out var err))
        {
            AlertService.CreateErrorAlert(err);
            return;
        }

        AlertService.CreateSuccessAlert("Tags changed.");
        await ScopedDataBus.CloseMenuAsync(this, Identifier);
        await Bucket.RequestRefreshAsync();
    }
}