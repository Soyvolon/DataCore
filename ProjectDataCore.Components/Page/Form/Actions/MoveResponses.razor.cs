namespace ProjectDataCore.Components.Page.Form.Actions;

public partial class MoveResponses : ActionBase
{
    [Inject]
    public required IFormService FormService { get; set; }

    public Guid SelectedBucketId { get; set; } = Guid.Empty;
    public List<DynamicFormBucket> AllBuckets { get; set; } = [];
    public string Reason { get; set; } = "";

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (firstRender)
        {
            var formRes = await FormService.GetFormFromBucketKeyAsync(Bucket.BucketSettings!.Bucket);
            if (!formRes.GetResult(out var form, out var err))
            {
                AlertService.CreateErrorAlert(err);
                return;
            }

            var loadRes = await FormService.LoadFormBucketsAsync(form);
            if (!loadRes.GetResult(out err))
            {
                AlertService.CreateErrorAlert(err);
                return;
            }

            AllBuckets = form.Buckets;
            StateHasChanged();
        }
    }

    protected async Task OnMoveResponsesAsync()
    {
        if (string.IsNullOrWhiteSpace(Reason))
        {
            AlertService.CreateWarnAlert("A reason is required.");
            return;
        }

        var bucket = AllBuckets.Where(e => e.Key == SelectedBucketId)
            .FirstOrDefault();

        if (bucket is null)
        {
            AlertService.CreateWarnAlert("No bucket was selected.");
            return;
        }

        var res = await FormResponseService.MoveResponsesAsync(bucket, Reason, [.. SelectedResponses]);

        if (!res.GetResult(out var err))
        {
            AlertService.CreateErrorAlert(err);
            return;
        }

        AlertService.CreateSuccessAlert("Responses moved.");
        await ScopedDataBus.CloseMenuAsync(this, Identifier);
        await Bucket.RequestRefreshAsync();
    }
}