namespace ProjectDataCore.Components.Page.Form.Actions;

public partial class DeleteResponses : ActionBase
{
    protected async Task OnDeleteResponsesAsync()
    {
        var res = await FormResponseService.DeleteResponsesAsync([.. SelectedResponses]);

        if (!res.GetResult(out var err))
        {
            AlertService.CreateErrorAlert(err);
            return;
        }

        AlertService.CreateSuccessAlert("Responses deleted.");
        await ScopedDataBus.CloseMenuAsync(this, Identifier);
        await Bucket.RequestRefreshAsync();
	}
}