using ProjectDataCore.Components.Layout.Form.Bucket;
using ProjectDataCore.Data.Services.Alert;
using ProjectDataCore.Data.Structures.Assignable.Configuration;
using ProjectDataCore.Data.Structures.Page.Components.Parameters.Form;
using ProjectDataCore.Data.Structures.Page.Components.Scope;
using ProjectDataCore.Data.Structures.Util.List;

namespace ProjectDataCore.Components.Page.Form;

[Component(typeof(BucketComponentSettings), PageComponentListing.Forms_FormBucket,
    Name = "Form Bucket",
    ShortName = "Bucket",
    IconPath = "svg/mat-icons/cleaning_bucket.svg",
    Category = "Forms"
)]
public partial class FormBucketComponent : FormComponentBase
{
	[Inject]
	public required IFormService FormService { get; set; }
	[Inject]
	public required IFormResponseService FormResponseService { get; set; }
	[Inject]
	public required IAlertService AlertService { get; set; }

	[CascadingParameter(Name = "ActiveUser")]
	public DataCoreUser? ActiveUser { get; set; }

	public BucketComponentSettings? BucketSettings { get => ComponentData as BucketComponentSettings; }
	protected bool HasActions { get => BucketSettings is not null 
										&& (BucketSettings.AllowDelete
										|| BucketSettings.AllowMove
										|| BucketSettings.AllowTagEditing); }


    #region Active Use
    public List<DynamicFormResponse> ActiveResponses { get; set; } = [];
    public DynamicForm? ActiveForm { get; set; } = null;
    public ListSelectionData SelectionData { get; set; } = new();

    protected string SelectedResponseAction { get; set; } = "";

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (firstRender)
        {
            if (EditComponent is null)
            {
                await LoadActiveResponsesAsync();
            }
            else
            {
                await LoadFormsAsync();
            }
        }
    }

    protected override async Task OnParametersSetAsync()
    {
        await base.OnParametersSetAsync();

        if (BucketSettings is not null)
            BucketSettings.DisplayOrders 
                = BucketSettings.DisplayOrders.OrderBy(e => e.Order).ToList();
    }

    protected async Task LoadActiveResponsesAsync()
    {
        if (BucketSettings is null)
            return;

        var bucketRes = await FormService.GetBucketFromIDAsync(BucketSettings.Bucket);
        if (!bucketRes.GetResult(out var bucket, out var err))
        {
            AlertService.CreateErrorAlert(err);
            return;
        }

        var res = await FormResponseService.LoadResponsesForBucketAsync(bucket);
        if (!res.GetResult(out err))
        {
            AlertService.CreateErrorAlert(err);
            return;
        }

        ActiveResponses = bucket.Responses;

        ActiveForm = ActiveResponses.FirstOrDefault()?.Form;

        if (EditComponent is not null)
            await EditComponent.RequestStateHasChangedAsync();
        else StateHasChanged();
    }

    protected void OnSelectedResponseValueChanged(string newAction)
    {
        SelectedResponseAction = newAction;
    }

    public async Task RequestRefreshAsync()
    {
        await LoadActiveResponsesAsync();
    }
    #endregion

    #region Configuration
    protected List<DynamicForm> Forms { get; set; } = new();
    protected List<DynamicForm> SelectedFormList { get; set; } = new();
    protected List<string> FormNames { get; set; } = new();

    private DynamicForm? _selectedForm = null;
    protected DynamicForm? SelectedForm 
    { 
        get => _selectedForm;
        set
        {
            _selectedResponseTemplate = null;
            _selectedForm = value;
        }
    }

    private DynamicFormResponse? _selectedResponseTemplate = null;
    protected DynamicFormResponse? SelectedResponseTemplate
    {
        get => _selectedResponseTemplate ??= SelectedForm?.CreateResponse();
    }

    protected List<DynamicFormBucket> SelectedBucketList { get; set; } = new();
    protected List<string> BucketNames { get; set; } = new();
    protected DynamicFormBucket? SelectedBucket { get; set; }

    protected List<BaseAssignableConfiguration> SelectedConfigurations { get; set; } = new();

    protected async Task LoadFormsAsync()
	{        
        Forms = await FormService.GetAllFormsAsync();
		FormNames = Forms.Select(e  => e.Name).ToList();
		SelectedFormList = Forms.Where(e => e.Buckets.Any(e => e.Key == BucketSettings?.Bucket)).ToList();

		await OnSelectedFormChangedAsync();

        if (EditComponent is not null)
            await EditComponent.RequestStateHasChangedAsync();
    }

	protected async Task OnSelectedFormChangedAsync()
	{
		SelectedForm = SelectedFormList.FirstOrDefault();

		BucketSettings!.ItemSummaryDisplayConfig ??= new();
        BucketSettings.ItemSummaryDisplayConfig.DisplayComponentSettings ??= new()
        {
            Data = BucketSettings.ItemSummaryDisplayConfig,
            ScopeProviders = BucketSettings.ScopeProviders
        };

        await LoadBucketSettingsAsync();

        if (EditComponent is not null)
            await EditComponent.RequestStateHasChangedAsync();
    }

    protected async Task LoadBucketSettingsAsync()
    {
		if (SelectedForm is null)
			return;

		await FormService.LoadFormBucketsAsync(SelectedForm);

        BucketNames = SelectedForm.Buckets.Select(x => x.Name).ToList();

		if (EditComponent is not null)
			await EditComponent.RequestStateHasChangedAsync();
    }

    protected async Task OnSelectedBucketChangedAsync()
    {
        SelectedBucket = SelectedBucketList.FirstOrDefault();

        if (BucketSettings is not null)
            BucketSettings.Bucket = SelectedBucket?.Key ?? Guid.Empty;

        if (EditComponent is not null)
            await EditComponent.RequestStateHasChangedAsync();
    }

    protected async Task AddSelectedConfigurationsToDisplayListAsync()
    {
        if (BucketSettings is null)
            return;

        var orderings = SelectedConfigurations.Select(e => new BucketValueDisplayOrder()
        {
            AssignableConfiguration = e,
            ConfigurationKey = e.Key,

            SettingsKey = BucketSettings.Key
        });

        BucketSettings.DisplayOrders.AddRange(orderings);

        BucketSettings.RecalculateOrder();

        SelectedConfigurations.Clear();

        if (EditComponent is not null)
            await EditComponent.RequestStateHasChangedAsync();
    }

    protected async Task RemoveConfigurationFromDisplayAsync(int index)
    {
        if (BucketSettings is null)
            return;

        BucketSettings.DisplayOrders.RemoveAt(index);

        BucketSettings.RecalculateOrder();

        if (EditComponent is not null)
            await EditComponent.RequestStateHasChangedAsync();
    }
    #endregion
}