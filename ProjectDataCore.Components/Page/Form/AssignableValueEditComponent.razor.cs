using ProjectDataCore.Data.Services.Alert;
using ProjectDataCore.Data.Services.User;
using ProjectDataCore.Data.Structures.Assignable.Configuration;
using ProjectDataCore.Data.Structures.Assignable.Value;
using ProjectDataCore.Data.Structures.Auth;
using ProjectDataCore.Data.Structures.Page.Components.Parameters.Form;
using ProjectDataCore.Data.Structures.Page.Components.Pool;
using ProjectDataCore.Data.Structures.Page.Components.Pool.Callback;
using ProjectDataCore.Data.Structures.Page.Components.Scope;

namespace ProjectDataCore.Components.Page.Form;

[Component(typeof(FormComponentSettings), PageComponentListing.Forms_AssignableValueEdit,
    Name = "Assignable Value Editor",
    ShortName = "Value Edit",
    IconPath = "svg/mat-icons/edit.svg",
    Category = "Forms"
)]
public partial class AssignableValueEditComponent : FormComponentBase
{
    public delegate void AssignableClearEventArgs(object sender);
    public event AssignableClearEventArgs? AssignableClear;

    [Inject]
    public required IUserService UserService { get; set; }
    [Inject]
    public required IAlertService AlertService { get; set; }

    protected BaseAssignableValue? AssignableValue { get; set; }

    protected override async Task OnParametersSetAsync()
    {
        await base.OnParametersSetAsync();

        if (PageEdit)
        {
            ScopeNames = FormSettings?.ScopeProviders
                .Select(e => e.ProvidingScope.DisplayName).ToList() ?? new();
        }
    }

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (firstRender)
        {
            await LoadValueAsync();

            StateHasChanged();
        }
    }

    protected override void Pool_ValuesRequested(PageComponentSettingsBase sender,
        Action<ValuesRequestedEventCallbackArgs> callback,
        ValuesRequestedEventArgs args)
    {
        base.Pool_ValuesRequested(sender, callback, args);

        if (sender == ComponentData)
            return;

        var callbackArgs = new AssignableValueRequestedCallbackArgs()
        {
            Value = AssignableValue
        };

        callback.Invoke(callbackArgs);
    }

    protected override void Pool_ClearValuesRequested(PageComponentSettingsBase sender)
    {
        base.Pool_ClearValuesRequested(sender);

        if (sender == ComponentData)
            return;

        AssignableClear?.Invoke(this);
    }

	protected async Task LoadValueAsync()
    {
        if (FormSettings is null)
            return;

        var scope = FormSettings.ScopeProviders
            .Where(e => e.ProvidingScopeId == FormSettings.ProvidingScopeId)
            .FirstOrDefault();

        if (scope is null)
            return;

        if (PageEdit)
        {
            ScopeToPickFrom.Clear();
            ScopeToPickFrom.Add(scope);

            await ChangeScope();
        }

        if (FormSettings.AssignableConfiguration is null)
            return;

        if (PageEdit)
        {
            SelectedAssignables.Clear();
            SelectedAssignables.Add(FormSettings.AssignableConfiguration);

            ChangeAssignable();
        }

        var assignable = await scope.ProvidingScope.GetConfigurationAssignableAsync(UserService);

        if (assignable is null)
            return;

        AssignableValue = FormSettings.AssignableConfiguration.GetAssignableValue(assignable);
    }

    #region Scopes
    private List<IAssignable> AddedAssignables { get; set; } = new();

	protected override void AssignableScope_AssignablesChanged(PageComponentSettingsBase sender)
	{
		base.AssignableScope_AssignablesChanged(sender);

		_ = Task.Run(LoadValueAsync);
	}

	protected void AssignableValueChanged()
    {
        if (AssignableValue?.GetValue() is not IAssignable assignable)
        {
            foreach(var item in AddedAssignables)
                RemoveAssignableFromScopes(item);

            AddedAssignables.Clear();
            return;
        }

        AddedAssignables.Add(assignable);
		AddAssignableToScopes(assignable);
	}
    #endregion

    #region Assignable Configuration
    protected List<string> ScopeNames { get; set; } = new();
    protected List<AssignableScopeListenerContainer> ScopeToPickFrom { get; set; } = new();
    protected AssignableScope? SelectedScope { get; set; } = null;

    protected List<BaseAssignableConfiguration> Assignables { get; set; } = new();
    protected List<string> AssignableNames { get; set; } = new();

    protected List<BaseAssignableConfiguration> SelectedAssignables { get; set; } = new();
    protected BaseAssignableConfiguration? SelectedAssignable { get; set; } = null;

    protected async Task OnScopeToPickChanged()
    {
        await ChangeScope();

        if (EditComponent is not null)
            await EditComponent.RequestStateHasChangedAsync();
    }

    protected async Task ChangeScope()
    {
        SelectedScope = ScopeToPickFrom.FirstOrDefault()?.ProvidingScope;

        if (FormSettings is not null)
        {
            if (SelectedScope is not null)
            {
                Assignables = (await SelectedScope.GetConfigurationAssignableAsync(UserService))
                    ?.AssignableValues
                    .Select(e => e.AssignableConfiguration)
                    .ToList() ?? new();

                if (Assignables is null)
                {
                    AlertService.CreateWarnAlert("No assignables found.", true);
                    return;
                }

                AssignableNames = Assignables.Select(e => e.GetPropConfigDisplay()).ToList();
            }
            else
            {
                Assignables.Clear();
                AssignableNames.Clear();
                SelectedAssignables.Clear();

                ChangeAssignable();
            }

            FormSettings.ProvidingScopeId = SelectedScope?.Key ?? default;
        }
    }

    protected async Task OnAssignableToEditChanged()
    {
        ChangeAssignable();

        if (EditComponent is not null)
            await EditComponent.RequestStateHasChangedAsync();
    }

    protected void ChangeAssignable()
    {
        if (FormSettings is null
            || SelectedScope is null)
            return;

        FormSettings.AssignableConfiguration?.FormComponents.Remove(FormSettings);

        SelectedAssignable = SelectedAssignables.FirstOrDefault();

        FormSettings.AssignableConfiguration = SelectedAssignable;
        FormSettings.AssignableConfigurationKey = SelectedAssignable?.Key;

        // ensure the linked settings are created, but don't delete them if the config is
        // changed off.
        if (FormSettings.AssignableConfiguration is ILinkedAssignableConfiguration)
            FormSettings.LinkedAssignableInputSettings ??= new();
    }

    protected void SetDataAuthAreaSelection(AuthorizationArea area)
    {
        if (FormSettings?.LinkedAssignableInputSettings is null)
            return;

        FormSettings.LinkedAssignableInputSettings.DataAuthorizationArea = area;
        FormSettings.LinkedAssignableInputSettings.DataAuthorizationAreaKey = area.Key;

        EditComponent?.RequestStateHasChanged();
    }

    protected void ClearDataAuthAreaSelection()
    {
        if (FormSettings?.LinkedAssignableInputSettings is null)
            return;

        FormSettings.LinkedAssignableInputSettings.DataAuthorizationArea = null;
        FormSettings.LinkedAssignableInputSettings.DataAuthorizationAreaKey = null;

        EditComponent?.RequestStateHasChanged();
    }
    #endregion
}